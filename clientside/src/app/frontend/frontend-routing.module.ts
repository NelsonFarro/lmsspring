/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FrontendComponent} from './frontend.component';
import { RoleGuard } from 'src/app/lib/guards/role.guard';
import { ArticleeditorPageComponent } from 'src/app/pages/articleeditor/articleeditor.page.component';
import { AcademyHomePageComponent } from 'src/app/pages/academy-home/academy-home.page.component';
import { LessonPageComponent } from 'src/app/pages/lesson/lesson.page.component';
import { CoursePageComponent } from 'src/app/pages/course/course.page.component';
import { StatsPageComponent } from 'src/app/pages/stats/stats.page.component';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

export const frontendRoutes: Routes = [
	{
		path: '',
		component: FrontendComponent,
		canActivate: [
			RoleGuard,
			// % protected region % [Add any extra guards for default page here] off begin
			// % protected region % [Add any extra guards for default page here] end
		],
		data: {
			expectedRoles: [
				'CORE_USER',
				'ADMINISTRATOR',
				// % protected region % [Add additional roles for default page here] off begin
				// % protected region % [Add additional roles for default page here] end
			],
			// % protected region % [Customise the data passed to default page here] off begin
			// % protected region % [Customise the data passed to default page here] end
		},
		children: [
			// Admin Crud tile for user and entities
			{
				path: '',
				pathMatch: 'full',
				redirectTo: 'academy-home',
			},
			{
				path: 'articleeditor',
				loadChildren: () => import('../pages/articleeditor/articleeditor.page.module').then(m => m.ArticleeditorPageModule),
				canActivate: [
					RoleGuard,
					// % protected region % [Add any additional guards for ArticleEditor here] off begin
					// % protected region % [Add any additional guards for ArticleEditor here] end
				],
				data: {
					expectedRoles: [
						'ADMINISTRATOR',
						'CORE_USER',
						// % protected region % [Add additional roles to ArticleEditor here] off begin
						// % protected region % [Add additional roles to ArticleEditor here] end
					],
					// % protected region % [Customise your data passed to ArticleEditor here] off begin
					// % protected region % [Customise your data passed to ArticleEditor here] end
				},
				// % protected region % [Add additional route properties here for articleeditor] off begin
				// % protected region % [Add additional route properties here for articleeditor] end
			},
			{
				path: 'academy-home',
				loadChildren: () => import('../pages/academy-home/academy-home.page.module').then(m => m.AcademyHomePageModule),
				canActivate: [
					RoleGuard,
					// % protected region % [Add any additional guards for Academy Home here] off begin
					// % protected region % [Add any additional guards for Academy Home here] end
				],
				data: {
					expectedRoles: [
						'CORE_USER',
						'ADMINISTRATOR',
						// % protected region % [Add additional roles to Academy Home here] off begin
						// % protected region % [Add additional roles to Academy Home here] end
					],
					// % protected region % [Customise your data passed to Academy Home here] off begin
					// % protected region % [Customise your data passed to Academy Home here] end
				},
				// % protected region % [Add additional route properties here for academy-home] off begin
				// % protected region % [Add additional route properties here for academy-home] end
			},
			{
				path: 'lesson',
				loadChildren: () => import('../pages/lesson/lesson.page.module').then(m => m.LessonPageModule),
				canActivate: [
					RoleGuard,
					// % protected region % [Add any additional guards for Lesson here] off begin
					// % protected region % [Add any additional guards for Lesson here] end
				],
				data: {
					expectedRoles: [
						'ADMINISTRATOR',
						'CORE_USER',
						// % protected region % [Add additional roles to Lesson here] off begin
						// % protected region % [Add additional roles to Lesson here] end
					],
					// % protected region % [Customise your data passed to Lesson here] off begin
					// % protected region % [Customise your data passed to Lesson here] end
				},
				// % protected region % [Add additional route properties here for lesson] off begin
				// % protected region % [Add additional route properties here for lesson] end
			},
			{
				path: 'course',
				loadChildren: () => import('../pages/course/course.page.module').then(m => m.CoursePageModule),
				canActivate: [
					RoleGuard,
					// % protected region % [Add any additional guards for Course here] off begin
					// % protected region % [Add any additional guards for Course here] end
				],
				data: {
					expectedRoles: [
						'CORE_USER',
						'ADMINISTRATOR',
						// % protected region % [Add additional roles to Course here] off begin
						// % protected region % [Add additional roles to Course here] end
					],
					// % protected region % [Customise your data passed to Course here] off begin
					// % protected region % [Customise your data passed to Course here] end
				},
				// % protected region % [Add additional route properties here for course] off begin
				// % protected region % [Add additional route properties here for course] end
			},
			{
				path: 'stats',
				loadChildren: () => import('../pages/stats/stats.page.module').then(m => m.StatsPageModule),
				canActivate: [
					RoleGuard,
					// % protected region % [Add any additional guards for Stats here] off begin
					// % protected region % [Add any additional guards for Stats here] end
				],
				data: {
					expectedRoles: [
						'CORE_USER',
						'ADMINISTRATOR',
						// % protected region % [Add additional roles to Stats here] off begin
						// % protected region % [Add additional roles to Stats here] end
					],
					// % protected region % [Customise your data passed to Stats here] off begin
					// % protected region % [Customise your data passed to Stats here] end
				},
				// % protected region % [Add additional route properties here for stats] off begin
				// % protected region % [Add additional route properties here for stats] end
			},
			// % protected region % [Add any additional frontend routes here] off begin
			// % protected region % [Add any additional frontend routes here] end
		]
	}

];

@NgModule({
	declarations: [
		// % protected region % [Add any additional declarations here] off begin
		// % protected region % [Add any additional declarations here] end
	],
	imports: [
		RouterModule.forChild(frontendRoutes),
		// % protected region % [Add any additional imports] off begin
		// % protected region % [Add any additional imports] end
	],
	exports: [
		RouterModule,
		// % protected region % [Add any additional exports] off begin
		// % protected region % [Add any additional exports] end
	],
	// % protected region % [Add any additional module data] off begin
	// % protected region % [Add any additional module data] end
})
export class FrontendRoutingModule { }
