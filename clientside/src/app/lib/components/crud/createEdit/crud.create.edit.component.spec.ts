/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import { DebugElement } from '@angular/core';
import { CommonModule } from '@angular/common';
import { waitForAsync, ComponentFixture, fakeAsync, flush, TestBed, tick } from '@angular/core/testing';
import { FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

import { AdministratorModel } from 'src/app/models/administrator/administrator.model';
import { AdministratorModelAudit } from 'src/app/models/administrator/administrator.model.state';
import { ApplicationLocaleModel } from 'src/app/models/applicationLocale/application_locale.model';
import { ApplicationLocaleModelAudit } from 'src/app/models/applicationLocale/application_locale.model.state';
import { ArticleModel } from 'src/app/models/article/article.model';
import { ArticleModelAudit } from 'src/app/models/article/article.model.state';
import { BookModel } from 'src/app/models/book/book.model';
import { BookModelAudit } from 'src/app/models/book/book.model.state';
import { CoreUserModel } from 'src/app/models/coreUser/core_user.model';
import { CoreUserModelAudit } from 'src/app/models/coreUser/core_user.model.state';
import { CourseModel } from 'src/app/models/course/course.model';
import { CourseModelAudit } from 'src/app/models/course/course.model.state';
import { CourseCategoryModel } from 'src/app/models/courseCategory/course_category.model';
import { CourseCategoryModelAudit } from 'src/app/models/courseCategory/course_category.model.state';
import { CourseLessonModel } from 'src/app/models/courseLesson/course_lesson.model';
import { CourseLessonModelAudit } from 'src/app/models/courseLesson/course_lesson.model.state';
import { LessonModel } from 'src/app/models/lesson/lesson.model';
import { LessonModelAudit } from 'src/app/models/lesson/lesson.model.state';
import { TagModel } from 'src/app/models/tag/tag.model';
import { TagModelAudit } from 'src/app/models/tag/tag.model.state';
import { RoleModel } from 'src/app/models/role/role.model';
import { RoleModelAudit } from 'src/app/models/role/role.model.state';
import { PrivilegeModel } from 'src/app/models/privilege/privilege.model';
import { PrivilegeModelAudit } from 'src/app/models/privilege/privilege.model.state';
import { WorkflowModel } from 'src/app/models/workflow/workflow.model';
import { WorkflowModelAudit } from 'src/app/models/workflow/workflow.model.state';
import { WorkflowStateModel } from 'src/app/models/workflowState/workflow_state.model';
import { WorkflowStateModelAudit } from 'src/app/models/workflowState/workflow_state.model.state';
import { WorkflowTransitionModel } from 'src/app/models/workflowTransition/workflow_transition.model';
import { WorkflowTransitionModelAudit } from 'src/app/models/workflowTransition/workflow_transition.model.state';
import { WorkflowVersionModel } from 'src/app/models/workflowVersion/workflow_version.model';
import { WorkflowVersionModelAudit } from 'src/app/models/workflowVersion/workflow_version.model.state';
import { LessonFormSubmissionModel } from 'src/app/models/lessonFormSubmission/lesson_form_submission.model';
import { LessonFormSubmissionModelAudit } from 'src/app/models/lessonFormSubmission/lesson_form_submission.model.state';
import { LessonFormVersionModel } from 'src/app/models/lessonFormVersion/lesson_form_version.model';
import { LessonFormVersionModelAudit } from 'src/app/models/lessonFormVersion/lesson_form_version.model.state';
import { LessonFormTileModel } from 'src/app/models/lessonFormTile/lesson_form_tile.model';
import { LessonFormTileModelAudit } from 'src/app/models/lessonFormTile/lesson_form_tile.model.state';

import { ModelProperty, ModelRelation } from 'src/app/lib/models/abstract.model';
import { CommonComponentModule } from 'src/app/lib/components/common.component.module';
import { CrudCreateEditComponent } from './crud.create.edit.component';
import { StoreModule } from '@ngrx/store';
import { reducers, clearState } from 'src/app/models/model.reducer';
import { initialRouterState, initialModelState } from 'src/app/models/model.state';
import { createReactiveFormFromModel } from 'src/app/lib/models/model-utils';
import { ValidatorInputUtils } from 'src/testing/helpers/validator-input-utils';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

const requiredValidators = [
	{entityName: 'applicationLocale', attributeName: 'locale'},
	{entityName: 'applicationLocale', attributeName: 'key'},
	{entityName: 'applicationLocale', attributeName: 'value'},
	{entityName: 'article', attributeName: 'title'},
	{entityName: 'article', attributeName: 'summary'},
	{entityName: 'book', attributeName: 'name'},
	{entityName: 'coreUser', attributeName: 'firstName'},
	{entityName: 'coreUser', attributeName: 'lastName'},
	{entityName: 'course', attributeName: 'name'},
	{entityName: 'courseCategory', attributeName: 'name'},
	{entityName: 'lesson', attributeName: 'summary'},
	{entityName: 'lesson', attributeName: 'name'},
	{entityName: 'tag', attributeName: 'name'},
	{entityName: 'role', attributeName: 'name'},
	{entityName: 'privilege', attributeName: 'name'},
	{entityName: 'privilege', attributeName: 'targetEntity'},
	{entityName: 'privilege', attributeName: 'allowCreate'},
	{entityName: 'privilege', attributeName: 'allowRead'},
	{entityName: 'privilege', attributeName: 'allowUpdate'},
	{entityName: 'privilege', attributeName: 'allowDelete'},
	{entityName: 'workflow', attributeName: 'name'},
	{entityName: 'workflowState', attributeName: 'stepName'},
	{entityName: 'workflowTransition', attributeName: 'transitionName'},
	{entityName: 'workflowVersion', attributeName: 'workflowName'},
	{entityName: 'lessonFormVersion', attributeName: 'version'},
	{entityName: 'lessonFormVersion', attributeName: 'formData'},
];

const rangeValidators = [
];

// Excludes length validators on attributes which also have other validators applied
const lengthValidators = [
];

const numericValidators = [
];

const alphanumericValidators = [
];

const urlValidators = [
];

const emailValidators = [
];

const uuidValidators = [
];

// % protected region % [Add any additional constants here] off begin
// % protected region % [Add any additional constants here] end

describe('Crud Create Edit Tests', () => {
	let modelForm: FormGroup;
	let modelProperties: ModelProperty[];
	let modelRelations: { [name: string]: ModelRelation };

	let fixture: ComponentFixture<CrudCreateEditComponent<any, any>>;
	let crudElement: CrudCreateEditComponent<any, any>;
	let formGroup: DebugElement;

	// % protected region % [Add any additional test class methods here] off begin
	// % protected region % [Add any additional test class methods here] end

	function createTestingComponent(entityName: string) {
		let model;
		switch (entityName) {
			case 'administrator':
				model = new AdministratorModel();
				modelProperties = AdministratorModel.getProps();
				modelRelations = AdministratorModel.getRelations();
				modelForm = createReactiveFormFromModel(modelProperties, modelRelations, false);

				fixture = TestBed.createComponent<CrudCreateEditComponent<AdministratorModel,
				AdministratorModelAudit>>(CrudCreateEditComponent);
				break;

			case 'applicationLocale':
				model = new ApplicationLocaleModel();
				modelProperties = ApplicationLocaleModel.getProps();
				modelRelations = ApplicationLocaleModel.getRelations();
				modelForm = createReactiveFormFromModel(modelProperties, modelRelations, false);

				fixture = TestBed.createComponent<CrudCreateEditComponent<ApplicationLocaleModel,
				ApplicationLocaleModelAudit>>(CrudCreateEditComponent);
				break;

			case 'article':
				model = new ArticleModel();
				modelProperties = ArticleModel.getProps();
				modelRelations = ArticleModel.getRelations();
				modelForm = createReactiveFormFromModel(modelProperties, modelRelations, false);

				fixture = TestBed.createComponent<CrudCreateEditComponent<ArticleModel,
				ArticleModelAudit>>(CrudCreateEditComponent);
				break;

			case 'book':
				model = new BookModel();
				modelProperties = BookModel.getProps();
				modelRelations = BookModel.getRelations();
				modelForm = createReactiveFormFromModel(modelProperties, modelRelations, false);

				fixture = TestBed.createComponent<CrudCreateEditComponent<BookModel,
				BookModelAudit>>(CrudCreateEditComponent);
				break;

			case 'coreUser':
				model = new CoreUserModel();
				modelProperties = CoreUserModel.getProps();
				modelRelations = CoreUserModel.getRelations();
				modelForm = createReactiveFormFromModel(modelProperties, modelRelations, false);

				fixture = TestBed.createComponent<CrudCreateEditComponent<CoreUserModel,
				CoreUserModelAudit>>(CrudCreateEditComponent);
				break;

			case 'course':
				model = new CourseModel();
				modelProperties = CourseModel.getProps();
				modelRelations = CourseModel.getRelations();
				modelForm = createReactiveFormFromModel(modelProperties, modelRelations, false);

				fixture = TestBed.createComponent<CrudCreateEditComponent<CourseModel,
				CourseModelAudit>>(CrudCreateEditComponent);
				break;

			case 'courseCategory':
				model = new CourseCategoryModel();
				modelProperties = CourseCategoryModel.getProps();
				modelRelations = CourseCategoryModel.getRelations();
				modelForm = createReactiveFormFromModel(modelProperties, modelRelations, false);

				fixture = TestBed.createComponent<CrudCreateEditComponent<CourseCategoryModel,
				CourseCategoryModelAudit>>(CrudCreateEditComponent);
				break;

			case 'courseLesson':
				model = new CourseLessonModel();
				modelProperties = CourseLessonModel.getProps();
				modelRelations = CourseLessonModel.getRelations();
				modelForm = createReactiveFormFromModel(modelProperties, modelRelations, false);

				fixture = TestBed.createComponent<CrudCreateEditComponent<CourseLessonModel,
				CourseLessonModelAudit>>(CrudCreateEditComponent);
				break;

			case 'lesson':
				model = new LessonModel();
				modelProperties = LessonModel.getProps();
				modelRelations = LessonModel.getRelations();
				modelForm = createReactiveFormFromModel(modelProperties, modelRelations, false);

				fixture = TestBed.createComponent<CrudCreateEditComponent<LessonModel,
				LessonModelAudit>>(CrudCreateEditComponent);
				break;

			case 'tag':
				model = new TagModel();
				modelProperties = TagModel.getProps();
				modelRelations = TagModel.getRelations();
				modelForm = createReactiveFormFromModel(modelProperties, modelRelations, false);

				fixture = TestBed.createComponent<CrudCreateEditComponent<TagModel,
				TagModelAudit>>(CrudCreateEditComponent);
				break;

			case 'role':
				model = new RoleModel();
				modelProperties = RoleModel.getProps();
				modelRelations = RoleModel.getRelations();
				modelForm = createReactiveFormFromModel(modelProperties, modelRelations, false);

				fixture = TestBed.createComponent<CrudCreateEditComponent<RoleModel,
				RoleModelAudit>>(CrudCreateEditComponent);
				break;

			case 'privilege':
				model = new PrivilegeModel();
				modelProperties = PrivilegeModel.getProps();
				modelRelations = PrivilegeModel.getRelations();
				modelForm = createReactiveFormFromModel(modelProperties, modelRelations, false);

				fixture = TestBed.createComponent<CrudCreateEditComponent<PrivilegeModel,
				PrivilegeModelAudit>>(CrudCreateEditComponent);
				break;

			case 'workflow':
				model = new WorkflowModel();
				modelProperties = WorkflowModel.getProps();
				modelRelations = WorkflowModel.getRelations();
				modelForm = createReactiveFormFromModel(modelProperties, modelRelations, false);

				fixture = TestBed.createComponent<CrudCreateEditComponent<WorkflowModel,
				WorkflowModelAudit>>(CrudCreateEditComponent);
				break;

			case 'workflowState':
				model = new WorkflowStateModel();
				modelProperties = WorkflowStateModel.getProps();
				modelRelations = WorkflowStateModel.getRelations();
				modelForm = createReactiveFormFromModel(modelProperties, modelRelations, false);

				fixture = TestBed.createComponent<CrudCreateEditComponent<WorkflowStateModel,
				WorkflowStateModelAudit>>(CrudCreateEditComponent);
				break;

			case 'workflowTransition':
				model = new WorkflowTransitionModel();
				modelProperties = WorkflowTransitionModel.getProps();
				modelRelations = WorkflowTransitionModel.getRelations();
				modelForm = createReactiveFormFromModel(modelProperties, modelRelations, false);

				fixture = TestBed.createComponent<CrudCreateEditComponent<WorkflowTransitionModel,
				WorkflowTransitionModelAudit>>(CrudCreateEditComponent);
				break;

			case 'workflowVersion':
				model = new WorkflowVersionModel();
				modelProperties = WorkflowVersionModel.getProps();
				modelRelations = WorkflowVersionModel.getRelations();
				modelForm = createReactiveFormFromModel(modelProperties, modelRelations, false);

				fixture = TestBed.createComponent<CrudCreateEditComponent<WorkflowVersionModel,
				WorkflowVersionModelAudit>>(CrudCreateEditComponent);
				break;

			case 'lessonFormSubmission':
				model = new LessonFormSubmissionModel();
				modelProperties = LessonFormSubmissionModel.getProps();
				modelRelations = LessonFormSubmissionModel.getRelations();
				modelForm = createReactiveFormFromModel(modelProperties, modelRelations, false);

				fixture = TestBed.createComponent<CrudCreateEditComponent<LessonFormSubmissionModel,
				LessonFormSubmissionModelAudit>>(CrudCreateEditComponent);
				break;

			case 'lessonFormVersion':
				model = new LessonFormVersionModel();
				modelProperties = LessonFormVersionModel.getProps();
				modelRelations = LessonFormVersionModel.getRelations();
				modelForm = createReactiveFormFromModel(modelProperties, modelRelations, false);

				fixture = TestBed.createComponent<CrudCreateEditComponent<LessonFormVersionModel,
				LessonFormVersionModelAudit>>(CrudCreateEditComponent);
				break;

			case 'lessonFormTile':
				model = new LessonFormTileModel();
				modelProperties = LessonFormTileModel.getProps();
				modelRelations = LessonFormTileModel.getRelations();
				modelForm = createReactiveFormFromModel(modelProperties, modelRelations, false);

				fixture = TestBed.createComponent<CrudCreateEditComponent<LessonFormTileModel,
				LessonFormTileModelAudit>>(CrudCreateEditComponent);
				break;

		}

		crudElement = fixture.debugElement.componentInstance;
		crudElement.model = model;
		crudElement.modelProperties = modelProperties;
		crudElement.modelRelations = modelRelations;
		crudElement.modelFormGroup = modelForm;
		crudElement.isDisabled = false;
		fixture.detectChanges();

		formGroup = fixture
			.debugElement
			.query(By.css('.crud__form-container'));

		// % protected region % [Add any additional logic when creating the testing component here] off begin
		// % protected region % [Add any additional logic when creating the testing component here] end
	}

	beforeEach(waitForAsync (() => {
		TestBed.configureTestingModule({
			imports: [
				CommonModule,
				FormsModule,
				ReactiveFormsModule,
				CommonComponentModule,
				StoreModule.forRoot(reducers, {
					initialState: {
						router: initialRouterState,
						models: initialModelState,
					},
					metaReducers: [
						clearState,
					],
				}),
				// % protected region % [Add additional TestBed imports here] off begin
				// % protected region % [Add additional TestBed imports here] end
			],
			declarations: [
				CrudCreateEditComponent
			]
			// % protected region % [Add any additional fields to configureTestingModule here] off begin
			// % protected region % [Add any additional fields to configureTestingModule here] end
		}).compileComponents();
	}));

	afterEach(() => {
		(fixture.nativeElement as HTMLElement).remove();

		// % protected region % [Add additional logic to the afterEach method here] off begin
		// % protected region % [Add additional logic to the afterEach method here] end
	});

	requiredValidators.forEach(validator => {
		it('should be invalid when violating ' + validator.entityName + ' required validator on ' + validator.attributeName, () => {
			createTestingComponent(validator.entityName);

			// Set value to empty so that we can check the required validator is violated
			crudElement.modelFormGroup.get(validator.attributeName).setValue('');
			fixture.detectChanges();
			expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeFalsy();
		});

		// Required validator can be applied in addition to any other validator.
		// As a result we don't check that the form control is valid after setting a value, as a value may not
		// fulfil the other validators present
	});

	rangeValidators.forEach(validator => {
		it('should be invalid when violating ' + validator.entityName + ' range validator on ' + validator.attributeName, () => {
			createTestingComponent(validator.entityName);

			crudElement.modelFormGroup.get(validator.attributeName).setValue(validator.lower - 1);
			fixture.detectChanges();
			expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeFalsy();

			crudElement.modelFormGroup.get(validator.attributeName).setValue(validator.upper + 1);
			fixture.detectChanges();
			expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeFalsy();
		});

		it('should have valid form group when not volating ' + validator.entityName + ' range validator on ' + validator.attributeName, () => {
			createTestingComponent(validator.entityName);

			crudElement.modelFormGroup.get(validator.attributeName).setValue(validator.lower);
			fixture.detectChanges();
			expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeTruthy();

			crudElement.modelFormGroup.get(validator.attributeName).setValue(validator.upper);
			fixture.detectChanges();
			expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeTruthy();
		});
	});

	lengthValidators.forEach(validator => {
		it('should be invalid when violating ' + validator.entityName + ' length validator on ' + validator.attributeName, () => {
			createTestingComponent(validator.entityName);

			// Check that string shorter than minimum length violates the validator
			if (validator.min) {
				crudElement.modelFormGroup.get(validator.attributeName).setValue(ValidatorInputUtils.lengthString(validator.min - 1));
				fixture.detectChanges();
				expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeFalsy();
			}

			// Check that string longer than maximum length violated the validator
			if (validator.max) {
				crudElement.modelFormGroup.get('name').setValue(ValidatorInputUtils.lengthString(validator.max + 1));
				fixture.detectChanges();
				expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeFalsy();
			}
		});

		it('should have valid form group when not volating ' + validator.entityName + ' length validator on ' + validator.attributeName, () => {
			createTestingComponent(validator.entityName);

			// Check that string of minimum length does not violate the validator
			if (validator.min) {
				crudElement.modelFormGroup.get(validator.attributeName).setValue(ValidatorInputUtils.lengthString(validator.min));
				fixture.detectChanges();
				expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeTruthy();
			}

			// Check that string of maximum length does not violate the validator
			if (validator.max) {
				crudElement.modelFormGroup.get(validator.attributeName).setValue(ValidatorInputUtils.lengthString(validator.max));
				fixture.detectChanges();
				expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeTruthy();
			}
		});
	});

	numericValidators.forEach(validator => {
		it('should be invalid when violating ' + validator.entityName + ' numeric validator on ' + validator.attributeName, () => {
			createTestingComponent(validator.entityName);

			if (validator.min) {
				crudElement.modelFormGroup.get(validator.attributeName).setValue(ValidatorInputUtils.numeric(true, validator.min - 1));
				fixture.detectChanges();
				expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeFalsy();

				crudElement.modelFormGroup.get(validator.attributeName).setValue(ValidatorInputUtils.numeric(false, validator.min));
				fixture.detectChanges();
				expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeFalsy();
			}

			if (validator.max) {
				crudElement.modelFormGroup.get(validator.attributeName).setValue(ValidatorInputUtils.numeric(true, validator.max + 1));
				fixture.detectChanges();
				expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeFalsy();

				crudElement.modelFormGroup.get(validator.attributeName).setValue(ValidatorInputUtils.numeric(false, validator.max));
				fixture.detectChanges();
				expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeFalsy();
			}

			if (!validator.min && !validator.max) {
				crudElement.modelFormGroup.get(validator.attributeName).setValue(ValidatorInputUtils.numeric(false, 6));
				fixture.detectChanges();
				expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeFalsy();
			}
		});

		it('should have valid form group when not volating '
		 		+ validator.entityName + ' numeric validator on ' + validator.attributeName, () => {
			createTestingComponent(validator.entityName);

			if (validator.min) {
				crudElement.modelFormGroup.get(validator.attributeName).setValue(ValidatorInputUtils.numeric(true, validator.min));
				fixture.detectChanges();
				expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeTruthy();
			}

			if (validator.max) {
				crudElement.modelFormGroup.get(validator.attributeName).setValue(ValidatorInputUtils.numeric(true, validator.max));
				fixture.detectChanges();
				expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeTruthy();
			}

			if (!validator.min && !validator.max) {
				crudElement.modelFormGroup.get(validator.attributeName).setValue(ValidatorInputUtils.numeric(true, 6));
				fixture.detectChanges();
				expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeTruthy();
			}
		});
	});

	alphanumericValidators.forEach(validator => {
		it('should be invalid when violating ' + validator.entityName + ' alphanumeric validator on ' + validator.attributeName, () => {
			createTestingComponent(validator.entityName);

			if (validator.min) {
				crudElement.modelFormGroup.get(validator.attributeName).setValue(ValidatorInputUtils.alphanumeric(true, validator.min - 1));
				fixture.detectChanges();
				expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeFalsy();

				crudElement.modelFormGroup.get(validator.attributeName).setValue(ValidatorInputUtils.alphanumeric(false, validator.min));
				fixture.detectChanges();
				expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeFalsy();
			}

			if (validator.max) {
				crudElement.modelFormGroup.get(validator.attributeName).setValue(ValidatorInputUtils.alphanumeric(true, validator.max + 1));
				fixture.detectChanges();
				expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeFalsy();

				crudElement.modelFormGroup.get(validator.attributeName).setValue(ValidatorInputUtils.alphanumeric(false, validator.max));
				fixture.detectChanges();
				expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeFalsy();
			}

			if (!validator.min && !validator.max) {
				crudElement.modelFormGroup.get(validator.attributeName).setValue(ValidatorInputUtils.alphanumeric(false, 6));
				fixture.detectChanges();
				expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeFalsy();
			}
		});

		it('should have valid form group when not volating '
		 		+ validator.entityName + ' alphanumeric validator on ' + validator.attributeName, () => {
			createTestingComponent(validator.entityName);

			if (validator.min) {
				crudElement.modelFormGroup.get(validator.attributeName).setValue(ValidatorInputUtils.alphanumeric(true, validator.min));
				fixture.detectChanges();
				expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeTruthy();
			}

			if (validator.max) {
				crudElement.modelFormGroup.get(validator.attributeName).setValue(ValidatorInputUtils.alphanumeric(true, validator.max));
				fixture.detectChanges();
				expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeTruthy();
			}

			if (!validator.min && !validator.max) {
				crudElement.modelFormGroup.get(validator.attributeName).setValue(ValidatorInputUtils.alphanumeric(true, 6));
				fixture.detectChanges();
				expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeTruthy();
			}
		});
	});

	urlValidators.forEach(validator => {
		it('should be invalid when violating ' + validator.entityName + ' url validator on ' + validator.attributeName, () => {
			createTestingComponent(validator.entityName);

			crudElement.modelFormGroup.get(validator.attributeName).setValue(ValidatorInputUtils.url(false));
			fixture.detectChanges();
			expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeFalsy();
		});

		it('should have valid form group when not volating ' + validator.entityName + ' url validator on ' + validator.attributeName, () => {
			createTestingComponent(validator.entityName);

			crudElement.modelFormGroup.get(validator.attributeName).setValue(ValidatorInputUtils.url(true));
			fixture.detectChanges();
			expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeTruthy();
		});
	});

	emailValidators.forEach(validator => {
		it('should be invalid when violating ' + validator.entityName + ' email validator on ' + validator.attributeName, () => {
			createTestingComponent(validator.entityName);

			crudElement.modelFormGroup.get(validator.attributeName).setValue(ValidatorInputUtils.email(false));
			fixture.detectChanges();
			expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeFalsy();
		});

		it('should have valid form group when not volating ' + validator.entityName + ' email validator on ' + validator.attributeName, () => {
			createTestingComponent(validator.entityName);

			crudElement.modelFormGroup.get(validator.attributeName).setValue(ValidatorInputUtils.email(true));
			fixture.detectChanges();
			expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeTruthy();
		});
	});

	uuidValidators.forEach(validator => {
		it('should be invalid when violating ' + validator.entityName + ' uuid validator on ' + validator.attributeName, () => {
			createTestingComponent(validator.entityName);

			crudElement.modelFormGroup.get(validator.attributeName).setValue(ValidatorInputUtils.uuid(false));
			fixture.detectChanges();
			expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeFalsy();
		});

		it('should have valid form group when not volating ' + validator.entityName + ' uuid validator on ' + validator.attributeName, () => {
			createTestingComponent(validator.entityName);

			crudElement.modelFormGroup.get(validator.attributeName).setValue(ValidatorInputUtils.uuid(true));
			fixture.detectChanges();
			expect(crudElement.modelFormGroup.get(validator.attributeName).valid).toBeTruthy();
		});
	});

	// % protected region % [Add any additional test cases here] off begin
	// % protected region % [Add any additional test cases here] end
});
