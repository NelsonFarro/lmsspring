/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {WorkflowComponent} from './workflow.component';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {DebugElement, SimpleChange} from '@angular/core';
import {of} from 'rxjs';
import {By} from '@angular/platform-browser';
import SpyObj = jasmine.SpyObj;
import {CommonModule} from '@angular/common';
import {WorkflowStateService} from '../../../services/workflowState/workflow_state.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DropdownComponent} from '../dropdown/dropdown.component';
import {NgSelectModule} from '@ng-select/ng-select';
import {InputErrorMessageComponent} from '../abstract.input.component';
import {WorkflowDataFactory} from '../../utils/factories/workflow-data-factory';
import {WorkflowStateDataFactory} from '../../utils/factories/workflow-state-data-factory';
import {WorkflowVersionDataFactory} from '../../utils/factories/workflow-version-data-factory';
import {WorkflowVersionModel} from '../../../models/workflowVersion/workflow_version.model';


describe('Workflow component', () => {
	let fixture: ComponentFixture<WorkflowComponent>;
	let workflowComponent: WorkflowComponent;
	let workflowStateService: SpyObj<WorkflowStateService>;
	let workflowDataFactory: WorkflowDataFactory;
	let workflowVersionDataFactory: WorkflowVersionDataFactory;
	let workflowStateDataFactory: WorkflowStateDataFactory;

	beforeEach(() => {
		workflowStateService = jasmine.createSpyObj<WorkflowStateService>('workflowStateService', ['getNextStates']);

		TestBed.configureTestingModule({
			declarations: [
				WorkflowComponent,
				DropdownComponent,
				InputErrorMessageComponent
			],
			imports: [
				CommonModule,
				FormsModule,
				ReactiveFormsModule,
				NgSelectModule
			],
			providers: [
				{
					provide: WorkflowStateService,
					useValue: workflowStateService
				}
			]
		});

		fixture = TestBed.createComponent(WorkflowComponent);
		workflowComponent = fixture.componentInstance;

		workflowDataFactory = new WorkflowDataFactory();
		workflowVersionDataFactory = new WorkflowVersionDataFactory();
		workflowStateDataFactory = new WorkflowStateDataFactory();
	});

	afterEach(() => {
		// Need to do this since for some reason the last component queried from the fixture will be rendered on the
		// browser
		if (fixture.nativeElement instanceof HTMLElement) {
			(fixture.nativeElement as HTMLElement).remove();
		}

		// % protected region % [Add any additional logic after each test here] off begin
		// % protected region % [Add any additional logic after each test here] end
	});

	it('should be created normally', () => {
		expect(workflowComponent).toBeDefined();
		expect(workflowComponent).toBeTruthy();
	});

	it('should display correct workflow state', () => {
		const workflows = workflowVersionDataFactory.createAll();
		const workflowsWithState = workflows.map(workflowVersion => {
			workflowVersion.articleAssociation = true;
			return {
				workflowVersion: workflowVersion,
				currentState: workflowStateDataFactory.create(),
				nextStates$: of(workflowStateDataFactory.createAll())
			};
		});

		workflowComponent.workflows = workflows;
		workflowComponent.workflowDisplayOptions = workflowsWithState;
		workflowComponent.updateWorkflowForm();
		fixture.detectChanges();

		const h4s = fixture.debugElement.queryAll(By.css('h4'));
		const dropdowns = fixture.debugElement.queryAll(By.directive(DropdownComponent));

		expect(h4s.length).toEqual(workflows.length);
		expect(dropdowns.length).toEqual(workflows.length);

		for (let i = 0; i < workflows.length; ++i) {
			expect(h4s[i].nativeElement.innerText).toEqual(`${workflows[i].workflowName} Workflow`);
			expect((dropdowns[i].componentInstance as DropdownComponent).placeholder).toEqual(`Current State: ${workflowsWithState[i].currentState.stepName}`);
		}
	});

	it('should display warning text when not state available for a workflow', () =>  {
		const workflows: WorkflowVersionModel[] = workflowVersionDataFactory.createAll().map(workflowVersion => {
			workflowVersion.articleAssociation = true;
			return workflowVersion;
		});

		const workflowDisplayOptions = workflows.map(workflowVersion => {

			const currentState = workflowStateDataFactory.create();
			const nextStates = workflowStateDataFactory.createAll();
			const nextStates$ = of(nextStates);
			workflowVersion.startState = currentState;

			workflowStateService.getNextStates.withArgs(currentState.id).and.returnValue(nextStates$);

			return {
				workflowVersion: workflowVersion,
				currentState: currentState,
				nextStates$: nextStates$,
				nextStates: nextStates,
			};
		});

		const workflowWithoutState = workflowVersionDataFactory.create();
		workflowWithoutState.startState = null;

		workflows.push(workflowWithoutState);

		workflowComponent.workflows = workflows;
		workflowComponent.ngOnChanges({
			workflows: new SimpleChange(null, workflows, null)
		});
		fixture.detectChanges();

		const h4s = fixture.debugElement.queryAll(By.css('h4'));
		const noticeText: DebugElement[] = fixture.debugElement.queryAll(By.css('p.text--notice'));
		const dropdowns: DebugElement[] = fixture.debugElement.queryAll(By.directive(DropdownComponent));

		expect(h4s.length).toEqual(workflows.length);
		expect(workflowStateService.getNextStates).toHaveBeenCalledTimes(workflows.length - 1);
		expect(dropdowns.length).toEqual(workflows.length - 1);
		expect(noticeText.length).toBe(1);
	});
});
