/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {
	AfterViewInit,
	Component,
	ComponentFactoryResolver,
	Directive,
	EventEmitter,
	Input,
	Output,
	HostBinding,
	QueryList,
	Type,
	ViewContainerRef,
	ViewChildren
} from '@angular/core';
import {Subject} from 'rxjs';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {FormMode, FormQuestionData, FormSlideData} from '../form-interfaces';
import {FormCustomQuestionComponent} from '../form-questions/form-custom-question/form-custom-question.component';
import {AbstractComponent} from '../../../components/abstract.component';
import {ButtonStyle, ButtonSize, IconPosition} from '../../../components/button/button.component';
import {getQuestionType} from '../form-utils';
import * as uuid from 'uuid';
import * as _ from 'lodash';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end
/**
 * Allow dynamic component loading.
 */
@Directive({
	selector: '[cb-form-slide-directive]',
})
export class FormSlideDirective implements AfterViewInit {
	@Input()
	data: FormQuestionData;

	@Input()
	formMode: FormMode;

	/**
	 * Emitter used to propagate question duplicate event upward.
	 */
	@Output('editQuestion')
	editQuestionEmitter = new EventEmitter<{questionData: FormQuestionData, questionTypeSubject: Subject<string>}>();

	/**
	 * Emitter used to propagate question edit event upward.
	 */
	@Output('duplicateQuestion')
	duplicateQuestionEmitter = new EventEmitter<FormQuestionData>();

	/**
	 * Emitter used to propagate question delete event upward.
	 */
	@Output('deleteQuestion')
	deleteQuestionEmitter = new EventEmitter<FormQuestionData>();

	/**
	 * Emitter used to propagate question delete event upward.
	 */
	@Output('changeOrder')
	changeOrderEmitter = new EventEmitter<{change: number, question: FormQuestionData}>();

	/**
	 * Subject to receive the changes of question type
	 */
	questionTypeSubject = new Subject<string>();

	// % protected region % [Add any additional fields for FormSlideDirective here] off begin
	// % protected region % [Add any additional fields for FormSlideDirective here] end

	constructor(
		public viewContainerRef: ViewContainerRef,
		private readonly componentFactoryResolver: ComponentFactoryResolver,
	) {
		this.questionTypeSubject.subscribe((questionType) => {
			this.loadQuestion();
		});
	}

	/**
	 * @inhertDoc
	 */
	ngAfterViewInit() {
		this.loadQuestion();
	}

	/**
	 * Dynamically load the question component
	 */
	private loadQuestion() {
		// % protected region % [Add any additional logic before main body of loadQuestion here] off begin
		// % protected region % [Add any additional logic before main body of loadQuestion here] end

		// Create new form question component dynamically and then load that into the slot marked by FormSlideDirective
		const componentFactory = this.componentFactoryResolver.resolveComponentFactory(getQuestionType(this.data.type));
		const viewContainerRef = this.viewContainerRef;
		viewContainerRef.clear();
		const componentRef = viewContainerRef.createComponent(componentFactory);
		const component = componentRef.instance as FormCustomQuestionComponent;

		component.formMode = this.formMode;
		component.data = this.data;

		// Listen events and pass to parent component
		component.editEmitter.subscribe((toBeEdited: FormQuestionData) => {
			this.editQuestionEmitter.emit({questionData: toBeEdited, questionTypeSubject: this.questionTypeSubject});
		});

		component.duplicateEmitter.subscribe((toBeDuplicated: FormQuestionData) => {
			this.duplicateQuestionEmitter.emit(toBeDuplicated);
		});

		component.deleteEmitter.subscribe((toBeDeleted: FormQuestionData) => {
			this.deleteQuestionEmitter.emit(toBeDeleted);
		});

		component.changeOrderEmitter.subscribe((changes) => {
			this.changeOrderEmitter.emit(changes);
		});

		componentRef.changeDetectorRef.detectChanges();

		// % protected region % [Add any additional logic after main body of loadQuestion here] off begin
		// % protected region % [Add any additional logic after main body of loadQuestion here] end
	}

	// % protected region % [Add any additional methods for FormSlideDirective here] off begin
	// % protected region % [Add any additional methods for FormSlideDirective here] end
}

@Component({
	selector: '*[cb-form-slide],cb-form-slide',
	templateUrl: './form-slide.component.html',
	// % protected region % [Add any additional component configurations here] off begin
	// % protected region % [Add any additional component configurations here] end
})
export class FormSlideComponent extends AbstractComponent {
	readonly IconPosition = IconPosition;
	readonly ButtonStyle = ButtonStyle;
	readonly ButtonSize = ButtonSize;

	/**
	 * All the question slots so we can start replacing it with the real component.
	 */
	@ViewChildren(FormSlideDirective)
	questions: QueryList<FormSlideDirective>;

	/**
	 * Default host class.
	 */
	@HostBinding('class')
	get formSlideContainerCSSClass() {
		return [
			'form__slide-container',
			// % protected region % [Add any additional host classes here] off begin
			// % protected region % [Add any additional host classes here] end
		].join(' ');
	}

	/**
	 * Containing data related to this slide. Also used to transfer question related data into their respective questions.
	 */
	@Input()
	data: FormSlideData;

	@Input()
	formMode: FormMode;

	/**
	 * Emitter used to propagate question duplicate event upward.
	 */
	@Output('editQuestion')
	editQuestionEmitter = new EventEmitter();

	/**
	 * Emitter used to propagate question edit event upward.
	 */
	@Output('duplicateQuestion')
	duplicateQuestionEmitter = new EventEmitter<FormQuestionData>();

	/**
	 * Emitter used to propagate question delete event upward.
	 */
	@Output('deleteQuestion')
	deleteQuestionEmitter = new EventEmitter<FormQuestionData>();

	// % protected region % [Add any additional class properties here] off begin
	// % protected region % [Add any additional class properties here] end

	constructor(
			private readonly componentFactoryResolver: ComponentFactoryResolver,
			// % protected region % [Add any additional constructor parameters here] off begin
			// % protected region % [Add any additional constructor parameters here] end
	) {
		super();

		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}

	/**
	 * Triggered when the user wants to add a new question.
	 */
	onNewQuestion() {
		this.data.questionsData.push({
			type: 'textfield',
			id: uuid.v4(),
			questionNumber: this.data.questionsData.length,
			questionContent: null,
			questionSubtext: null,
			name: '',
			label: '',
			options: {}
		});
	}

	/**
	 * Triggered when user want to duplicate question
	 */
	duplicateQuestion(toBeDuplicated: FormQuestionData) {
		// % protected region % [Add any additional logic for duplicateQuestion before the main body here] off begin
		// % protected region % [Add any additional logic for duplicateQuestion before the main body here] end

		const newQuestion = _.cloneDeep(toBeDuplicated);
		newQuestion.id = uuid.v4();
		newQuestion.questionNumber = this.data.questionsData.length;
		this.data.questionsData.push(newQuestion);

		// % protected region % [Add any additional logic for duplicateQuestion after the main body here] off begin
		// % protected region % [Add any additional logic for duplicateQuestion after the main body here] end
	}

	/**
	 * Triggered when user want to delete question
	 */
	deleteQuestion(toBeDeleted: FormQuestionData) {
		// % protected region % [Add any additional logic for deleteQuestion before the main body here] off begin
		// % protected region % [Add any additional logic for deleteQuestion before the main body here] end

		_.remove(this.data.questionsData, questionData => questionData.id === toBeDeleted.id);

		// % protected region % [Add any additional logic for deleteQuestion after the main body here] off begin
		// % protected region % [Add any additional logic for deleteQuestion after the main body here] end
	}

	/**
	 * Change the order of question when click move up / move down button
	 */
	orderChange(changes: {change: number, question: FormQuestionData}) {
		const question = changes.question;
		const change = changes.change;

		const fromIndex = question.questionNumber;
		const toIndex = fromIndex + change;

		// Do not need to change since already the last one or first  one
		if (toIndex < 0 || toIndex >= this.data.questionsData.length) {
			return;
		}
		moveItemInArray(this.data.questionsData, fromIndex, toIndex);
		this.data.questionsData.forEach((questionData, index) => questionData.questionNumber = index );
	}

	/**
	 * Handle drag and drop event in the list
	 */
	onQuestionDropped(event: CdkDragDrop<FormQuestionData[]>) {
		moveItemInArray(this.data.questionsData, event.previousIndex, event.currentIndex);
		this.data.questionsData.forEach((questionData, index) => questionData.questionNumber = index );
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
