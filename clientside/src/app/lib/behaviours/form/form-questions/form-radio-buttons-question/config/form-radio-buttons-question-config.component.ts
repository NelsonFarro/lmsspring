/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Component, OnChanges, SimpleChanges} from '@angular/core';
import {FormCustomQuestionConfigComponent} from '../../form-custom-question/config/form-custom-question-config.component';
import {
	ButtonAccentColour,
	ButtonSize,
	ButtonStyle,
	IconPosition
} from '../../../../../components/button/button.component';
import * as uuid from 'uuid';
import * as _ from 'lodash';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * Config option for a single radio button
 */
interface RadioButtonsOption {
	id: string;
	name: string;
	value: string;
	description?: string;
	// % protected region % [Add any additional fields for RadioButtonsOption here] off begin
	// % protected region % [Add any additional fields for RadioButtonsOption here] end
}

/**
 * Config options for radio buttons question
 */
interface RadioButtonsQuestionOptions {
	/**
	 * Options of check boxes
	 */
	options: RadioButtonsOption[];
	// % protected region % [Add any additional fields for RadioButtonsQuestionOptions here] off begin
	// % protected region % [Add any additional fields for RadioButtonsQuestionOptions here] end
}

/**
 * Component to config radio buttons question
 */
@Component({
	selector: 'div[cb-form-radio-buttons-question-config]',
	templateUrl: './form-radio-buttons-question-config.component.html',
	// % protected region % [Add any additional component configurations here] off begin
	// % protected region % [Add any additional component configurations here] end
})
export class FormRadioButtonsQuestionConfigComponent extends FormCustomQuestionConfigComponent {

	readonly IconPosition = IconPosition;
	readonly ButtonStyle = ButtonStyle;
	readonly ButtonSize = ButtonSize;
	readonly ButtonAccentColour = ButtonAccentColour;

	/**
	 * Options of radio buttons question
	 */
	radioButtonOptionsOptions: RadioButtonsQuestionOptions;

	// % protected region % [Add any additional class properties here] off begin
	// % protected region % [Add any additional class properties here] end

	/**
	 * Add a new question option
	 */
	onAddOptionClicked() {
		// % protected region % [Add any additional logic before main body of onAddOptionClicked here] off begin
		// % protected region % [Add any additional logic before main body of onAddOptionClicked here] end

		const newOption = {
			id: uuid.v4(),
			name: '',
		};
		this.data.options.options.push(newOption);

		// % protected region % [Add any additional logic after main body of onAddOptionClicked here] off begin
		// % protected region % [Add any additional logic after main body of onAddOptionClicked here] end
	}

	/**
	 * When the input question data changed, parse QuestionData
	 */
	parseData() {
		super.parseData();

		// % protected region % [Add any additional logic before main process of parseData here] off begin
		// % protected region % [Add any additional logic before main process of parseData here] end

		if (!this.data.options.options ) {
			this.data.options.options = [];
		}
		this.radioButtonOptionsOptions = this.data.options as RadioButtonsQuestionOptions;

		// % protected region % [Add any additional logic after main process of parseData here] off begin
		// % protected region % [Add any additional logic after main process of parseData here] end
	}

	/**
	 * When delete button clicked, delete the radio button option
	 */
	onDeleteButtonClicked(radioButtonOption: RadioButtonsOption) {
		// % protected region % [Add any additional logic before main process of onDeleteButtonClicked here] off begin
		// % protected region % [Add any additional logic before main process of onDeleteButtonClicked here] end

		_.remove(this.radioButtonOptionsOptions.options, (option) => option.id === radioButtonOption.id);

		// % protected region % [Add any additional logic after main process of onDeleteButtonClicked here] off begin
		// % protected region % [Add any additional logic after main process of onDeleteButtonClicked here] end
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
