/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {CommonComponentModule} from '../../../components/common.component.module';
import {CommonPipeModule} from '../../../pipes/common.pipe.module';
import {FormPreCustomQuestionComponent} from './form-custom-question/form-pre-custom-question.component';
import {FormCustomQuestionComponent} from './form-custom-question/form-custom-question.component';
import {FormCustomQuestionConfigComponent} from './form-custom-question/config/form-custom-question-config.component';
import {FormStatementComponent} from './form-statement/form-statement.component';
import {FormStatementConfigComponent} from './form-statement/config/form-statement-config.component';
import {FormTextfieldQuestionComponent} from './form-textfield-question/form-textfield-question.component';
import {FormTextfieldQuestionConfigComponent} from './form-textfield-question/config/form-textfield-question-config.component';
import {FormCheckboxQuestionComponent} from './form-checkbox-question/form-checkbox-question.component';
import {FormCheckboxQuestionConfigComponent} from './form-checkbox-question/config/form-checkbox-question-config.component';
import {FormRadioButtonsQuestionComponent} from './form-radio-buttons-question/form-radio-buttons-question.component';
import {FormRadioButtonsQuestionConfigComponent} from './form-radio-buttons-question/config/form-radio-buttons-question-config.component';
import {FormPostCustomQuestionComponent} from './form-custom-question/form-post-custom-question.component';
import {FormDateQuestionConfigComponent} from './form-date-question/config/form-date-question-config.component';
import {FormDateQuestionComponent} from './form-date-question/form-date-question.component';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

@NgModule({
	declarations: [
		FormCustomQuestionComponent,
		FormCustomQuestionConfigComponent,
		FormPreCustomQuestionComponent,
		FormPostCustomQuestionComponent,
		FormStatementComponent,
		FormStatementConfigComponent,
		FormTextfieldQuestionComponent,
		FormTextfieldQuestionConfigComponent,
		FormCheckboxQuestionComponent,
		FormCheckboxQuestionConfigComponent,
		FormRadioButtonsQuestionComponent,
		FormRadioButtonsQuestionConfigComponent,
		FormDateQuestionComponent,
		FormDateQuestionConfigComponent,
		// % protected region % [Add any additional declaration here] off begin
		// % protected region % [Add any additional declaration here] end
	],
	imports: [
		CommonModule,
		CommonComponentModule,
		FormsModule,
		ReactiveFormsModule,
		CommonPipeModule,
		// % protected region % [Add any additional module imports here] off begin
		// % protected region % [Add any additional module imports here] end
	],
	exports: [
		FormCustomQuestionComponent,
		FormCustomQuestionConfigComponent,
		FormPreCustomQuestionComponent,
		FormPostCustomQuestionComponent,
		FormStatementComponent,
		FormStatementConfigComponent,
		FormTextfieldQuestionComponent,
		FormTextfieldQuestionConfigComponent,
		FormCheckboxQuestionComponent,
		FormCheckboxQuestionConfigComponent,
		FormRadioButtonsQuestionComponent,
		FormRadioButtonsQuestionConfigComponent,
		FormDateQuestionComponent,
		FormDateQuestionConfigComponent,
		// % protected region % [Add any additional exports here] off begin
		// % protected region % [Add any additional exports here] end
	],
	providers: [
		// % protected region % [Add any additional providers here] off begin
		// % protected region % [Add any additional providers here] end
	],
	entryComponents: [
		FormCustomQuestionComponent,
		FormCustomQuestionConfigComponent,
		FormStatementComponent,
		FormStatementConfigComponent,
		FormTextfieldQuestionComponent,
		FormTextfieldQuestionConfigComponent,
		FormCheckboxQuestionComponent,
		FormCheckboxQuestionConfigComponent,
		FormRadioButtonsQuestionComponent,
		FormRadioButtonsQuestionConfigComponent,
		FormDateQuestionComponent,
		FormDateQuestionConfigComponent,
		// % protected region % [Add any additional entry components here] off begin
		// % protected region % [Add any additional entry components here] end
	],
	// % protected region % [Add any additional module configurations here] off begin
	// % protected region % [Add any additional module configurations here] end
})
export class FormBehaviourQuestionsModule {
	// % protected region % [Add any module class logic here] off begin
	// % protected region % [Add any module class logic here] end
}
