/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {CommonComponentModule} from '../../components/common.component.module';
import {CommonPipeModule} from '../../pipes/common.pipe.module';
import {FormBehaviourQuestionsModule} from './form-questions/form-behaviour-questions.module';
import {FormSlideComponent, FormSlideDirective} from './form-slide/form-slide.component';
import {FormBuilderComponent} from './form-builder/form-builder.component';
import {DragDropModule} from '@angular/cdk/drag-drop';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

@NgModule({
	declarations: [
		FormSlideComponent,
		FormSlideDirective,
		FormBuilderComponent
		// % protected region % [Add any additional declaration here] off begin
		// % protected region % [Add any additional declaration here] end
	],
	imports: [
		CommonModule,
		CommonComponentModule,
		FormsModule,
		ReactiveFormsModule,
		CommonPipeModule,
		FormBehaviourQuestionsModule,
		DragDropModule,
		// % protected region % [Add any additional module imports here] off begin
		// % protected region % [Add any additional module imports here] end
	],
	exports: [
		FormSlideComponent,
		FormSlideDirective,
		FormBuilderComponent,
		// % protected region % [Add any additional exports here] off begin
		// % protected region % [Add any additional exports here] end
	],
	providers: [
		// % protected region % [Add any additional providers here] off begin
		// % protected region % [Add any additional providers here] end
	],
	entryComponents: [
		// % protected region % [Add any additional entry components here] off begin
		// % protected region % [Add any additional entry components here] end
	],
	// % protected region % [Add any additional module configurations here] off begin
	// % protected region % [Add any additional module configurations here] end
})
export class FormBehaviourModule {
	// % protected region % [Add any module class logic here] off begin
	// % protected region % [Add any module class logic here] end
}
