/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

// All interfaces for Form Behaviour

/**
 * Which mode to display questions:
 *
 * 1. report: when questions are displayed in a report. Enabling this means that everything is non-editable.
 * 2. response: when questions are used during a submission. Enabling this means that answer box is editable.
 * 3. build: when question are used during form building. Enabling this means that the answer box is non-editable.
 */
export type FormMode = 'build' | 'response' | 'report';

/**
 * Normal data map that will be fed into slide components when they are loaded at run-time.
 */
export interface FormSlideData {
	/**
	 * Id of slide
	 */
	id: string;
	/**
	 * Order of slide
	 */
	order: number;
	/**
	 * Payload with concrete data for the slide.
	 */
	data: any;
	/**
	 * Question data to be fed into contained questions within this slide.
	 */
	questionsData: FormQuestionData[];
	[s: string]: any;
}

/**
 * Normal data map that will be fed into question components when they are loaded at run-time. Include some common shared
 * properties between multiple components.
 */
export interface FormQuestionData {
	/**
	 * Type of question
	 */
	type: string;
	/**
	 * Id of question in the form
	 */
	id: string;
	/**
	 * Current order number of the question inside the containing slide.
	 */
	questionNumber: number;
	/**
	 * What the question is.
	 */
	questionContent: string;
	/**
	 * Subtext to supplement the question content.
	 */
	questionSubtext: string;
	/**
	 * The answer given by a user.
	 */
	answer?: any;
	/**
	 * What options can be applied to the accompanying question. Note that each subclass question components may have its
	 * own options interface to accommodate with different options.
	 */
	options: any;

	/**
	 * Other fields of question
	 */
	[s: string]: any;
}
