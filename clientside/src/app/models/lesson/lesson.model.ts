/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {FormGroup, Validators} from '@angular/forms';
import {Group, AbstractModel, ModelProperty, ModelPropertyType, ModelRelation, ModelRelationType} from '../../lib/models/abstract.model';
import {DifficultyEnum, difficultyEnumArray} from '../../enums/difficulty.enum';
import {CourseLessonModel} from '../courseLesson/course_lesson.model';
import {LessonFormTileModel} from '../lessonFormTile/lesson_form_tile.model';
import {LessonFormVersionModel} from '../lessonFormVersion/lesson_form_version.model';
import * as _ from 'lodash';
import {QueryOperation, Where} from '../../lib/services/http/interfaces';
import {ElementType} from '../../lib/components/abstract.input.component';
import {FileModel} from '../../lib/models/file.model';
import { CustomValidators } from 'src/app/lib/utils/validators/custom-validators';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * POJO model class used to store information related to the entity.
 */
export class LessonModel extends AbstractModel {
	/**
	 * The fields which are set as searchable in the entity model
	 * The fields could be used in search in the server side
	 * The fields would be by default used as search in the crud tile.
	 * You could also use this in other tiles for searching.
	 */
	static searchFields: string[] = [
		'summary',
		'description',
		'duration',
		'difficulty',
		// % protected region % [Add any additional searchable field names here] off begin
		// % protected region % [Add any additional searchable field names here] end
	];

	/**
	 * Attributes to be shown in value to display
	 */
	static displayAttributes: string[] = [
		// % protected region % [Change displayAttributes here if needed] off begin
		'summary',
		// % protected region % [Change displayAttributes here if needed] end
	];

	static modelPropGroups: { [s: string]: Group } = {
		// % protected region % [Add groups for the entity here] off begin
		// % protected region % [Add groups for the entity here] end
	};

	readonly className = 'LessonModel';

	/**
	 * Default value to be displayed in dropdown etc
	 */
	get valueToDisplay(): string {
		// % protected region % [Change displayName here if needed] off begin
		return LessonModel.displayAttributes.map((attr) => this[attr]).join(' ');
		// % protected region % [Change displayName here if needed] end
	}

	/**
	 * The summary of the lesson.
	 */
	summary: string;

	/**
	 * The description of the lesson detailing what it is about..
	 */
	description: string;

	/**
	 * A lesson has a cover image..
	 */
	coverImage: FileModel[];

	/**
	 * A lesson has a duration in minutes..
	 */
	duration: number;

	/**
	 * Name of the form.
	 */
	name: string;

	/**
	 * {docoDescription=TODO: Get doco description, springFoxDataTypeProperty=, position=6, example=Sally}.
	 */
	difficulty: DifficultyEnum;

	publishedVersionId: string;

	courseLessonsIds: string[] = [];

	versionsIds: string[] = [];

	formTilesIds: string[] = [];

	modelPropGroups: { [s: string]: Group } = LessonModel.modelPropGroups;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	static getProps(): ModelProperty[] {
		return super.getProps().concat([
			{
				name: 'summary',
				// % protected region % [Set displayName for Summary here] off begin
				displayName: 'Summary',
				// % protected region % [Set displayName for Summary here] end
				type: ModelPropertyType.STRING,
				// % protected region % [Set display element type for Summary here] off begin
				elementType: ElementType.INPUT,
				// % protected region % [Set display element type for Summary here] end
				// % protected region % [Set isSensitive for Summary here] off begin
				isSensitive: false,
				// % protected region % [Set isSensitive for Summary here] end
				// % protected region % [Set readonly for Summary here] off begin
				readOnly: false,
				// % protected region % [Set readonly for Summary here] end
				validators: [
					Validators.required,
					// % protected region % [Add other validators for Summary here] off begin
					// % protected region % [Add other validators for Summary here] end
				],
				// % protected region % [Add any additional model attribute properties for Summary here] off begin
				// % protected region % [Add any additional model attribute properties for Summary here] end
			},
			{
				name: 'description',
				// % protected region % [Set displayName for Description here] off begin
				displayName: 'Description',
				// % protected region % [Set displayName for Description here] end
				type: ModelPropertyType.STRING,
				// % protected region % [Set display element type for Description here] off begin
				elementType: ElementType.TEXTAREA,
				// % protected region % [Set display element type for Description here] end
				// % protected region % [Set isSensitive for Description here] off begin
				isSensitive: false,
				// % protected region % [Set isSensitive for Description here] end
				// % protected region % [Set readonly for Description here] off begin
				readOnly: false,
				// % protected region % [Set readonly for Description here] end
				validators: [
					// % protected region % [Add other validators for Description here] off begin
					// % protected region % [Add other validators for Description here] end
				],
				// % protected region % [Add any additional model attribute properties for Description here] off begin
				// % protected region % [Add any additional model attribute properties for Description here] end
			},
			{
				name: 'coverImage',
				// % protected region % [Set displayName for Cover Image here] off begin
				displayName: 'Cover Image',
				// % protected region % [Set displayName for Cover Image here] end
				type: ModelPropertyType.FILE,
				// % protected region % [Set display element type for Cover Image here] off begin
				elementType: ElementType.FILE,
				// % protected region % [Set display element type for Cover Image here] end
				// % protected region % [Set isSensitive for Cover Image here] off begin
				isSensitive: false,
				// % protected region % [Set isSensitive for Cover Image here] end
				// % protected region % [Set readonly for Cover Image here] off begin
				readOnly: false,
				// % protected region % [Set readonly for Cover Image here] end
				validators: [
					// % protected region % [Add other validators for Cover Image here] off begin
					// % protected region % [Add other validators for Cover Image here] end
				],
				// % protected region % [Add any additional model attribute properties for Cover Image here] off begin
				// % protected region % [Add any additional model attribute properties for Cover Image here] end
			},
			{
				name: 'duration',
				// % protected region % [Set displayName for Duration here] off begin
				displayName: 'Duration',
				// % protected region % [Set displayName for Duration here] end
				type: ModelPropertyType.NUMBER,
				// % protected region % [Set display element type for Duration here] off begin
				elementType: ElementType.NUMBER,
				// % protected region % [Set display element type for Duration here] end
				// % protected region % [Set isSensitive for Duration here] off begin
				isSensitive: false,
				// % protected region % [Set isSensitive for Duration here] end
				// % protected region % [Set readonly for Duration here] off begin
				readOnly: false,
				// % protected region % [Set readonly for Duration here] end
				validators: [
					// % protected region % [Add other validators for Duration here] off begin
					// % protected region % [Add other validators for Duration here] end
				],
				// % protected region % [Add any additional model attribute properties for Duration here] off begin
				// % protected region % [Add any additional model attribute properties for Duration here] end
			},
			{
				name: 'difficulty',
				// % protected region % [Set displayName for Difficulty here] off begin
				displayName: 'Difficulty',
				// % protected region % [Set displayName for Difficulty here] end
				type: ModelPropertyType.ENUM,
				enumLiterals: difficultyEnumArray,
				// % protected region % [Set display element type for Difficulty here] off begin
				elementType: ElementType.ENUM,
				// % protected region % [Set display element type for Difficulty here] end
				// % protected region % [Set isSensitive for Difficulty here] off begin
				isSensitive: false,
				// % protected region % [Set isSensitive for Difficulty here] end
				// % protected region % [Set readonly for Difficulty here] off begin
				readOnly: false,
				// % protected region % [Set readonly for Difficulty here] end
				validators: [
					// % protected region % [Add other validators for Difficulty here] off begin
					// % protected region % [Add other validators for Difficulty here] end
				],
				// % protected region % [Add any additional model attribute properties for Difficulty here] off begin
				// % protected region % [Add any additional model attribute properties for Difficulty here] end
			},
			{
				name: 'name',
				// % protected region % [Set displayName for Name here] off begin
				displayName: 'Name',
				// % protected region % [Set displayName for Name here] end
				type: ModelPropertyType.STRING,
				// % protected region % [Set display element type for Name here] off begin
				elementType: ElementType.INPUT,
				// % protected region % [Set display element type for Name here] end
				// % protected region % [Set isSensitive for Name here] off begin
				isSensitive: false,
				// % protected region % [Set isSensitive for Name here] end
				// % protected region % [Set readonly for Name here] off begin
				readOnly: false,
				// % protected region % [Set readonly for Name here] end
				validators: [
					Validators.required,
					// % protected region % [Add other validators for Name here] off begin
					// % protected region % [Add other validators for Name here] end
				],
				// % protected region % [Add any additional model attribute properties for Name here] off begin
				// % protected region % [Add any additional model attribute properties for Name here] end
			},
			// % protected region % [Add any additional class field names here] off begin
			// % protected region % [Add any additional class field names here] end
		]);
	}

	/**
	 * The relations of the entity
	 */
	static getRelations(): { [name: string]: ModelRelation } {
		return {
			...super.getRelations(),
			courseLessons: {
				type: ModelRelationType.MANY,
				name: 'courseLessonsIds',
				// % protected region % [Customise your 1-1 or 1-M label for Course Lessons here] off begin
				label: 'Course Lessons',
				// % protected region % [Customise your 1-1 or 1-M label for Course Lessons here] end
				// % protected region % [Customise your display name for Course Lessons here] off begin
				displayName: 'order',
				// % protected region % [Customise your display name for Course Lessons here] end
				validators: [
					// % protected region % [Add other validators for Course Lessons here] off begin
					// % protected region % [Add other validators for Course Lessons here] end
				],
				// % protected region % [Add any additional field for relation Course Lessons here] off begin
				// % protected region % [Add any additional field for relation Course Lessons here] end
			},
			versions: {
				type: ModelRelationType.MANY,
				name: 'versionsIds',
				// % protected region % [Customise your 1-1 or 1-M label for Versions here] off begin
				label: 'Versions',
				// % protected region % [Customise your 1-1 or 1-M label for Versions here] end
				// % protected region % [Customise your display name for Versions here] off begin
				displayName: 'version',
				// % protected region % [Customise your display name for Versions here] end
				validators: [
					// % protected region % [Add other validators for Versions here] off begin
					// % protected region % [Add other validators for Versions here] end
				],
				// % protected region % [Add any additional field for relation Versions here] off begin
				// % protected region % [Add any additional field for relation Versions here] end
			},
			formTiles: {
				type: ModelRelationType.MANY,
				name: 'formTilesIds',
				// % protected region % [Customise your 1-1 or 1-M label for Form tiles here] off begin
				label: 'Form tiles',
				// % protected region % [Customise your 1-1 or 1-M label for Form tiles here] end
				// % protected region % [Customise your display name for Form tiles here] off begin
				displayName: 'formTileName',
				// % protected region % [Customise your display name for Form tiles here] end
				validators: [
					// % protected region % [Add other validators for Form tiles here] off begin
					// % protected region % [Add other validators for Form tiles here] end
				],
				// % protected region % [Add any additional field for relation Form tiles here] off begin
				// % protected region % [Add any additional field for relation Form tiles here] end
			},
			publishedVersion: {
				type: ModelRelationType.ONE,
				name: 'publishedVersionId',
				// % protected region % [Customise your 1-1 or 1-M label for Published version here] off begin
				label: 'Published version',
				// % protected region % [Customise your 1-1 or 1-M label for Published version here] end
				// % protected region % [Customise your display name for Published version here] off begin
				displayName: 'version',
				// % protected region % [Customise your display name for Published version here] end
				validators: [
					// % protected region % [Add other validators for Published version here] off begin
					// % protected region % [Add other validators for Published version here] end
				],
				// % protected region % [Add any additional field for relation Published version here] off begin
				// % protected region % [Add any additional field for relation Published version here] end
			},
		};
	}

	/**
	 * Convert the form group to the query conditions
	 */
	static convertFilterToCondition(formGroup: FormGroup): Where[][] {
		let conditions: Where[][] = [];

		// % protected region % [Overide the default convertFilterToCondition here] off begin
		Object.keys(formGroup.value).forEach((key) => {
			switch (key) {
				case 'summary':
					break;
				case 'description':
					break;
				case 'duration':
					break;
				case 'difficulty':
					conditions.push([
						{
							path: key,
							operation: QueryOperation.EQUAL,
							value: formGroup.value[key],
						}
					]);
					break;
				case 'created':
					const created = formGroup.value[key];
					// is the range of date
					if (created instanceof Array) {
						conditions.push([
							{
								path: key,
								operation: QueryOperation.GREATER_THAN_OR_EQUAL,
								value: created[0]
							}
						]);
						conditions.push([
							{
								path: key,
								operation: QueryOperation.LESS_THAN_OR_EQUAL,
								value: created[1]
							}
						]);
					}
			}
		});
		// % protected region % [Overide the default convertFilterToCondition here] end


		return conditions;
	}

	/**
	 * Convert a nested JSON object into an array of flatten objects.
	 */
	static deepParse(data: string | { [K in keyof LessonModel]?: LessonModel[K] }, currentModel?): AbstractModel[] {
		if (currentModel == null) {
			currentModel = new LessonModel(data);
		}

		let returned: AbstractModel[] = [currentModel];
		const json = typeof data === 'string' ? JSON.parse(data) : data;

		// Outgoing one to one
		if (json.publishedVersion) {
			currentModel.publishedVersionId = json.publishedVersion.id;
			returned = _.union(returned, LessonFormVersionModel.deepParse(json.publishedVersion));
		}

		// Outgoing one to many
		if (json.courseLessons) {
			currentModel.courseLessonsIds = json.courseLessons.map(model => model.id);
			returned = _.union(returned, _.flatten(json.courseLessons.map(model => CourseLessonModel.deepParse(model))));
		}
		// Outgoing one to many
		if (json.versions) {
			currentModel.versionsIds = json.versions.map(model => model.id);
			returned = _.union(returned, _.flatten(json.versions.map(model => LessonFormVersionModel.deepParse(model))));
		}
		// Outgoing one to many
		if (json.formTiles) {
			currentModel.formTilesIds = json.formTiles.map(model => model.id);
			returned = _.union(returned, _.flatten(json.formTiles.map(model => LessonFormTileModel.deepParse(model))));
		}

		// % protected region % [Customise your deep parse before return here] off begin
		// % protected region % [Customise your deep parse before return here] end

		return returned;
	}

	/**
	 * @example
	 *
	 * `let lessonModel = new LessonModel(data);`
	 *
	 * @param data The input data to be initialised as the LessonModel,
	 *    it is expected as a JSON string or as a nullable LessonModel.
	 */
	constructor(data?: string | Partial<LessonModel>) {
		super(data);

		if (data) {
			const json = typeof data === 'string'
				? JSON.parse(data) as Partial<LessonModel>
				: data;

			this.summary = json.summary;
			this.description = json.description;
			this.coverImage = json.coverImage?.map(item => new FileModel(item));
			this.duration = json.duration;
			this.difficulty = json.difficulty;
			this.name = json.name;
			this.difficulty = json.difficulty;
			this.publishedVersionId = json.publishedVersionId;
			this.courseLessonsIds = json.courseLessonsIds;
			this.versionsIds = json.versionsIds;
			this.formTilesIds = json.formTilesIds;
			// % protected region % [Add any additional logic here after set the data] off begin
			// % protected region % [Add any additional logic here after set the data] end
		}

		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}

	toJSON() {
		return {
			...super.toJSON(),
			summary: this.summary,
			description: this.description,
			coverImage: this.coverImage,
			duration: this.duration,
			difficulty: this.difficulty,
			name: this.name,
			publishedVersionId: this.publishedVersionId,
			courseLessonsIds: this.courseLessonsIds,
			versionsIds: this.versionsIds,
			formTilesIds: this.formTilesIds,
			// % protected region % [Add any additional logic here to json] off begin
			// % protected region % [Add any additional logic here to json] end
		};
	}

	getPropDisplayNames(): { [s: string]: ModelProperty } {
		const returned = {};
		LessonModel.getProps().map(prop => returned[prop.name] = prop);
		return returned;
	}

	applyUpdates(updates: any): LessonModel {
		let newModelJson = this.toJSON();

		if (updates.summary) {
			newModelJson.summary = updates.summary;
		}

		if (updates.description) {
			newModelJson.description = updates.description;
		}

		if (updates.coverImage) {
			newModelJson.coverImage = updates.coverImage;
		}

		if (updates.duration) {
			newModelJson.duration = updates.duration;
		}

		if (updates.difficulty) {
			newModelJson.difficulty = updates.difficulty;
		}

		if (updates.name) {
			newModelJson.name = updates.name;
		}

		if (updates.difficulty) {
			newModelJson.difficulty = updates.difficulty;
		}

		if (updates.publishedVersionId) {
			newModelJson.publishedVersionId = updates.publishedVersionId;
		}

		if (updates.courseLessonsIds) {
			newModelJson.courseLessonsIds = updates.courseLessonsIds;
		}

		if (updates.versionsIds) {
			newModelJson.versionsIds = updates.versionsIds;
		}

		if (updates.formTilesIds) {
			newModelJson.formTilesIds = updates.formTilesIds;
		}

		return new LessonModel(newModelJson);
	}

	/**
	 * @inheritDoc
	 */
	difference(other: AbstractModel): any {
		if (!(other instanceof LessonModel)) {
			return {};
		}

		const diff = {};

		for (const key of _.keys(this)) {
			const thisValue = this[key];
			const otherValue = other[key];

			// Handle dates differently
			if (thisValue instanceof Date) {
				let thisDate = (thisValue) ? thisValue.getTime() : null;
				let otherDate = (otherValue) ? otherValue.getTime() : null;

				if (thisDate !== otherDate) {
					diff[key] = thisValue;
				}
			} else if (['coverImage'].includes(key)) {;
				const thisFiles = JSON.stringify(thisValue);
				const otherFiles = JSON.stringify(otherValue);

				if (thisFiles !== otherFiles) {
					diff[key] = thisValue;
				}
			} else if (thisValue !== otherValue) {
				diff[key] = thisValue;
			}
		}

		return _.omit(diff, [
			'created',
			'modified',
			'publishedVersionId',
			'courseLessonsIds',
			'versionsIds',
			'formTilesIds',
			// % protected region % [Add any other fields to omit here] off begin
			// % protected region % [Add any other fields to omit here] end
		]);
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
