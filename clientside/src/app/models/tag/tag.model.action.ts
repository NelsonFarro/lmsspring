/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Action} from '@ngrx/store';
import {TagModel} from './tag.model';
import {TagModelAudit} from './tag.model.state';
import {PassableStateConfig} from '../../lib/services/http/interfaces';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * List of Tag model actions to be dispatched by NgRx.
 */
export enum TagModelActionTypes {
	CREATE_TAG = '[ENTITY] Create TagModel',
	CREATE_TAG_OK = '[ENTITY] Create TagModel successfully',
	CREATE_TAG_FAIL = '[ENTITY] Create TagModel failed',

	CREATE_ALL_TAG = '[ENTITY] Create All TagModel',
	CREATE_ALL_TAG_OK = '[ENTITY] Create All TagModel successfully',
	CREATE_ALL_TAG_FAIL = '[ENTITY] Create All TagModel failed',

	DELETE_TAG = '[ENTITY] Delete TagModel',
	DELETE_TAG_OK = '[ENTITY] Delete TagModel successfully',
	DELETE_TAG_FAIL = '[ENTITY] Delete TagModel failed',


	DELETE_TAG_EXCLUDING_IDS = '[ENTITY] Delete TagModels Excluding Ids',
	DELETE_TAG_EXCLUDING_IDS_OK = '[ENTITY] Delete TagModels Excluding Ids successfully',
	DELETE_TAG_EXCLUDING_IDS_FAIL = '[ENTITY] Delete TagModels Excluding Ids failed',

	DELETE_ALL_TAG = '[ENTITY] Delete all TagModels',
	DELETE_ALL_TAG_OK = '[ENTITY] Delete all TagModels successfully',
	DELETE_ALL_TAG_FAIL = '[ENTITY] Delete all TagModels failed',

	UPDATE_TAG = '[ENTITY] Update TagModel',
	UPDATE_TAG_OK = '[ENTITY] Update TagModel successfully',
	UPDATE_TAG_FAIL = '[ENTITY] Update TagModel failed',

	UPDATE_ALL_TAG = '[ENTITY] Update all TagModel',
	UPDATE_ALL_TAG_OK = '[ENTITY] Update all TagModel successfully',
	UPDATE_ALL_TAG_FAIL = '[ENTITY] Update all TagModel failed',

	FETCH_TAG= '[ENTITY] Fetch TagModel',
	FETCH_TAG_OK = '[ENTITY] Fetch TagModel successfully',
	FETCH_TAG_FAIL = '[ENTITY] Fetch TagModel failed',

	FETCH_TAG_AUDIT= '[ENTITY] Fetch TagModel audit',
	FETCH_TAG_AUDIT_OK = '[ENTITY] Fetch TagModel audit successfully',
	FETCH_TAG_AUDIT_FAIL = '[ENTITY] Fetch TagModel audit failed',

	FETCH_TAG_AUDITS_BY_ENTITY_ID= '[ENTITY] Fetch TagModel audits by entity id',
	FETCH_TAG_AUDITS_BY_ENTITY_ID_OK = '[ENTITY] Fetch TagModel audits by entity id successfully',
	FETCH_TAG_AUDITS_BY_ENTITY_ID_FAIL = '[ENTITY] Fetch TagModel audits by entity id failed',

	FETCH_ALL_TAG = '[ENTITY] Fetch all TagModel',
	FETCH_ALL_TAG_OK = '[ENTITY] Fetch all TagModel successfully',
	FETCH_ALL_TAG_FAIL = '[ENTITY] Fetch all TagModel failed',

	FETCH_TAG_WITH_QUERY = '[ENTITY] Fetch TagModel with query',
	FETCH_TAG_WITH_QUERY_OK = '[ENTITY] Fetch TagModel with query successfully',
	FETCH_TAG_WITH_QUERY_FAIL = '[ENTITY] Fetch TagModel with query failed',

	FETCH_LAST_TAG_WITH_QUERY = '[ENTITY] Fetch last TagModel with query',
	FETCH_LAST_TAG_WITH_QUERY_OK = '[ENTITY] Fetch last TagModel with query successfully',
	FETCH_LAST_TAG_WITH_QUERY_FAIL = '[ENTITY] Fetch last TagModel with query failed',

	EXPORT_TAG = '[ENTITY] Export TagModel',
	EXPORT_TAG_OK = '[ENTITY] Export TagModel successfully',
	EXPORT_TAG_FAIL = '[ENTITY] Export TagModel failed',

	EXPORT_ALL_TAG = '[ENTITY] Export All TagModels',
	EXPORT_ALL_TAG_OK = '[ENTITY] Export All TagModels successfully',
	EXPORT_ALL_TAG_FAIL = '[ENTITY] Export All TagModels failed',

	EXPORT_TAG_EXCLUDING_IDS = '[ENTITY] Export TagModels excluding Ids',
	EXPORT_TAG_EXCLUDING_IDS_OK = '[ENTITY] Export TagModel excluding Ids successfully',
	EXPORT_TAG_EXCLUDING_IDS_FAIL = '[ENTITY] Export TagModel excluding Ids failed',

	COUNT_TAGS = '[ENTITY] Fetch number of TagModel records',
	COUNT_TAGS_OK = '[ENTITY] Fetch number of TagModel records successfully ',
	COUNT_TAGS_FAIL = '[ENTITY] Fetch number of TagModel records failed',

	IMPORT_TAGS = '[ENTITY] Import TagModels',
	IMPORT_TAGS_OK = '[ENTITY] Import TagModels successfully',
	IMPORT_TAGS_FAIL = '[ENTITY] Import TagModels fail',


	INITIALISE_TAG_COLLECTION_STATE = '[ENTITY] Initialize the CollectionState of TagModel',
	// % protected region % [Add any additional model actions here] off begin
	// % protected region % [Add any additional model actions here] end
}

export abstract class BaseTagAction implements Action {
	readonly className: string = 'TagModel';

	abstract readonly type: string;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	protected constructor(
		// % protected region % [Add any additional constructor parameters here] off begin
		// % protected region % [Add any additional constructor parameters here] end
		public readonly afterwardActions: Action[] = []
	) {
		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}
}

export class TagAction extends BaseTagAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for TagAction here] off begin
	// % protected region % [Add any additional class fields for TagAction here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<TagModel>,
		// % protected region % [Add any additional constructor parameters for TagAction here] off begin
		// % protected region % [Add any additional constructor parameters for TagAction here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for TagAction here] off begin
			// % protected region % [Add any additional constructor arguments for TagAction here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for TagAction here] off begin
		// % protected region % [Add any additional constructor logic for TagAction here] end
	}

	// % protected region % [Add any additional class methods for TagAction here] off begin
	// % protected region % [Add any additional class methods for TagAction here] end
}

export class TagActionOK extends BaseTagAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for TagActionOK here] off begin
	// % protected region % [Add any additional class fields for TagActionOK here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<TagModel>,
		// % protected region % [Add any additional constructor parameters for TagActionOK here] off begin
		// % protected region % [Add any additional constructor parameters for TagActionOK here] end
		afterwardActions: Action[] = [],
		public audits?: TagModelAudit[],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for TagActionOK here] off begin
			// % protected region % [Add any additional constructor arguments for TagActionOK here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for TagActionOK here] off begin
		// % protected region % [Add any additional constructor logic for TagActionOK here] end
	}

	// % protected region % [Add any additional class methods for TagActionOK here] off begin
	// % protected region % [Add any additional class methods for TagActionOK here] end
}

export class TagActionFail extends BaseTagAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for TagActionFail here] off begin
	// % protected region % [Add any additional class fields for TagActionFail here] end

	public constructor(
		public typeInput: string,
		public readonly error?: any,
		public stateConfig?: PassableStateConfig<TagModel>,
		// % protected region % [Add any additional constructor parameters for TagActionFail here] off begin
		// % protected region % [Add any additional constructor parameters for TagActionFail here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for TagActionFail here] off begin
			// % protected region % [Add any additional constructor arguments for TagActionFail here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for TagActionFail here] off begin
		// % protected region % [Add any additional constructor logic for TagActionFail here] end
	}

	// % protected region % [Add any additional class methods for TagActionFail here] off begin
	// % protected region % [Add any additional class methods for TagActionFail here] end
}

export function isTagModelAction(e: any): e is BaseTagAction {
	return Object.values(TagModelActionTypes).includes(e);
}

// % protected region % [Add any additional actions here] off begin
// % protected region % [Add any additional actions here] end
