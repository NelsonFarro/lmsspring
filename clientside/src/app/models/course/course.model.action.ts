/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Action} from '@ngrx/store';
import {CourseModel} from './course.model';
import {CourseModelAudit} from './course.model.state';
import {PassableStateConfig} from '../../lib/services/http/interfaces';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * List of Course model actions to be dispatched by NgRx.
 */
export enum CourseModelActionTypes {
	CREATE_COURSE = '[ENTITY] Create CourseModel',
	CREATE_COURSE_OK = '[ENTITY] Create CourseModel successfully',
	CREATE_COURSE_FAIL = '[ENTITY] Create CourseModel failed',

	CREATE_ALL_COURSE = '[ENTITY] Create All CourseModel',
	CREATE_ALL_COURSE_OK = '[ENTITY] Create All CourseModel successfully',
	CREATE_ALL_COURSE_FAIL = '[ENTITY] Create All CourseModel failed',

	DELETE_COURSE = '[ENTITY] Delete CourseModel',
	DELETE_COURSE_OK = '[ENTITY] Delete CourseModel successfully',
	DELETE_COURSE_FAIL = '[ENTITY] Delete CourseModel failed',


	DELETE_COURSE_EXCLUDING_IDS = '[ENTITY] Delete CourseModels Excluding Ids',
	DELETE_COURSE_EXCLUDING_IDS_OK = '[ENTITY] Delete CourseModels Excluding Ids successfully',
	DELETE_COURSE_EXCLUDING_IDS_FAIL = '[ENTITY] Delete CourseModels Excluding Ids failed',

	DELETE_ALL_COURSE = '[ENTITY] Delete all CourseModels',
	DELETE_ALL_COURSE_OK = '[ENTITY] Delete all CourseModels successfully',
	DELETE_ALL_COURSE_FAIL = '[ENTITY] Delete all CourseModels failed',

	UPDATE_COURSE = '[ENTITY] Update CourseModel',
	UPDATE_COURSE_OK = '[ENTITY] Update CourseModel successfully',
	UPDATE_COURSE_FAIL = '[ENTITY] Update CourseModel failed',

	UPDATE_ALL_COURSE = '[ENTITY] Update all CourseModel',
	UPDATE_ALL_COURSE_OK = '[ENTITY] Update all CourseModel successfully',
	UPDATE_ALL_COURSE_FAIL = '[ENTITY] Update all CourseModel failed',

	FETCH_COURSE= '[ENTITY] Fetch CourseModel',
	FETCH_COURSE_OK = '[ENTITY] Fetch CourseModel successfully',
	FETCH_COURSE_FAIL = '[ENTITY] Fetch CourseModel failed',

	FETCH_COURSE_AUDIT= '[ENTITY] Fetch CourseModel audit',
	FETCH_COURSE_AUDIT_OK = '[ENTITY] Fetch CourseModel audit successfully',
	FETCH_COURSE_AUDIT_FAIL = '[ENTITY] Fetch CourseModel audit failed',

	FETCH_COURSE_AUDITS_BY_ENTITY_ID= '[ENTITY] Fetch CourseModel audits by entity id',
	FETCH_COURSE_AUDITS_BY_ENTITY_ID_OK = '[ENTITY] Fetch CourseModel audits by entity id successfully',
	FETCH_COURSE_AUDITS_BY_ENTITY_ID_FAIL = '[ENTITY] Fetch CourseModel audits by entity id failed',

	FETCH_ALL_COURSE = '[ENTITY] Fetch all CourseModel',
	FETCH_ALL_COURSE_OK = '[ENTITY] Fetch all CourseModel successfully',
	FETCH_ALL_COURSE_FAIL = '[ENTITY] Fetch all CourseModel failed',

	FETCH_COURSE_WITH_QUERY = '[ENTITY] Fetch CourseModel with query',
	FETCH_COURSE_WITH_QUERY_OK = '[ENTITY] Fetch CourseModel with query successfully',
	FETCH_COURSE_WITH_QUERY_FAIL = '[ENTITY] Fetch CourseModel with query failed',

	FETCH_LAST_COURSE_WITH_QUERY = '[ENTITY] Fetch last CourseModel with query',
	FETCH_LAST_COURSE_WITH_QUERY_OK = '[ENTITY] Fetch last CourseModel with query successfully',
	FETCH_LAST_COURSE_WITH_QUERY_FAIL = '[ENTITY] Fetch last CourseModel with query failed',

	EXPORT_COURSE = '[ENTITY] Export CourseModel',
	EXPORT_COURSE_OK = '[ENTITY] Export CourseModel successfully',
	EXPORT_COURSE_FAIL = '[ENTITY] Export CourseModel failed',

	EXPORT_ALL_COURSE = '[ENTITY] Export All CourseModels',
	EXPORT_ALL_COURSE_OK = '[ENTITY] Export All CourseModels successfully',
	EXPORT_ALL_COURSE_FAIL = '[ENTITY] Export All CourseModels failed',

	EXPORT_COURSE_EXCLUDING_IDS = '[ENTITY] Export CourseModels excluding Ids',
	EXPORT_COURSE_EXCLUDING_IDS_OK = '[ENTITY] Export CourseModel excluding Ids successfully',
	EXPORT_COURSE_EXCLUDING_IDS_FAIL = '[ENTITY] Export CourseModel excluding Ids failed',

	COUNT_COURSES = '[ENTITY] Fetch number of CourseModel records',
	COUNT_COURSES_OK = '[ENTITY] Fetch number of CourseModel records successfully ',
	COUNT_COURSES_FAIL = '[ENTITY] Fetch number of CourseModel records failed',

	IMPORT_COURSES = '[ENTITY] Import CourseModels',
	IMPORT_COURSES_OK = '[ENTITY] Import CourseModels successfully',
	IMPORT_COURSES_FAIL = '[ENTITY] Import CourseModels fail',


	INITIALISE_COURSE_COLLECTION_STATE = '[ENTITY] Initialize the CollectionState of CourseModel',
	// % protected region % [Add any additional model actions here] off begin
	// % protected region % [Add any additional model actions here] end
}

export abstract class BaseCourseAction implements Action {
	readonly className: string = 'CourseModel';

	abstract readonly type: string;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	protected constructor(
		// % protected region % [Add any additional constructor parameters here] off begin
		// % protected region % [Add any additional constructor parameters here] end
		public readonly afterwardActions: Action[] = []
	) {
		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}
}

export class CourseAction extends BaseCourseAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for CourseAction here] off begin
	// % protected region % [Add any additional class fields for CourseAction here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<CourseModel>,
		// % protected region % [Add any additional constructor parameters for CourseAction here] off begin
		// % protected region % [Add any additional constructor parameters for CourseAction here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for CourseAction here] off begin
			// % protected region % [Add any additional constructor arguments for CourseAction here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for CourseAction here] off begin
		// % protected region % [Add any additional constructor logic for CourseAction here] end
	}

	// % protected region % [Add any additional class methods for CourseAction here] off begin
	// % protected region % [Add any additional class methods for CourseAction here] end
}

export class CourseActionOK extends BaseCourseAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for CourseActionOK here] off begin
	// % protected region % [Add any additional class fields for CourseActionOK here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<CourseModel>,
		// % protected region % [Add any additional constructor parameters for CourseActionOK here] off begin
		// % protected region % [Add any additional constructor parameters for CourseActionOK here] end
		afterwardActions: Action[] = [],
		public audits?: CourseModelAudit[],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for CourseActionOK here] off begin
			// % protected region % [Add any additional constructor arguments for CourseActionOK here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for CourseActionOK here] off begin
		// % protected region % [Add any additional constructor logic for CourseActionOK here] end
	}

	// % protected region % [Add any additional class methods for CourseActionOK here] off begin
	// % protected region % [Add any additional class methods for CourseActionOK here] end
}

export class CourseActionFail extends BaseCourseAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for CourseActionFail here] off begin
	// % protected region % [Add any additional class fields for CourseActionFail here] end

	public constructor(
		public typeInput: string,
		public readonly error?: any,
		public stateConfig?: PassableStateConfig<CourseModel>,
		// % protected region % [Add any additional constructor parameters for CourseActionFail here] off begin
		// % protected region % [Add any additional constructor parameters for CourseActionFail here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for CourseActionFail here] off begin
			// % protected region % [Add any additional constructor arguments for CourseActionFail here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for CourseActionFail here] off begin
		// % protected region % [Add any additional constructor logic for CourseActionFail here] end
	}

	// % protected region % [Add any additional class methods for CourseActionFail here] off begin
	// % protected region % [Add any additional class methods for CourseActionFail here] end
}

export function isCourseModelAction(e: any): e is BaseCourseAction {
	return Object.values(CourseModelActionTypes).includes(e);
}

// % protected region % [Add any additional actions here] off begin
// % protected region % [Add any additional actions here] end
