/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {FormGroup, Validators} from '@angular/forms';
import {Group, AbstractModel, ModelProperty, ModelPropertyType, ModelRelation, ModelRelationType} from '../../lib/models/abstract.model';
import {DifficultyEnum, difficultyEnumArray} from '../../enums/difficulty.enum';
import {CourseCategoryModel} from '../courseCategory/course_category.model';
import {CourseLessonModel} from '../courseLesson/course_lesson.model';
import * as _ from 'lodash';
import {QueryOperation, Where} from '../../lib/services/http/interfaces';
import {ElementType} from '../../lib/components/abstract.input.component';
import {FileModel} from '../../lib/models/file.model';
import { CustomValidators } from 'src/app/lib/utils/validators/custom-validators';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * POJO model class used to store information related to the entity.
 */
export class CourseModel extends AbstractModel {
	/**
	 * The fields which are set as searchable in the entity model
	 * The fields could be used in search in the server side
	 * The fields would be by default used as search in the crud tile.
	 * You could also use this in other tiles for searching.
	 */
	static searchFields: string[] = [
		'name',
		'summary',
		'difficulty',
		// % protected region % [Add any additional searchable field names here] off begin
		// % protected region % [Add any additional searchable field names here] end
	];

	/**
	 * Attributes to be shown in value to display
	 */
	static displayAttributes: string[] = [
		// % protected region % [Change displayAttributes here if needed] off begin
		'name',
		// % protected region % [Change displayAttributes here if needed] end
	];

	static modelPropGroups: { [s: string]: Group } = {
		// % protected region % [Add groups for the entity here] off begin
		// % protected region % [Add groups for the entity here] end
	};

	readonly className = 'CourseModel';

	/**
	 * Default value to be displayed in dropdown etc
	 */
	get valueToDisplay(): string {
		// % protected region % [Change displayName here if needed] off begin
		return CourseModel.displayAttributes.map((attr) => this[attr]).join(' ');
		// % protected region % [Change displayName here if needed] end
	}

	/**
	 * The name of the course..
	 */
	name: string;

	/**
	 * A brief summary of the course..
	 */
	summary: string;

	/**
	 * The cover image of the course..
	 */
	coverImage: FileModel[];

	/**
	 * {docoDescription=TODO: Get doco description, springFoxDataTypeProperty=, position=5, example=Sally}.
	 */
	difficulty: DifficultyEnum;

	courseCategoryId: string;

	courseLessonsIds: string[] = [];

	modelPropGroups: { [s: string]: Group } = CourseModel.modelPropGroups;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	static getProps(): ModelProperty[] {
		return super.getProps().concat([
			{
				name: 'name',
				// % protected region % [Set displayName for Name here] off begin
				displayName: 'Name',
				// % protected region % [Set displayName for Name here] end
				type: ModelPropertyType.STRING,
				// % protected region % [Set display element type for Name here] off begin
				elementType: ElementType.INPUT,
				// % protected region % [Set display element type for Name here] end
				// % protected region % [Set isSensitive for Name here] off begin
				isSensitive: false,
				// % protected region % [Set isSensitive for Name here] end
				// % protected region % [Set readonly for Name here] off begin
				readOnly: false,
				// % protected region % [Set readonly for Name here] end
				validators: [
					Validators.required,
					// % protected region % [Add other validators for Name here] off begin
					// % protected region % [Add other validators for Name here] end
				],
				// % protected region % [Add any additional model attribute properties for Name here] off begin
				// % protected region % [Add any additional model attribute properties for Name here] end
			},
			{
				name: 'summary',
				// % protected region % [Set displayName for Summary here] off begin
				displayName: 'Summary',
				// % protected region % [Set displayName for Summary here] end
				type: ModelPropertyType.STRING,
				// % protected region % [Set display element type for Summary here] off begin
				elementType: ElementType.INPUT,
				// % protected region % [Set display element type for Summary here] end
				// % protected region % [Set isSensitive for Summary here] off begin
				isSensitive: false,
				// % protected region % [Set isSensitive for Summary here] end
				// % protected region % [Set readonly for Summary here] off begin
				readOnly: false,
				// % protected region % [Set readonly for Summary here] end
				validators: [
					// % protected region % [Add other validators for Summary here] off begin
					// % protected region % [Add other validators for Summary here] end
				],
				// % protected region % [Add any additional model attribute properties for Summary here] off begin
				// % protected region % [Add any additional model attribute properties for Summary here] end
			},
			{
				name: 'coverImage',
				// % protected region % [Set displayName for Cover Image here] off begin
				displayName: 'Cover Image',
				// % protected region % [Set displayName for Cover Image here] end
				type: ModelPropertyType.FILE,
				// % protected region % [Set display element type for Cover Image here] off begin
				elementType: ElementType.FILE,
				// % protected region % [Set display element type for Cover Image here] end
				// % protected region % [Set isSensitive for Cover Image here] off begin
				isSensitive: false,
				// % protected region % [Set isSensitive for Cover Image here] end
				// % protected region % [Set readonly for Cover Image here] off begin
				readOnly: false,
				// % protected region % [Set readonly for Cover Image here] end
				validators: [
					// % protected region % [Add other validators for Cover Image here] off begin
					// % protected region % [Add other validators for Cover Image here] end
				],
				// % protected region % [Add any additional model attribute properties for Cover Image here] off begin
				// % protected region % [Add any additional model attribute properties for Cover Image here] end
			},
			{
				name: 'difficulty',
				// % protected region % [Set displayName for Difficulty here] off begin
				displayName: 'Difficulty',
				// % protected region % [Set displayName for Difficulty here] end
				type: ModelPropertyType.ENUM,
				enumLiterals: difficultyEnumArray,
				// % protected region % [Set display element type for Difficulty here] off begin
				elementType: ElementType.ENUM,
				// % protected region % [Set display element type for Difficulty here] end
				// % protected region % [Set isSensitive for Difficulty here] off begin
				isSensitive: false,
				// % protected region % [Set isSensitive for Difficulty here] end
				// % protected region % [Set readonly for Difficulty here] off begin
				readOnly: false,
				// % protected region % [Set readonly for Difficulty here] end
				validators: [
					// % protected region % [Add other validators for Difficulty here] off begin
					// % protected region % [Add other validators for Difficulty here] end
				],
				// % protected region % [Add any additional model attribute properties for Difficulty here] off begin
				// % protected region % [Add any additional model attribute properties for Difficulty here] end
			},
			// % protected region % [Add any additional class field names here] off begin
			// % protected region % [Add any additional class field names here] end
		]);
	}

	/**
	 * The relations of the entity
	 */
	static getRelations(): { [name: string]: ModelRelation } {
		return {
			...super.getRelations(),
			courseLessons: {
				type: ModelRelationType.MANY,
				name: 'courseLessonsIds',
				// % protected region % [Customise your 1-1 or 1-M label for Course Lessons here] off begin
				label: 'Course Lessons',
				// % protected region % [Customise your 1-1 or 1-M label for Course Lessons here] end
				// % protected region % [Customise your display name for Course Lessons here] off begin
				displayName: 'order',
				// % protected region % [Customise your display name for Course Lessons here] end
				validators: [
					// % protected region % [Add other validators for Course Lessons here] off begin
					// % protected region % [Add other validators for Course Lessons here] end
				],
				// % protected region % [Add any additional field for relation Course Lessons here] off begin
				// % protected region % [Add any additional field for relation Course Lessons here] end
			},
			courseCategory: {
				type: ModelRelationType.ONE,
				name: 'courseCategoryId',
				// % protected region % [Customise your label for Course Category here] off begin
				label: 'Course Category',
				// % protected region % [Customise your label for Course Category here] end
				// % protected region % [Customise your display name for Course Category here] off begin
				// TODO change implementation to use OrderBy or create new metamodel property DisplayBy
				displayName: 'name',
				// % protected region % [Customise your display name for Course Category here] end
				validators: [
					// % protected region % [Add other validators for Course Category here] off begin
					// % protected region % [Add other validators for Course Category here] end
				],
				// % protected region % [Add any additional field for relation Course Category here] off begin
				// % protected region % [Add any additional field for relation Course Category here] end
			},
		};
	}

	/**
	 * Convert the form group to the query conditions
	 */
	static convertFilterToCondition(formGroup: FormGroup): Where[][] {
		let conditions: Where[][] = [];

		// % protected region % [Overide the default convertFilterToCondition here] off begin
		Object.keys(formGroup.value).forEach((key) => {
			switch (key) {
				case 'name':
					break;
				case 'summary':
					break;
				case 'difficulty':
					conditions.push([
						{
							path: key,
							operation: QueryOperation.EQUAL,
							value: formGroup.value[key],
						}
					]);
					break;
				case 'created':
					const created = formGroup.value[key];
					// is the range of date
					if (created instanceof Array) {
						conditions.push([
							{
								path: key,
								operation: QueryOperation.GREATER_THAN_OR_EQUAL,
								value: created[0]
							}
						]);
						conditions.push([
							{
								path: key,
								operation: QueryOperation.LESS_THAN_OR_EQUAL,
								value: created[1]
							}
						]);
					}
			}
		});
		// % protected region % [Overide the default convertFilterToCondition here] end


		return conditions;
	}

	/**
	 * Convert a nested JSON object into an array of flatten objects.
	 */
	static deepParse(data: string | { [K in keyof CourseModel]?: CourseModel[K] }, currentModel?): AbstractModel[] {
		if (currentModel == null) {
			currentModel = new CourseModel(data);
		}

		let returned: AbstractModel[] = [currentModel];
		const json = typeof data === 'string' ? JSON.parse(data) : data;

		// Incoming one to many
		if (json.courseCategory) {
			currentModel.courseCategoryId = json.courseCategory.id;
			returned = _.union(returned, CourseCategoryModel.deepParse(json.courseCategory));
		}

		// Outgoing one to many
		if (json.courseLessons) {
			currentModel.courseLessonsIds = json.courseLessons.map(model => model.id);
			returned = _.union(returned, _.flatten(json.courseLessons.map(model => CourseLessonModel.deepParse(model))));
		}

		// % protected region % [Customise your deep parse before return here] off begin
		// % protected region % [Customise your deep parse before return here] end

		return returned;
	}

	/**
	 * @example
	 *
	 * `let courseModel = new CourseModel(data);`
	 *
	 * @param data The input data to be initialised as the CourseModel,
	 *    it is expected as a JSON string or as a nullable CourseModel.
	 */
	constructor(data?: string | Partial<CourseModel>) {
		super(data);

		if (data) {
			const json = typeof data === 'string'
				? JSON.parse(data) as Partial<CourseModel>
				: data;

			this.name = json.name;
			this.summary = json.summary;
			this.coverImage = json.coverImage?.map(item => new FileModel(item));
			this.difficulty = json.difficulty;
			this.difficulty = json.difficulty;
			this.courseCategoryId = json.courseCategoryId;
			this.courseLessonsIds = json.courseLessonsIds;
			// % protected region % [Add any additional logic here after set the data] off begin
			// % protected region % [Add any additional logic here after set the data] end
		}

		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}

	toJSON() {
		return {
			...super.toJSON(),
			name: this.name,
			summary: this.summary,
			coverImage: this.coverImage,
			difficulty: this.difficulty,
			courseCategoryId: this.courseCategoryId,
			courseLessonsIds: this.courseLessonsIds,
			// % protected region % [Add any additional logic here to json] off begin
			// % protected region % [Add any additional logic here to json] end
		};
	}

	getPropDisplayNames(): { [s: string]: ModelProperty } {
		const returned = {};
		CourseModel.getProps().map(prop => returned[prop.name] = prop);
		return returned;
	}

	applyUpdates(updates: any): CourseModel {
		let newModelJson = this.toJSON();

		if (updates.name) {
			newModelJson.name = updates.name;
		}

		if (updates.summary) {
			newModelJson.summary = updates.summary;
		}

		if (updates.coverImage) {
			newModelJson.coverImage = updates.coverImage;
		}

		if (updates.difficulty) {
			newModelJson.difficulty = updates.difficulty;
		}

		if (updates.difficulty) {
			newModelJson.difficulty = updates.difficulty;
		}

		if (updates.courseCategoryId) {
			newModelJson.courseCategoryId = updates.courseCategoryId;
		}

		if (updates.courseLessonsIds) {
			newModelJson.courseLessonsIds = updates.courseLessonsIds;
		}

		return new CourseModel(newModelJson);
	}

	/**
	 * @inheritDoc
	 */
	difference(other: AbstractModel): any {
		if (!(other instanceof CourseModel)) {
			return {};
		}

		const diff = {};

		for (const key of _.keys(this)) {
			const thisValue = this[key];
			const otherValue = other[key];

			// Handle dates differently
			if (thisValue instanceof Date) {
				let thisDate = (thisValue) ? thisValue.getTime() : null;
				let otherDate = (otherValue) ? otherValue.getTime() : null;

				if (thisDate !== otherDate) {
					diff[key] = thisValue;
				}
			} else if (['coverImage'].includes(key)) {;
				const thisFiles = JSON.stringify(thisValue);
				const otherFiles = JSON.stringify(otherValue);

				if (thisFiles !== otherFiles) {
					diff[key] = thisValue;
				}
			} else if (thisValue !== otherValue) {
				diff[key] = thisValue;
			}
		}

		return _.omit(diff, [
			'created',
			'modified',
			'courseCategoryIds',
			'courseLessonsIds',
			// % protected region % [Add any other fields to omit here] off begin
			// % protected region % [Add any other fields to omit here] end
		]);
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
