/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Action} from '@ngrx/store';
import {LessonFormSubmissionModel} from './lesson_form_submission.model';
import {LessonFormSubmissionModelAudit} from './lesson_form_submission.model.state';
import {PassableStateConfig} from '../../lib/services/http/interfaces';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * List of Lesson Form Submission model actions to be dispatched by NgRx.
 */
export enum LessonFormSubmissionModelActionTypes {
	CREATE_LESSON_FORM_SUBMISSION = '[ENTITY] Create LessonFormSubmissionModel',
	CREATE_LESSON_FORM_SUBMISSION_OK = '[ENTITY] Create LessonFormSubmissionModel successfully',
	CREATE_LESSON_FORM_SUBMISSION_FAIL = '[ENTITY] Create LessonFormSubmissionModel failed',

	CREATE_ALL_LESSON_FORM_SUBMISSION = '[ENTITY] Create All LessonFormSubmissionModel',
	CREATE_ALL_LESSON_FORM_SUBMISSION_OK = '[ENTITY] Create All LessonFormSubmissionModel successfully',
	CREATE_ALL_LESSON_FORM_SUBMISSION_FAIL = '[ENTITY] Create All LessonFormSubmissionModel failed',

	DELETE_LESSON_FORM_SUBMISSION = '[ENTITY] Delete LessonFormSubmissionModel',
	DELETE_LESSON_FORM_SUBMISSION_OK = '[ENTITY] Delete LessonFormSubmissionModel successfully',
	DELETE_LESSON_FORM_SUBMISSION_FAIL = '[ENTITY] Delete LessonFormSubmissionModel failed',


	DELETE_LESSON_FORM_SUBMISSION_EXCLUDING_IDS = '[ENTITY] Delete LessonFormSubmissionModels Excluding Ids',
	DELETE_LESSON_FORM_SUBMISSION_EXCLUDING_IDS_OK = '[ENTITY] Delete LessonFormSubmissionModels Excluding Ids successfully',
	DELETE_LESSON_FORM_SUBMISSION_EXCLUDING_IDS_FAIL = '[ENTITY] Delete LessonFormSubmissionModels Excluding Ids failed',

	DELETE_ALL_LESSON_FORM_SUBMISSION = '[ENTITY] Delete all LessonFormSubmissionModels',
	DELETE_ALL_LESSON_FORM_SUBMISSION_OK = '[ENTITY] Delete all LessonFormSubmissionModels successfully',
	DELETE_ALL_LESSON_FORM_SUBMISSION_FAIL = '[ENTITY] Delete all LessonFormSubmissionModels failed',

	UPDATE_LESSON_FORM_SUBMISSION = '[ENTITY] Update LessonFormSubmissionModel',
	UPDATE_LESSON_FORM_SUBMISSION_OK = '[ENTITY] Update LessonFormSubmissionModel successfully',
	UPDATE_LESSON_FORM_SUBMISSION_FAIL = '[ENTITY] Update LessonFormSubmissionModel failed',

	UPDATE_ALL_LESSON_FORM_SUBMISSION = '[ENTITY] Update all LessonFormSubmissionModel',
	UPDATE_ALL_LESSON_FORM_SUBMISSION_OK = '[ENTITY] Update all LessonFormSubmissionModel successfully',
	UPDATE_ALL_LESSON_FORM_SUBMISSION_FAIL = '[ENTITY] Update all LessonFormSubmissionModel failed',

	FETCH_LESSON_FORM_SUBMISSION= '[ENTITY] Fetch LessonFormSubmissionModel',
	FETCH_LESSON_FORM_SUBMISSION_OK = '[ENTITY] Fetch LessonFormSubmissionModel successfully',
	FETCH_LESSON_FORM_SUBMISSION_FAIL = '[ENTITY] Fetch LessonFormSubmissionModel failed',

	FETCH_LESSON_FORM_SUBMISSION_AUDIT= '[ENTITY] Fetch LessonFormSubmissionModel audit',
	FETCH_LESSON_FORM_SUBMISSION_AUDIT_OK = '[ENTITY] Fetch LessonFormSubmissionModel audit successfully',
	FETCH_LESSON_FORM_SUBMISSION_AUDIT_FAIL = '[ENTITY] Fetch LessonFormSubmissionModel audit failed',

	FETCH_LESSON_FORM_SUBMISSION_AUDITS_BY_ENTITY_ID= '[ENTITY] Fetch LessonFormSubmissionModel audits by entity id',
	FETCH_LESSON_FORM_SUBMISSION_AUDITS_BY_ENTITY_ID_OK = '[ENTITY] Fetch LessonFormSubmissionModel audits by entity id successfully',
	FETCH_LESSON_FORM_SUBMISSION_AUDITS_BY_ENTITY_ID_FAIL = '[ENTITY] Fetch LessonFormSubmissionModel audits by entity id failed',

	FETCH_ALL_LESSON_FORM_SUBMISSION = '[ENTITY] Fetch all LessonFormSubmissionModel',
	FETCH_ALL_LESSON_FORM_SUBMISSION_OK = '[ENTITY] Fetch all LessonFormSubmissionModel successfully',
	FETCH_ALL_LESSON_FORM_SUBMISSION_FAIL = '[ENTITY] Fetch all LessonFormSubmissionModel failed',

	FETCH_LESSON_FORM_SUBMISSION_WITH_QUERY = '[ENTITY] Fetch LessonFormSubmissionModel with query',
	FETCH_LESSON_FORM_SUBMISSION_WITH_QUERY_OK = '[ENTITY] Fetch LessonFormSubmissionModel with query successfully',
	FETCH_LESSON_FORM_SUBMISSION_WITH_QUERY_FAIL = '[ENTITY] Fetch LessonFormSubmissionModel with query failed',

	FETCH_LAST_LESSON_FORM_SUBMISSION_WITH_QUERY = '[ENTITY] Fetch last LessonFormSubmissionModel with query',
	FETCH_LAST_LESSON_FORM_SUBMISSION_WITH_QUERY_OK = '[ENTITY] Fetch last LessonFormSubmissionModel with query successfully',
	FETCH_LAST_LESSON_FORM_SUBMISSION_WITH_QUERY_FAIL = '[ENTITY] Fetch last LessonFormSubmissionModel with query failed',

	EXPORT_LESSON_FORM_SUBMISSION = '[ENTITY] Export LessonFormSubmissionModel',
	EXPORT_LESSON_FORM_SUBMISSION_OK = '[ENTITY] Export LessonFormSubmissionModel successfully',
	EXPORT_LESSON_FORM_SUBMISSION_FAIL = '[ENTITY] Export LessonFormSubmissionModel failed',

	EXPORT_ALL_LESSON_FORM_SUBMISSION = '[ENTITY] Export All LessonFormSubmissionModels',
	EXPORT_ALL_LESSON_FORM_SUBMISSION_OK = '[ENTITY] Export All LessonFormSubmissionModels successfully',
	EXPORT_ALL_LESSON_FORM_SUBMISSION_FAIL = '[ENTITY] Export All LessonFormSubmissionModels failed',

	EXPORT_LESSON_FORM_SUBMISSION_EXCLUDING_IDS = '[ENTITY] Export LessonFormSubmissionModels excluding Ids',
	EXPORT_LESSON_FORM_SUBMISSION_EXCLUDING_IDS_OK = '[ENTITY] Export LessonFormSubmissionModel excluding Ids successfully',
	EXPORT_LESSON_FORM_SUBMISSION_EXCLUDING_IDS_FAIL = '[ENTITY] Export LessonFormSubmissionModel excluding Ids failed',

	COUNT_LESSON_FORM_SUBMISSIONS = '[ENTITY] Fetch number of LessonFormSubmissionModel records',
	COUNT_LESSON_FORM_SUBMISSIONS_OK = '[ENTITY] Fetch number of LessonFormSubmissionModel records successfully ',
	COUNT_LESSON_FORM_SUBMISSIONS_FAIL = '[ENTITY] Fetch number of LessonFormSubmissionModel records failed',

	IMPORT_LESSON_FORM_SUBMISSIONS = '[ENTITY] Import LessonFormSubmissionModels',
	IMPORT_LESSON_FORM_SUBMISSIONS_OK = '[ENTITY] Import LessonFormSubmissionModels successfully',
	IMPORT_LESSON_FORM_SUBMISSIONS_FAIL = '[ENTITY] Import LessonFormSubmissionModels fail',


	INITIALISE_LESSON_FORM_SUBMISSION_COLLECTION_STATE = '[ENTITY] Initialize the CollectionState of LessonFormSubmissionModel',
	// % protected region % [Add any additional model actions here] off begin
	// % protected region % [Add any additional model actions here] end
}

export abstract class BaseLessonFormSubmissionAction implements Action {
	readonly className: string = 'LessonFormSubmissionModel';

	abstract readonly type: string;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	protected constructor(
		// % protected region % [Add any additional constructor parameters here] off begin
		// % protected region % [Add any additional constructor parameters here] end
		public readonly afterwardActions: Action[] = []
	) {
		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}
}

export class LessonFormSubmissionAction extends BaseLessonFormSubmissionAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for LessonFormSubmissionAction here] off begin
	// % protected region % [Add any additional class fields for LessonFormSubmissionAction here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<LessonFormSubmissionModel>,
		// % protected region % [Add any additional constructor parameters for LessonFormSubmissionAction here] off begin
		// % protected region % [Add any additional constructor parameters for LessonFormSubmissionAction here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for LessonFormSubmissionAction here] off begin
			// % protected region % [Add any additional constructor arguments for LessonFormSubmissionAction here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for LessonFormSubmissionAction here] off begin
		// % protected region % [Add any additional constructor logic for LessonFormSubmissionAction here] end
	}

	// % protected region % [Add any additional class methods for LessonFormSubmissionAction here] off begin
	// % protected region % [Add any additional class methods for LessonFormSubmissionAction here] end
}

export class LessonFormSubmissionActionOK extends BaseLessonFormSubmissionAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for LessonFormSubmissionActionOK here] off begin
	// % protected region % [Add any additional class fields for LessonFormSubmissionActionOK here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<LessonFormSubmissionModel>,
		// % protected region % [Add any additional constructor parameters for LessonFormSubmissionActionOK here] off begin
		// % protected region % [Add any additional constructor parameters for LessonFormSubmissionActionOK here] end
		afterwardActions: Action[] = [],
		public audits?: LessonFormSubmissionModelAudit[],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for LessonFormSubmissionActionOK here] off begin
			// % protected region % [Add any additional constructor arguments for LessonFormSubmissionActionOK here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for LessonFormSubmissionActionOK here] off begin
		// % protected region % [Add any additional constructor logic for LessonFormSubmissionActionOK here] end
	}

	// % protected region % [Add any additional class methods for LessonFormSubmissionActionOK here] off begin
	// % protected region % [Add any additional class methods for LessonFormSubmissionActionOK here] end
}

export class LessonFormSubmissionActionFail extends BaseLessonFormSubmissionAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for LessonFormSubmissionActionFail here] off begin
	// % protected region % [Add any additional class fields for LessonFormSubmissionActionFail here] end

	public constructor(
		public typeInput: string,
		public readonly error?: any,
		public stateConfig?: PassableStateConfig<LessonFormSubmissionModel>,
		// % protected region % [Add any additional constructor parameters for LessonFormSubmissionActionFail here] off begin
		// % protected region % [Add any additional constructor parameters for LessonFormSubmissionActionFail here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for LessonFormSubmissionActionFail here] off begin
			// % protected region % [Add any additional constructor arguments for LessonFormSubmissionActionFail here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for LessonFormSubmissionActionFail here] off begin
		// % protected region % [Add any additional constructor logic for LessonFormSubmissionActionFail here] end
	}

	// % protected region % [Add any additional class methods for LessonFormSubmissionActionFail here] off begin
	// % protected region % [Add any additional class methods for LessonFormSubmissionActionFail here] end
}

export function isLessonFormSubmissionModelAction(e: any): e is BaseLessonFormSubmissionAction {
	return Object.values(LessonFormSubmissionModelActionTypes).includes(e);
}

// % protected region % [Add any additional actions here] off begin
// % protected region % [Add any additional actions here] end
