/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Action} from '@ngrx/store';
import {WorkflowModel} from './workflow.model';
import {WorkflowModelAudit} from './workflow.model.state';
import {PassableStateConfig} from '../../lib/services/http/interfaces';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * List of Workflow model actions to be dispatched by NgRx.
 */
export enum WorkflowModelActionTypes {
	CREATE_WORKFLOW = '[ENTITY] Create WorkflowModel',
	CREATE_WORKFLOW_OK = '[ENTITY] Create WorkflowModel successfully',
	CREATE_WORKFLOW_FAIL = '[ENTITY] Create WorkflowModel failed',

	CREATE_ALL_WORKFLOW = '[ENTITY] Create All WorkflowModel',
	CREATE_ALL_WORKFLOW_OK = '[ENTITY] Create All WorkflowModel successfully',
	CREATE_ALL_WORKFLOW_FAIL = '[ENTITY] Create All WorkflowModel failed',

	DELETE_WORKFLOW = '[ENTITY] Delete WorkflowModel',
	DELETE_WORKFLOW_OK = '[ENTITY] Delete WorkflowModel successfully',
	DELETE_WORKFLOW_FAIL = '[ENTITY] Delete WorkflowModel failed',


	DELETE_WORKFLOW_EXCLUDING_IDS = '[ENTITY] Delete WorkflowModels Excluding Ids',
	DELETE_WORKFLOW_EXCLUDING_IDS_OK = '[ENTITY] Delete WorkflowModels Excluding Ids successfully',
	DELETE_WORKFLOW_EXCLUDING_IDS_FAIL = '[ENTITY] Delete WorkflowModels Excluding Ids failed',

	DELETE_ALL_WORKFLOW = '[ENTITY] Delete all WorkflowModels',
	DELETE_ALL_WORKFLOW_OK = '[ENTITY] Delete all WorkflowModels successfully',
	DELETE_ALL_WORKFLOW_FAIL = '[ENTITY] Delete all WorkflowModels failed',

	UPDATE_WORKFLOW = '[ENTITY] Update WorkflowModel',
	UPDATE_WORKFLOW_OK = '[ENTITY] Update WorkflowModel successfully',
	UPDATE_WORKFLOW_FAIL = '[ENTITY] Update WorkflowModel failed',

	UPDATE_ALL_WORKFLOW = '[ENTITY] Update all WorkflowModel',
	UPDATE_ALL_WORKFLOW_OK = '[ENTITY] Update all WorkflowModel successfully',
	UPDATE_ALL_WORKFLOW_FAIL = '[ENTITY] Update all WorkflowModel failed',

	FETCH_WORKFLOW= '[ENTITY] Fetch WorkflowModel',
	FETCH_WORKFLOW_OK = '[ENTITY] Fetch WorkflowModel successfully',
	FETCH_WORKFLOW_FAIL = '[ENTITY] Fetch WorkflowModel failed',

	FETCH_WORKFLOW_AUDIT= '[ENTITY] Fetch WorkflowModel audit',
	FETCH_WORKFLOW_AUDIT_OK = '[ENTITY] Fetch WorkflowModel audit successfully',
	FETCH_WORKFLOW_AUDIT_FAIL = '[ENTITY] Fetch WorkflowModel audit failed',

	FETCH_WORKFLOW_AUDITS_BY_ENTITY_ID= '[ENTITY] Fetch WorkflowModel audits by entity id',
	FETCH_WORKFLOW_AUDITS_BY_ENTITY_ID_OK = '[ENTITY] Fetch WorkflowModel audits by entity id successfully',
	FETCH_WORKFLOW_AUDITS_BY_ENTITY_ID_FAIL = '[ENTITY] Fetch WorkflowModel audits by entity id failed',

	FETCH_ALL_WORKFLOW = '[ENTITY] Fetch all WorkflowModel',
	FETCH_ALL_WORKFLOW_OK = '[ENTITY] Fetch all WorkflowModel successfully',
	FETCH_ALL_WORKFLOW_FAIL = '[ENTITY] Fetch all WorkflowModel failed',

	FETCH_WORKFLOW_WITH_QUERY = '[ENTITY] Fetch WorkflowModel with query',
	FETCH_WORKFLOW_WITH_QUERY_OK = '[ENTITY] Fetch WorkflowModel with query successfully',
	FETCH_WORKFLOW_WITH_QUERY_FAIL = '[ENTITY] Fetch WorkflowModel with query failed',

	FETCH_LAST_WORKFLOW_WITH_QUERY = '[ENTITY] Fetch last WorkflowModel with query',
	FETCH_LAST_WORKFLOW_WITH_QUERY_OK = '[ENTITY] Fetch last WorkflowModel with query successfully',
	FETCH_LAST_WORKFLOW_WITH_QUERY_FAIL = '[ENTITY] Fetch last WorkflowModel with query failed',

	EXPORT_WORKFLOW = '[ENTITY] Export WorkflowModel',
	EXPORT_WORKFLOW_OK = '[ENTITY] Export WorkflowModel successfully',
	EXPORT_WORKFLOW_FAIL = '[ENTITY] Export WorkflowModel failed',

	EXPORT_ALL_WORKFLOW = '[ENTITY] Export All WorkflowModels',
	EXPORT_ALL_WORKFLOW_OK = '[ENTITY] Export All WorkflowModels successfully',
	EXPORT_ALL_WORKFLOW_FAIL = '[ENTITY] Export All WorkflowModels failed',

	EXPORT_WORKFLOW_EXCLUDING_IDS = '[ENTITY] Export WorkflowModels excluding Ids',
	EXPORT_WORKFLOW_EXCLUDING_IDS_OK = '[ENTITY] Export WorkflowModel excluding Ids successfully',
	EXPORT_WORKFLOW_EXCLUDING_IDS_FAIL = '[ENTITY] Export WorkflowModel excluding Ids failed',

	COUNT_WORKFLOWS = '[ENTITY] Fetch number of WorkflowModel records',
	COUNT_WORKFLOWS_OK = '[ENTITY] Fetch number of WorkflowModel records successfully ',
	COUNT_WORKFLOWS_FAIL = '[ENTITY] Fetch number of WorkflowModel records failed',

	IMPORT_WORKFLOWS = '[ENTITY] Import WorkflowModels',
	IMPORT_WORKFLOWS_OK = '[ENTITY] Import WorkflowModels successfully',
	IMPORT_WORKFLOWS_FAIL = '[ENTITY] Import WorkflowModels fail',


	INITIALISE_WORKFLOW_COLLECTION_STATE = '[ENTITY] Initialize the CollectionState of WorkflowModel',
	// % protected region % [Add any additional model actions here] off begin
	// % protected region % [Add any additional model actions here] end
}

export abstract class BaseWorkflowAction implements Action {
	readonly className: string = 'WorkflowModel';

	abstract readonly type: string;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	protected constructor(
		// % protected region % [Add any additional constructor parameters here] off begin
		// % protected region % [Add any additional constructor parameters here] end
		public readonly afterwardActions: Action[] = []
	) {
		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}
}

export class WorkflowAction extends BaseWorkflowAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for WorkflowAction here] off begin
	// % protected region % [Add any additional class fields for WorkflowAction here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<WorkflowModel>,
		// % protected region % [Add any additional constructor parameters for WorkflowAction here] off begin
		// % protected region % [Add any additional constructor parameters for WorkflowAction here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for WorkflowAction here] off begin
			// % protected region % [Add any additional constructor arguments for WorkflowAction here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for WorkflowAction here] off begin
		// % protected region % [Add any additional constructor logic for WorkflowAction here] end
	}

	// % protected region % [Add any additional class methods for WorkflowAction here] off begin
	// % protected region % [Add any additional class methods for WorkflowAction here] end
}

export class WorkflowActionOK extends BaseWorkflowAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for WorkflowActionOK here] off begin
	// % protected region % [Add any additional class fields for WorkflowActionOK here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<WorkflowModel>,
		// % protected region % [Add any additional constructor parameters for WorkflowActionOK here] off begin
		// % protected region % [Add any additional constructor parameters for WorkflowActionOK here] end
		afterwardActions: Action[] = [],
		public audits?: WorkflowModelAudit[],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for WorkflowActionOK here] off begin
			// % protected region % [Add any additional constructor arguments for WorkflowActionOK here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for WorkflowActionOK here] off begin
		// % protected region % [Add any additional constructor logic for WorkflowActionOK here] end
	}

	// % protected region % [Add any additional class methods for WorkflowActionOK here] off begin
	// % protected region % [Add any additional class methods for WorkflowActionOK here] end
}

export class WorkflowActionFail extends BaseWorkflowAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for WorkflowActionFail here] off begin
	// % protected region % [Add any additional class fields for WorkflowActionFail here] end

	public constructor(
		public typeInput: string,
		public readonly error?: any,
		public stateConfig?: PassableStateConfig<WorkflowModel>,
		// % protected region % [Add any additional constructor parameters for WorkflowActionFail here] off begin
		// % protected region % [Add any additional constructor parameters for WorkflowActionFail here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for WorkflowActionFail here] off begin
			// % protected region % [Add any additional constructor arguments for WorkflowActionFail here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for WorkflowActionFail here] off begin
		// % protected region % [Add any additional constructor logic for WorkflowActionFail here] end
	}

	// % protected region % [Add any additional class methods for WorkflowActionFail here] off begin
	// % protected region % [Add any additional class methods for WorkflowActionFail here] end
}

export function isWorkflowModelAction(e: any): e is BaseWorkflowAction {
	return Object.values(WorkflowModelActionTypes).includes(e);
}

// % protected region % [Add any additional actions here] off begin
// % protected region % [Add any additional actions here] end
