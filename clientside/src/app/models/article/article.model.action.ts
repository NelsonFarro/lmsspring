/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Action} from '@ngrx/store';
import {ArticleModel} from './article.model';
import {ArticleModelAudit} from './article.model.state';
import {PassableStateConfig} from '../../lib/services/http/interfaces';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * List of Article model actions to be dispatched by NgRx.
 */
export enum ArticleModelActionTypes {
	CREATE_ARTICLE = '[ENTITY] Create ArticleModel',
	CREATE_ARTICLE_OK = '[ENTITY] Create ArticleModel successfully',
	CREATE_ARTICLE_FAIL = '[ENTITY] Create ArticleModel failed',

	CREATE_ALL_ARTICLE = '[ENTITY] Create All ArticleModel',
	CREATE_ALL_ARTICLE_OK = '[ENTITY] Create All ArticleModel successfully',
	CREATE_ALL_ARTICLE_FAIL = '[ENTITY] Create All ArticleModel failed',

	DELETE_ARTICLE = '[ENTITY] Delete ArticleModel',
	DELETE_ARTICLE_OK = '[ENTITY] Delete ArticleModel successfully',
	DELETE_ARTICLE_FAIL = '[ENTITY] Delete ArticleModel failed',


	DELETE_ARTICLE_EXCLUDING_IDS = '[ENTITY] Delete ArticleModels Excluding Ids',
	DELETE_ARTICLE_EXCLUDING_IDS_OK = '[ENTITY] Delete ArticleModels Excluding Ids successfully',
	DELETE_ARTICLE_EXCLUDING_IDS_FAIL = '[ENTITY] Delete ArticleModels Excluding Ids failed',

	DELETE_ALL_ARTICLE = '[ENTITY] Delete all ArticleModels',
	DELETE_ALL_ARTICLE_OK = '[ENTITY] Delete all ArticleModels successfully',
	DELETE_ALL_ARTICLE_FAIL = '[ENTITY] Delete all ArticleModels failed',

	UPDATE_ARTICLE = '[ENTITY] Update ArticleModel',
	UPDATE_ARTICLE_OK = '[ENTITY] Update ArticleModel successfully',
	UPDATE_ARTICLE_FAIL = '[ENTITY] Update ArticleModel failed',

	UPDATE_ALL_ARTICLE = '[ENTITY] Update all ArticleModel',
	UPDATE_ALL_ARTICLE_OK = '[ENTITY] Update all ArticleModel successfully',
	UPDATE_ALL_ARTICLE_FAIL = '[ENTITY] Update all ArticleModel failed',

	FETCH_ARTICLE= '[ENTITY] Fetch ArticleModel',
	FETCH_ARTICLE_OK = '[ENTITY] Fetch ArticleModel successfully',
	FETCH_ARTICLE_FAIL = '[ENTITY] Fetch ArticleModel failed',

	FETCH_ARTICLE_AUDIT= '[ENTITY] Fetch ArticleModel audit',
	FETCH_ARTICLE_AUDIT_OK = '[ENTITY] Fetch ArticleModel audit successfully',
	FETCH_ARTICLE_AUDIT_FAIL = '[ENTITY] Fetch ArticleModel audit failed',

	FETCH_ARTICLE_AUDITS_BY_ENTITY_ID= '[ENTITY] Fetch ArticleModel audits by entity id',
	FETCH_ARTICLE_AUDITS_BY_ENTITY_ID_OK = '[ENTITY] Fetch ArticleModel audits by entity id successfully',
	FETCH_ARTICLE_AUDITS_BY_ENTITY_ID_FAIL = '[ENTITY] Fetch ArticleModel audits by entity id failed',

	FETCH_ALL_ARTICLE = '[ENTITY] Fetch all ArticleModel',
	FETCH_ALL_ARTICLE_OK = '[ENTITY] Fetch all ArticleModel successfully',
	FETCH_ALL_ARTICLE_FAIL = '[ENTITY] Fetch all ArticleModel failed',

	FETCH_ARTICLE_WITH_QUERY = '[ENTITY] Fetch ArticleModel with query',
	FETCH_ARTICLE_WITH_QUERY_OK = '[ENTITY] Fetch ArticleModel with query successfully',
	FETCH_ARTICLE_WITH_QUERY_FAIL = '[ENTITY] Fetch ArticleModel with query failed',

	FETCH_LAST_ARTICLE_WITH_QUERY = '[ENTITY] Fetch last ArticleModel with query',
	FETCH_LAST_ARTICLE_WITH_QUERY_OK = '[ENTITY] Fetch last ArticleModel with query successfully',
	FETCH_LAST_ARTICLE_WITH_QUERY_FAIL = '[ENTITY] Fetch last ArticleModel with query failed',

	EXPORT_ARTICLE = '[ENTITY] Export ArticleModel',
	EXPORT_ARTICLE_OK = '[ENTITY] Export ArticleModel successfully',
	EXPORT_ARTICLE_FAIL = '[ENTITY] Export ArticleModel failed',

	EXPORT_ALL_ARTICLE = '[ENTITY] Export All ArticleModels',
	EXPORT_ALL_ARTICLE_OK = '[ENTITY] Export All ArticleModels successfully',
	EXPORT_ALL_ARTICLE_FAIL = '[ENTITY] Export All ArticleModels failed',

	EXPORT_ARTICLE_EXCLUDING_IDS = '[ENTITY] Export ArticleModels excluding Ids',
	EXPORT_ARTICLE_EXCLUDING_IDS_OK = '[ENTITY] Export ArticleModel excluding Ids successfully',
	EXPORT_ARTICLE_EXCLUDING_IDS_FAIL = '[ENTITY] Export ArticleModel excluding Ids failed',

	COUNT_ARTICLES = '[ENTITY] Fetch number of ArticleModel records',
	COUNT_ARTICLES_OK = '[ENTITY] Fetch number of ArticleModel records successfully ',
	COUNT_ARTICLES_FAIL = '[ENTITY] Fetch number of ArticleModel records failed',

	IMPORT_ARTICLES = '[ENTITY] Import ArticleModels',
	IMPORT_ARTICLES_OK = '[ENTITY] Import ArticleModels successfully',
	IMPORT_ARTICLES_FAIL = '[ENTITY] Import ArticleModels fail',


	INITIALISE_ARTICLE_COLLECTION_STATE = '[ENTITY] Initialize the CollectionState of ArticleModel',
	// % protected region % [Add any additional model actions here] off begin
	// % protected region % [Add any additional model actions here] end
}

export abstract class BaseArticleAction implements Action {
	readonly className: string = 'ArticleModel';

	abstract readonly type: string;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	protected constructor(
		// % protected region % [Add any additional constructor parameters here] off begin
		// % protected region % [Add any additional constructor parameters here] end
		public readonly afterwardActions: Action[] = []
	) {
		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}
}

export class ArticleAction extends BaseArticleAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for ArticleAction here] off begin
	// % protected region % [Add any additional class fields for ArticleAction here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<ArticleModel>,
		// % protected region % [Add any additional constructor parameters for ArticleAction here] off begin
		// % protected region % [Add any additional constructor parameters for ArticleAction here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for ArticleAction here] off begin
			// % protected region % [Add any additional constructor arguments for ArticleAction here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for ArticleAction here] off begin
		// % protected region % [Add any additional constructor logic for ArticleAction here] end
	}

	// % protected region % [Add any additional class methods for ArticleAction here] off begin
	// % protected region % [Add any additional class methods for ArticleAction here] end
}

export class ArticleActionOK extends BaseArticleAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for ArticleActionOK here] off begin
	// % protected region % [Add any additional class fields for ArticleActionOK here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<ArticleModel>,
		// % protected region % [Add any additional constructor parameters for ArticleActionOK here] off begin
		// % protected region % [Add any additional constructor parameters for ArticleActionOK here] end
		afterwardActions: Action[] = [],
		public audits?: ArticleModelAudit[],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for ArticleActionOK here] off begin
			// % protected region % [Add any additional constructor arguments for ArticleActionOK here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for ArticleActionOK here] off begin
		// % protected region % [Add any additional constructor logic for ArticleActionOK here] end
	}

	// % protected region % [Add any additional class methods for ArticleActionOK here] off begin
	// % protected region % [Add any additional class methods for ArticleActionOK here] end
}

export class ArticleActionFail extends BaseArticleAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for ArticleActionFail here] off begin
	// % protected region % [Add any additional class fields for ArticleActionFail here] end

	public constructor(
		public typeInput: string,
		public readonly error?: any,
		public stateConfig?: PassableStateConfig<ArticleModel>,
		// % protected region % [Add any additional constructor parameters for ArticleActionFail here] off begin
		// % protected region % [Add any additional constructor parameters for ArticleActionFail here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for ArticleActionFail here] off begin
			// % protected region % [Add any additional constructor arguments for ArticleActionFail here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for ArticleActionFail here] off begin
		// % protected region % [Add any additional constructor logic for ArticleActionFail here] end
	}

	// % protected region % [Add any additional class methods for ArticleActionFail here] off begin
	// % protected region % [Add any additional class methods for ArticleActionFail here] end
}

export function isArticleModelAction(e: any): e is BaseArticleAction {
	return Object.values(ArticleModelActionTypes).includes(e);
}

// % protected region % [Add any additional actions here] off begin
// % protected region % [Add any additional actions here] end
