/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Action} from '@ngrx/store';
import {LessonFormTileModel} from './lesson_form_tile.model';
import {LessonFormTileModelAudit} from './lesson_form_tile.model.state';
import {PassableStateConfig} from '../../lib/services/http/interfaces';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * List of Lesson Form Tile model actions to be dispatched by NgRx.
 */
export enum LessonFormTileModelActionTypes {
	CREATE_LESSON_FORM_TILE = '[ENTITY] Create LessonFormTileModel',
	CREATE_LESSON_FORM_TILE_OK = '[ENTITY] Create LessonFormTileModel successfully',
	CREATE_LESSON_FORM_TILE_FAIL = '[ENTITY] Create LessonFormTileModel failed',

	CREATE_ALL_LESSON_FORM_TILE = '[ENTITY] Create All LessonFormTileModel',
	CREATE_ALL_LESSON_FORM_TILE_OK = '[ENTITY] Create All LessonFormTileModel successfully',
	CREATE_ALL_LESSON_FORM_TILE_FAIL = '[ENTITY] Create All LessonFormTileModel failed',

	DELETE_LESSON_FORM_TILE = '[ENTITY] Delete LessonFormTileModel',
	DELETE_LESSON_FORM_TILE_OK = '[ENTITY] Delete LessonFormTileModel successfully',
	DELETE_LESSON_FORM_TILE_FAIL = '[ENTITY] Delete LessonFormTileModel failed',


	DELETE_LESSON_FORM_TILE_EXCLUDING_IDS = '[ENTITY] Delete LessonFormTileModels Excluding Ids',
	DELETE_LESSON_FORM_TILE_EXCLUDING_IDS_OK = '[ENTITY] Delete LessonFormTileModels Excluding Ids successfully',
	DELETE_LESSON_FORM_TILE_EXCLUDING_IDS_FAIL = '[ENTITY] Delete LessonFormTileModels Excluding Ids failed',

	DELETE_ALL_LESSON_FORM_TILE = '[ENTITY] Delete all LessonFormTileModels',
	DELETE_ALL_LESSON_FORM_TILE_OK = '[ENTITY] Delete all LessonFormTileModels successfully',
	DELETE_ALL_LESSON_FORM_TILE_FAIL = '[ENTITY] Delete all LessonFormTileModels failed',

	UPDATE_LESSON_FORM_TILE = '[ENTITY] Update LessonFormTileModel',
	UPDATE_LESSON_FORM_TILE_OK = '[ENTITY] Update LessonFormTileModel successfully',
	UPDATE_LESSON_FORM_TILE_FAIL = '[ENTITY] Update LessonFormTileModel failed',

	UPDATE_ALL_LESSON_FORM_TILE = '[ENTITY] Update all LessonFormTileModel',
	UPDATE_ALL_LESSON_FORM_TILE_OK = '[ENTITY] Update all LessonFormTileModel successfully',
	UPDATE_ALL_LESSON_FORM_TILE_FAIL = '[ENTITY] Update all LessonFormTileModel failed',

	FETCH_LESSON_FORM_TILE= '[ENTITY] Fetch LessonFormTileModel',
	FETCH_LESSON_FORM_TILE_OK = '[ENTITY] Fetch LessonFormTileModel successfully',
	FETCH_LESSON_FORM_TILE_FAIL = '[ENTITY] Fetch LessonFormTileModel failed',

	FETCH_LESSON_FORM_TILE_AUDIT= '[ENTITY] Fetch LessonFormTileModel audit',
	FETCH_LESSON_FORM_TILE_AUDIT_OK = '[ENTITY] Fetch LessonFormTileModel audit successfully',
	FETCH_LESSON_FORM_TILE_AUDIT_FAIL = '[ENTITY] Fetch LessonFormTileModel audit failed',

	FETCH_LESSON_FORM_TILE_AUDITS_BY_ENTITY_ID= '[ENTITY] Fetch LessonFormTileModel audits by entity id',
	FETCH_LESSON_FORM_TILE_AUDITS_BY_ENTITY_ID_OK = '[ENTITY] Fetch LessonFormTileModel audits by entity id successfully',
	FETCH_LESSON_FORM_TILE_AUDITS_BY_ENTITY_ID_FAIL = '[ENTITY] Fetch LessonFormTileModel audits by entity id failed',

	FETCH_ALL_LESSON_FORM_TILE = '[ENTITY] Fetch all LessonFormTileModel',
	FETCH_ALL_LESSON_FORM_TILE_OK = '[ENTITY] Fetch all LessonFormTileModel successfully',
	FETCH_ALL_LESSON_FORM_TILE_FAIL = '[ENTITY] Fetch all LessonFormTileModel failed',

	FETCH_LESSON_FORM_TILE_WITH_QUERY = '[ENTITY] Fetch LessonFormTileModel with query',
	FETCH_LESSON_FORM_TILE_WITH_QUERY_OK = '[ENTITY] Fetch LessonFormTileModel with query successfully',
	FETCH_LESSON_FORM_TILE_WITH_QUERY_FAIL = '[ENTITY] Fetch LessonFormTileModel with query failed',

	FETCH_LAST_LESSON_FORM_TILE_WITH_QUERY = '[ENTITY] Fetch last LessonFormTileModel with query',
	FETCH_LAST_LESSON_FORM_TILE_WITH_QUERY_OK = '[ENTITY] Fetch last LessonFormTileModel with query successfully',
	FETCH_LAST_LESSON_FORM_TILE_WITH_QUERY_FAIL = '[ENTITY] Fetch last LessonFormTileModel with query failed',

	EXPORT_LESSON_FORM_TILE = '[ENTITY] Export LessonFormTileModel',
	EXPORT_LESSON_FORM_TILE_OK = '[ENTITY] Export LessonFormTileModel successfully',
	EXPORT_LESSON_FORM_TILE_FAIL = '[ENTITY] Export LessonFormTileModel failed',

	EXPORT_ALL_LESSON_FORM_TILE = '[ENTITY] Export All LessonFormTileModels',
	EXPORT_ALL_LESSON_FORM_TILE_OK = '[ENTITY] Export All LessonFormTileModels successfully',
	EXPORT_ALL_LESSON_FORM_TILE_FAIL = '[ENTITY] Export All LessonFormTileModels failed',

	EXPORT_LESSON_FORM_TILE_EXCLUDING_IDS = '[ENTITY] Export LessonFormTileModels excluding Ids',
	EXPORT_LESSON_FORM_TILE_EXCLUDING_IDS_OK = '[ENTITY] Export LessonFormTileModel excluding Ids successfully',
	EXPORT_LESSON_FORM_TILE_EXCLUDING_IDS_FAIL = '[ENTITY] Export LessonFormTileModel excluding Ids failed',

	COUNT_LESSON_FORM_TILES = '[ENTITY] Fetch number of LessonFormTileModel records',
	COUNT_LESSON_FORM_TILES_OK = '[ENTITY] Fetch number of LessonFormTileModel records successfully ',
	COUNT_LESSON_FORM_TILES_FAIL = '[ENTITY] Fetch number of LessonFormTileModel records failed',

	IMPORT_LESSON_FORM_TILES = '[ENTITY] Import LessonFormTileModels',
	IMPORT_LESSON_FORM_TILES_OK = '[ENTITY] Import LessonFormTileModels successfully',
	IMPORT_LESSON_FORM_TILES_FAIL = '[ENTITY] Import LessonFormTileModels fail',


	INITIALISE_LESSON_FORM_TILE_COLLECTION_STATE = '[ENTITY] Initialize the CollectionState of LessonFormTileModel',
	// % protected region % [Add any additional model actions here] off begin
	// % protected region % [Add any additional model actions here] end
}

export abstract class BaseLessonFormTileAction implements Action {
	readonly className: string = 'LessonFormTileModel';

	abstract readonly type: string;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	protected constructor(
		// % protected region % [Add any additional constructor parameters here] off begin
		// % protected region % [Add any additional constructor parameters here] end
		public readonly afterwardActions: Action[] = []
	) {
		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}
}

export class LessonFormTileAction extends BaseLessonFormTileAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for LessonFormTileAction here] off begin
	// % protected region % [Add any additional class fields for LessonFormTileAction here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<LessonFormTileModel>,
		// % protected region % [Add any additional constructor parameters for LessonFormTileAction here] off begin
		// % protected region % [Add any additional constructor parameters for LessonFormTileAction here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for LessonFormTileAction here] off begin
			// % protected region % [Add any additional constructor arguments for LessonFormTileAction here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for LessonFormTileAction here] off begin
		// % protected region % [Add any additional constructor logic for LessonFormTileAction here] end
	}

	// % protected region % [Add any additional class methods for LessonFormTileAction here] off begin
	// % protected region % [Add any additional class methods for LessonFormTileAction here] end
}

export class LessonFormTileActionOK extends BaseLessonFormTileAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for LessonFormTileActionOK here] off begin
	// % protected region % [Add any additional class fields for LessonFormTileActionOK here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<LessonFormTileModel>,
		// % protected region % [Add any additional constructor parameters for LessonFormTileActionOK here] off begin
		// % protected region % [Add any additional constructor parameters for LessonFormTileActionOK here] end
		afterwardActions: Action[] = [],
		public audits?: LessonFormTileModelAudit[],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for LessonFormTileActionOK here] off begin
			// % protected region % [Add any additional constructor arguments for LessonFormTileActionOK here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for LessonFormTileActionOK here] off begin
		// % protected region % [Add any additional constructor logic for LessonFormTileActionOK here] end
	}

	// % protected region % [Add any additional class methods for LessonFormTileActionOK here] off begin
	// % protected region % [Add any additional class methods for LessonFormTileActionOK here] end
}

export class LessonFormTileActionFail extends BaseLessonFormTileAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for LessonFormTileActionFail here] off begin
	// % protected region % [Add any additional class fields for LessonFormTileActionFail here] end

	public constructor(
		public typeInput: string,
		public readonly error?: any,
		public stateConfig?: PassableStateConfig<LessonFormTileModel>,
		// % protected region % [Add any additional constructor parameters for LessonFormTileActionFail here] off begin
		// % protected region % [Add any additional constructor parameters for LessonFormTileActionFail here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for LessonFormTileActionFail here] off begin
			// % protected region % [Add any additional constructor arguments for LessonFormTileActionFail here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for LessonFormTileActionFail here] off begin
		// % protected region % [Add any additional constructor logic for LessonFormTileActionFail here] end
	}

	// % protected region % [Add any additional class methods for LessonFormTileActionFail here] off begin
	// % protected region % [Add any additional class methods for LessonFormTileActionFail here] end
}

export function isLessonFormTileModelAction(e: any): e is BaseLessonFormTileAction {
	return Object.values(LessonFormTileModelActionTypes).includes(e);
}

// % protected region % [Add any additional actions here] off begin
// % protected region % [Add any additional actions here] end
