/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Params, RouterStateSnapshot} from '@angular/router';
import {RouterReducerState, RouterStateSerializer} from '@ngrx/router-store';
import {RoutingEffect} from '../lib/routing/routing.effect';
import {QueryParams} from '../lib/services/http/interfaces';
import {AdministratorEffect} from './administrator/administrator.model.effect';
import {
	AdministratorModelState,
	initialState as AdministratorModelInitialState
} from './administrator/administrator.model.state';
import {ApplicationLocaleEffect} from './applicationLocale/application_locale.model.effect';
import {
	ApplicationLocaleModelState,
	initialState as ApplicationLocaleModelInitialState
} from './applicationLocale/application_locale.model.state';
import {ArticleEffect} from './article/article.model.effect';
import {
	ArticleModelState,
	initialState as ArticleModelInitialState
} from './article/article.model.state';
import {BookEffect} from './book/book.model.effect';
import {
	BookModelState,
	initialState as BookModelInitialState
} from './book/book.model.state';
import {CoreUserEffect} from './coreUser/core_user.model.effect';
import {
	CoreUserModelState,
	initialState as CoreUserModelInitialState
} from './coreUser/core_user.model.state';
import {CourseEffect} from './course/course.model.effect';
import {
	CourseModelState,
	initialState as CourseModelInitialState
} from './course/course.model.state';
import {CourseCategoryEffect} from './courseCategory/course_category.model.effect';
import {
	CourseCategoryModelState,
	initialState as CourseCategoryModelInitialState
} from './courseCategory/course_category.model.state';
import {CourseLessonEffect} from './courseLesson/course_lesson.model.effect';
import {
	CourseLessonModelState,
	initialState as CourseLessonModelInitialState
} from './courseLesson/course_lesson.model.state';
import {LessonEffect} from './lesson/lesson.model.effect';
import {
	LessonModelState,
	initialState as LessonModelInitialState
} from './lesson/lesson.model.state';
import {TagEffect} from './tag/tag.model.effect';
import {
	TagModelState,
	initialState as TagModelInitialState
} from './tag/tag.model.state';
import {RoleEffect} from './role/role.model.effect';
import {
	RoleModelState,
	initialState as RoleModelInitialState
} from './role/role.model.state';
import {PrivilegeEffect} from './privilege/privilege.model.effect';
import {
	PrivilegeModelState,
	initialState as PrivilegeModelInitialState
} from './privilege/privilege.model.state';
import {WorkflowEffect} from './workflow/workflow.model.effect';
import {
	WorkflowModelState,
	initialState as WorkflowModelInitialState
} from './workflow/workflow.model.state';
import {WorkflowStateEffect} from './workflowState/workflow_state.model.effect';
import {
	WorkflowStateModelState,
	initialState as WorkflowStateModelInitialState
} from './workflowState/workflow_state.model.state';
import {WorkflowTransitionEffect} from './workflowTransition/workflow_transition.model.effect';
import {
	WorkflowTransitionModelState,
	initialState as WorkflowTransitionModelInitialState
} from './workflowTransition/workflow_transition.model.state';
import {WorkflowVersionEffect} from './workflowVersion/workflow_version.model.effect';
import {
	WorkflowVersionModelState,
	initialState as WorkflowVersionModelInitialState
} from './workflowVersion/workflow_version.model.state';
import {LessonFormSubmissionEffect} from './lessonFormSubmission/lesson_form_submission.model.effect';
import {
	LessonFormSubmissionModelState,
	initialState as LessonFormSubmissionModelInitialState
} from './lessonFormSubmission/lesson_form_submission.model.state';
import {LessonFormVersionEffect} from './lessonFormVersion/lesson_form_version.model.effect';
import {
	LessonFormVersionModelState,
	initialState as LessonFormVersionModelInitialState
} from './lessonFormVersion/lesson_form_version.model.state';
import {LessonFormTileEffect} from './lessonFormTile/lesson_form_tile.model.effect';
import {
	LessonFormTileModelState,
	initialState as LessonFormTileModelInitialState
} from './lessonFormTile/lesson_form_tile.model.state';
import {AbstractModel} from '../lib/models/abstract.model';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * The State of a collection of the model
 * This would store the queryParams of specific collection, which could be shared through the store
 */
export interface CollectionState {
	/**
	 * The query parameters to be used for querying of this collection
	 */
	queryParams: QueryParams;

	/**
	 * The total number of results on the server that match the queryParams
	 */
	collectionCount: number;

	/**
	 * The ids of the data in this collection
	 */
	ids?: string[];
}

/**
 * Types of audit query.
 */
export enum AuditQueryType {
	CREATE = 'CREATE',
	UPDATE = 'UPDATE',
	DELETE = 'DELETE'
}

/**
 * State containing audits against a specific entity.
 */
export interface AbstractModelAudit<E extends AbstractModel> {
	entity: E;
	timestamp: string;
	type: AuditQueryType;
	authorId: string;
	authorFirstName: string;
	authorLastName: string;
	// % protected region % [Add any model audit properties here] off begin
	// % protected region % [Add any model audit properties here] end
}

/**
 * State containing all models in the current application. This acts essentially as the central store of models for the
 * application.
 */
export interface ModelState {
	AdministratorModel: AdministratorModelState;
	ApplicationLocaleModel: ApplicationLocaleModelState;
	ArticleModel: ArticleModelState;
	BookModel: BookModelState;
	CoreUserModel: CoreUserModelState;
	CourseModel: CourseModelState;
	CourseCategoryModel: CourseCategoryModelState;
	CourseLessonModel: CourseLessonModelState;
	LessonModel: LessonModelState;
	TagModel: TagModelState;
	RoleModel: RoleModelState;
	PrivilegeModel: PrivilegeModelState;
	WorkflowModel: WorkflowModelState;
	WorkflowStateModel: WorkflowStateModelState;
	WorkflowTransitionModel: WorkflowTransitionModelState;
	WorkflowVersionModel: WorkflowVersionModelState;
	LessonFormSubmissionModel: LessonFormSubmissionModelState;
	LessonFormVersionModel: LessonFormVersionModelState;
	LessonFormTileModel: LessonFormTileModelState;
	// % protected region % [Add any additional ModelState attributes here] off begin
	// % protected region % [Add any additional ModelState attributes here] end
}

/**
 * Initial model state of the application.
 */
export const initialModelState: ModelState = {
	AdministratorModel: {
		...AdministratorModelInitialState,
		// % protected region % [Add any additional state for model Administrator here] off begin
		// % protected region % [Add any additional state for model Administrator here] end
	},
	ApplicationLocaleModel: {
		...ApplicationLocaleModelInitialState,
		// % protected region % [Add any additional state for model Application Locale here] off begin
		// % protected region % [Add any additional state for model Application Locale here] end
	},
	ArticleModel: {
		...ArticleModelInitialState,
		// % protected region % [Add any additional state for model Article here] off begin
		// % protected region % [Add any additional state for model Article here] end
	},
	BookModel: {
		...BookModelInitialState,
		// % protected region % [Add any additional state for model Book here] off begin
		// % protected region % [Add any additional state for model Book here] end
	},
	CoreUserModel: {
		...CoreUserModelInitialState,
		// % protected region % [Add any additional state for model Core User here] off begin
		// % protected region % [Add any additional state for model Core User here] end
	},
	CourseModel: {
		...CourseModelInitialState,
		// % protected region % [Add any additional state for model Course here] off begin
		// % protected region % [Add any additional state for model Course here] end
	},
	CourseCategoryModel: {
		...CourseCategoryModelInitialState,
		// % protected region % [Add any additional state for model Course Category here] off begin
		// % protected region % [Add any additional state for model Course Category here] end
	},
	CourseLessonModel: {
		...CourseLessonModelInitialState,
		// % protected region % [Add any additional state for model Course Lesson here] off begin
		// % protected region % [Add any additional state for model Course Lesson here] end
	},
	LessonModel: {
		...LessonModelInitialState,
		// % protected region % [Add any additional state for model Lesson here] off begin
		// % protected region % [Add any additional state for model Lesson here] end
	},
	TagModel: {
		...TagModelInitialState,
		// % protected region % [Add any additional state for model Tag here] off begin
		// % protected region % [Add any additional state for model Tag here] end
	},
	RoleModel: {
		...RoleModelInitialState,
		// % protected region % [Add any additional state for model Role here] off begin
		// % protected region % [Add any additional state for model Role here] end
	},
	PrivilegeModel: {
		...PrivilegeModelInitialState,
		// % protected region % [Add any additional state for model Privilege here] off begin
		// % protected region % [Add any additional state for model Privilege here] end
	},
	WorkflowModel: {
		...WorkflowModelInitialState,
		// % protected region % [Add any additional state for model Workflow here] off begin
		// % protected region % [Add any additional state for model Workflow here] end
	},
	WorkflowStateModel: {
		...WorkflowStateModelInitialState,
		// % protected region % [Add any additional state for model Workflow State here] off begin
		// % protected region % [Add any additional state for model Workflow State here] end
	},
	WorkflowTransitionModel: {
		...WorkflowTransitionModelInitialState,
		// % protected region % [Add any additional state for model Workflow Transition here] off begin
		// % protected region % [Add any additional state for model Workflow Transition here] end
	},
	WorkflowVersionModel: {
		...WorkflowVersionModelInitialState,
		// % protected region % [Add any additional state for model Workflow Version here] off begin
		// % protected region % [Add any additional state for model Workflow Version here] end
	},
	LessonFormSubmissionModel: {
		...LessonFormSubmissionModelInitialState,
		// % protected region % [Add any additional state for model Lesson Form Submission here] off begin
		// % protected region % [Add any additional state for model Lesson Form Submission here] end
	},
	LessonFormVersionModel: {
		...LessonFormVersionModelInitialState,
		// % protected region % [Add any additional state for model Lesson Form Version here] off begin
		// % protected region % [Add any additional state for model Lesson Form Version here] end
	},
	LessonFormTileModel: {
		...LessonFormTileModelInitialState,
		// % protected region % [Add any additional state for model Lesson Form Tile here] off begin
		// % protected region % [Add any additional state for model Lesson Form Tile here] end
	},
	// % protected region % [Add any additional intitialModelState attributes here] off begin
	// % protected region % [Add any additional intitialModelState attributes here] end
};

/**
 * State containing all everything in the current application. This acts essentially as the central store of the
 * application, including the router and the model state.
 */
export interface AppState {
	router: RouterReducerState<RouterState>;
	models: ModelState;
	// % protected region % [Add any additional app state definition here] off begin
	// % protected region % [Add any additional app state definition here] end
}

/**
 * List of all effects for each model in the application.
 */
export const effects = [
	RoutingEffect,
	AdministratorEffect,
	ApplicationLocaleEffect,
	ArticleEffect,
	BookEffect,
	CoreUserEffect,
	CourseEffect,
	CourseCategoryEffect,
	CourseLessonEffect,
	LessonEffect,
	TagEffect,
	RoleEffect,
	PrivilegeEffect,
	WorkflowEffect,
	WorkflowStateEffect,
	WorkflowTransitionEffect,
	WorkflowVersionEffect,
	LessonFormSubmissionEffect,
	LessonFormVersionEffect,
	LessonFormTileEffect,
	// % protected region % [Add any additional effects here] off begin
	// % protected region % [Add any additional effects here] end
];

/**
 * Define the state for the Angular router. Since the original one contains many unused data which can be removed
 * otherwise, this interface is used with the custom serialiser below to provide a simpler router state.
 */
export interface RouterState {
	url: string;
	urls: string[];
	params: Params;
	queryParams: Params;
	data: any;
	// % protected region % [Add any additional properties for RouterState here] off begin
	// % protected region % [Add any additional properties for RouterState here] end
}

/**
 * Define the initial state for router when first bootstrapped.
 */
export const initialRouterState: RouterReducerState<RouterState> = {
	state: {
		url: '/',
		urls: [],
		params: [],
		queryParams: [],
		data: {},
		// % protected region % [Add any additional initial state for RouterState here] off begin
		// % protected region % [Add any additional initial state for RouterState here] end
	},
	navigationId: -1
};

/**
 * Custom serializer used for parsing the original router state provided by Angular.
 */
export class CustomSerializer implements RouterStateSerializer<RouterState> {
	serialize(routerState: RouterStateSnapshot): RouterState {
		let route = routerState.root;
		const urls: string[] = [];

		while (route.firstChild) {
			route.firstChild.url.forEach(u => urls.push(u.path));
			route = route.firstChild;
		}

		const {
			url,
			root: {
				queryParams,
				data,
				// % protected region % [Add any additional extraction of routerState properties here] off begin
				// % protected region % [Add any additional extraction of routerState properties here] end
			}
		} = routerState;
		const {params} = route;

		// Only return an object including the URL, params and query params
		// instead of the entire snapshot
		return {
			url,
			urls,
			params,
			queryParams,
			data,
			// % protected region % [Add any additional properties to be returned here] off begin
			// % protected region % [Add any additional properties to be returned here] end
		};
	}
}

// % protected region % [Add any additional stuffs here] off begin
// % protected region % [Add any additional stuffs here] end
