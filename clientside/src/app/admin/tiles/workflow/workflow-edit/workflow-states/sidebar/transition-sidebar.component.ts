/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {AbstractComponent} from '../../../../../../lib/components/abstract.component';
import {
	Component,
	EventEmitter,
	HostBinding,
	Input,
	OnChanges,
	Output, QueryList,
	SimpleChanges,
	ViewChildren
} from '@angular/core';
import {ButtonAccentColour, ButtonStyle, IconPosition} from '../../../../../../lib/components/button/button.component';
import {WorkflowStateModel} from '../../../../../../models/workflowState/workflow_state.model';
import {Observable, of} from 'rxjs';
import {WorkflowTransitionModel} from '../../../../../../models/workflowTransition/workflow_transition.model';
import {filter, take} from 'rxjs/operators';
import {WorkflowTransitionModelState} from '../../../../../../models/workflowTransition/workflow_transition.model.state';
import * as workflowTransitionModelAction from '../../../../../../models/workflowTransition/workflow_transition.model.action';
import {ActionsSubject, Store} from '@ngrx/store';
import {Expand, QueryOperation, QueryParams} from '../../../../../../lib/services/http/interfaces';
import {getWorkflowTransitionCollectionModels} from '../../../../../../models/workflowTransition/workflow_transition.model.selector';
import {TransitionEditComponent} from './transition-edit/transition-edit.component';
import * as uuid from 'uuid';
import * as _ from 'lodash';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

@Component({
	selector: 'section[cb-workflow-transition-sidebar]',
	templateUrl: './transition-sidebar.component.html',
	styleUrls: [
		'./transition-sidebar.component.scss',
		// % protected region % [Add any additional styles here] off begin
		// % protected region % [Add any additional styles here] end
	]
})
export class TransitionSidebarComponent extends AbstractComponent implements OnChanges {
	buttonStyle = ButtonStyle;
	buttonColor = ButtonAccentColour;
	iconPos = IconPosition;

	@HostBinding('class')
	className = 'workflow-properties';

	/**
	 * Id of the source workflow state
	 */
	@Input()
	workflowStateId: string;

	/**
	 * Available states to be selected
	 */
	@Input()
	workflowStates: WorkflowStateModel[];

	/**
	 * Event emitter when close button clicked
	 */
	@Output()
	closeButtonClicked: EventEmitter<null> = new EventEmitter();

	/**
	 * Event emitter when transition changed
	 */
	@Output()
	transitionChanged: EventEmitter<null> = new EventEmitter();

	/**
	 * All transition edit component in the page
	 */
	@ViewChildren(TransitionEditComponent)
	transitionEditComponents: QueryList<TransitionEditComponent>;

	/**
	 * Existing workflow states
	 */
	workflowTransitions$: Observable<WorkflowTransitionModel[]>;

	/**
	 * New crated workflow states
	 */
	newWorkflowTransitions: WorkflowTransitionModel[];

	/**
	 * Whether save button is disabled
	 */
	buttonDisabled: boolean = false;

	/**
	 * Collection id of workflow transition in current sidebar
	 */
	collectionId: string;

	/**
	 * Expands all attributes
	 */
	get defaultExpands(): Expand[] {
		let expands: Expand[] =  Object.entries(WorkflowTransitionModel.getRelations()).map(
			([key, entry]): Expand => {
				return {
					name: key,
					fields: ['id', entry.displayName]
				};
			}
		);
		return expands;
	}

	/**
	 * Default query parameter to fetch all the transitions from this state
	 */
	get defaultQueryParams(): QueryParams {
		return {
			pageIndex: 0,
			pageSize: 1000,
			where: [
				[
					{
						path: 'sourceStateId',
						operation: QueryOperation.EQUAL,
						value: this.workflowStateId
					}
				]
			],
			expands: this.defaultExpands
		};
	}

	// % protected region % [Add any additional fields here] off begin
	// % protected region % [Add any additional fields here] end

	/**
	 * @inheritDoc
	 */
	constructor(
		private store: Store<{model: WorkflowTransitionModelState}>,
		private readonly actionSubjects: ActionsSubject,
	) {
		super();

		// % protected region % [Add any constructor logic here] off begin
		// % protected region % [Add any constructor logic here] end
	}

	/**
	 * @inheritDoc
	 */
	ngOnChanges(changes: SimpleChanges): void {
		if (changes.hasOwnProperty('workflowStateId')) {
			if (changes.workflowStateId) {

				// Get Transition of the state
				this.collectionId = `workflow-transitions-state-id-${this.workflowStateId}`;
				this.newWorkflowTransitions = [];

				this.store.dispatch(new workflowTransitionModelAction.WorkflowTransitionAction(
					workflowTransitionModelAction.WorkflowTransitionModelActionTypes.INITIALISE_WORKFLOW_TRANSITION_COLLECTION_STATE,
					{
						collectionId: this.collectionId
					}
				));

				this.store.dispatch(new workflowTransitionModelAction.WorkflowTransitionAction(
					workflowTransitionModelAction.WorkflowTransitionModelActionTypes.FETCH_WORKFLOW_TRANSITION_WITH_QUERY,
					{
						collectionId: this.collectionId,
						queryParams: this.defaultQueryParams
					}
				));

				this.workflowTransitions$ = this.store.select(getWorkflowTransitionCollectionModels, this.collectionId);
			} else {
				this.workflowTransitions$ = of([]);
			}
		}

		// % protected region % [Add any additional logic for ngOnChanges here] off begin
		// % protected region % [Add any additional logic for ngOnChanges here] end
	}

	/**
	 * Event triggered when add button clicked
	 */
	onAddButtonClick() {
		this.newWorkflowTransitions.push(new WorkflowTransitionModel({
			sourceStateId: this.workflowStateId
		}));
	}

	/**
	 * Event triggered when closed button clicked
	 */
	onCloseButtonClicked() {
		this.closeButtonClicked.emit();
	}

	/**
	 * Handle event to delete transition
	 */
	onTransitionDeleted(workflowTransition: WorkflowTransitionModel) {
		if (workflowTransition.id) {
			this.store.dispatch(new workflowTransitionModelAction.WorkflowTransitionAction(
				workflowTransitionModelAction.WorkflowTransitionModelActionTypes.DELETE_WORKFLOW_TRANSITION,
				{
					targetModelId: workflowTransition.id
				}
			));
			this.transitionChanged.emit();
		} else {
			_.remove(this.newWorkflowTransitions, (state) => state === workflowTransition);
		}

		// % protected region % [Add any additional logic for onTransitionDeleted here] off begin
		// % protected region % [Add any additional logic for onTransitionDeleted here] end
	}

	/**
	 * Event triggered when save button clicked
	 */
	onSaveClicked() {
		if (this.transitionEditComponents.toArray().every(comp => comp.validateFormGroup())) {
			const transitionsToCreate = [];
			const transitionsToUpdate = [];
			const updates = [];

			this.transitionEditComponents.forEach(workflowComponent => {
				// Update existed workflow component
				const changes = workflowComponent.getUpdateValue();
				const workflowTransition = workflowComponent.workflowTransition;
				if (workflowComponent.workflowTransition.id) {
					transitionsToUpdate.push(workflowTransition);
					updates.push(changes);
				} else {
					Object.assign(workflowTransition, changes);
					transitionsToCreate.push(workflowTransition);
				}
			});

			this.buttonDisabled = true;

			// Create variables for uuid of action and to control whether button is enabled
			const updateRequestId = uuid.v4();
			const createRequestId = uuid.v4();
			let sendingCreateRequest = false;
			let sendingUpdateRequest = false;

			if (transitionsToCreate.length > 0) {
				this.store.dispatch(new workflowTransitionModelAction.WorkflowTransitionAction(
					workflowTransitionModelAction.WorkflowTransitionModelActionTypes.CREATE_ALL_WORKFLOW_TRANSITION,
					{
						targetModels: transitionsToCreate,
						collectionId: this.collectionId,
						requestId: createRequestId,
						queryParams: {
							expands: this.defaultExpands
						}
					}
				));
			} else {
				sendingCreateRequest = false;
			}

			if (transitionsToUpdate.length > 0) {
				this.store.dispatch(new workflowTransitionModelAction.WorkflowTransitionAction(
					workflowTransitionModelAction.WorkflowTransitionModelActionTypes.UPDATE_ALL_WORKFLOW_TRANSITION,
					{
						targetModels: transitionsToUpdate,
						updates: updates,
						collectionId: this.collectionId,
						requestId: updateRequestId,
						queryParams: {
							expands: this.defaultExpands
						}
					}
				));
			} else {
				sendingUpdateRequest = false;
			}

			this.actionSubjects.pipe(
				filter((action: workflowTransitionModelAction.WorkflowTransitionActionOK ) => {
					return action.type === workflowTransitionModelAction.WorkflowTransitionModelActionTypes.CREATE_ALL_WORKFLOW_TRANSITION_OK
						&& action.stateConfig.requestId === createRequestId;
				}),
				take(1)
			).subscribe(() => {
				sendingUpdateRequest = false;

				if (!sendingUpdateRequest && !sendingCreateRequest) {
					this.transitionChanged.emit();
				}

				this.buttonDisabled = sendingUpdateRequest || sendingCreateRequest;
			});

			this.actionSubjects.pipe(
				filter((action: workflowTransitionModelAction.WorkflowTransitionActionOK ) => {
					return action.type === workflowTransitionModelAction.WorkflowTransitionModelActionTypes.UPDATE_ALL_WORKFLOW_TRANSITION_OK
						&& action.stateConfig.requestId === updateRequestId;
				}),
				take(1)
			).subscribe(() => {
				sendingUpdateRequest = false;

				if (!sendingUpdateRequest && !sendingCreateRequest) {
					this.transitionChanged.emit();
				}
				// Fetch to update
				this.buttonDisabled = sendingUpdateRequest || sendingCreateRequest;
			});

			this.buttonDisabled = sendingCreateRequest || sendingUpdateRequest;

			this.newWorkflowTransitions = [];
		}

		// % protected region % [Add any additional logic for onSaveClicked here] off begin
		// % protected region % [Add any additional logic for onSaveClicked here] end
	}

	// % protected region % [Add any additional methods here] off begin
	// % protected region % [Add any additional methods here] end
}
