/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {AbstractComponent} from '../../../../../../lib/components/abstract.component';
import {Component, EventEmitter, HostBinding, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {ButtonAccentColour, ButtonStyle, IconPosition} from '../../../../../../lib/components/button/button.component';
import {WorkflowStateModel} from '../../../../../../models/workflowState/workflow_state.model';
import {FormArray, FormGroup} from '@angular/forms';
import {createReactiveFormFromModel} from '../../../../../../lib/models/model-utils';
import {Store} from '@ngrx/store';
import {RouterState} from '../../../../../../models/model.state';
import * as routerActions from '../../../../../../lib/routing/routing.action';
import {ActivatedRoute} from '@angular/router';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

@Component({
	selector: 'div[cb-workflow-state-edit]',
	templateUrl: './workflow-state-edit.component.html',
	styleUrls: [
		'./workflow-state-edit.component.scss',
		// % protected region % [Add any additional styles here] off begin
		// % protected region % [Add any additional styles here] end
	]
})
export class WorkflowStateEditComponent extends AbstractComponent implements OnChanges {
	buttonStyle = ButtonStyle;
	buttonAccentColour = ButtonAccentColour;
	iconPos = IconPosition;

	@HostBinding('class')
	className = 'workflow__states-step';

	@Input()
	workflowState: WorkflowStateModel;

	stateForm: FormGroup;

	@Output()
	deleteClicked: EventEmitter<WorkflowStateModel> = new EventEmitter();

	@Output()
	editClicked: EventEmitter<string> = new EventEmitter();

	@Output()
	startClicked: EventEmitter<WorkflowStateEditComponent> = new EventEmitter();

	// % protected region % [Add any additional fields here] off begin
	// % protected region % [Add any additional fields here] end

	constructor(
		// % protected region % [Add any additional constructor parameters here] off begin
		// % protected region % [Add any additional constructor parameters here] end
	) {
		super();
		this.stateForm = createReactiveFormFromModel(WorkflowStateModel.getProps(), WorkflowStateModel.getRelations());

		// % protected region % [Add any constructor logic here] off begin
		// % protected region % [Add any constructor logic here] end
	}

	/**
	 * @inheritDoc
	 */
	ngOnChanges(changes: SimpleChanges): void {
		if (changes.hasOwnProperty('workflowState')) {
			this.stateForm.patchValue(changes.workflowState.currentValue);
		}

		// % protected region % [Add any additional logic for ngOnChanges here] off begin
		// % protected region % [Add any additional logic for ngOnChanges here] end
	}


	/**
	 * Event be triggered when Is Start State Button clicked
	 */
	isStartStateClicked() {
		this.startClicked.emit(this);

		// Mark is Start State to true
		this.setIsStartState(true);

		// % protected region % [Add any additional logic for isStartStateClicked here] off begin
		// % protected region % [Add any additional logic for isStartStateClicked here] end
	}

	/**
	 * Change the value of isStartState
	 */
	setIsStartState(isStartState: boolean) {
		this.stateForm.patchValue({
			isStartState: isStartState
		});
		this.stateForm.controls.isStartState.markAsDirty();

		// % protected region % [Add any additional logic for setIsStartState here] off begin
		// % protected region % [Add any additional logic for setIsStartState here] end
	}

	/**
	 * Validate form and trigger update to show error message
	 */
	validateFormAndUpdateValue(): boolean {
		if (this.stateForm.invalid) {
			Object.entries(this.stateForm.controls).forEach(([key, control]) => {
				control.updateValueAndValidity();
			});
		}

		// % protected region % [Add any additional logic for validateFormAndUpdateValue here] off begin
		// % protected region % [Add any additional logic for validateFormAndUpdateValue here] end

		return this.stateForm.valid;
	}

	/**
	 * Get changes from the form
	 */
	getUpdateValue() {
		const updatedValue = {};
		Object.entries(this.stateForm.controls).forEach(([key, control]) => {
			if (control.dirty) {
				updatedValue[key] = control.value;
			}
		});

		// % protected region % [Add any additional logic for getUpdateValue here] off begin
		// % protected region % [Add any additional logic for getUpdateValue here] end
		return updatedValue;
	}

	onDeleteButtonClicked() {
		this.deleteClicked.emit(this.workflowState);
	}

	onEditButtonClicked() {
		this.editClicked.emit(this.workflowState.id);
	}

	// % protected region % [Add any additional methods here] off begin
	// % protected region % [Add any additional methods here] end
}
