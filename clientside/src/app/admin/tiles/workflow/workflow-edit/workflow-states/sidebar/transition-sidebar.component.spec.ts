/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import Spy = jasmine.Spy;
import {By} from '@angular/platform-browser';
import {DebugElement, SimpleChange} from '@angular/core';
import {ToastrModule, ToastrService} from 'ngx-toastr';
import {BehaviorSubject, of} from 'rxjs';
import {waitForAsync, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {Store} from '@ngrx/store';
import {TransitionSidebarComponent} from './transition-sidebar.component';
import {WorkflowStateModel} from '../../../../../../models/workflowState/workflow_state.model';
import * as transitionActions from '../../../../../../models/workflowTransition/workflow_transition.model.action';
import {WorkflowStateDataFactory} from '../../../../../../lib/utils/factories/workflow-state-data-factory';
import {WorkflowEditModule} from '../../workflow-edit.module';
import {RouterTestingModule} from '@angular/router/testing';
import {RouterLinkDirective} from '../../../../../../../testing/helpers/router-link-stub';
import {WorkflowTransitionModelState, initialState as initialWorkflowTransitionModelState} from '../../../../../../models/workflowTransition/workflow_transition.model.state';
import {WorkflowTransitionModel} from '../../../../../../models/workflowTransition/workflow_transition.model';
import {WorkflowTransitionDataFactory} from '../../../../../../lib/utils/factories/workflow-transition-data-factory';
import {getWorkflowTransitionCollectionModels} from '../../../../../../models/workflowTransition/workflow_transition.model.selector';
import {TransitionEditComponent} from './transition-edit/transition-edit.component';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

let store: MockStore<{ model: WorkflowTransitionModelState }>;

let sourceWorkflowState: WorkflowStateModel;

let targetWorkflowStates: WorkflowStateModel[];

let testingWorkflowTransitions: WorkflowTransitionModel[];

// Workflow States to create in database
let testingNewWorkflowTransitions: WorkflowTransitionModel[];

let testingUpdateTransitions: WorkflowTransitionModel[];

let workflowTransitionSubject: BehaviorSubject<WorkflowTransitionModel[]>;

let workflowTransitionModelState: WorkflowTransitionModelState;

let collectionId: string;

let dispatchSpy: Spy;

// % protected region % [Add any additional variables here] off begin
// % protected region % [Add any additional variables here] end

/**
 * Create testing workflow states
 */
const prepareTestingData = () => {
	const testingDataNumber = 10;
	const workflowStateDataFactory = new WorkflowStateDataFactory();
	const workflowTransitionDataFactory = new WorkflowTransitionDataFactory();

	// Create 3 times of testing data
	// The 1/3 for original existing data
	// The 1/3 for creating new data
	// The last 1/3 for updating existing data
	targetWorkflowStates = workflowStateDataFactory.createAll(testingDataNumber * 3);

	// Create source workflow state

	sourceWorkflowState = workflowStateDataFactory.create();

	// Use the first 1/3 as target state for existing workflow transitions
	testingWorkflowTransitions = workflowTransitionDataFactory.createAll(testingDataNumber);
	testingWorkflowTransitions.forEach((transition, index) => {
		transition.sourceStateId = sourceWorkflowState.id;
		transition.targetStateId = targetWorkflowStates[index].id;
	});

	// Use the second 1/3 as target state
	testingNewWorkflowTransitions = workflowTransitionDataFactory.createAll(testingDataNumber);
	testingNewWorkflowTransitions.forEach((transition, index) => {
		transition.sourceStateId = sourceWorkflowState.id;
		transition.targetStateId = targetWorkflowStates[index + testingDataNumber].id;
	});

	// Use the third 1/3 as target state
	testingUpdateTransitions = workflowTransitionDataFactory.createAll(testingDataNumber);
	testingUpdateTransitions.forEach((transition, index) => {
		transition.sourceStateId = sourceWorkflowState.id;
		transition.targetStateId = targetWorkflowStates[index + testingDataNumber * 2].id;
	});

	collectionId = `workflow-transitions-state-id-${sourceWorkflowState.id}`;
};

/**
 * Spy on selector in Store
 */
const spySelectorsInStore = () =>  {
	// Setup the Mock Store and fake selector
	store = TestBed.inject(Store) as MockStore<{ model: WorkflowTransitionModelState }>;

	workflowTransitionModelState = initialWorkflowTransitionModelState;
	store.setState({model: workflowTransitionModelState});

	// Create Behavior Subjects to trigger later
	workflowTransitionSubject = new BehaviorSubject(testingWorkflowTransitions);

	// Create spy on select function to return value
	spyOn(store, 'select')
		.withArgs(getWorkflowTransitionCollectionModels, collectionId).and.returnValue(workflowTransitionSubject);

	// Create spy on dispatch function
	dispatchSpy = spyOn(store, 'dispatch');
	dispatchSpy.and.callThrough();
};

/**
 * Click create state button to create a new workflow state
 */
const createNewTransition = (fixture: ComponentFixture<TransitionSidebarComponent>) => {
	const createButtonEl: DebugElement = fixture.debugElement.query(By.css('button.icon-plus'));
	// Click create button to create a new state
	createButtonEl.nativeElement.click();
	tick();
	fixture.detectChanges();
};

describe('Workflow Transition Sidebar Component', () => {

	let fixture: ComponentFixture<TransitionSidebarComponent>;
	let transitionSidebarComponent: TransitionSidebarComponent;

	beforeEach(waitForAsync (() => {

		TestBed.configureTestingModule({
			imports: [
				WorkflowEditModule,
				RouterTestingModule,
				ToastrModule.forRoot({
					iconClasses: {
						success: 'alert__success',
						info: 'alert__info',
						warning: 'alert__warning',
						error: 'alert__danger'
					},
					toastClass: '',
					positionClass: 'alert-container',
					preventDuplicates: true
				})
			],
			providers: [
				provideMockStore(),
				RouterLinkDirective,
				ToastrService,
			]
		}).compileComponents().then(() => {

			prepareTestingData();
			spySelectorsInStore();

			fixture = TestBed.createComponent(TransitionSidebarComponent);

			transitionSidebarComponent = fixture.debugElement.componentInstance;
			transitionSidebarComponent.workflowStateId = sourceWorkflowState.id;
			transitionSidebarComponent.workflowStates = targetWorkflowStates;
			transitionSidebarComponent.ngOnChanges({
				workflowStateId: new SimpleChange(null, sourceWorkflowState.id, true)
			});

			// % protected region % [Add any additional logic before each test for 'Workflow Details Component' here] off begin
			// % protected region % [Add any additional logic before each test for 'Workflow Details Component' here] end
		});
	}));

	afterEach(() => {
		// Need to do this since for some reason the last component queried from the fixture will be rendered on the
		// browser
		if (fixture.nativeElement instanceof HTMLElement) {
			(fixture.nativeElement as HTMLElement).remove();
		}

		// % protected region % [Add any additional logic after each test for 'Workflow Details Component' here] off begin
		// % protected region % [Add any additional logic after each test for 'Workflow Details Component' here] end
	});

	it('should fetch workflow transitions according to source state id', () => {
		// % protected region % [Add any logic before main process of 'should fetch workflow transitions according to source state id' here] off begin
		// % protected region % [Add any logic before main process of 'should fetch workflow transitions according to source state id' here] end

		fixture.detectChanges();
		const expectedInitialAction = new transitionActions.WorkflowTransitionAction(
			transitionActions.WorkflowTransitionModelActionTypes.INITIALISE_WORKFLOW_TRANSITION_COLLECTION_STATE,
			{
				collectionId: collectionId,
			}
		);

		const expectedFetchAction = new transitionActions.WorkflowTransitionAction(
			transitionActions.WorkflowTransitionModelActionTypes.FETCH_WORKFLOW_TRANSITION_WITH_QUERY,
			{
				collectionId: collectionId,
				queryParams: transitionSidebarComponent.defaultQueryParams
			}
		);
		expect(dispatchSpy.calls.argsFor(0)).toEqual([expectedInitialAction]);
		expect(dispatchSpy.calls.argsFor(1)).toEqual([expectedFetchAction]);

		const actualTransitionEditComp = fixture.debugElement.queryAll(By.directive(TransitionEditComponent));
		expect(actualTransitionEditComp.length).toBe(testingWorkflowTransitions.length);

		// % protected region % [Add any logic after main process of 'should fetch workflow transitions according to source state id' here] off begin
		// % protected region % [Add any logic after main process of 'should fetch workflow transitions according to source state id' here] end
	});

	it('should call saving function when click on save button', fakeAsync(() => {
		// % protected region % [Add any logic after main process of 'should call saving function when click on save button' here] off begin
		// % protected region % [Add any logic after main process of 'should call saving function when click on save button' here] end

		fixture.detectChanges();

		spyOn(transitionSidebarComponent, 'onSaveClicked');
		const saveButtonEl = fixture.debugElement.query(By.css('button.workflow-properties__save'));
		saveButtonEl.nativeElement.click();

		tick();
		fixture.detectChanges();

		expect(transitionSidebarComponent.onSaveClicked).toHaveBeenCalled();

		// % protected region % [Add any logic after main process of 'should call saving function when click on save button' here] off begin
		// % protected region % [Add any logic after main process of 'should call saving function when click on save button' here] end
	}));

	it('should create new workflow transition when click add transition button', fakeAsync(() => {
		// % protected region % [Add any logic before main process of 'should create new workflow transition when click add transition button' here] off begin
		// % protected region % [Add any logic before main process of 'should create new workflow transition when click add transition button' here] end

		fixture.detectChanges();
		const addTransitionEl: DebugElement = fixture.debugElement.query(By.css('button.workflow-properties__add'));
		testingNewWorkflowTransitions.forEach(transition => {
			addTransitionEl.nativeElement.click();
		});

		tick();
		fixture.detectChanges();

		const actualTransitionEditComp = fixture.debugElement.queryAll(By.directive(TransitionEditComponent));
		expect(actualTransitionEditComp.length).toBe(testingWorkflowTransitions.length + testingNewWorkflowTransitions.length);
		expect(transitionSidebarComponent.newWorkflowTransitions.length).toBe(testingNewWorkflowTransitions.length);

		// % protected region % [Add any logic after main process of 'should create new workflow transition when click add transition button' here] off begin
		// % protected region % [Add any logic after main process of 'should create new workflow transition when click add transition button' here] end
	}));

	it('should dispatch create action when saving with new created transition', fakeAsync(() => {
		// % protected region % [Add any logic before main process of 'should dispatch create action when saving with new created transition' here] off begin
		// % protected region % [Add any logic before main process of 'should dispatch create action when saving with new created transition' here] end

		fixture.detectChanges();

		const addTransitionEl: DebugElement = fixture.debugElement.query(By.css('button.workflow-properties__add'));
		const saveButtonEl = fixture.debugElement.query(By.css('button.workflow-properties__save'));

		testingNewWorkflowTransitions.forEach(transition => {
			addTransitionEl.nativeElement.click();
		});

		tick();

		// Update new created transitions
		transitionSidebarComponent.newWorkflowTransitions.forEach((transition, index) => {
			transition.targetStateId = testingNewWorkflowTransitions[index].targetStateId;
			transition.transitionName = testingNewWorkflowTransitions[index].transitionName;
		});

		tick();
		fixture.detectChanges();
		transitionSidebarComponent.onSaveClicked();

		const expectedAction: transitionActions.WorkflowTransitionAction = new transitionActions.WorkflowTransitionAction(
			transitionActions.WorkflowTransitionModelActionTypes.CREATE_ALL_WORKFLOW_TRANSITION,
			{
				targetModels: testingNewWorkflowTransitions,
				collectionId: collectionId,
			}
		);

		const actualAction: transitionActions.WorkflowTransitionAction = dispatchSpy.calls.all().find(dispatch => {
			const action = dispatch.args[0];
			return action instanceof  transitionActions.WorkflowTransitionAction
					&& action.type === transitionActions.WorkflowTransitionModelActionTypes.CREATE_ALL_WORKFLOW_TRANSITION;
		}).args[0];

		expect(actualAction.type).toEqual(expectedAction.type);
		actualAction.stateConfig.targetModels.forEach((transition, index) => {
			expect(transition.transitionName).toEqual(testingNewWorkflowTransitions[index].transitionName);
			expect(transition.targetStateId).toEqual(testingNewWorkflowTransitions[index].targetStateId);
			expect(transition.sourceStateId).toEqual(sourceWorkflowState.id);
			expect(transition.id).toBeUndefined();
		});
		expect(actualAction.stateConfig.collectionId).toEqual(expectedAction.stateConfig.collectionId);
		expect(actualAction.stateConfig.queryParams.expands).toEqual(transitionSidebarComponent.defaultExpands);
		expect(actualAction.stateConfig.requestId).not.toBeNull('Should have request id to ');

		// % protected region % [Add any logic after main process of 'should dispatch create action when saving with new created transition' here] off begin
		// % protected region % [Add any logic after main process of 'should dispatch create action when saving with new created transition' here] end
	}));

	it('should dispatch updating action when saving with existing transition', fakeAsync(() => {
		// % protected region % [Add any logic before main process of 'should dispatch updating action when saving with existing transition' here] off begin
		// % protected region % [Add any logic before main process of 'should dispatch updating action when saving with existing transition' here] end

		fixture.detectChanges();

		const transitionEditComponentEls: DebugElement[] = fixture.debugElement.queryAll(By.directive(TransitionEditComponent));

		transitionEditComponentEls.forEach((element, index) => {
			const transitionEditComponent: TransitionEditComponent = element.componentInstance;
			transitionEditComponent.transitionFormGroup.patchValue({
				transitionName: testingUpdateTransitions[index].transitionName,
				targetStateId: testingUpdateTransitions[index].targetStateId
			});
			transitionEditComponent.transitionFormGroup.get('transitionName').markAsDirty();
			transitionEditComponent.transitionFormGroup.get('targetStateId').markAsDirty();
			fixture.detectChanges();
		});

		fixture.detectChanges();

		transitionSidebarComponent.onSaveClicked();
		fixture.detectChanges();

		const expectedAction: transitionActions.WorkflowTransitionAction = new transitionActions.WorkflowTransitionAction(
			transitionActions.WorkflowTransitionModelActionTypes.UPDATE_ALL_WORKFLOW_TRANSITION,
			{
				targetModels: testingUpdateTransitions,
				collectionId: collectionId,
			}
		);

		const actualAction: transitionActions.WorkflowTransitionAction = dispatchSpy.calls.all().find(dispatch => {
			const action = dispatch.args[0];
			return action instanceof  transitionActions.WorkflowTransitionAction
					&& action.type === transitionActions.WorkflowTransitionModelActionTypes.UPDATE_ALL_WORKFLOW_TRANSITION;
		}).args[0];

		expect(actualAction.type).toEqual(expectedAction.type);
		expect(actualAction.stateConfig.targetModels.length).toEqual(expectedAction.stateConfig.targetModels.length);
		actualAction.stateConfig.updates.forEach((updates, index) => {
			expect(updates.transitionName).toEqual(testingUpdateTransitions[index].transitionName);
			expect(updates.targetStateId).toEqual(testingUpdateTransitions[index].targetStateId);
			expect(updates.id).toBeUndefined();
			expect(updates.sourceStateId).toBeUndefined();
			expect(actualAction.stateConfig.targetModels[index].id).toEqual(testingWorkflowTransitions[index].id);
		});
		expect(actualAction.stateConfig.collectionId).toEqual(expectedAction.stateConfig.collectionId);
		expect(actualAction.stateConfig.queryParams.expands).toEqual(transitionSidebarComponent.defaultExpands);
		expect(actualAction.stateConfig.requestId).not.toBeNull('Should have request id to track request');

		// % protected region % [Add any logic after main process of 'should dispatch updating action when saving with existing transition' here] off begin
		// % protected region % [Add any logic after main process of 'should dispatch updating action when saving with existing transition' here] end
	}));

	it('should dispatch delete action when clicking delete button', fakeAsync(() => {
		// % protected region % [Add any logic before main process of 'should dispatch delete action when clicking delete button' here] off begin
		// % protected region % [Add any logic before main process of 'should dispatch delete action when clicking delete button' here] end

		fixture.detectChanges();

		const deleteButtonEls: DebugElement[] = fixture.debugElement.queryAll(By.css('button.workflow__delete-transition'));

		testingWorkflowTransitions.forEach((workflowToDelete, index) => {
			deleteButtonEls[index].nativeElement.click();

			tick();
			fixture.detectChanges();

			const expectedAction: transitionActions.WorkflowTransitionAction = new transitionActions.WorkflowTransitionAction(
				transitionActions.WorkflowTransitionModelActionTypes.DELETE_WORKFLOW_TRANSITION,
				{
					targetModelId: workflowToDelete.id
				}
			);

			const actualAction = dispatchSpy.calls.mostRecent().args[0];

			expect(actualAction.type).toEqual(expectedAction.type);
			expect(actualAction.stateConfig.targetModelId).toEqual(expectedAction.stateConfig.targetModelId);
		});

		// % protected region % [Add any logic after main process of 'should dispatch delete action when clicking delete button' here] off begin
		// % protected region % [Add any logic after main process of 'should dispatch delete action when clicking delete button' here] end
	}));

	// % protected region % [Add any tests for TransitionSidebarComponent here] off begin
	// % protected region % [Add any tests for TransitionSidebarComponent here] end
});

// % protected region % [Add any additional describes here] off begin
// % protected region % [Add any additional describes here] end
