/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {
	initialState as initialWorkflowModelVersionState,
	WorkflowVersionModelState
} from 'src/app/models/workflowVersion/workflow_version.model.state';
import {RouterState} from 'src/app/models/model.state';
import {BehaviorSubject} from 'rxjs';
import {waitForAsync, ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';
import {Store} from '@ngrx/store';
import {DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';
import {WorkflowVersionModel} from 'src/app/models/workflowVersion/workflow_version.model';
import {getWorkflowVersionModelWithId} from 'src/app/models/workflowVersion/workflow_version.model.selector';
import {WorkflowVersionDataFactory} from 'src/app/lib/utils/factories/workflow-version-data-factory';
import {WorkflowEditModule} from '../workflow-edit.module';
import {RouterLinkDirective} from 'src/testing/helpers/router-link-stub';
import {RouterTestingModule} from '@angular/router/testing';
import {WorkflowDetailsComponent} from './workflow-details.component';
import {ActivatedRouteStub} from 'src/testing/helpers/activated-route-stub';
import {ActivatedRoute} from '@angular/router';
import {WorkflowModel} from 'src/app/models/workflow/workflow.model';
import {WorkflowDataFactory} from 'src/app/lib/utils/factories/workflow-data-factory';
import {getWorkflowModelWithId} from 'src/app/models/workflow/workflow.model.selector';
import * as versionAction from 'src/app/models/workflowVersion/workflow_version.model.action';
import {TextfieldComponent} from 'src/app/lib/components/textfield/textfield.component';
import {DropdownComponent} from 'src/app/lib/components/dropdown/dropdown.component';
import {TextareaComponent} from 'src/app/lib/components/textarea/textarea.component';
import * as routerAction from 'src/app/lib/routing/routing.action';
import {DropdownTestingHelper} from 'src/testing/helpers/dropdown-testing-helper';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

let testingWorkflow: WorkflowModel;
let testingWorkflowVersion: WorkflowVersionModel;

let fixture: ComponentFixture<WorkflowDetailsComponent>;
let workflowDetailsComponent: WorkflowDetailsComponent;

let store: MockStore<{ model: WorkflowVersionModelState }>;
let routerStore: MockStore<{router: RouterState}>;
let activeRouter: ActivatedRouteStub;

let workflowVersionModelState: WorkflowVersionModelState;

let workflowVersionSubject: BehaviorSubject<WorkflowVersionModel>;
let workflowSubject: BehaviorSubject<WorkflowModel>;

const workflowVersionDataFactory = new WorkflowVersionDataFactory();
const workflowDataFactory = new WorkflowDataFactory();

/**
 * Creating testing workflow and workflow version and link them together
 */
function prepareTestingData() {
	testingWorkflow = workflowDataFactory.create();
	testingWorkflowVersion = workflowVersionDataFactory.create();
	testingWorkflow.versionsIds = [testingWorkflowVersion.id];
	testingWorkflowVersion.workflowId = testingWorkflow.id;
}

// % protected region % [Add any additional variables here] off begin
// % protected region % [Add any additional variables here] end

/**
 * Spy on selector in Store
 */
function spySelectorsInStore()  {
	// Setup the Mock Store and fake selector
	store = TestBed.inject(Store) as MockStore<{ model: WorkflowVersionModelState }>;
	routerStore = TestBed.inject(Store) as MockStore<{ router: RouterState }>;

	workflowVersionModelState = initialWorkflowModelVersionState;
	store.setState({model: workflowVersionModelState});

	// Create Behavior Subjects to trigger later
	workflowVersionSubject = new BehaviorSubject(testingWorkflowVersion);
	workflowSubject = new BehaviorSubject(testingWorkflow);

	// Create spy on select function to return value
	spyOn(store, 'select')
		.withArgs(getWorkflowVersionModelWithId, testingWorkflowVersion.id).and.returnValue(workflowVersionSubject)
		.withArgs(getWorkflowModelWithId, testingWorkflow.id).and.returnValue(workflowSubject);
}

describe('Workflow Details Component', () => {
	beforeEach(waitForAsync (() => {
		activeRouter = new ActivatedRouteStub();

		TestBed.configureTestingModule({
			imports: [
				WorkflowEditModule,
				RouterTestingModule,
			],
			providers: [
				provideMockStore(),
				RouterLinkDirective,
				{
					provide: ActivatedRoute,
					useValue: activeRouter,
				}
			]
		}).compileComponents().then(() => {

			prepareTestingData();
			spySelectorsInStore();

			activeRouter.parent = new ActivatedRouteStub();
			activeRouter.parent.setParamMap({});
			fixture = TestBed.createComponent(WorkflowDetailsComponent);
			workflowDetailsComponent = fixture.debugElement.componentInstance;

			// % protected region % [Add any additional logic before each test for 'Workflow Details Component' here] off begin
			// % protected region % [Add any additional logic before each test for 'Workflow Details Component' here] end
		});
	}));

	afterEach(() => {
		// Need to do this since for some reason the last component queried from the fixture will be rendered on the
		// browser
		if (fixture.nativeElement instanceof HTMLElement) {
			(fixture.nativeElement as HTMLElement).remove();
		}

		// % protected region % [Add any additional logic after each test for 'Workflow Details Component' here] off begin
		// % protected region % [Add any additional logic after each test for 'Workflow Details Component' here] end
	});

	it('should have form with dom structure meets standard', () => {

		// % protected region % [Add any additional logic before main process of 'should have form with dom structure meets standard' here] off begin
		// % protected region % [Add any additional logic before main process of 'should have form with dom structure meets standard' here] end

		fixture.detectChanges();

		const workflowDetailEl = fixture.debugElement.query(By.css('.workflow__details form'));
		expect(workflowDetailEl).not.toBeNull('class name for workflow detail form is wrong');

		const textFieldEl: DebugElement[] = workflowDetailEl.queryAll(By.directive(TextfieldComponent));
		const textAreaEl = workflowDetailEl.queryAll(By.directive(TextareaComponent));
		const dropdownEl = workflowDetailEl.queryAll(By.directive(DropdownComponent));

		expect(textFieldEl.length).toBe(1, 'Should have text field for workflow name');
		expect(textAreaEl.length).toBe(1, 'Should have text area for workflow description');
		expect(dropdownEl.length).toBe(1, 'Should have dropdown for association entities');

		const workflowNameComponent: TextfieldComponent = textFieldEl[0].componentInstance;
		expect(workflowNameComponent.label).toBe('Workflow Name');
		expect(workflowNameComponent.name).toBe('Workflow Name');

		const workflowDescription: TextareaComponent = textAreaEl[0].componentInstance;
		expect(workflowDescription.label).toBe('Workflow Description');
		expect(workflowDescription.name).toBe('Workflow Description');

		const workflowEntities: DropdownComponent = dropdownEl[0].componentInstance;
		expect(workflowEntities.label).toBe('Entities');

		// % protected region % [Add any additional logic after main process of 'should have form with dom structure meets standard' here] off begin
		// % protected region % [Add any additional logic after main process of 'should have form with dom structure meets standard' here] end
	});

	// % protected region % [Add any tests for 'Workflow Details Component' here] off begin
	// % protected region % [Add any tests for 'Workflow Details Component' here] end
});

describe('Workflow Details Create Component', () => {
	// % protected region % [Add any additional variables for here 'Workflow Details Create Component'] off begin
	// % protected region % [Add any additional variables for here 'Workflow Details Create Component'] end

	beforeEach(waitForAsync (() => {
		activeRouter = new ActivatedRouteStub();

		TestBed.configureTestingModule({
			imports: [
				WorkflowEditModule,
				RouterTestingModule,
			],
			providers: [
				provideMockStore(),
				RouterLinkDirective,
				{
					provide: ActivatedRoute,
					useValue: activeRouter,
				}
			]
		}).compileComponents().then(() => {

			prepareTestingData();
			spySelectorsInStore();

			activeRouter.parent = new ActivatedRouteStub();
			activeRouter.parent.setParamMap({});
			fixture = TestBed.createComponent(WorkflowDetailsComponent);
			workflowDetailsComponent = fixture.debugElement.componentInstance;

			// % protected region % [Add any additional logic before each test for 'Workflow Details Create Component' here] off begin
			// % protected region % [Add any additional logic before each test for 'Workflow Details Create Component' here] end
		});
	}));

	afterEach(() => {
		// Need to do this since for some reason the last component queried from the fixture will be rendered on the
		// browser
		if (fixture.nativeElement instanceof HTMLElement) {
			(fixture.nativeElement as HTMLElement).remove();
		}

		// % protected region % [Add any additional logic after each test for 'Workflow Details Create Component' here] off begin
		// % protected region % [Add any additional logic after each test for 'Workflow Details Create Component' here] end
	});

	it('should create a new workflow and workflow version when creating workflow', () => {
		// % protected region % [Add any additional logic before main process of 'should create a new workflow and workflow version when creating workflow' here] off begin
		// % protected region % [Add any additional logic before main process of 'should create a new workflow and workflow version when creating workflow' here] end

		// Create Should not have any parameters
		fixture.detectChanges();
		let draftVersion: WorkflowVersionModel = workflowDetailsComponent.draftVersion;
		expect(draftVersion).not.toBeNull();
		expect(draftVersion.id).toBeUndefined();

		// % protected region % [Add any additional logic after main process of 'should create a new workflow and workflow version when creating workflow' here] off begin
		// % protected region % [Add any additional logic after main process of 'should create a new workflow and workflow version when creating workflow' here] end
	});

	// % protected region % [Add any additional tests for 'Workflow Details Create Component' here] off begin
	// % protected region % [Add any additional tests for 'Workflow Details Create Component' here] end
});

describe('Workflow Details Edit Component', () => {
	// % protected region % [Add any additional variables for 'Workflow Details Edit Component' here] off begin
	// % protected region % [Add any additional variables for 'Workflow Details Edit Component' here] end

	beforeEach(waitForAsync (() => {
		activeRouter = new ActivatedRouteStub();

		TestBed.configureTestingModule({
			imports: [
				WorkflowEditModule,
				RouterTestingModule,
			],
			providers: [
				provideMockStore(),
				RouterLinkDirective,
				{
					provide: ActivatedRoute,
					useValue: activeRouter,
				}
			]
		}).compileComponents().then(() => {

			prepareTestingData();
			spySelectorsInStore();

			activeRouter.parent = new ActivatedRouteStub();
			activeRouter.parent.setParamMap({
				versionId: testingWorkflowVersion.id
			});
			fixture = TestBed.createComponent(WorkflowDetailsComponent);
			workflowDetailsComponent = fixture.debugElement.componentInstance;

			// % protected region % [Add any additional logic before each test for 'Workflow Details Edit Component' here] off begin
			// % protected region % [Add any additional logic before each test for 'Workflow Details Edit Component' here] end
		});
	}));

	afterEach(() => {
		// Need to do this since for some reason the last component queried from the fixture will be rendered on the
		// browser
		if (fixture.nativeElement instanceof HTMLElement) {
			(fixture.nativeElement as HTMLElement).remove();
		}

		// % protected region % [Add any additional logic after each test for 'Workflow Details Edit Component' here] off begin
		// % protected region % [Add any additional logic after each test for 'Workflow Details Edit Component' here] end

	});

	it('should fetch the workflow from server according to version id in url', () => {
		// % protected region % [Add any additional logic before main process of 'should fetch the workflow from server according to version id in url' here] off begin
		// % protected region % [Add any additional logic before main process of 'should fetch the workflow from server according to version id in url' here] end

		fixture.detectChanges();
		let draftVersion: WorkflowVersionModel = workflowDetailsComponent.draftVersion;
		expect(draftVersion).not.toBeNull();
		expect(draftVersion.id).toBe(testingWorkflowVersion.id);

		// % protected region % [Add any additional logic after main process of 'should fetch the workflow from server according to version id in url' here] off begin
		// % protected region % [Add any additional logic after main process of 'should fetch the workflow from server according to version id in url' here] end
	});

	it('should just update workflow version when save draft', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should just update workflow version when save draft' here] off begin
		// % protected region % [Add any additional logic before main process of 'should just update workflow version when save draft' here] end

		fixture.detectChanges();

		// Update form
		workflowDetailsComponent.workflowDetailsForm.patchValue({
			workflowName: 'Testing Name',
			workflowDescription: 'Testing Description'
		});

		spyOn(store, 'dispatch').and.callThrough();

		workflowDetailsComponent.onSaveDraftClicked();

		const expectedAction = new versionAction.WorkflowVersionAction(
			versionAction.WorkflowVersionModelActionTypes.UPDATE_WORKFLOW_VERSION,
			{
				targetModel: workflowDetailsComponent.draftVersion,
				updates: workflowDetailsComponent.modelChanges
			},
			[
				new routerAction.NavigateRoutingAction(['states'],
					{
						relativeTo: activeRouter.parent
					}),
			]
		);

		const createVersionAction = new versionAction.WorkflowVersionAction(
			versionAction.WorkflowVersionModelActionTypes.CREATE_WORKFLOW_VERSION,
			{
				targetModel: workflowDetailsComponent.draftVersion,
			}
		);

		expect(store.dispatch).toHaveBeenCalledWith(expectedAction);
		expect(store.dispatch).not.toHaveBeenCalledWith(createVersionAction);

		// % protected region % [Add any additional logic after main process of 'should just update workflow version when save draft' here] off begin
		// % protected region % [Add any additional logic after main process of 'should just update workflow version when save draft' here] end
	}));

	it('should select associated entities', fakeAsync(() => {

		fixture.detectChanges();

		const updatedWorkflowVersion = workflowVersionDataFactory.create();

		const dropdown = fixture.debugElement.query(By.css('ng-select'));

		// Select / Unselect based on updated model
		if (updatedWorkflowVersion.articleAssociation) {
			DropdownTestingHelper.selectByText(dropdown, 'Article', fixture);
		} else {
			DropdownTestingHelper.unselectByText(dropdown, 'Article', fixture);
		}


		// Apply Changes
		workflowDetailsComponent.onSaveDraftClicked();

		// Verify all association attributes
		expect(workflowDetailsComponent.modelChanges['articleAssociation']).toBe(updatedWorkflowVersion.articleAssociation);

	}));


	// % protected region % [Add any additional tests for 'Workflow Details Edit Component' here] off begin
	// % protected region % [Add any additional tests for 'Workflow Details Edit Component' here] end
});

// % protected region % [Add any additional describes here] off begin
// % protected region % [Add any additional describes here] end
