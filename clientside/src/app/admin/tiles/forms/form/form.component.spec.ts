/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { ComponentFixture, waitForAsync, TestBed } from '@angular/core/testing';
import { FormComponent } from './form.component';
import { AuthenticationService } from 'src/app/lib/services/authentication/authentication.service';
import { CommonModule } from '@angular/common';
import { CommonComponentModule } from 'src/app/lib/components/common.component.module';
import { StoreModule } from '@ngrx/store';
import { reducers, clearState } from 'src/app/models/model.reducer';
import { initialRouterState, initialModelState } from 'src/app/models/model.state';
import { RouterTestingModule } from '@angular/router/testing';
import * as uuid from 'uuid';

import { LessonModel } from 'src/app/models/lesson/lesson.model';

class MockAuthenticationService {
	get isLoggedIn() {
		return true;
	}
}

const formEntities = [
	{name: 'Lesson', index: 0},
];

describe('Testing Form Tile Component', () => {
	let fixture: ComponentFixture<FormComponent>;
	let formComponent: FormComponent;

	let authenticationService: AuthenticationService;

	function createFormTestingComponent(entityName: string) {
		let model;
		switch (entityName) {
			case 'Lesson':
				model = new LessonModel();
				break;
		}
		model.id = uuid.v4();
		return model;
	}

	beforeEach(waitForAsync (() => {

		TestBed.configureTestingModule({
			imports: [
				CommonModule,
				FormsModule,
				ReactiveFormsModule,
				CommonComponentModule,
				RouterTestingModule,
				StoreModule.forRoot(reducers, {
					initialState: {
						router: initialRouterState,
						models: initialModelState,
					},
					metaReducers: [
						clearState,
					],
				}),
			],
			declarations: [
				FormComponent,
			],
			providers: [
				{
					provide: AuthenticationService,
					useClass: MockAuthenticationService
				}
			],
		}).compileComponents().then(() => {

			authenticationService = TestBed.inject(AuthenticationService);
			spyOnProperty(authenticationService, 'isLoggedIn').and.returnValue(true);

			fixture = TestBed.createComponent(FormComponent);
			formComponent = fixture.debugElement.componentInstance;
			fixture.detectChanges();
		});
	}));

	afterEach(() => {
		// Need to do this since for some reason the last component queried from the fixture will be rendered on the
		// browser
		if (fixture.nativeElement instanceof HTMLElement) {
			(fixture.nativeElement as HTMLElement).remove();
		}
	});

	it ('Creates Form Component', () => {
		expect(formComponent).toBeDefined();
	});

	formEntities.forEach(form => {
		const formNameKebabCase = form.name.toLowerCase().replace(' ', '-');

		it ('Renders ' + form.name + ' form type button', () => {
			const btn = fixture.debugElement.query(By.css('button#' + formNameKebabCase));

			expect(btn).toBeTruthy();
		});

		it ('Toggles form block visibility when clicking ' + form.name + ' form type button', () => {
			const btn = fixture.debugElement.query(By.css('button#' + formNameKebabCase));
			const accordion = fixture.debugElement.query(By.css('div.' + formNameKebabCase + '-accordion'));

			expect(accordion.nativeElement).toHaveClass('accordion__info--collapsed');
			expect(accordion.nativeElement).not.toHaveClass('accordion__info--expanded');

			btn.nativeElement.click();
			fixture.detectChanges();

			expect(accordion.nativeElement).not.toHaveClass('accordion__info--collapsed');
			expect(accordion.nativeElement).toHaveClass('accordion__info--expanded');
		});

		it ('Renders the New Form button and has the correct href for the ' + form.name + ' Form', () => {
			const btn = fixture.debugElement.query(By.css('button#' + formNameKebabCase));

			btn.nativeElement.click();
			fixture.detectChanges();

			const newFormButton = fixture.debugElement.query(By.css('a.' + formNameKebabCase + '-new-form'));

			expect(newFormButton).toBeTruthy();
			expect(newFormButton.nativeElement.href).toContain('/admin/entities/' + formNameKebabCase + '/create');
		});

		it ('Does not render additional forms in the ' + form.name + ' form list when none are present', () => {
			fixture.detectChanges();

			const btn = fixture.debugElement.query(By.css('button#' + formNameKebabCase));

			btn.nativeElement.click();
			fixture.detectChanges();

			const instances = fixture.debugElement.queryAll(By.css('div.form-item'));
			expect(instances.length).toBe(0);
		});

		it ('Renders additional forms in the ' + form.name + ' form list', () => {
			formComponent.forms[form.index].instances = [createFormTestingComponent(form.name)];
			fixture.detectChanges();

			const btn = fixture.debugElement.query(By.css('button#' + formNameKebabCase));

			btn.nativeElement.click();
			fixture.detectChanges();
		});
	});
});
