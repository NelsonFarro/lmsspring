/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {appRoutes} from './forms-tile.routes';
import {CommonComponentModule} from '../../../lib/components/common.component.module';
import {FormSlideBuilderComponent} from './form-slide-builder/form-slide-builder.component';
import {FormComponent} from './form/form.component';
import {FormWrapperComponent} from './form-wrapper/form-wrapper.component';
import {CommonPipeModule} from '../../../lib/pipes/common.pipe.module';
import {FormBehaviourModule} from '../../../lib/behaviours/form/form-behaviour.module';
import {FormSlideBuilderQuestionSidebarComponent} from './form-slide-builder/question-sidebar/form-slide-builder-question-sidebar.component';
import {SlideEditSidebarComponent} from './form-slide-builder/slide-edit-sidebar/slide-edit-sidebar.component';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

@NgModule({
	declarations: [
		FormSlideBuilderComponent,
		FormSlideBuilderQuestionSidebarComponent,
		SlideEditSidebarComponent,
		FormComponent,
		FormWrapperComponent,
		// % protected region % [Add any additional declaration here] off begin
		// % protected region % [Add any additional declaration here] end
	],
	imports: [
		RouterModule.forChild(appRoutes),
		CommonModule,
		CommonComponentModule,
		FormsModule,
		ReactiveFormsModule,
		CommonPipeModule,
		FormBehaviourModule,
		// % protected region % [Add any additional module imports here] off begin
		// % protected region % [Add any additional module imports here] end
	],
	exports: [
		FormSlideBuilderComponent,
		FormComponent,
		FormWrapperComponent,
		// % protected region % [Add any additional exports here] off begin
		// % protected region % [Add any additional exports here] end
	],
	providers: [
		// % protected region % [Add any additional providers here] off begin
		// % protected region % [Add any additional providers here] end
	],
	entryComponents: [
		// % protected region % [Add any additional entry components here] off begin
		// % protected region % [Add any additional entry components here] end
	],
	// % protected region % [Add any additional module configurations here] off begin
	// % protected region % [Add any additional module configurations here] end
})
export class FormsTileModule {
	// % protected region % [Add any module class logic here] off begin
	// % protected region % [Add any module class logic here] end
}
