/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {
	Component,
	EventEmitter,
	Input, OnChanges,
	Output, SimpleChanges,
} from '@angular/core';
import {AbstractComponent} from 'src/app/lib/components/abstract.component';
import {
	ButtonStyle,
	ButtonSize,
	IconPosition,
	ButtonAccentColour
} from 'src/app/lib/components/button/button.component';
import {TextfieldType} from 'src/app/lib/components/textfield/textfield.component';
import {FormSlideData} from 'src/app/lib/behaviours/form/form-interfaces';
import {ButtonGroupAlignment} from 'src/app/lib/components/buttonGroup/button.group.component';
import * as _ from 'lodash';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * Sidebar in Form slide builder to edit slide configuration
 */
@Component({
	selector: '*[cb-form-slide-builder-slide-edit-sidebar]',
	templateUrl: './slide-edit-sidebar.component.html',
	// % protected region % [Add any additional configurations here] off begin
	// % protected region % [Add any additional configurations here] end
})
export class SlideEditSidebarComponent extends AbstractComponent implements OnChanges {
	readonly TextfieldType = TextfieldType;
	readonly IconPosition = IconPosition;
	readonly ButtonSize = ButtonSize;
	readonly ButtonStyle = ButtonStyle;
	readonly ButtonAccentColour = ButtonAccentColour;
	readonly ButtonGroupAlignment = ButtonGroupAlignment;

	@Output()
	close: EventEmitter<null> = new EventEmitter();

	/**
	 * The slide to be edited.
	 */
	@Input()
	slide: FormSlideData;

	/**
	 * The previous slide data before save button clicked
	 */
	previousSlideData: FormSlideData;

	// % protected region % [Add any additional class properties here] off begin
	// % protected region % [Add any additional class properties here] end

	constructor(
		// % protected region % [Add any additional constructor parameters here] off begin
		// % protected region % [Add any additional constructor parameters here] end
	) {
		super();
	}

	/**
	 * @inheritDoc
	 */
	ngOnChanges(changes: SimpleChanges): void {
		if (changes.hasOwnProperty('slide')) {
			this.previousSlideData = _.cloneDeep(changes.slide.currentValue);
		}
	}


	/**
	 * Event triggered when save button clicked
	 */
	onSaveButtonClicked() {
		this.close.emit(null);
	}

	/**
	 * Event triggered when cancel button clicked
	 */
	onCloseButtonClicked() {
		Object.assign(this.slide, this.previousSlideData);
		this.close.emit(null);
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
