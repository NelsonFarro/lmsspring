/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Component, HostBinding, Input, TemplateRef, ViewChild, ViewContainerRef} from '@angular/core';
import {fromEvent, Subject, Subscription} from 'rxjs';
import {FormControl} from '@angular/forms';
import {Overlay, OverlayRef} from '@angular/cdk/overlay';
import {TemplatePortal} from '@angular/cdk/portal';
import {filter, take} from 'rxjs/operators';
import {AbstractComponent} from '../../../../lib/components/abstract.component';
import {ButtonStyle, ButtonSize, IconPosition} from '../../../../lib/components/button/button.component';
import {TextfieldType} from '../../../../lib/components/textfield/textfield.component';
import {FormMode, FormQuestionData, FormSlideData} from '../../../../lib/behaviours/form/form-interfaces';
import * as uuid from 'uuid';
import * as _ from 'lodash';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

@Component({
	selector: 'section[cb-form-slide-builder]',
	templateUrl: './form-slide-builder.component.html'
})
export class FormSlideBuilderComponent extends AbstractComponent {
	readonly TextfieldType = TextfieldType;
	readonly IconPosition = IconPosition;
	readonly ButtonSize = ButtonSize;
	readonly ButtonStyle = ButtonStyle;

	sideMenuMode: 'question-edit' | 'slide-view' | 'slide-edit' = 'slide-view';

	@ViewChild('slideMenu', {static: false})
	slideMenu: TemplateRef<any>;

	overlayRef: OverlayRef;

	/**
	 * Default host class.
	 */
	@HostBinding('class')
	get formSlideBuilderCSSClass() {
		const defaultClasses = [
			this.className,
			// % protected region % [Add any additional host classes here] off begin
			// % protected region % [Add any additional host classes here] end
		];

		switch (this.formMode) {
			case 'build':
				defaultClasses.push('slide-builder');
				// % protected region % [Add any additional host classes for build mode here] off begin
				// % protected region % [Add any additional host classes for build mode here] end
				break;

			case 'report':
				defaultClasses.push('forms-preview');
				// % protected region % [Add any additional host classes for report mode here] off begin
				// % protected region % [Add any additional host classes for report mode here] end
				break;
		}

		return defaultClasses.join(' ');
	}

	@HostBinding('attr.aria-label')
	get formSlideBuilderCSSAria() {
		return [
			'slide-builder',
			// % protected region % [Add any additional aria label here] off begin
			// % protected region % [Add any additional aria label here] end
		].join(' ');
	}

	/**
	 * Slides to be displayed which also includes nested questions.
	 */
	@Input()
	slidesData: FormSlideData[] = [];

	@Input()
	formMode: FormMode;

	/**
	 * The current question to be edited. Used for the sidebar in question edit mode.
	 */
	currentQuestion: FormQuestionData;

	/**
	 * Subject for changing of question type
	 */
	questionTypeSubject = new Subject<string>();

	/**
	 * The current slide to be edited. Used for the sidebar in slide edit mode.
	 */
	currentSlide: FormSlideData;

	/**
	 * Used to handle outside clicking of the menu.
	 */
	slideMenuSub: Subscription;

	// % protected region % [Add any additional class properties here] off begin
	// % protected region % [Add any additional class properties here] end

	constructor(
		private readonly overlay: Overlay,
		private readonly viewContainerRef: ViewContainerRef,
		// % protected region % [Add any additional constructor parameters here] off begin
		// % protected region % [Add any additional constructor parameters here] end
	) {
		super(
			// % protected region % [Add any additional constructor arguments here] off begin
			// % protected region % [Add any additional constructor arguments here] end
		);

		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}

	/**
	 * Triggered on when the user wants to create a new slide.
	 */
	onNewSlide() {
		// % protected region % [Add any additional logic before main body of onNewSlide here] off begin
		// % protected region % [Add any additional logic before main body of onNewSlide here] end

		this.slidesData.push({
			id: uuid.v4(),
			order: this.slidesData.length,
			data: {
				name: 'New Slide'
			},
			questionsData: []
		});

		// % protected region % [Add any additional logic after main body of onNewSlide here] off begin
		// % protected region % [Add any additional logic after main body of onNewSlide here] end
	}

	/**
	 * Triggered when the user wants to edit a question
	 */
	onEditQuestion($event: {questionData: FormQuestionData, questionTypeSubject: Subject<string>}) {
		// % protected region % [Add any additional logic before main body of onEditQuestion here] off begin
		// % protected region % [Add any additional logic before main body of onEditQuestion here] end

		this.currentQuestion = $event.questionData;
		this.questionTypeSubject = $event.questionTypeSubject;
		this.sideMenuMode = 'question-edit';

		// % protected region % [Add any additional logic after main body of onEditQuestion here] off begin
		// % protected region % [Add any additional logic after main body of onEditQuestion here] end
	}

	/**
	 * Triggered when the user wants to edit a slide.
	 */
	onEditSlide(slideData: FormSlideData) {
		// % protected region % [Add any additional logic before main body of onEditonEditSlideQuestion here] off begin
		// % protected region % [Add any additional logic before main body of onEditonEditSlideQuestion here] end

		this.closeSlideMenu();
		this.currentSlide = slideData;
		this.sideMenuMode = 'slide-edit';

		// % protected region % [Add any additional logic after main body of onEditonEditSlideQuestion here] off begin
		// % protected region % [Add any additional logic after main body of onEditonEditSlideQuestion here] end
	}

	/**
	 * Duplicate the slide
	 */
	onDuplicateSlide(slideData: FormSlideData) {
		// % protected region % [Add any additional logic before main body of onDuplicateSlide here] off begin
		// % protected region % [Add any additional logic before main body of onDuplicateSlide here] end

		const duplicatedSlide = _.cloneDeep(slideData);
		duplicatedSlide.id = uuid.v4();
		duplicatedSlide.questionsData.forEach(
			questionData => {
				questionData.id = uuid.v4();
			}
		);
		duplicatedSlide.order = this.slidesData.push(duplicatedSlide);

		// % protected region % [Add any additional logic after main body of onDuplicateSlide here] off begin
		// % protected region % [Add any additional logic after main body of onDuplicateSlide here] end
	}

	/**
	 * Triggered when the user want to delete a slide
	 */
	onDeleteSlide(slideData: FormSlideData) {
		// % protected region % [Add any additional logic before main body of onDeleteSlide here] off begin
		// % protected region % [Add any additional logic before main body of onDeleteSlide here] end

		_.remove(this.slidesData, (slide) => slide.id === slideData.id);

		// % protected region % [Add any additional logic after main body of onDeleteSlide here] off begin
		// % protected region % [Add any additional logic after main body of onDeleteSlide here] end
	}

	/**
	 * Used to close slide menu when click outside or open elsewhere.
	 */
	closeSlideMenu() {
		// % protected region % [Add any additional logic before main body of closeSlideMenu here] off begin
		// % protected region % [Add any additional logic before main body of closeSlideMenu here] end

		if (this.slideMenuSub) {
			this.slideMenuSub.unsubscribe();
		}
		if (this.overlayRef) {
			this.overlayRef.dispose();
			this.overlayRef = null;
		}

		// % protected region % [Add any additional logic after main body of closeSlideMenu here] off begin
		// % protected region % [Add any additional logic after main body of closeSlideMenu here] end
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
