/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Component, Input, OnChanges, SimpleChanges, OnInit} from '@angular/core';
import {map} from 'rxjs/operators';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {AbstractComponent} from '../../../../../lib/components/abstract.component';
import {LessonFormSubmissionModel} from '../../../../../models/lessonFormSubmission/lesson_form_submission.model';
import {LessonModel} from '../../../../../models/lesson/lesson.model';
import {LessonFormVersionModel} from '../../../../../models/lessonFormVersion/lesson_form_version.model';
import {FormMode, FormSlideData} from '../../../../../lib/behaviours/form/form-interfaces';
import {getLessonFormVersionModelWithId} from '../../../../../models/lessonFormVersion/lesson_form_version.model.selector';
import * as versionModelAction from '../../../../../models/lessonFormVersion/lesson_form_version.model.action';
import {ButtonStyle} from '../../../../../lib/components/button/button.component';
import {RouterState} from '../../../../../models/model.state';
import {getRouterState} from '../../../../../models/model.selector';
import {extractSubmissionData, parseSubmissionData} from '../../../../../lib/behaviours/form/form-utils';
import { v4 as uuidv4 } from 'uuid';
import * as submissionModelAction from 'src/app/models/lessonFormSubmission/lesson_form_submission.model.action';
import { getLessonFormSubmissionModelWithId } from 'src/app/models/lessonFormSubmission/lesson_form_submission.model.selector';
import {NavigateRoutingAction} from '../../../../../lib/routing/routing.action';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

@Component({
	selector: 'cb-lesson-form-submission-form-submission, section[cb-lesson-form-submission-form-submission]',
	templateUrl: './lesson-form-submission-form-submission.component.html',
	// % protected region % [Add any additional component configurations here] off begin
	// % protected region % [Add any additional component configurations here] end
})
export class LessonFormSubmissionFormSubmissionComponent extends AbstractComponent implements OnInit {
	readonly ButtonStyle = ButtonStyle;

	@Input()
	targetModelId: string;

	model: LessonFormSubmissionModel;

	form: LessonModel;

	currentVersion: LessonFormVersionModel;

	currentVersion$: Observable<LessonFormVersionModel>;

	slidesData: FormSlideData[];

	formMode: FormMode = 'report';

	// % protected region % [Add any additional fields here] off begin
	// % protected region % [Add any additional fields here] end

	/**
	 * @inheritDoc
	 */
	constructor(
		private readonly routerStore: Store<{ router: RouterState }>,
		private readonly activatedRoute: ActivatedRoute,
		private readonly store: Store<{model: LessonFormVersionModel}>,
		private readonly submissionStore: Store<{model: LessonFormSubmissionModel}>,
		// % protected region % [Add any additional constructor parameters here] off begin
		// % protected region % [Add any additional constructor parameters here] end
	) {
		super();
		this.routerStore.select(getRouterState).subscribe(routerState => {
			if (routerState.url.includes('view')) {
				this.formMode = 'report';
			} else {
				this.formMode = 'response';
			}

			this.targetModelId = routerState.params.id;
		});

		// % protected region % [Add any additional consturctor logic here] off begin
		// % protected region % [Add any additional consturctor logic here] end
	}

	ngOnInit() {


		this.store.dispatch(new submissionModelAction.LessonFormSubmissionAction(
			submissionModelAction.LessonFormSubmissionModelActionTypes.FETCH_LESSON_FORM_SUBMISSION,
			{
				targetModelId: this.targetModelId,
				queryParams: {
					expands: [{
						name: 'submittedForm',
						fields: ['id']
					}]
				}
			}
		));


		this.submissionStore.select(getLessonFormSubmissionModelWithId, this.targetModelId).subscribe(model => {
			this.model = model;
			this.updateFormModel(model);
		});
	}

	updateFormModel(submissionModel: LessonFormSubmissionModel) {
		// % protected region % [Add any additional logic before main process of updateFormModel here] off begin
		// % protected region % [Add any additional logic before main process of updateFormModel here] end

		if (!(submissionModel && submissionModel.submittedFormId)) {
			return;
		}

		const formVersionId = submissionModel.submittedFormId;
		this.store.dispatch(new versionModelAction.LessonFormVersionAction(
			versionModelAction.LessonFormVersionModelActionTypes.FETCH_LESSON_FORM_VERSION,
			{
				targetModelId: formVersionId,
				// % protected region % [Add any config here to fetch form version model] off begin
				// % protected region % [Add any config here to fetch form version model] end
			}
		));

		this.currentVersion$ = this.store.select(getLessonFormVersionModelWithId, formVersionId);

		this.currentVersion$.pipe(
			map(
				formVersion => {

					const formData: FormSlideData[] = parseSubmissionData(submissionModel.submissionData, formVersion.formData);
					return formData;
				}
			),
			// % protected region % [Add any additional pipes for currentVerson$] off begin
			// % protected region % [Add any additional pipes for currentVerson$] end
		).subscribe(slidesData => {
			this.slidesData = slidesData;
			// % protected region % [Add any additional logic in subscription of currentVerson$] off begin
			// % protected region % [Add any additional logic in subscription of currentVerson$] end
		});

		// % protected region % [Add any additional logic after main process of updateFormModel here] off begin
		// % protected region % [Add any additional logic after main process of updateFormModel here] end
	}

	onSaveButtonClicked() {
		const submissionData = extractSubmissionData(this.slidesData);
		this.store.dispatch(new submissionModelAction.LessonFormSubmissionAction(
			submissionModelAction.LessonFormSubmissionModelActionTypes.UPDATE_LESSON_FORM_SUBMISSION,
			{
				targetModel: this.model,
				updates: {
					submissionData: JSON.stringify(submissionData),
				}
			},
		// % protected region % [Add any additional logic after updating success here] off begin
		// % protected region % [Add any additional logic after updating success here] end
		));

		this.store.dispatch(new NavigateRoutingAction(['../..'], {
			relativeTo: this.activatedRoute,
			// % protected region % [Add any additional route options] off begin
			// % protected region % [Add any additional route options] end
		}));
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
