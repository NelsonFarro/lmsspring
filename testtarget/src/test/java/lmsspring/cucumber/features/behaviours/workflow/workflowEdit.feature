###
# @bot-written
#
# WARNING AND NOTICE
# Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
# Full Software Licence as accepted by you before being granted access to this source code and other materials,
# the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
# commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
# licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
# including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
# access, download, storage, and/or use of this source code.
#
# BOT WARNING
# This file is bot-written.
# Any changes out side of "protected regions" will be lost next time the bot makes any changes.
###

@botwritten
@behaviour @behaviour-workflow @workflow @edit
Feature: Workflow Edit

	Background:
		Given I navigate to the login page
			And I login with username "administrator@example.com" with password "password"
			And I see the homepage
			And I navigate to the workflow page in admin section
			And I click create button in workflow list page

	@workflow-version
	Scenario: Edit workflow version
		Given I create a new workflow version with id "edit-workflow-version-version"
			And I navigate to the workflow page in admin section
			And I navigate to the edit page for the workflow version with id "edit-workflow-version-version"
		When I update the workflow version with id "edit-workflow-version-version"
			And I navigate to the workflow page in admin section
			And I see the workflow entity with id "edit-workflow-version-version" in the list
			And I navigate to the edit page for the workflow version with id "edit-workflow-version-version"
		Then The workflow with id "edit-workflow-version-version" is correct

	@workflow-version @workflow-states
	Scenario: Edit a workflow state
		Given I create a new workflow version with id "workflow-states-edit-version"
			And I navigate to the workflow page in admin section
			And I navigate to the edit page for the workflow version with id "workflow-states-edit-version"
			And I click on "States" tab in workflow builder
			And I create 1 new workflow states for workflow version with id "workflow-states-edit-version"
			And I refresh the page
			And I click on "States" tab in workflow builder
			And The workflow states are correct for the workflow version with id "workflow-states-edit-version"
			And I refresh the page
		When I edit an existing workflow state for the workflow version with id "workflow-states-edit-version"
			And I refresh the page
		Then The workflow states are correct for the workflow version with id "workflow-states-edit-version"

	@workflow-version @workflow-states @workflow-transitions
	Scenario: Edit transition
		Given I create a new workflow version with id "workflow-version-with-states-and-transitions-edit"
			And I create 3 new workflow states for workflow version with id "workflow-version-with-states-and-transitions-edit"
			And I have a start state
			And I create sequential transitions for the existing states for the workflow version with id "workflow-version-with-states-and-transitions-edit"
			And I refresh the page
			And I navigate to the workflow page in admin section
			And I navigate to the edit page for the workflow version with id "workflow-version-with-states-and-transitions-edit"
			And I click on "States" tab in workflow builder
			And The workflow states are correct for the workflow version with id "workflow-version-with-states-and-transitions-edit"
			And I refresh the page
		When I edit the existing workflow transitions for the workflow version with id "workflow-version-with-states-and-transitions-edit"
		Then The workflow states are correct for the workflow version with id "workflow-version-with-states-and-transitions-edit"
			And The transitions are correct for the workflow version with id "workflow-version-with-states-and-transitions-edit"

# % protected region % [Add additional scenarios for Workflow Create feature] off begin
# % protected region % [Add additional scenarios for Workflow Create feature] end

