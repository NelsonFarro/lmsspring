###
# @bot-written
#
# WARNING AND NOTICE
# Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
# Full Software Licence as accepted by you before being granted access to this source code and other materials,
# the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
# commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
# licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
# including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
# access, download, storage, and/or use of this source code.
#
# BOT WARNING
# This file is bot-written.
# Any changes out side of "protected regions" will be lost next time the bot makes any changes.
###

@botwritten
@behaviour @behaviour-workflow @workflow @delete
Feature: Workflow Delete

	Background:
		Given I navigate to the login page
			And I login with username "administrator@example.com" with password "password"
			And I see the homepage
			And I navigate to the workflow page in admin section
			And I click create button in workflow list page

	@workflow-version @workflow-states @workflow-transitions
	Scenario: Delete transition
		Given I create a new workflow version with id "workflow-version-with-states-and-transitions-delete"
			And I create 3 new workflow states for workflow version with id "workflow-version-with-states-and-transitions-delete"
			And I have a start state
			And I create sequential transitions for the existing states for the workflow version with id "workflow-version-with-states-and-transitions-delete"
			And I refresh the page
			And I navigate to the workflow page in admin section
			And I navigate to the edit page for the workflow version with id "workflow-version-with-states-and-transitions-delete"
			And I click on "States" tab in workflow builder
			And The workflow states are correct for the workflow version with id "workflow-version-with-states-and-transitions-delete"
		When I delete the existing workflow transitions for the workflow version with id "workflow-version-with-states-and-transitions-delete"
			And I refresh the page
		Then The workflow states are correct for the workflow version with id "workflow-version-with-states-and-transitions-delete"
			And The transitions are correct for the workflow version with id "workflow-version-with-states-and-transitions-delete"

	@workflow-version @workflow-states @state-migration @article
	Scenario: Migrate Article to Default State
		Given I create a new workflow version with id "workflow-default-state-migration" and entity association "Article"
			And I create 3 new workflow states for workflow version with id "workflow-default-state-migration"
			And I have a start state
			And I create sequential transitions for the existing states for the workflow version with id "workflow-default-state-migration"
			And I navigate to the "Article" backend page
			And I create a valid "Article"
		When I update the state of the workflow version with id "workflow-default-state-migration" connected to the "Article" entity
			And I navigate to the workflow page in admin section
			And I navigate to the edit page for the workflow version with id "workflow-default-state-migration"
			And I click on "States" tab in workflow builder
			And The workflow states are correct for the workflow version with id "workflow-default-state-migration"
			And I refresh the page
			And I delete the existing workflow transitions for the workflow version with id "workflow-default-state-migration"
			And I delete all states associated with the workflow version with id "workflow-default-state-migration" except for the starting state
		Then The start state of the workflow version with id "workflow-default-state-migration" connected to the "Article" entity is active

	@workflow-version @workflow-states @article
	Scenario: Unassign a Workflow from Article entity
		Given I create a new workflow version with id "unassign-workflow-from-entity" and entity association "Article"
			And I create 3 new workflow states for workflow version with id "unassign-workflow-from-entity"
			And I have a start state
			And I create sequential transitions for the existing states for the workflow version with id "unassign-workflow-from-entity"
			And I navigate to the "Article" backend page
			And I create a valid "Article"
		When I navigate to the workflow page in admin section
			And I navigate to the edit page for the workflow version with id "unassign-workflow-from-entity"
			And I unassign entities from the workflow version with id "unassign-workflow-from-entity"
		Then the workflow version with id "unassign-workflow-from-entity" is not present for the "Article" entity
	
	
# % protected region % [Add additional scenarios for Workflow Delete feature] off begin
# % protected region % [Add additional scenarios for Workflow Delete feature] end

