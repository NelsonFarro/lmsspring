/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.cucumber.pom.admin.workflow.workflowcreateedit.workflowstate;

import org.openqa.selenium.support.ui.WebDriverWait;
import lmsspring.cucumber.pom.admin.workflow.workflowcreateedit.workflowstate.elements.StateBlockElement;
import lmsspring.cucumber.pom.admin.workflow.workflowcreateedit.workflowstate.elements.TransitionSlideOutElement;
import lmsspring.cucumber.pom.pages.AbstractPage;
import lmsspring.cucumber.utils.*;
import lmsspring.cucumber.pom.enums.VisibilityEnum;
import lmsspring.entities.WorkflowStateEntity;
import lmsspring.entities.WorkflowTransitionEntity;
import com.google.inject.Inject;
import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.extern.slf4j.Slf4j;
import lombok.Getter;
import org.apache.commons.lang3.RandomUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import java.util.*;

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * Page Object Model for Workflow States Page
 */
@Slf4j
@ScenarioScoped
public class WorkflowStatesPage extends AbstractPage {

	private final static String[] entityNamesWithBehaviourWorkflow = { "Article" };

	/**
	 * Text field of step name in a state step
	 */
	private final static String stepNameFieldXpath = ".//input[@name='Step Name']";

	/**
	 * Start state button of a state
	 */
	private final static String startStateButtonXpath = ".//button[contains(@class, 'workflow__start-state')]";

	/**
	 * Div of workflow state
	 */
	private final static String workflowStateBlockXpath = "//div[contains(@class, 'workflow__states-step')]";

	@Getter
	@FindBy(xpath = workflowStateBlockXpath)
	private List<StateBlockElement> allWorkflowStates;

	@Getter
	@FindBy(xpath = "//div[contains(@class, 'workflow__states-step') and not(@data-id)]")
	private List<StateBlockElement> newWorkflowStates;

	@Getter
	@FindBy(xpath = "//div[contains(@class, 'workflow__states-step') and @data-id]")
	private List<StateBlockElement> existingWorkflowStates;

	@Getter
	@FindBy(xpath = "//div[contains(@class, 'workflow__new-state')]/button[contains(@class, 'icon-plus')]")
	private WebElement addStateButton;

	@Getter
	@FindBy(xpath = "//button[normalize-space()='Save Draft']")
	private WebElement saveButton;

	@Getter
	@FindBy(xpath = "//button[contains(@class, 'workflow__start-state') and contains(@class, 'btn--solid')]")
	private WebElement startStateButton;

	@Getter
	@FindBy(xpath = "//h2[normalize-space()='Edit workflow']")
	private WebElement editWorkflowHeading;

	@Getter
	@FindBy(className = "workflow-properties")
	private TransitionSlideOutElement transitionSlideOutElement;

	// % protected region % [Add any additional fields here] off begin
	// % protected region % [Add any additional fields here] end

	@Inject
	public WorkflowStatesPage(
			// % protected region % [Add any additional constructor parameters here] off begin
			// % protected region % [Add any additional constructor parameters here] end
			WebDriver webDriver,
			Properties properties
	) {
		super(webDriver, properties, "edit/{{workflowVersionId}}/states");

		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}

	public void clickSave() {
		this.saveButton.click();
		WaitUtils.waitForPage(webDriver);
	}

	/**
	 * Verify dom structure of states tab in workflow builder
	 */
	public void verifyDomStructure() {

		log.debug("Verifying dom structure of workflow states tab");

		Assert.assertNotNull(addStateButton);

		log.debug("Verified dom structure of workflow states tab");
	}

	/**
	 * Wait for page loading all workflow items
	 */
	public void waitForWorkflowItemsLoaded() {
		log.debug("Waiting for all workflow loaded");

		try {
			WaitUtils.waitForElement(webDriver, By.xpath(workflowStateBlockXpath), VisibilityEnum.VISIBLE);

			// % protected region % [Add any additional logic for 'waitForWorkflowItemsLoaded' here] off begin
			// % protected region % [Add any additional logic for 'waitForWorkflowItemsLoaded' here] end
		} catch (Exception except) {
			log.debug("No workflow item exists in the list");
		}
	}

	/**
	 * Check whether any workflow state items exist in workflow list page
	 */
	public boolean hasWorkflowStateItem() {
		waitForWorkflowItemsLoaded();
		return !allWorkflowStates.isEmpty();
		// % protected region % [Add any additional logic for 'hasWorkflowItem' here] off begin
		// % protected region % [Add any additional logic for 'hasWorkflowItem' here] end
	}

	/**
	 * Get the id of workflow state id from page by the index in list
	 * @param index Index of workflow state in the list
	 * @return Id of workflow state
	 */
	public String getWorkflowStateIdFromPageByIndex(int index) {
		var workflowStateBlock = allWorkflowStates.get(index);
		return workflowStateBlock.getAttribute("data-id");
	}

	/**
	 * Get a random existing workflow state from the page
	 * @return Id of a random existing workflow state.
	 */
	public String getWorkflowStateId() {
		int index = RandomUtils.nextInt(0, allWorkflowStates.size());

		return getWorkflowStateIdFromPageByIndex(index);
	}

	/**
	 * Find block element of a workflow state by its id
	 * @param workflowStateId Id of workflow state to find
	 * @return Div element of the workflow state to find
	 */
	public StateBlockElement findWorkflowStateBlockById(String workflowStateId) {
		this.waitForWorkflowItemsLoaded();
		this.clearAndWaitForToastNotifications();

		for (int i = 0; i < this.getExistingWorkflowStates().size(); i++) {
			if (this.getExistingWorkflowStates().get(i).getStateId().equals(workflowStateId)) {
				return this.getExistingWorkflowStates().get(i);
			}
		}

		throw new NoSuchElementException(String.format("Could not find workflow state with id %s", workflowStateId));
	}

	/**
	 * Fill all the fields in workflow states and save it
	 * @param workflowStateEl Workflow State div to fill with
	 * @param workflowStateEntity Workflow state entity to fill with
	 */
	private void fillWorkflowStateFormAndSave(WebElement workflowStateEl, WorkflowStateEntity workflowStateEntity) {
		this.fillWorkflowStateForm(workflowStateEl, workflowStateEntity);
		MouseClickUtils.clickOnElement(webDriver, saveButton);
		ModalUtils.waitForModalPresent(webDriver);
	}

	/**
	 * Fill all the fields in workflow states and save it
	 * @param workflowStateEl Workflow State div to fill with
	 * @param workflowStateEntity Workflow state entity to fill with
	 */
	private void fillWorkflowStateForm(WebElement workflowStateEl, WorkflowStateEntity workflowStateEntity) {
		WebElement stepNameField = workflowStateEl.findElement(By.xpath(stepNameFieldXpath));
		TypingUtils.clearAndType(webDriver, stepNameField, workflowStateEntity.getStepName());
		clickStartStateIfRequired(workflowStateEl, workflowStateEntity);
	}

	public void createNewWorkflowState(WorkflowStateEntity workflowStateEntity) {
		this.createNewWorkflowState(workflowStateEntity, true);
	}

	/**
	 * Create new workflow state
	 */
	public void createNewWorkflowState(WorkflowStateEntity workflowStateEntity, boolean save) {
		log.debug("Creating new workflow state");

		try {
			MouseClickUtils.clickOnElement(webDriver, addStateButton);

			// Get next empty form field
			WebElement newWorkflowStateEl = newWorkflowStates.stream()
					.filter(state -> state.findElement(By.xpath(stepNameFieldXpath)).getAttribute("value").isBlank())
					.findFirst().orElseThrow();

			if (save) {
				fillWorkflowStateFormAndSave(newWorkflowStateEl, workflowStateEntity);
			} else {
				fillWorkflowStateForm(newWorkflowStateEl, workflowStateEntity);
			}

		} catch (Exception except) {
			log.error("Could not create a new workflow state", except);
		}

		log.debug("New workflow state created");
	}

	/**
	 * Given an existing workflow state, find it in workflow states tab and update it
	 * @param workflowState Workflow state to update
	 */
	public void updateWorkflowState(WorkflowStateEntity workflowState) {
		log.debug("Updating existing workflow state");

		WebElement workflowStateEl = findWorkflowStateBlockById(workflowState.getId().toString());

		fillWorkflowStateFormAndSave(workflowStateEl, workflowState);

		log.debug("Updated workflow state");
	}

	/**
	 * Whether already has a workflow state.
	 *
	 * Uses {@link #hasStartState()} when wait is false.
	 *
	 * @param wait Set whether or not we wait to see if the start state becomes available
	 * @return Already has a start state
	 */
	public boolean hasStartState(boolean wait) {
		if (wait) {
			try {
				WaitUtils.waitForElement(webDriver, this.startStateButton, VisibilityEnum.VISIBLE);
				return true;
			} catch (Exception e) {
				return false;
			}
		} else {
			return this.hasStartState();
		}
	}

	/**
	 * Whether already has a workflow state.
	 * We do not wait for the start state to be available as we might in {@link #hasStartState(boolean wait)}
	 * @return Already has a start state
	 */
	public boolean hasStartState() {
		return allWorkflowStates.stream()
				.anyMatch(StateBlockElement::isStartState);
	}

	/**
	 * Turn on is start state if no start states.
	 * @param newWorkflowStateEl Block element of the new workflow state
	 * @param workflowState workflow state to update
	 */
	private void clickStartStateIfRequired(WebElement newWorkflowStateEl, WorkflowStateEntity workflowState) {

		if (!hasStartState()) {
			workflowState.setIsStartState(true);
			WebElement startButton = newWorkflowStateEl.findElement(By.xpath(startStateButtonXpath));
			MouseClickUtils.clickOnElement(webDriver, startButton);
		}
	}

	/**
	 * Check whether workflow is exist in the page
	 * @param workflowStateEntity workflow state to check whether exists in the page
	 */
	public void verifyStateExistsInPage(WorkflowStateEntity workflowStateEntity) {
		try {
			WaitUtils.waitForElement(webDriver, By.xpath("//div[contains(@class, 'workflow__states-step')]"), VisibilityEnum.VISIBLE);
			boolean workflowStateExists = allWorkflowStates.stream().anyMatch(
					workflowElement -> workflowElement.validate(workflowStateEntity)
			);
			Assert.assertTrue(workflowStateExists, "Workflow state is not present on the page");
		} catch (Exception except) {
			log.error("Failed", except);
		}
	}

	/**
	 * Create a transition between two states
	 * @param transitionEntity The transition to create
	 */
	public void createTransition(WorkflowTransitionEntity transitionEntity, WebDriverWait webDriverWait) {
		this.waitForWorkflowItemsLoaded();
		var sourceStateElement = this.findWorkflowStateBlockById(transitionEntity.getSourceState().getId().toString());

		webDriverWait.until(x -> sourceStateElement.getEditTransitionsButton().size() > 0);
		sourceStateElement.getEditTransitionsButton().get(0).click();

		webDriverWait.until(x -> this.transitionSlideOutElement.getAddTransitionButton().size() > 0);
		this.transitionSlideOutElement.clickAddTransitionButton();

		var firstEmptyTransitionBlock = this.transitionSlideOutElement.getFirstEmptyTransitionBlockElement().orElseThrow();
		firstEmptyTransitionBlock.fillInTransitionDetails(webDriver, transitionEntity);
		transitionSlideOutElement.saveTransitions();
		WaitUtils.waitForPage(webDriver);
	}

	/**
	 * Edit an existing transition between two states.
	 * @param transitionEntity The new edited transition
	 */
	public void editTransition(WorkflowTransitionEntity transitionEntity, String previousName, WebDriverWait webDriverWait) {
		this.waitForWorkflowItemsLoaded();

		var workflowStateBlock = this.findWorkflowStateBlockById(transitionEntity.getSourceState().getId().toString());
		webDriverWait.until(x -> workflowStateBlock.getEditTransitionsButton().size() > 0);
		workflowStateBlock.getEditTransitionsButton().get(0).click();

		webDriverWait.until(x -> this.transitionSlideOutElement.getAddTransitionButton().size() > 0);

		// Edit the current transition details
		var transitionBlock = this.transitionSlideOutElement.getFirstTransitionBlockElementWithName(previousName).orElseThrow();
		transitionBlock.fillInTransitionDetails(webDriver, transitionEntity);

		transitionSlideOutElement.saveTransitions();
		WaitUtils.waitForPage(webDriver);
	}

	/**
	 * Validate the symbol table transition against the transition details on the page
	 * @param transitionEntity The transition entity used to validate against the live page
	 */
	public void validateTransition(WorkflowTransitionEntity transitionEntity, WebDriverWait webDriverWait) {
		this.waitForWorkflowItemsLoaded();
		var workflowStateBlock = this.findWorkflowStateBlockById(transitionEntity.getSourceState().getId().toString());

		webDriverWait.until(x -> workflowStateBlock.getEditTransitionsButton().size() > 0);
		workflowStateBlock.getEditTransitionsButton().get(0).click();

		webDriverWait.until(x -> this.transitionSlideOutElement.getAddTransitionButton().size() > 0);

		var transitionBlock = this.getTransitionSlideOutElement()
				.getFirstTransitionBlockElementWithName(transitionEntity.getTransitionName()).orElseThrow();
		transitionBlock.validateTransitionDetails(webDriver, transitionEntity);
	}

	/**
	 * Delete the given transition from the workflow
	 * @param transitionEntity The transition entity to use as a reference to delete
	 */
	public void deleteTransition(WorkflowTransitionEntity transitionEntity, WebDriverWait webDriverWait) {
		this.waitForWorkflowItemsLoaded();

		var workflowStateBlock = this.findWorkflowStateBlockById(transitionEntity.getSourceState().getId().toString());

		this.clearAndWaitForToastNotifications();
		webDriverWait.until(x -> workflowStateBlock.getEditTransitionsButton().size() > 0);
		workflowStateBlock.getEditTransitionsButton().get(0).click();

		webDriverWait.until(x -> this.transitionSlideOutElement.getTransitionBlockElements().size() > 0);

		var transitionBlock = this.getTransitionSlideOutElement()
				.getFirstTransitionBlockElementWithName(transitionEntity.getTransitionName()).orElseThrow();

		transitionBlock.deleteTransition();
		WaitUtils.waitForPage(webDriver);
	}

	// % protected region % [Add any additional methods here] off begin
	// % protected region % [Add any additional methods here] end
}
