/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

package lmsspring.cucumber.pom.admin.workflow.workflowcreateedit.workflowstate.elements;

import com.github.webdriverextensions.WebComponent;
import lmsspring.cucumber.utils.DropdownUtils;
import lmsspring.cucumber.utils.TypingUtils;
import lmsspring.entities.WorkflowStateEntity;
import lmsspring.entities.WorkflowTransitionEntity;
import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.Arrays;
import java.util.Collections;

// % protected region % [Add additional imports here] off begin
// % protected region % [Add additional imports here] end

@Slf4j
@ScenarioScoped
public class TransitionBlockElement extends WebComponent {
	@Getter
	@FindBy(name = "Transition Name")
	private WebElement transitionNameInput;

	@Getter
	@FindBy(id = "workflow-transition-outgoing-state-field-field")
	private WebElement outgoingStateDropdown;

	@Getter
	@FindBy(className = "workflow__delete-transition")
	private WebElement deleteTransitionButton;

	public String getTransitionName() {
		return this.transitionNameInput.getAttribute("value");
	}

	public void deleteTransition() {
		this.deleteTransitionButton.click();
	}

	public void fillInTransitionDetails(WebDriver webDriver, WorkflowTransitionEntity transitionEntity) {
		TypingUtils.clearAndType(webDriver, this.transitionNameInput, transitionEntity.getTransitionName());
		DropdownUtils.selectOptionByName(webDriver, this.outgoingStateDropdown, transitionEntity.getTargetState().getStepName());
	}

	public void validateTransitionDetails(WebDriver webDriver, WorkflowTransitionEntity transitionEntity) {
		DropdownUtils.verifyMultiSelect(this.outgoingStateDropdown, Collections.singletonList(transitionEntity.getTransitionName()));
	}

	// % protected region % [Add additional class methods here] off begin
	// % protected region % [Add additional class methods here] end
}
