/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.cucumber.pom.admin.workflow.workflowcreateedit;

import com.google.inject.Inject;
import lmsspring.cucumber.pom.admin.workflow.workflowcreateedit.workflowdetails.WorkflowDetailPage;
import lmsspring.cucumber.pom.admin.workflow.workflowcreateedit.workflowstate.WorkflowStatesPage;
import lmsspring.cucumber.pom.enums.VisibilityEnum;
import lmsspring.cucumber.pom.pages.AbstractPage;
import lmsspring.entities.WorkflowVersionEntity;
import lmsspring.cucumber.utils.WaitUtils;
import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.List;
import java.util.Properties;

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * Page Object Model for Workflow Create and Edit Page
 */
@Slf4j
@ScenarioScoped
public class WorkflowCreateEditPage extends AbstractPage {

	protected static String[] entityNamesWithBehaviourWorkflow = { "Article" };

	private final String headerXpath = "//section[contains(@class, 'header-bar') and contains(@class, 'workflow__header')]";

	private final String headerDetailXpath = headerXpath +  "/div[contains(@class, 'version-details')]";

	private final String headerTitleXpath = headerDetailXpath + "/h2";

	@Getter
	@FindBy(xpath = headerXpath)
	private WebElement workflowHeader;

	@Getter
	@FindBy(xpath = headerTitleXpath)
	private WebElement title;

	@Getter
	@FindBy(xpath = headerXpath + "/div[contains(@class, tabs)]/ul")
	private WebElement tabWrapper;

	@Getter
	@FindBy(xpath = headerXpath + "/div[contains(@class, tabs)]/ul/li")
	private List<WebElement> tabs;

	@Inject
	@Getter
	@FindBy(xpath = "//section[contains(@class, 'workflow__states workflow__states__edit')]")
	private WorkflowStatesPage workflowStatesPage;

	@Inject
	@Getter
	@FindBy(xpath = "//section[contains(@class, 'workflow__edit'")
	private WorkflowDetailPage workflowDetailPage;

	// % protected region % [Add any additional fields here] off begin
	// % protected region % [Add any additional fields here] end

	@Inject
	public WorkflowCreateEditPage(
			@NonNull WebDriver webDriver,
			@NonNull Properties properties,
			@NonNull String pageUrlSlug
	) {
		super(webDriver, properties, "admin/behaviour/workflow/edit/{{workflowVersionId}}/" + pageUrlSlug);
	}


	public void navigate(WorkflowVersionEntity workflowVersionEntity) {
		log.debug("Navigating to the workflow state page in admin session for the workflow version with id " + workflowVersionEntity.getId());

		webDriver.get(this.pageUrl.replace("{{workflowVersionId}}", workflowVersionEntity.getId().toString()));

		log.debug("Navigated to the workflow state page in admin session for the workflow version with id " + workflowVersionEntity.getId());
	}

	/**
	 * Verify the common dom structure for workflow create and edit page
	 */
	private void verifyCreateEditPage() {
		Assert.assertTrue(workflowHeader.isDisplayed());
		// % protected region % [Add any additional logic for 'verifyCreateEditPage' here] off begin
		// % protected region % [Add any additional logic for 'verifyCreateEditPage' here] end
	}

	/**
	 * Verify the common dom structure for workflow create page
	 */
	public void verifyCreatePage() {
		log.debug("Verifying workflow create page");

		verifyCreateEditPage();
		Assert.assertEquals(title.getText().trim(), "Create workflow");

		// % protected region % [Add any additional logic for 'verifyCreatePage' here] off begin
		// % protected region % [Add any additional logic for 'verifyCreatePage' here] end

		log.debug("Verified workflow create page");
	}

	/**
	 * Verify the common dom structure for workflow edit page
	 */
	public void verifyEditPage() {
		log.debug("Verifying workflow edit page");

		verifyCreateEditPage();
		Assert.assertEquals(title.getText().trim(), "Edit workflow");

		// % protected region % [Add any additional logic for 'verifyEditPage' here] off begin
		// % protected region % [Add any additional logic for 'verifyEditPage' here] end

		log.debug("Verified workflow edit page");
	}

	/**
	 * Click tab in workflow builder page by name
	 */
	public void clickTabByName(String tabName) {
		log.debug("Clicking tab in workflow builder header by tab name");

		try {
			this.waitForTabs();
			tabs.stream()
					.filter(
							el -> el.findElement(By.tagName("a")).getText().equals(tabName)
					)
					.findFirst()
					.orElseThrow()
					.click();
			
			if (tabName.matches("States")) {
				WaitUtils.waitForElement(webDriver, new By.ByXPath
						("//section[contains(@class, 'workflow__states workflow__states__edit')]"),
						VisibilityEnum.VISIBLE);
			}
		} catch (Exception except) {
			log.error("Could not find tag name in workflow builder header", except);
		}

		log.debug("Clicking tab in workflow builder header by tab name");
	}

	private void waitForTabs() throws Exception {
		WaitUtils.waitForElement(webDriver, this.getTabWrapper(), VisibilityEnum.VISIBLE);
	}

	// % protected region % [Add any additional methods here] off begin
	// % protected region % [Add any additional methods here] end
}
