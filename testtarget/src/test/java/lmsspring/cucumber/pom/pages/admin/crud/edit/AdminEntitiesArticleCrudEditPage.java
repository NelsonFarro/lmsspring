/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.cucumber.pom.pages.admin.crud.edit;

import com.google.inject.Inject;
import lmsspring.cucumber.utils.*;
import lmsspring.entities.AbstractEntity;
import lmsspring.entities.ArticleEntity;
import lmsspring.cucumber.pom.pages.admin.crud.edit.elements.CrudWorkflowElement;
import lmsspring.entities.WorkflowStateEntity;
import lmsspring.entities.WorkflowVersionEntity;
import lmsspring.cucumber.pom.enums.VisibilityEnum;
import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import lombok.NonNull;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.interactions.Actions;
import java.util.*;

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * ArticlePage is a Page POM that is associated with the admin/entities/article url.
 *
 */
@Slf4j
@ScenarioScoped
public class AdminEntitiesArticleCrudEditPage extends CrudEdit {

	// % protected region % [Add any additional fields here] off begin
	// % protected region % [Add any additional fields here] end


	@FindBy(how = How.XPATH, using = "//*[@id='title-field']")
	private WebElement titleField;
	@FindBy(how = How.XPATH, using = "//*[@id='summary-field']")
	private WebElement summaryField;
	@FindBy(how = How.XPATH, using = "//*[@id='content-field']")
	private WebElement contentField;
	@FindBy(how = How.XPATH, using = "//div[contains(@class, 'workflow__display')]")
	private List<CrudWorkflowElement> workflows;
	@FindBy(tagName = "cb-workflow")
	private WebElement workflowListElement;

	// Outgoing one-to-one

	// Incoming one-to-one

	// Outgoing one-to-many

	// Incoming one-to-many
	@FindBy(how = How.XPATH, using = "//ng-select[@id='bookId-field']")
	private WebElement bookField;

	// Outgoing many-to-many
	@FindBy(how = How.XPATH, using = "//ng-select[@id='workflowStatesIds-field']")
	private WebElement workflowStatesField;

	// Incoming many-to-many
	@FindBy(how = How.XPATH, using = "//ng-select[@id='tagsIds-field']")
	private WebElement tagsField;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	@Inject
	public AdminEntitiesArticleCrudEditPage(
			// % protected region % [Add any additional constructor parameters here] off begin
			// % protected region % [Add any additional constructor parameters here] end
			@NonNull WebDriver webDriver,
			@NonNull Properties properties
	) {
		super(
			// % protected region % [Add any additional constructor arguments here] off begin
			// % protected region % [Add any additional constructor arguments here] end
			webDriver,
			properties,
			"admin/entities/article/create"
		);

		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end

		log.trace("Initialised {}", this.getClass().getSimpleName());
	}

	@Override
	protected void fillInEntityInformation(AbstractEntity abstractEntity)
	{
		var entity = (ArticleEntity) abstractEntity;
		titleField.sendKeys(entity.getTitle());
		summaryField.sendKeys(entity.getSummary());
		contentField.sendKeys(entity.getContent());

		saveButton.click();
	}
	/**
	* Changes the active state of the specified workflow by clicking on it's dropdown and selecting the
	* next state.  For this to succeed a transition from the start state to another state in the workflow
	* must have been created
	*
	* @param workflowName The name of the workflow to change the state for
	*/
	public void changeWorkflowState(String workflowName) throws Exception {
		WaitUtils.waitForElement(webDriver, this.workflowListElement, VisibilityEnum.VISIBLE);
		var workflow = this.workflows.get(this.workflows.size() - 1);
		if (workflow.getWorkflowTitle().getText().contains(workflowName)) {
			workflow.getStateDropdown().click();
			workflow.getStateDropdownOptions().get(0).click();
			saveButton.click();
			return;
		}
	
		throw new RuntimeException("Workflow with name " + workflowName  + " does not exist");
	}
	
	/**
	* Determines whether the start state of the specified workflow is currently active.
	* This function is run after removing all of the states from the workflow except the starting state to check
	* that the starting state has migrated properly, so the start state will be at index 0 of the states array
	*
	* @param workflowVersion the workflow to examine the start state of
	* @return a boolean stating whether the workflows start state is active or not
	*/
	public boolean isWorkflowStartStateActive(WorkflowVersionEntity workflowVersion) throws Exception {
		WaitUtils.waitForElement(webDriver, this.workflowListElement, VisibilityEnum.VISIBLE);
		var startState = (WorkflowStateEntity) workflowVersion.getStates().toArray()[0];
	
		var workflow = this.workflows.get(this.workflows.size() - 1);
		if (workflow.getWorkflowTitle().getText().contains(workflowVersion.getWorkflowName())) {
			return workflow.getText().contains("Current State: " + startState.getStepName());
		}
	
		throw new RuntimeException("Workflow with name " + workflowVersion.getWorkflowName()  + " does not exist");
	}
	
	/**
	* Checks whether the specified workflow is currently associated with the entity
	*
	* @param workflowVersion The workflow which is to be checked against the entity
	* @return a boolean stating whether the workflow behaviour is associated with the entity
	*/
	public boolean workflowAssigned(WorkflowVersionEntity workflowVersion) throws Exception {
		WaitUtils.waitForElement(webDriver, this.workflowListElement, VisibilityEnum.VISIBLE);
		var workflow = this.workflows.get(this.workflows.size() - 1);
		return workflow.getWorkflowTitle().getText().contains(workflowVersion.getWorkflowName());
	}
	// % protected region % [Add any additional methods here] off begin
	// % protected region % [Add any additional methods here] end
}