/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.cucumber.pom.pages.admin.crud.view;

import com.google.inject.Inject;
import lmsspring.cucumber.utils.*;
import lmsspring.entities.AbstractEntity;
import lmsspring.entities.ArticleEntity;
import lmsspring.cucumber.pom.pages.admin.crud.edit.elements.CrudWorkflowElement;
import lmsspring.entities.WorkflowStateEntity;
import lmsspring.entities.WorkflowVersionEntity;
import lmsspring.cucumber.pom.enums.VisibilityEnum;
import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import lombok.NonNull;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.interactions.Actions;
import java.util.*;

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * ArticlePage is a Page POM that is associated with the admin/entities/article url.
 *
 */
@Slf4j
@ScenarioScoped
public class AdminEntitiesArticleCrudViewPage extends CrudView {

	// % protected region % [Add any additional fields here] off begin
	// % protected region % [Add any additional fields here] end



	@FindBy(how = How.XPATH, using = "//*[@id='title-field']")
	private WebElement titleField;
	@FindBy(how = How.XPATH, using = "//*[@id='summary-field']")
	private WebElement summaryField;
	@FindBy(how = How.XPATH, using = "//*[@id='content-field']")
	private WebElement contentField;
	@FindBy(how = How.XPATH, using = "//div[contains(@class, 'workflow__display')]")
	private List<CrudWorkflowElement> workflows;
	@FindBy(tagName = "cb-workflow")
	private WebElement workflowListElement;

	// Outgoing one-to-one

	// Incoming one-to-one

	// Outgoing one-to-many

	// Incoming one-to-many
	@FindBy(how = How.XPATH, using = "//ng-select[@id='bookId-field']")
	private WebElement bookField;

	// Outgoing many-to-many
	@FindBy(how = How.XPATH, using = "//ng-select[@id='workflowStatesIds-field']")
	private WebElement workflowStatesField;

	// Incoming many-to-many
	@FindBy(how = How.XPATH, using = "//ng-select[@id='tagsIds-field']")
	private WebElement tagsField;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	@Inject
	public AdminEntitiesArticleCrudViewPage(
			// % protected region % [Add any additional constructor parameters here] off begin
			// % protected region % [Add any additional constructor parameters here] end
			@NonNull WebDriver webDriver,
			@NonNull Properties properties
	) {
		super(
			// % protected region % [Add any additional constructor arguments here] off begin
			// % protected region % [Add any additional constructor arguments here] end
			webDriver,
			properties,
			"admin/entities/article/view"
		);

		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end

		log.trace("Initialised {}", this.getClass().getSimpleName());
	}

	// % protected region % [Add any additional methods here] off begin
	// % protected region % [Add any additional methods here] end
}