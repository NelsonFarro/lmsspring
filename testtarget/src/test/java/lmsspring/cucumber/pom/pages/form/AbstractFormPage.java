/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

package lmsspring.cucumber.pom.pages.form;

import com.google.inject.Inject;
import lmsspring.cucumber.pom.enums.VisibilityEnum;
import lmsspring.cucumber.pom.pages.AbstractPage;
import lmsspring.cucumber.utils.TypingUtils;
import lmsspring.cucumber.utils.WaitUtils;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.*;

@Slf4j
public abstract class AbstractFormPage extends AbstractPage {

    private final String submissionTextXpath = "//section[contains(@class, 'forms-submission__success')]";

	@Getter
	@FindBy(xpath = "//div[@id='question0']/input")
	private WebElement textfieldQuestionInput;

	@Getter
	@FindBy(xpath = "//cb-button-group//button")
	private List<WebElement> formButtons;

	@Getter
	@FindBy(xpath = submissionTextXpath)
	private List<WebElement> formSubmissionSection;

    // % protected region % [Add any additional class fields here] off begin
    // % protected region % [Add any additional class fields here] end

   protected AbstractFormPage(
			// % protected region % [Add any additional constructor parameters here] off begin
			// % protected region % [Add any additional constructor parameters here] end
			@NonNull WebDriver webDriver,
			@NonNull Properties properties,
			String pageUrlSlug
	) {
		// % protected region % [Add any additional constructor logic before the main body here] off begin
		// % protected region % [Add any additional constructor logic before the main body here] end
		super(
			// % protected region % [Add any additional constructor arguments here] off begin
			// % protected region % [Add any additional constructor arguments here] end
			webDriver,
			properties,
			pageUrlSlug
		);

		// % protected region % [Add any additional constructor logic after the main body here] off begin
		// % protected region % [Add any additional constructor logic after the main body here] end
	}

    /**
	 * Fills in the form on the form page and clicks the submit button
	 *
	 * Current tests create a form with a single textfield question, and this function assumes
	 * that the form on the page has been created in this way.  As such, this function may not work
	 * for other form configurations.
	 *
	 * In order to fill in other forms, subclasses of AbstractFormPage can override this function
	 */
    public void fillForm(String value, boolean wait) throws Exception {
		if (wait) {
			WaitUtils.waitForElement(webDriver, this.getTextfieldQuestionInput(), VisibilityEnum.CLICKABLE);
		}

		TypingUtils.clearAndType(webDriver, this.getTextfieldQuestionInput(), value);
		this.getFormButtons().get(1).click();
	}

	public boolean wasFormSubmitted() throws Exception {
		WaitUtils.waitForElement(webDriver, By.xpath(this.submissionTextXpath), VisibilityEnum.VISIBLE);
    	return this.getFormSubmissionSection().size() == 1;
	}

    // % protected region % [Add any additional class methods here] off begin
    // % protected region % [Add any additional class methods here] end
}