/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.cucumber.stepdefs.behaviours.workflow;

import com.google.inject.Inject;
import lmsspring.cucumber.context.TestContext;
import lmsspring.cucumber.utils.NavigationUtils;
import lmsspring.cucumber.utils.WaitUtils;
import lmsspring.cucumber.pom.admin.workflow.workflowcreateedit.workflowstate.elements.StateBlockElement;
import lmsspring.cucumber.pom.admin.workflow.workflowcreateedit.WorkflowCreateEditPage;
import lmsspring.cucumber.pom.admin.workflow.workflowcreateedit.workflowdetails.WorkflowDetailPage;
import lmsspring.cucumber.pom.admin.workflow.workflowcreateedit.workflowstate.WorkflowStatesPage;
import lmsspring.cucumber.pom.admin.workflow.workflowlist.WorkflowListPage;
import lmsspring.cucumber.stepdefs.AbstractStepDef;
import lmsspring.entities.WorkflowStateEntity;
import lmsspring.entities.WorkflowVersionEntity;
import lmsspring.cucumber.pom.pages.admin.crud.edit.AdminEntitiesArticleCrudEditPage;
import lmsspring.cucumber.pom.pages.admin.crud.list.AdminEntitiesArticleCrudListPage;
import lmsspring.inject.factories.WorkflowStateEntityFactory;
import lmsspring.inject.factories.WorkflowTransitionEntityFactory;
import lmsspring.inject.factories.WorkflowVersionEntityFactory;
import io.cucumber.java.en.*;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.*;

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

public class WorkflowBuilderStepDef extends AbstractStepDef {

	@Inject
	private WorkflowListPage workflowListPage;

	@Inject
	private WorkflowDetailPage workflowDetailPage;

	@Inject
	private WorkflowCreateEditPage workflowCreateEditPage;

	@Inject
	private WorkflowStatesPage workflowStatesPage;

	@Inject
	private TestContext testContext;

	@Inject
	private WorkflowVersionEntityFactory workflowVersionEntityFactory;

	@Inject
	private WorkflowStateEntityFactory workflowStateEntityFactory;

	@Inject
	private WorkflowTransitionEntityFactory workflowTransitionEntityFactory;

	@Inject
	private AdminEntitiesArticleCrudEditPage adminArticleCrudEdit;

	@Inject
	private AdminEntitiesArticleCrudListPage adminArticleCrudList;


	// % protected region % [Add any additional fields here] off begin
	// % protected region % [Add any additional fields here] end

	//Given Step Defs
	@Given("^I navigate to the workflow page in admin section$")
	public void navigate() {
		workflowListPage.navigate();
		// % protected region % [Add any additional logic for 'I navigate to the workflow page in admin section$' here] off begin
		// % protected region % [Add any additional logic for 'I navigate to the workflow page in admin section$' here] end
	}

	@Given("I click create button in workflow list page")
	public void iClickCreateButtonInWorkflowListPage() {
		workflowListPage.clickCreateButton();
		// % protected region % [Add any additional logic for 'I click create button in workflow list page' here] off begin
		// % protected region % [Add any additional logic for 'I click create button in workflow list page' here] end
	}

	// % protected region % [Add any additional Given Step Defs here] off begin
	// % protected region % [Add any additional Given Step Defs here] end

	//When Step Defs
	@When("I update the workflow version with id {string}")
	public void iUpdateWorkflowVersionWithId(String id) {
		WorkflowVersionEntity workflowVersion = this.getWorkflowVersionEntityFromId(id);
		workflowDetailPage.fillWorkflowDetailFormAndSave(workflowVersion);
		// % protected region % [Add any additional logic for 'I update workflow version' here] off begin
		// % protected region % [Add any additional logic for 'I update workflow version' here] end
	}

	@When("I click on {string} tab in workflow builder")
	public void iClickOnTabInWorkflowBuilder(String tabName) {
		this.workflowCreateEditPage.clearAndWaitForToastNotifications();
		workflowCreateEditPage.clickTabByName(tabName);
	}

	@When("I edit an existing workflow state for the workflow version with id {string}")
	public void iEditAnExistingWorkflowState(String workflowVersionId) {
		webDriverWait.until(x -> this.workflowStatesPage.getAllWorkflowStates().size() > 0);

		WorkflowVersionEntity workflowVersionEntity = this.getWorkflowVersionEntityFromId(workflowVersionId);
		WorkflowStateEntity workflowStateToUpdate = workflowStateEntityFactory.createWithNoRef();
		String workflowStateIdToUpdate = workflowStatesPage.getWorkflowStateId();
		workflowStateToUpdate.setId(UUID.fromString(workflowStateIdToUpdate));

		workflowVersionEntity.unsetStates();
		workflowVersionEntity.addStates(workflowStateToUpdate);
		workflowStateToUpdate.setWorkflowVersion(workflowVersionEntity);
		this.testContext.put(workflowVersionId, workflowVersionEntity);

		workflowStatesPage.updateWorkflowState(workflowStateToUpdate);
		webDriverWait.until(x -> this.workflowStatesPage.getAllWorkflowStates().size() > 0);

		this.syncSymbolTableWithDisplayedStates(workflowVersionId);
	}

	@When("I create {integer} new workflow states for workflow version with id {string}")
	public void iCreateNewWorkflowStates(Integer numOfStates, String workflowVersionId) {
		var workflowVersion = this.getWorkflowVersionEntityFromId(workflowVersionId);

		for (int i = 0; i < numOfStates; ++i) {
			WorkflowStateEntity workflowStateToCreate = workflowStateEntityFactory.createWithNoRef();
			this.workflowStatesPage.createNewWorkflowState(workflowStateToCreate, false);
			workflowVersion.addStates(workflowStateToCreate);
		}

		this.workflowStatesPage.clickSave();
		webDriverWait.until(x -> this.workflowStatesPage.getAllWorkflowStates().size() > 0);


		// Update the ID's for the new states in the symbol table
		var statesOnPage = this.workflowStatesPage.getAllWorkflowStates();
		var preStates = new HashSet<>(workflowVersion.getStates());
		workflowVersion.unsetStates();

		for (int i = 0; i < statesOnPage.size(); i++) {
			var currentStateOnPage = statesOnPage.get(i);
			var matchingState = preStates
					.stream()
					.filter(state -> state.getStepName().equals(currentStateOnPage.getStateName())).findFirst().orElseThrow();

				matchingState.setId(UUID.fromString(currentStateOnPage.getStateId()));
				matchingState.setIsStartState(currentStateOnPage.isStartState());
				workflowVersion.addStates(matchingState);
		}

		this.testContext.put(workflowVersionId, workflowVersion);
	}

	@When("I create sequential transitions for the existing states for the workflow version with id {string}")
	public void iCreateSequentialTransitionsForTheExistingStatesForWorkflowVersionWithId(String workflowVersionId) {
		var workflowVersion = this.getWorkflowVersionEntityFromId(workflowVersionId);

		// Confirm we are on the correct page and that the states on the page are as expected
		webDriverWait.until(x -> this.workflowStatesPage.getAllWorkflowStates().size() > 0);

		workflowVersion.getStates().forEach(state -> {
			this.workflowStatesPage.verifyStateExistsInPage(state);
		});

		// Given that the order of our states doesn't matter beyond the start and end, we will create an arbitrary order of states
		var orderedStates = new ArrayList<WorkflowStateEntity>();
		var statesCopy = new HashSet<>(workflowVersion.getStates());
		var startState = workflowVersion.getStates().stream().filter(WorkflowStateEntity::getIsStartState).findFirst();

		// We want the start state to be first and the final arbitrary to be the last state
		if (startState.isPresent()) {
			orderedStates.add(startState.get());
			statesCopy.remove(startState.get());
		} else {
			Assert.fail("Cannot find the start start");
		}

		orderedStates.addAll(statesCopy);

		for (int i = 0; i < orderedStates.size(); i++) {
			// Create for all but the final state
			if (i != orderedStates.size() - 1) {
				var transition = this.workflowTransitionEntityFactory.createWithNoRef();
				transition.setSourceState(orderedStates.get(i));
				transition.setTargetState(orderedStates.get(i + 1));
				this.workflowStatesPage.createTransition(transition, webDriverWait);
			}
		}
	}

	@When("I edit the existing workflow transitions for the workflow version with id {string}")
	public void iEditTheExistingWorkflowTransitionsForTheWorkflowVersionWithId(String workflowVersionId) {
		var workflowVersion = this.getWorkflowVersionEntityFromId(workflowVersionId);
		webDriverWait.until(x -> this.workflowStatesPage.getAllWorkflowStates().size() > 0);


		workflowVersion.getStates().forEach(state -> {
			if (state.getOutgoingTransitions().size() != 0) {
				state.getOutgoingTransitions().forEach(transitionEntity -> {
					transitionEntity.setTargetState(
							workflowVersion.getStates().stream()
									.filter(s -> !s.equals(transitionEntity.getTargetState()))
									.findFirst()
									.orElseThrow()
					);
					var previousName = transitionEntity.getTransitionName();
					transitionEntity.setTransitionName(transitionEntity.getTransitionName().toUpperCase());
					this.workflowStatesPage.editTransition(transitionEntity, previousName, webDriverWait);
				});
			}
		});
	}

	@When("I delete the existing workflow transitions for the workflow version with id {string}")
	public void iDeleteTheExistingWorkflowTransitionsForTheWorkflowVersionWithId(String workflowVersionId) {
		var workflowVersion = this.getWorkflowVersionEntityFromId(workflowVersionId);
		webDriverWait.until(x -> this.workflowStatesPage.getAllWorkflowStates().size() > 0);


		workflowVersion.getStates().forEach(state -> {
			if (state.getOutgoingTransitions().size() != 0) {
				state.getOutgoingTransitions().forEach(transitionEntity -> {
					this.workflowStatesPage.deleteTransition(transitionEntity, webDriverWait);
					transitionEntity.getTargetState().removeIncomingTransitions(transitionEntity);
					state.removeOutgoingTransitions(transitionEntity);
				});
				workflowVersion.addStates(state);
			}
		});
		this.workflowStatesPage.getSaveButton().click();
	}

	@When("I update the state of the workflow version with id {string} connected to the {string} entity")
	public void iUpdateTheStateOfTheWorkflowVersionWithIdConnectedToTheEntity(String workflowVersionId, String entityName) throws Exception {
		var workflowVersion = this.getWorkflowVersionEntityFromId(workflowVersionId);
		this.workflowStatesPage.waitForWorkflowItemsLoaded();
		String workflowName = workflowVersion.getWorkflowName();

		switch (entityName) {
			case "Article":
				webDriverWait.until(x -> this.adminArticleCrudList.EditButtons.size() > 0);
				this.adminArticleCrudList.EditButtons.get(0).click();
				this.adminArticleCrudEdit.changeWorkflowState(workflowName);
				break;
			default:
				Assert.fail(entityName + " Cannot be associated with a workflow");
		}
	}

	@When("I delete all states associated with the workflow version with id {string} except for the starting state")
	public void iDeleteAllStatesAssociatedWithTheWorkflowVersionWithIdExceptForTheStartingState(String workflowVersionId) {
		var workflowVersion = this.getWorkflowVersionEntityFromId(workflowVersionId);
		webDriverWait.until(x -> this.workflowStatesPage.getAllWorkflowStates().size() > 0);
		var states = this.workflowStatesPage.getAllWorkflowStates();
		this.workflowStatesPage.clearAndWaitForToastNotifications();

		var newStates = new HashSet<WorkflowStateEntity>();
		for (StateBlockElement state: states) {
			if (state.isStartState()) {
				WorkflowStateEntity startState = new WorkflowStateEntity();
				//Only needs to be one, so exit from loop once start is found
				startState.setIsStartState(true);
				startState.setId(UUID.fromString(state.getStateId()));
				startState.setStepName(state.getStateName());
				newStates.add(startState);
				break;
			}
		}
		workflowVersion.setStates(newStates, true);

		this.testContext.put(workflowVersionId, workflowVersion);
	}

	@When("I unassign entities from the workflow version with id {string}")
	public void iUnassignEntitiesFromTheWorkflowVersionWithId(String workflowId) {
		var workflowVersion = this.getWorkflowVersionEntityFromId(workflowId);

		workflowVersion.setArticleAssociation(false);

		this.workflowDetailPage.fillWorkflowDetailFormAndSave(workflowVersion);
		this.testContext.put(workflowId, workflowVersion);
	}

	@When("I navigate to the edit page for the workflow version with id {string}")
	public void iNavigateToTheEditPageForTheWorkflowVersionWithId(String workflowId) {
		WorkflowVersionEntity workflowVersion = this.getWorkflowVersionEntityFromId(workflowId);
		this.workflowListPage.waitForWorkflowItemsLoaded();

		NavigationUtils.goToUrl(webDriver, properties.get("selenium.url") + "/admin/behaviour/workflow/edit/" + workflowVersion.getId().toString());
	}

	// % protected region % [Add any additional When Step Defs here] off begin
	// % protected region % [Add any additional When Step Defs here] end


	//Then Step Defs
	@Then("I see the workflow list page")
	public void iSeeTheWorkflowListPage() {
		webDriverWait.until(webDriver -> webDriver.getCurrentUrl().contains(properties.getProperty("selenium.url") + "/") &&
				!webDriver.getCurrentUrl().contains(this.workflowListPage.getPageUrl()));
		workflowListPage.verifyWorkflowListPage();
		// % protected region % [Add any additional logic for 'I see the workflow list page' here] off begin
		// % protected region % [Add any additional logic for 'I see the workflow list page' here] end
	}

	@Then("I create a new workflow version with id {string}")
	public void iCreateANewWorkflowVersionWithId(String id) {
		Objects.requireNonNull(this.workflowCreateEditPage.getWorkflowDetailPage(), "The Details page is not present");
		WorkflowVersionEntity workflowVersionToCreate = this.workflowVersionEntityFactory.createWithNoRef();
		this.workflowDetailPage.fillWorkflowDetailFormAndSave(workflowVersionToCreate);
		this.testContext.put(id, workflowVersionToCreate);
	}

	@Then("I see the workflow entity with id {string} in the list")
	public void iShouldSeeTheCreatedWorkflowEntityInTheList(String id) {
		WorkflowVersionEntity createdWorkflowVersion = this.getWorkflowVersionEntityFromId(id);
			WebElement createdWorkflowItem = workflowListPage.findWorkflowItemByName(createdWorkflowVersion.getWorkflowName());
			Assert.assertNotNull(createdWorkflowItem, "New created workflow item with id " + id + " does not existed");
	}

	@Then("The workflow create page is correct")
	public void theWorkflowCreatePageIsCorrect() {
		webDriverWait.until(webDriver -> webDriver.getCurrentUrl().contains(properties.getProperty("selenium.url") + "/") &&
				!webDriver.getCurrentUrl().contains(this.workflowCreateEditPage.getPageUrl()));
		this.workflowCreateEditPage.verifyCreatePage();
		// % protected region % [Add any additional logic for 'I create a new workflow version if not exist' here] off begin
		// % protected region % [Add any additional logic for 'I create a new workflow version if not exist' here] end
	}


	@Then("I click on the workflow version with id {string}")
	public void iClickOnTheWorkflowVersionWithId(String id) {
		WorkflowVersionEntity workflowVersion = this.getWorkflowVersionEntityFromId(id);
		this.workflowListPage.waitForWorkflowItemsLoaded();
		this.workflowListPage.clickWorkflowItemById(workflowVersion.getId().toString(), webDriverWait);
		// % protected region % [Add any additional logic for 'I click on the workflow to update' here] off begin
		// % protected region % [Add any additional logic for 'I click on the workflow to update' here] end
	}

	@Then("The workflow with id {string} is correct")
	public void iVerifyTheUpdatedWorkflow(String id) throws Exception {
		WorkflowVersionEntity updatedWorkflowVersion = this.getWorkflowVersionEntityFromId(id);
		workflowDetailPage.verifyWorkflowDetail(updatedWorkflowVersion);
	}

	@Then("The workflow state page is correct")
	public void iVerifyWorkflowStatePage() {
		workflowStatesPage.verifyDomStructure();
	}

	@Then("The workflow states are correct for the workflow version with id {string}")
	public void theWorkflowStatesAreCorrectForTheWorkflowVersionWithId(String workflowVersionId) {
		webDriverWait.until(x -> this.workflowStatesPage.getAllWorkflowStates().size() > 0);

		WorkflowVersionEntity updatedWorkflowVersion = this.getWorkflowVersionEntityFromId(workflowVersionId);
		updatedWorkflowVersion.getStates().forEach(state -> workflowStatesPage.verifyStateExistsInPage(state));
	}

	@Then("The transitions are correct for the workflow version with id {string}")
	public void theTransitionsAreCorrectForTheWorkflowVersionWithId(String workflowVersionId) {
		var workflowVersion = this.getWorkflowVersionEntityFromId(workflowVersionId);
		webDriverWait.until(x -> this.workflowStatesPage.getAllWorkflowStates().size() > 0);


		workflowVersion.getStates().forEach(state -> {
			if (state.getOutgoingTransitions().size() != 0) {
				state.getOutgoingTransitions().forEach(transitionEntity -> {
					this.workflowStatesPage.validateTransition(transitionEntity, webDriverWait);
				});
			}
		});
	}

	@Then("I have a start state")
	public void iHaveAStartState() {
		if (!workflowStatesPage.hasStartState(true)) {
			Assert.fail("Start state not present");
		}
	}

	@Then("The start state of the workflow version with id {string} connected to the {string} entity is active")
	public void theStartStateOfTheWorkflowVersionWithIdConnectedToTheEntityWithIdIsActive(String workflowVersionId, String entityName) throws Exception {
		var workflowVersion = this.getWorkflowVersionEntityFromId(workflowVersionId);

		switch (entityName) {
			case "Article":
				this.adminArticleCrudList.navigate();
				webDriverWait.until(x -> this.adminArticleCrudList.EditButtons.size() > 0);
				this.adminArticleCrudList.EditButtons.get(0).click();
				Assert.assertTrue(this.adminArticleCrudEdit.isWorkflowStartStateActive(workflowVersion));
				break;
			default:
				Assert.fail(entityName + " Cannot be associated with a workflow");
		}
	}

	@Then("the workflow version with id {string} is not present for the {string} entity")
	public void theWorkflowVersionWithIdIsNotPresentForTheEntity(String workflowVersionId, String entityName) throws Exception {
		var workflowVersion = this.getWorkflowVersionEntityFromId(workflowVersionId);

		switch (entityName) {
			case "Article":
				this.adminArticleCrudList.navigate();
				webDriverWait.until(x -> this.adminArticleCrudList.EditButtons.size() > 0);
				this.adminArticleCrudList.EditButtons.get(0).click();
				Assert.assertFalse(this.adminArticleCrudEdit.workflowAssigned(workflowVersion));
				break;
			default:
				Assert.fail(entityName + " Cannot be associated with a workflow");
		}
	}

	@Then("I create a new workflow version with id {string} and entity association {string}")
	public void iCreateANewWorkflowVersionWithIdAndEntityAssociation(String workflowVersionId, String entityName) {
		Objects.requireNonNull(this.workflowCreateEditPage.getWorkflowDetailPage(), "The Details page is not present");
		WorkflowVersionEntity workflowVersionToCreate = this.workflowVersionEntityFactory.createWithNoRef();

		workflowVersionToCreate.setArticleAssociation(false);

		if (entityName.matches("Article")) {
			workflowVersionToCreate.setArticleAssociation(true);
		}

		this.workflowDetailPage.fillWorkflowDetailFormAndSave(workflowVersionToCreate);
		this.testContext.put(workflowVersionId, workflowVersionToCreate);
	}

	// % protected region % [Add any additional Then Step Defs here] off begin
	// % protected region % [Add any additional Then Step Defs here] end

	//Helper Methods
	/**
	 * From a the id, return a workflow version entity from the test context
	 * @param id The id of the workflow version entity in the context.
	 * @return The workflow version entity that corresponds to the context id provided
	 */
	private WorkflowVersionEntity getWorkflowVersionEntityFromId(String id) {
		if (testContext.get(id) instanceof WorkflowVersionEntity) {
			return (WorkflowVersionEntity) testContext.get(id);
		} else {
			Assert.fail("Should have workflow with id " + id + " in the context");
			throw new RuntimeException("Could not find workflow from test context");
		}
	}

	/**
	 * Update the states for a given workflow version in the symbol table with the details for those states as displayed
	 *
	 * This assumes that all states for a given workflow version as present on a screen and overwrites the symbol table
	 * entries with the displayed values.
	 *
	 * @param symbolTableId The ID for the {@link WorkflowVersionEntity} entry to be updated in the symbol table
	 */
	private void syncSymbolTableWithDisplayedStates(String symbolTableId) {
		var workflowVersionEntity = this.getWorkflowVersionEntityFromId(symbolTableId);
		webDriverWait.until(x -> this.workflowStatesPage.getAllWorkflowStates().size() > 0);


		// Update the ID's for the new states in the symbol table
		var statesOnPage = this.workflowStatesPage.getAllWorkflowStates();
		var preStates = new HashSet<>(workflowVersionEntity.getStates());
		workflowVersionEntity.unsetStates();

		for (int i = 0; i < statesOnPage.size(); i++) {
			var currentStateOnPage = statesOnPage.get(i);
			var matchingState = preStates
					.stream()
					.filter(state -> state.getStepName().equals(currentStateOnPage.getStateName())).findFirst().orElseThrow();

			matchingState.setId(UUID.fromString(currentStateOnPage.getStateId()));
			matchingState.setIsStartState(currentStateOnPage.isStartState());
			workflowVersionEntity.addStates(matchingState);
		}

		this.testContext.put(symbolTableId, workflowVersionEntity);
	}

	// % protected region % [Add any additional methods here] off begin
	// % protected region % [Add any additional methods here] end
}

