/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.entities;

import lombok.*;
import javax.validation.constraints.NotNull;

import java.util.*;

// % protected region % [Import any additional imports here] off begin
// % protected region % [Import any additional imports here] end

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
public class TagEntity extends AbstractEntity {

	public TagEntity() {
		initialiseReferences();
	}

	private void initialiseReferences() {


		var ArticlesManyMany = new EntityReference();
			ArticlesManyMany.entityName = "Article";
			ArticlesManyMany.oppositeName = "Tags";
			ArticlesManyMany.name = "Articles";
			ArticlesManyMany.optional = true;
			ArticlesManyMany.type = "Many";
			ArticlesManyMany.oppositeType = "Many";

		References.add(ArticlesManyMany);
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Attributes
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Modify attribute annotation for Name here] off begin
	@ToString.Include
	@Setter
	// % protected region % [Modify attribute annotation for Name here] end
	private String name;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing one-to-one
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming one-to-one
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing one-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming one-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing many-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private Set<ArticleEntity> articles = new HashSet<>();

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming many-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing reference methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Similar to {@link this#addArticles(ArticleEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given ArticleEntity to be added to this entity
	 */
	public void addArticles(@NotNull ArticleEntity entity) {
		addArticles(entity, true);
	}

	/**
	 * Add a new ArticleEntity to Articles in this entity.
	 *
	 * @param entity the given ArticleEntity to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entity
	 */
	public void addArticles(@NonNull ArticleEntity entity, boolean reverseAdd) {
		if (!this.articles.contains(entity)) {
			this.articles.add(entity);
			if (reverseAdd) {
				entity.addTags(this, false);
			}
		}
	}

	/**
	 * Similar to {@link this#addArticles(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given entities to be added to this entity
	 */
	public void addArticles(@NotNull Collection<ArticleEntity> entities) {
		addArticles(entities, true);
	}

	/**
	 * Add new collection of ArticleEntity to articles in this entity.
	 *
	 * @param entities the given entities to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void addArticles(@NonNull Collection<ArticleEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> addArticles(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#removeArticles(ArticleEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given ArticleEntity to be set to this entity
	 */
	public void removeArticles(@NotNull ArticleEntity entity) {
		this.removeArticles(entity, true);
	}

	/**
	 * Remove the given ArticleEntity from this entity.
	 *
	 * @param entity the give ArticleEntity to be removed from this entity
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void removeArticles(@NotNull ArticleEntity entity, boolean reverse) {
		if (reverse) {
			entity.removeTags(this, false);
		}
		this.articles.remove(entity);
	}

	/**
	 * Similar to {@link this#removeArticles(Collection, boolean)} but
	 * default to true for reverse remove.
	 *
	 * @param entities the given entities to be removed to this entity
	 */
	public void removeArticles(@NotNull Collection<ArticleEntity> entities) {
		this.removeArticles(entities, true);
	}

	/**
	 * Remove the given collection of ArticleEntity in articles from  to this entity.
	 *
	 * @param entities the given entities to be removed to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void removeArticles(@NonNull Collection<ArticleEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> this.removeArticles(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#setArticles(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given entities to be set to this entity
	 */
	public void setArticles(@NotNull Collection<ArticleEntity> entities) {
		this.setArticles(entities, true);
	}

	/**
	 * Replace the current entities in articles with the given ones.
	 *
	 * @param entities the given entities to replace the old ones
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void setArticles(@NotNull Collection<ArticleEntity> entities, boolean reverseAdd) {
		this.unsetArticles();
		this.articles = new HashSet<>(entities);
		if (reverseAdd) {
			this.articles.forEach(entity -> entity.addTags(this, false));
		}
	}

	/**
	 * Similar to {@link this#unsetArticles(boolean)} but
	 * default to true for reverse unset
	 */
	public void unsetArticles() {
		this.unsetArticles(true);
	}

	/**
	 * Remove all entities in articles from this entity.
	 * @param doReverse whether this entity should be removed from the given entities
	 */
	public void unsetArticles(boolean doReverse) {
		if (doReverse) {
			this.articles.forEach(entity -> entity.removeTags(this, false));
		}
		this.articles.clear();
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



	// % protected region % [Add any additional class methods  here] off begin
	// % protected region % [Add any additional class methods  here] end
}
