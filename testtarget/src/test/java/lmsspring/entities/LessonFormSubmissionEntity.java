/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.entities;

import lombok.*;
import javax.validation.constraints.NotNull;


// % protected region % [Import any additional imports here] off begin
// % protected region % [Import any additional imports here] end

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
public class LessonFormSubmissionEntity extends AbstractEntity {

	public LessonFormSubmissionEntity() {
		initialiseReferences();
	}

	private void initialiseReferences() {


		var SubmittedFormOneMany = new EntityReference();
			SubmittedFormOneMany.entityName = "LessonFormVersion";
			SubmittedFormOneMany.oppositeName = "SubmittedForm";
			SubmittedFormOneMany.name = "Submission";
			SubmittedFormOneMany.optional = false;
			SubmittedFormOneMany.type = "One";
			SubmittedFormOneMany.oppositeType = "Many";

		References.add(SubmittedFormOneMany);
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Attributes
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Modify attribute annotation for Submission Data here] off begin
	@ToString.Include
	@Setter
	// % protected region % [Modify attribute annotation for Submission Data here] end
	private String submissionData;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing one-to-one
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming one-to-one
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing one-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming one-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private LessonFormVersionEntity submittedForm;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing many-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming many-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing reference methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Similar to {@link this#setSubmittedForm(LessonFormVersionEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given LessonFormVersionEntity to be set to this entity
	 */
	public void setSubmittedForm(@NotNull LessonFormVersionEntity entity) {
		setSubmittedForm(entity, true);
	}

	/**
	 * Set or update the submittedForm in this entity with single LessonFormVersionEntity.
	 *
	 * @param entity the given LessonFormVersionEntity to be set or updated to submittedForm
	 * @param reverseAdd whether this entity should be set or updated to the given entity
	 */
	public void setSubmittedForm(@NotNull LessonFormVersionEntity entity, boolean reverseAdd) {
		// % protected region % [Add any additional logic here before the main logic for setSubmittedForm here] off begin
		// % protected region % [Add any additional logic here before the main logic for setSubmittedForm here] end

		if (sameAsFormer(this.submittedForm, entity)) {
			return;
		}

		if (this.submittedForm != null) {
			this.submittedForm.removeSubmission(this, false);
		}
		this.submittedForm = entity;
		if (reverseAdd) {
			this.submittedForm.addSubmission(this, false);
		}

		// % protected region % [Add any additional logic here after the main logic for setLessonFormVersion here] off begin
		// % protected region % [Add any additional logic here after the main logic for setLessonFormVersion here] end
	}

	/**
	 * Similar to {@link this#unsetSubmittedForm(boolean)} but default to true.
	 */
	public void unsetSubmittedForm() {
		this.unsetSubmittedForm(true);
	}

	/**
	 * Remove Submitted form in this entity.
	 *
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void unsetSubmittedForm(boolean reverse) {
		if (reverse && this.submittedForm != null) {
			this.submittedForm.removeSubmission(this, false);
		}
		this.submittedForm = null;
	}

	// % protected region % [Add any additional class methods  here] off begin
	// % protected region % [Add any additional class methods  here] end
}
