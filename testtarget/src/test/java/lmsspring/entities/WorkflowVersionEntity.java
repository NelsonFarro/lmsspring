/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.entities;

import lombok.*;
import javax.validation.constraints.NotNull;

import java.util.*;

// % protected region % [Import any additional imports here] off begin
// % protected region % [Import any additional imports here] end

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
public class WorkflowVersionEntity extends AbstractEntity {

	public WorkflowVersionEntity() {
		initialiseReferences();
	}

	private void initialiseReferences() {


		var StatesOneMany = new EntityReference();
			StatesOneMany.entityName = "WorkflowState";
			StatesOneMany.oppositeName = "WorkflowVersion";
			StatesOneMany.name = "States";
			StatesOneMany.optional = true;
			StatesOneMany.type = "One";
			StatesOneMany.oppositeType = "Many";

		References.add(StatesOneMany);

		var WorkflowOneMany = new EntityReference();
			WorkflowOneMany.entityName = "Workflow";
			WorkflowOneMany.oppositeName = "Workflow";
			WorkflowOneMany.name = "Versions";
			WorkflowOneMany.optional = false;
			WorkflowOneMany.type = "One";
			WorkflowOneMany.oppositeType = "Many";

		References.add(WorkflowOneMany);

		var CurrentWorkflowOneOne = new EntityReference();
			CurrentWorkflowOneOne.entityName = "Workflow";
			CurrentWorkflowOneOne.oppositeName = "CurrentVersion";
			CurrentWorkflowOneOne.name = "CurrentWorkflow";
			CurrentWorkflowOneOne.optional = true;
			CurrentWorkflowOneOne.type = "One";
			CurrentWorkflowOneOne.oppositeType = "One";

		References.add(CurrentWorkflowOneOne);
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Attributes
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Modify attribute annotation for Workflow Name here] off begin
	@ToString.Include
	@Setter
	// % protected region % [Modify attribute annotation for Workflow Name here] end
	private String workflowName;

	// % protected region % [Modify attribute annotation for Workflow Description here] off begin
	@ToString.Include
	@Setter
	// % protected region % [Modify attribute annotation for Workflow Description here] end
	private String workflowDescription;

	// % protected region % [Modify attribute annotation for Version Number here] off begin
	@ToString.Include
	@Setter
	// % protected region % [Modify attribute annotation for Version Number here] end
	private Integer versionNumber;

	// % protected region % [Modify attribute annotation for Article Association here] off begin
	@ToString.Include
	@Setter
	// % protected region % [Modify attribute annotation for Article Association here] end
	private Boolean articleAssociation;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing one-to-one
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private WorkflowEntity currentWorkflow;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming one-to-one
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing one-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private Set<WorkflowStateEntity> states = new HashSet<>();

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming one-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private WorkflowEntity workflow;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing many-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming many-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing reference methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
	 * Similar to {@link this#addStates(WorkflowStateEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given WorkflowStateEntity to be added to this entity
	 */
	public void addStates(@NotNull WorkflowStateEntity entity) {
		addStates(entity, true);
	}

	/**
	 * Add a new WorkflowStateEntity to states in this entity.
	 *
	 * @param entity the given WorkflowStateEntity  to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entity
	 */
	public void addStates(@NonNull WorkflowStateEntity entity, boolean reverseAdd) {
		if (!this.states.contains(entity)) {
			states.add(entity);
			if (reverseAdd) {
				entity.setWorkflowVersion(this, false);
			}
		}
	}

	/**
	 * Similar to {@link this#addStates(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of WorkflowStateEntity to be added to this entity
	 */
	public void addStates(@NotNull Collection<WorkflowStateEntity> entities) {
		addStates(entities, true);
	}

	/**
	 * Add a new collection of WorkflowStateEntity to States in this entity.
	 *
	 * @param entities the given collection of WorkflowStateEntity to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void addStates(@NonNull Collection<WorkflowStateEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> addStates(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#removeStates(WorkflowStateEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given WorkflowStateEntity to be set to this entity
	 */
	public void removeStates(@NotNull WorkflowStateEntity entity) {
		this.removeStates(entity, true);
	}

	/**
	 * Remove the given WorkflowStateEntity from this entity.
	 *
	 * @param entity the given WorkflowStateEntity to be removed from this entity
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void removeStates(@NotNull WorkflowStateEntity entity, boolean reverse) {
		if (reverse) {
			entity.unsetWorkflowVersion(false);
		}
		this.states.remove(entity);
	}

	/**
	 * Similar to {@link this#removeStates(Collection, boolean)} but
	 * default to true for reverse remove.
	 *
	 * @param entities the given collection of WorkflowStateEntity to be removed to this entity
	 */
	public void removeStates(@NotNull Collection<WorkflowStateEntity> entities) {
		this.removeStates(entities, true);
	}

	/**
	 * Remove the given collection of WorkflowStateEntity from  to this entity.
	 *
	 * @param entities the given collection of WorkflowStateEntity to be removed to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void removeStates(@NonNull Collection<WorkflowStateEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> this.removeStates(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#setStates(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of WorkflowStateEntity to be set to this entity
	 */
	public void setStates(@NotNull Collection<WorkflowStateEntity> entities) {
		setStates(entities, true);
	}

	/**
	 * Replace the current entities in States with the given ones.
	 *
	 * @param entities the given collection of WorkflowStateEntity to replace the old ones
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void setStates(@NotNull Collection<WorkflowStateEntity> entities, boolean reverseAdd) {

		this.unsetStates();
		this.states = new HashSet<>(entities);
		if (reverseAdd) {
			this.states.forEach(statesEntity -> statesEntity.setWorkflowVersion(this, false));
		}
	}

	/**
	 * Similar to {@link this#unsetStates(boolean)} but
	 * default to true for reverse unset
	 */
	public void unsetStates() {
		this.unsetStates(true);
	}

	/**
	 * Remove all the entities in States from this entity.
	 * @param doReverse whether this entity should be removed from the given entities
	 */
	public void unsetStates(boolean doReverse) {
		if (doReverse) {
			this.states.forEach(statesEntity -> statesEntity.unsetWorkflowVersion(false));
		}
		this.states.clear();
	}

	/**
	 * Similar to {@link this#setCurrentWorkflow(WorkflowEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given WorkflowVersionEntity to be set to this entity
	 */
	public void setCurrentWorkflow(@NotNull WorkflowEntity entity) {
		setCurrentWorkflow(entity, true);
	}

	/**
	 * Set or update currentWorkflow with the given WorkflowEntity.
	 *
	 * @param entity the WorkflowVersionEntity to be set or updated
	 * @param reverseAdd whether this entity should be set or updated to the given entity
	 */
	public void setCurrentWorkflow(@NotNull WorkflowEntity entity, boolean reverseAdd) {
		// % protected region % [Add any additional logic here before the main logic for setCurrentWorkflow here] off begin
		// % protected region % [Add any additional logic here before the main logic for setCurrentWorkflow here] end

		if (sameAsFormer(this.currentWorkflow, entity)) {
			return;
		}

		if (this.currentWorkflow != null) {
			this.currentWorkflow.unsetCurrentVersion();
		}

		this.currentWorkflow = entity;

		if (reverseAdd) {
			this.currentWorkflow.setCurrentVersion(this, false);
		}

		// % protected region % [Add any additional logic here after the main logic for setCurrentWorkflow here] off begin
		// % protected region % [Add any additional logic here after the main logic for setCurrentWorkflow here] end
	}

	/**
	 * Similar to {@link this#unsetCurrentWorkflow(boolean)} but default to true.
	 */
	public void unsetCurrentWorkflow() {
		this.unsetCurrentWorkflow(true);
	}

	/**
	 * Remove the WorkflowEntity in Current Workflow from this entity.
	 *
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void unsetCurrentWorkflow(boolean reverse) {
		if (reverse && this.currentWorkflow != null) {
			this.currentWorkflow.unsetCurrentVersion(false);
		}
		this.currentWorkflow = null;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Similar to {@link this#setWorkflow(WorkflowEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given WorkflowEntity to be set to this entity
	 */
	public void setWorkflow(@NotNull WorkflowEntity entity) {
		setWorkflow(entity, true);
	}

	/**
	 * Set or update the workflow in this entity with single WorkflowEntity.
	 *
	 * @param entity the given WorkflowEntity to be set or updated to workflow
	 * @param reverseAdd whether this entity should be set or updated to the given entity
	 */
	public void setWorkflow(@NotNull WorkflowEntity entity, boolean reverseAdd) {
		// % protected region % [Add any additional logic here before the main logic for setWorkflow here] off begin
		// % protected region % [Add any additional logic here before the main logic for setWorkflow here] end

		if (sameAsFormer(this.workflow, entity)) {
			return;
		}

		if (this.workflow != null) {
			this.workflow.removeVersions(this, false);
		}
		this.workflow = entity;
		if (reverseAdd) {
			this.workflow.addVersions(this, false);
		}

		// % protected region % [Add any additional logic here after the main logic for setWorkflow here] off begin
		// % protected region % [Add any additional logic here after the main logic for setWorkflow here] end
	}

	/**
	 * Similar to {@link this#unsetWorkflow(boolean)} but default to true.
	 */
	public void unsetWorkflow() {
		this.unsetWorkflow(true);
	}

	/**
	 * Remove Workflow in this entity.
	 *
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void unsetWorkflow(boolean reverse) {
		if (reverse && this.workflow != null) {
			this.workflow.removeVersions(this, false);
		}
		this.workflow = null;
	}

	// % protected region % [Add any additional class methods  here] off begin
	// % protected region % [Add any additional class methods  here] end
}
