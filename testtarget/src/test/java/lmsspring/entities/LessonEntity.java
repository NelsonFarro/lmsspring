/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.entities;

import lmsspring.entities.enums.*;
import lmsspring.lib.file.models.FileEntity;
import lombok.*;
import javax.validation.constraints.NotNull;

import java.util.*;

// % protected region % [Import any additional imports here] off begin
// % protected region % [Import any additional imports here] end

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
public class LessonEntity extends AbstractEntity {

	public LessonEntity() {
		initialiseReferences();
	}

	private void initialiseReferences() {


		var CourseLessonsOneMany = new EntityReference();
			CourseLessonsOneMany.entityName = "CourseLesson";
			CourseLessonsOneMany.oppositeName = "Lesson";
			CourseLessonsOneMany.name = "CourseLessons";
			CourseLessonsOneMany.optional = true;
			CourseLessonsOneMany.type = "One";
			CourseLessonsOneMany.oppositeType = "Many";

		References.add(CourseLessonsOneMany);

		var VersionsOneMany = new EntityReference();
			VersionsOneMany.entityName = "LessonFormVersion";
			VersionsOneMany.oppositeName = "Form";
			VersionsOneMany.name = "Versions";
			VersionsOneMany.optional = true;
			VersionsOneMany.type = "One";
			VersionsOneMany.oppositeType = "Many";

		References.add(VersionsOneMany);

		var FormTilesOneMany = new EntityReference();
			FormTilesOneMany.entityName = "LessonFormTile";
			FormTilesOneMany.oppositeName = "Form";
			FormTilesOneMany.name = "FormTiles";
			FormTilesOneMany.optional = true;
			FormTilesOneMany.type = "One";
			FormTilesOneMany.oppositeType = "Many";

		References.add(FormTilesOneMany);

		var PublishedVersionOneOne = new EntityReference();
			PublishedVersionOneOne.entityName = "LessonFormVersion";
			PublishedVersionOneOne.oppositeName = "PublishedForm";
			PublishedVersionOneOne.name = "PublishedVersion";
			PublishedVersionOneOne.optional = true;
			PublishedVersionOneOne.type = "One";
			PublishedVersionOneOne.oppositeType = "One";

		References.add(PublishedVersionOneOne);
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Attributes
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Modify attribute annotation for Summary here] off begin
	@ToString.Include
	@Setter
	// % protected region % [Modify attribute annotation for Summary here] end
	private String summary;

	// % protected region % [Modify attribute annotation for Description here] off begin
	@ToString.Include
	@Setter
	// % protected region % [Modify attribute annotation for Description here] end
	private String description;

	// % protected region % [Modify attribute annotation for Duration here] off begin
	@ToString.Include
	@Setter
	// % protected region % [Modify attribute annotation for Duration here] end
	private Integer duration;

	// % protected region % [Modify attribute annotation for Difficulty here] off begin
	@ToString.Include
	@Setter
	// % protected region % [Modify attribute annotation for Difficulty here] end
	private DifficultyEnum difficulty;

	// % protected region % [Modify attribute annotation for Name here] off begin
	@ToString.Include
	@Setter
	// % protected region % [Modify attribute annotation for Name here] end
	private String name;

	private Set<FileEntity> coverImage = new HashSet<>();

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing one-to-one
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private LessonFormVersionEntity publishedVersion;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming one-to-one
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing one-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private Set<CourseLessonEntity> courseLessons = new HashSet<>();

	private Set<LessonFormVersionEntity> versions = new HashSet<>();

	private Set<LessonFormTileEntity> formTiles = new HashSet<>();

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming one-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing many-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming many-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing reference methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
	 * Similar to {@link this#addCourseLessons(CourseLessonEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given CourseLessonEntity to be added to this entity
	 */
	public void addCourseLessons(@NotNull CourseLessonEntity entity) {
		addCourseLessons(entity, true);
	}

	/**
	 * Add a new CourseLessonEntity to courseLessons in this entity.
	 *
	 * @param entity the given CourseLessonEntity  to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entity
	 */
	public void addCourseLessons(@NonNull CourseLessonEntity entity, boolean reverseAdd) {
		if (!this.courseLessons.contains(entity)) {
			courseLessons.add(entity);
			if (reverseAdd) {
				entity.setLesson(this, false);
			}
		}
	}

	/**
	 * Similar to {@link this#addCourseLessons(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of CourseLessonEntity to be added to this entity
	 */
	public void addCourseLessons(@NotNull Collection<CourseLessonEntity> entities) {
		addCourseLessons(entities, true);
	}

	/**
	 * Add a new collection of CourseLessonEntity to Course Lessons in this entity.
	 *
	 * @param entities the given collection of CourseLessonEntity to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void addCourseLessons(@NonNull Collection<CourseLessonEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> addCourseLessons(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#removeCourseLessons(CourseLessonEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given CourseLessonEntity to be set to this entity
	 */
	public void removeCourseLessons(@NotNull CourseLessonEntity entity) {
		this.removeCourseLessons(entity, true);
	}

	/**
	 * Remove the given CourseLessonEntity from this entity.
	 *
	 * @param entity the given CourseLessonEntity to be removed from this entity
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void removeCourseLessons(@NotNull CourseLessonEntity entity, boolean reverse) {
		if (reverse) {
			entity.unsetLesson(false);
		}
		this.courseLessons.remove(entity);
	}

	/**
	 * Similar to {@link this#removeCourseLessons(Collection, boolean)} but
	 * default to true for reverse remove.
	 *
	 * @param entities the given collection of CourseLessonEntity to be removed to this entity
	 */
	public void removeCourseLessons(@NotNull Collection<CourseLessonEntity> entities) {
		this.removeCourseLessons(entities, true);
	}

	/**
	 * Remove the given collection of CourseLessonEntity from  to this entity.
	 *
	 * @param entities the given collection of CourseLessonEntity to be removed to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void removeCourseLessons(@NonNull Collection<CourseLessonEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> this.removeCourseLessons(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#setCourseLessons(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of CourseLessonEntity to be set to this entity
	 */
	public void setCourseLessons(@NotNull Collection<CourseLessonEntity> entities) {
		setCourseLessons(entities, true);
	}

	/**
	 * Replace the current entities in Course Lessons with the given ones.
	 *
	 * @param entities the given collection of CourseLessonEntity to replace the old ones
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void setCourseLessons(@NotNull Collection<CourseLessonEntity> entities, boolean reverseAdd) {

		this.unsetCourseLessons();
		this.courseLessons = new HashSet<>(entities);
		if (reverseAdd) {
			this.courseLessons.forEach(courseLessonsEntity -> courseLessonsEntity.setLesson(this, false));
		}
	}

	/**
	 * Similar to {@link this#unsetCourseLessons(boolean)} but
	 * default to true for reverse unset
	 */
	public void unsetCourseLessons() {
		this.unsetCourseLessons(true);
	}

	/**
	 * Remove all the entities in Course Lessons from this entity.
	 * @param doReverse whether this entity should be removed from the given entities
	 */
	public void unsetCourseLessons(boolean doReverse) {
		if (doReverse) {
			this.courseLessons.forEach(courseLessonsEntity -> courseLessonsEntity.unsetLesson(false));
		}
		this.courseLessons.clear();
	}

/**
	 * Similar to {@link this#addVersions(LessonFormVersionEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given LessonFormVersionEntity to be added to this entity
	 */
	public void addVersions(@NotNull LessonFormVersionEntity entity) {
		addVersions(entity, true);
	}

	/**
	 * Add a new LessonFormVersionEntity to versions in this entity.
	 *
	 * @param entity the given LessonFormVersionEntity  to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entity
	 */
	public void addVersions(@NonNull LessonFormVersionEntity entity, boolean reverseAdd) {
		if (!this.versions.contains(entity)) {
			versions.add(entity);
			if (reverseAdd) {
				entity.setForm(this, false);
			}
		}
	}

	/**
	 * Similar to {@link this#addVersions(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of LessonFormVersionEntity to be added to this entity
	 */
	public void addVersions(@NotNull Collection<LessonFormVersionEntity> entities) {
		addVersions(entities, true);
	}

	/**
	 * Add a new collection of LessonFormVersionEntity to Versions in this entity.
	 *
	 * @param entities the given collection of LessonFormVersionEntity to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void addVersions(@NonNull Collection<LessonFormVersionEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> addVersions(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#removeVersions(LessonFormVersionEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given LessonFormVersionEntity to be set to this entity
	 */
	public void removeVersions(@NotNull LessonFormVersionEntity entity) {
		this.removeVersions(entity, true);
	}

	/**
	 * Remove the given LessonFormVersionEntity from this entity.
	 *
	 * @param entity the given LessonFormVersionEntity to be removed from this entity
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void removeVersions(@NotNull LessonFormVersionEntity entity, boolean reverse) {
		if (reverse) {
			entity.unsetForm(false);
		}
		this.versions.remove(entity);
	}

	/**
	 * Similar to {@link this#removeVersions(Collection, boolean)} but
	 * default to true for reverse remove.
	 *
	 * @param entities the given collection of LessonFormVersionEntity to be removed to this entity
	 */
	public void removeVersions(@NotNull Collection<LessonFormVersionEntity> entities) {
		this.removeVersions(entities, true);
	}

	/**
	 * Remove the given collection of LessonFormVersionEntity from  to this entity.
	 *
	 * @param entities the given collection of LessonFormVersionEntity to be removed to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void removeVersions(@NonNull Collection<LessonFormVersionEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> this.removeVersions(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#setVersions(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of LessonFormVersionEntity to be set to this entity
	 */
	public void setVersions(@NotNull Collection<LessonFormVersionEntity> entities) {
		setVersions(entities, true);
	}

	/**
	 * Replace the current entities in Versions with the given ones.
	 *
	 * @param entities the given collection of LessonFormVersionEntity to replace the old ones
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void setVersions(@NotNull Collection<LessonFormVersionEntity> entities, boolean reverseAdd) {

		this.unsetVersions();
		this.versions = new HashSet<>(entities);
		if (reverseAdd) {
			this.versions.forEach(versionsEntity -> versionsEntity.setForm(this, false));
		}
	}

	/**
	 * Similar to {@link this#unsetVersions(boolean)} but
	 * default to true for reverse unset
	 */
	public void unsetVersions() {
		this.unsetVersions(true);
	}

	/**
	 * Remove all the entities in Versions from this entity.
	 * @param doReverse whether this entity should be removed from the given entities
	 */
	public void unsetVersions(boolean doReverse) {
		if (doReverse) {
			this.versions.forEach(versionsEntity -> versionsEntity.unsetForm(false));
		}
		this.versions.clear();
	}

/**
	 * Similar to {@link this#addFormTiles(LessonFormTileEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given LessonFormTileEntity to be added to this entity
	 */
	public void addFormTiles(@NotNull LessonFormTileEntity entity) {
		addFormTiles(entity, true);
	}

	/**
	 * Add a new LessonFormTileEntity to formTiles in this entity.
	 *
	 * @param entity the given LessonFormTileEntity  to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entity
	 */
	public void addFormTiles(@NonNull LessonFormTileEntity entity, boolean reverseAdd) {
		if (!this.formTiles.contains(entity)) {
			formTiles.add(entity);
			if (reverseAdd) {
				entity.setForm(this, false);
			}
		}
	}

	/**
	 * Similar to {@link this#addFormTiles(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of LessonFormTileEntity to be added to this entity
	 */
	public void addFormTiles(@NotNull Collection<LessonFormTileEntity> entities) {
		addFormTiles(entities, true);
	}

	/**
	 * Add a new collection of LessonFormTileEntity to Form tiles in this entity.
	 *
	 * @param entities the given collection of LessonFormTileEntity to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void addFormTiles(@NonNull Collection<LessonFormTileEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> addFormTiles(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#removeFormTiles(LessonFormTileEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given LessonFormTileEntity to be set to this entity
	 */
	public void removeFormTiles(@NotNull LessonFormTileEntity entity) {
		this.removeFormTiles(entity, true);
	}

	/**
	 * Remove the given LessonFormTileEntity from this entity.
	 *
	 * @param entity the given LessonFormTileEntity to be removed from this entity
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void removeFormTiles(@NotNull LessonFormTileEntity entity, boolean reverse) {
		if (reverse) {
			entity.unsetForm(false);
		}
		this.formTiles.remove(entity);
	}

	/**
	 * Similar to {@link this#removeFormTiles(Collection, boolean)} but
	 * default to true for reverse remove.
	 *
	 * @param entities the given collection of LessonFormTileEntity to be removed to this entity
	 */
	public void removeFormTiles(@NotNull Collection<LessonFormTileEntity> entities) {
		this.removeFormTiles(entities, true);
	}

	/**
	 * Remove the given collection of LessonFormTileEntity from  to this entity.
	 *
	 * @param entities the given collection of LessonFormTileEntity to be removed to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void removeFormTiles(@NonNull Collection<LessonFormTileEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> this.removeFormTiles(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#setFormTiles(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of LessonFormTileEntity to be set to this entity
	 */
	public void setFormTiles(@NotNull Collection<LessonFormTileEntity> entities) {
		setFormTiles(entities, true);
	}

	/**
	 * Replace the current entities in Form tiles with the given ones.
	 *
	 * @param entities the given collection of LessonFormTileEntity to replace the old ones
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void setFormTiles(@NotNull Collection<LessonFormTileEntity> entities, boolean reverseAdd) {

		this.unsetFormTiles();
		this.formTiles = new HashSet<>(entities);
		if (reverseAdd) {
			this.formTiles.forEach(formTilesEntity -> formTilesEntity.setForm(this, false));
		}
	}

	/**
	 * Similar to {@link this#unsetFormTiles(boolean)} but
	 * default to true for reverse unset
	 */
	public void unsetFormTiles() {
		this.unsetFormTiles(true);
	}

	/**
	 * Remove all the entities in Form tiles from this entity.
	 * @param doReverse whether this entity should be removed from the given entities
	 */
	public void unsetFormTiles(boolean doReverse) {
		if (doReverse) {
			this.formTiles.forEach(formTilesEntity -> formTilesEntity.unsetForm(false));
		}
		this.formTiles.clear();
	}

	/**
	 * Similar to {@link this#setPublishedVersion(LessonFormVersionEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given LessonEntity to be set to this entity
	 */
	public void setPublishedVersion(@NotNull LessonFormVersionEntity entity) {
		setPublishedVersion(entity, true);
	}

	/**
	 * Set or update publishedVersion with the given LessonFormVersionEntity.
	 *
	 * @param entity the LessonEntity to be set or updated
	 * @param reverseAdd whether this entity should be set or updated to the given entity
	 */
	public void setPublishedVersion(@NotNull LessonFormVersionEntity entity, boolean reverseAdd) {
		// % protected region % [Add any additional logic here before the main logic for setPublishedVersion here] off begin
		// % protected region % [Add any additional logic here before the main logic for setPublishedVersion here] end

		if (sameAsFormer(this.publishedVersion, entity)) {
			return;
		}

		if (this.publishedVersion != null) {
			this.publishedVersion.unsetPublishedForm();
		}

		this.publishedVersion = entity;

		if (reverseAdd) {
			this.publishedVersion.setPublishedForm(this, false);
		}

		// % protected region % [Add any additional logic here after the main logic for setPublishedVersion here] off begin
		// % protected region % [Add any additional logic here after the main logic for setPublishedVersion here] end
	}

	/**
	 * Similar to {@link this#unsetPublishedVersion(boolean)} but default to true.
	 */
	public void unsetPublishedVersion() {
		this.unsetPublishedVersion(true);
	}

	/**
	 * Remove the LessonFormVersionEntity in Published version from this entity.
	 *
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void unsetPublishedVersion(boolean reverse) {
		if (reverse && this.publishedVersion != null) {
			this.publishedVersion.unsetPublishedForm(false);
		}
		this.publishedVersion = null;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



	public void addCoverImage(FileEntity newFile) {
		coverImage.add(newFile);
	}

	public void addAllCoverImage(Collection<FileEntity> newFiles) {
		coverImage.addAll(newFiles);
	}

	public void removeCoverImage(FileEntity newFile) {
		coverImage.remove(newFile);
	}

	public boolean containsCoverImage(FileEntity newFile) {
		return coverImage.contains(newFile);
	}

	public void clearAllCoverImage() {
		coverImage.clear();
	}

	// % protected region % [Add any additional class methods  here] off begin
	// % protected region % [Add any additional class methods  here] end
}
