/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.entities;

import lombok.*;
import javax.validation.constraints.NotNull;

import java.util.*;

// % protected region % [Import any additional imports here] off begin
// % protected region % [Import any additional imports here] end

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
public class CourseCategoryEntity extends AbstractEntity {

	public CourseCategoryEntity() {
		initialiseReferences();
	}

	private void initialiseReferences() {


		var CoursesOneMany = new EntityReference();
			CoursesOneMany.entityName = "Course";
			CoursesOneMany.oppositeName = "CourseCategory";
			CoursesOneMany.name = "Courses";
			CoursesOneMany.optional = true;
			CoursesOneMany.type = "One";
			CoursesOneMany.oppositeType = "Many";

		References.add(CoursesOneMany);
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Attributes
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Modify attribute annotation for Name here] off begin
	@ToString.Include
	@Setter
	// % protected region % [Modify attribute annotation for Name here] end
	private String name;

	// % protected region % [Modify attribute annotation for Colour here] off begin
	@ToString.Include
	@Setter
	// % protected region % [Modify attribute annotation for Colour here] end
	private String colour;

	// % protected region % [Modify attribute annotation for Summary here] off begin
	@ToString.Include
	@Setter
	// % protected region % [Modify attribute annotation for Summary here] end
	private String summary;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing one-to-one
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming one-to-one
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing one-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private Set<CourseEntity> courses = new HashSet<>();

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming one-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing many-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming many-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing reference methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
	 * Similar to {@link this#addCourses(CourseEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given CourseEntity to be added to this entity
	 */
	public void addCourses(@NotNull CourseEntity entity) {
		addCourses(entity, true);
	}

	/**
	 * Add a new CourseEntity to courses in this entity.
	 *
	 * @param entity the given CourseEntity  to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entity
	 */
	public void addCourses(@NonNull CourseEntity entity, boolean reverseAdd) {
		if (!this.courses.contains(entity)) {
			courses.add(entity);
			if (reverseAdd) {
				entity.setCourseCategory(this, false);
			}
		}
	}

	/**
	 * Similar to {@link this#addCourses(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of CourseEntity to be added to this entity
	 */
	public void addCourses(@NotNull Collection<CourseEntity> entities) {
		addCourses(entities, true);
	}

	/**
	 * Add a new collection of CourseEntity to Courses in this entity.
	 *
	 * @param entities the given collection of CourseEntity to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void addCourses(@NonNull Collection<CourseEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> addCourses(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#removeCourses(CourseEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given CourseEntity to be set to this entity
	 */
	public void removeCourses(@NotNull CourseEntity entity) {
		this.removeCourses(entity, true);
	}

	/**
	 * Remove the given CourseEntity from this entity.
	 *
	 * @param entity the given CourseEntity to be removed from this entity
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void removeCourses(@NotNull CourseEntity entity, boolean reverse) {
		if (reverse) {
			entity.unsetCourseCategory(false);
		}
		this.courses.remove(entity);
	}

	/**
	 * Similar to {@link this#removeCourses(Collection, boolean)} but
	 * default to true for reverse remove.
	 *
	 * @param entities the given collection of CourseEntity to be removed to this entity
	 */
	public void removeCourses(@NotNull Collection<CourseEntity> entities) {
		this.removeCourses(entities, true);
	}

	/**
	 * Remove the given collection of CourseEntity from  to this entity.
	 *
	 * @param entities the given collection of CourseEntity to be removed to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void removeCourses(@NonNull Collection<CourseEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> this.removeCourses(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#setCourses(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of CourseEntity to be set to this entity
	 */
	public void setCourses(@NotNull Collection<CourseEntity> entities) {
		setCourses(entities, true);
	}

	/**
	 * Replace the current entities in Courses with the given ones.
	 *
	 * @param entities the given collection of CourseEntity to replace the old ones
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void setCourses(@NotNull Collection<CourseEntity> entities, boolean reverseAdd) {

		this.unsetCourses();
		this.courses = new HashSet<>(entities);
		if (reverseAdd) {
			this.courses.forEach(coursesEntity -> coursesEntity.setCourseCategory(this, false));
		}
	}

	/**
	 * Similar to {@link this#unsetCourses(boolean)} but
	 * default to true for reverse unset
	 */
	public void unsetCourses() {
		this.unsetCourses(true);
	}

	/**
	 * Remove all the entities in Courses from this entity.
	 * @param doReverse whether this entity should be removed from the given entities
	 */
	public void unsetCourses(boolean doReverse) {
		if (doReverse) {
			this.courses.forEach(coursesEntity -> coursesEntity.unsetCourseCategory(false));
		}
		this.courses.clear();
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



	// % protected region % [Add any additional class methods  here] off begin
	// % protected region % [Add any additional class methods  here] end
}
