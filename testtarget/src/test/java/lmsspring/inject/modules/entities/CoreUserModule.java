/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.inject.modules.entities;

import lmsspring.entities.*;
import lmsspring.entities.enums.*;
import lmsspring.inject.factories.*;
import com.google.inject.AbstractModule;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.andreinc.mockneat.MockNeat;
import net.andreinc.mockneat.types.enums.StringType;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.UUID;

// % protected region % [Apply any additional imports here] off begin
// % protected region % [Apply any additional imports here] end

/**
 * Guice module for Core User used to register providers for dependency injection.
 */
@Slf4j
@AllArgsConstructor
public class CoreUserModule extends AbstractModule {
	// % protected region % [Apply any additional class fields here] off begin
	// % protected region % [Apply any additional class fields here] end

	@Override
	protected void configure() {
		log.trace("Configuring CoreUserModule");

		super.configure();

		// % protected region % [Apply any additional configure steps here] off begin
		// % protected region % [Apply any additional configure steps here] end

		log.trace("Configured CoreUserModule");
	}

	/**
	 * Return a factory for mass data generation.
	 */
	@Provides
	public CoreUserEntityFactory coreUserEntityFactory(
			// % protected region % [Apply any additional injected arguments for coreUserEntityFactory here] off begin
			// % protected region % [Apply any additional injected arguments for coreUserEntityFactory here] end
			Injector injector
	) {
		log.trace("Creating CoreUserEntityFactory");

		// % protected region % [Apply any additional logic for coreUserEntityFactory before the main body here] off begin
		// % protected region % [Apply any additional logic for coreUserEntityFactory before the main body here] end

		CoreUserEntityFactory entityFactory = new CoreUserEntityFactory(
				// % protected region % [Apply any additional constructor arguments for CoreUserEntityFactory here] off begin
				// % protected region % [Apply any additional constructor arguments for CoreUserEntityFactory here] end
				injector
		);

		// % protected region % [Apply any additional logic for coreUserEntityFactory after the main body here] off begin
		// % protected region % [Apply any additional logic for coreUserEntityFactory after the main body here] end

		log.trace("Created CoreUserEntityFactory");

		return entityFactory;
	}

	/**
	 * Return an empty Core User entity with no references set.
	 */
	@Provides
	@Named("coreUserEntityWithNoRef")
	public CoreUserEntity coreUserEntityWithNoRef(
			// % protected region % [Apply any additional constructor parameters for coreUserEntityWithNoRef here] off begin
			// % protected region % [Apply any additional constructor parameters for coreUserEntityWithNoRef here] end
			MockNeat mock
	) {
		log.trace("Creating entity of type CoreUserEntity with no reference");

		// % protected region % [Apply any additional logic for coreUserWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for coreUserWithNoRef before the main body here] end

		CoreUserEntity newEntity = new CoreUserEntity();
		newEntity.setId(UUID.randomUUID());
		newEntity.setCreated(OffsetDateTime.now());
		newEntity.setModified(OffsetDateTime.now());
		// % protected region % [Add customisation for First Name here] off begin
		String randomStringforFirstName = mock
				.strings()
				.get();
		newEntity.setFirstName(randomStringforFirstName);
		// % protected region % [Add customisation for First Name here] end
		// % protected region % [Add customisation for Last Name here] off begin
		String randomStringforLastName = mock
				.strings()
				.get();
		newEntity.setLastName(randomStringforLastName);
		// % protected region % [Add customisation for Last Name here] end
		// % protected region % [Add customisation for Name here] off begin
		String randomStringforName = mock
				.strings()
				.get();
		newEntity.setName(randomStringforName);
		// % protected region % [Add customisation for Name here] end
		// % protected region % [Add customisation for Username here] off begin
		String randomStringforUsername = mock
				.emails()
				.get();
		newEntity.setUsername(randomStringforUsername);
		// % protected region % [Add customisation for Username here] end
		// % protected region % [Add customisation for Password here] off begin
		String randomStringforPassword = mock
				.strings()
				.size(255)
				.get();
		newEntity.setPassword(randomStringforPassword);
		// % protected region % [Add customisation for Password here] end
		// % protected region % [Add customisation for Email Confirmed confirmed here] off begin
		newEntity.setEmailConfirmedConfirmed(mock.bools().get());
		// % protected region % [Add customisation for Email Confirmed confirmed here] end
		// % protected region % [Add customisation for Email here] off begin
		String randomStringforEmail = mock
				.strings()
				.get();
		newEntity.setEmail(randomStringforEmail);
		// % protected region % [Add customisation for Email here] end
		// % protected region % [Add customisation for Is Archived here] off begin
		newEntity.setIsArchived(mock.bools().get());
		// % protected region % [Add customisation for Is Archived here] end

		// % protected region % [Apply any additional logic for coreUserWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for coreUserWithNoRef after the main body here] end

		log.trace("Created entity of type CoreUserEntity with no reference");

		return newEntity;
	}

	/**
	 * Return a collection of Core User entities with no reference at all.
	 */
	@Provides
	@Named("coreUserEntitiesWithNoRef")
	public Collection<CoreUserEntity> coreUserEntitiesWithNoRef(
		// % protected region % [Apply any additional constructor parameters for coreUserEntitiesWithNoRef here] off begin
		// % protected region % [Apply any additional constructor parameters for coreUserEntitiesWithNoRef here] end
		CoreUserEntityFactory coreUserEntityFactory
	) {
		log.trace("Creating entities of type CoreUserEntity with no reference");

		// % protected region % [Apply any additional logic for coreUserEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for coreUserEntitiesWithNoRef before the main body here] end

		Collection<CoreUserEntity> newEntities = coreUserEntityFactory.createMultipleWithNoRef(10);

		// % protected region % [Apply any additional logic for coreUserEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for coreUserEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type CoreUserEntity with no reference");

		return newEntities;
	}

	/**
	 * Return a Core User entity with all references set.
	 */
	@Provides
	@Named("coreUserEntityWithRefs")
	public CoreUserEntity coreUserEntityWithRefs(
			// % protected region % [Apply any additional constructor parameters for coreUserEntityWithRefs here] off begin
			// % protected region % [Apply any additional constructor parameters for coreUserEntityWithRefs here] end
			Injector injector
	) {
		log.trace("Creating entity of type CoreUserEntity with references");

		// % protected region % [Apply any additional logic for coreUserEntityWithRefs before the main body here] off begin
		// % protected region % [Apply any additional logic for coreUserEntityWithRefs before the main body here] end

		CoreUserEntity coreUserEntity = injector.getInstance(Key.get(CoreUserEntity.class, Names.named("coreUserEntityWithNoRef")));

		// % protected region % [Apply any additional logic for coreUserEntityWithRefs after the main body here] off begin
		// % protected region % [Apply any additional logic for coreUserEntityWithRefs after the main body here] end

		log.trace("Created entity of type CoreUserEntity with references");

		return coreUserEntity;
	}

	/**
	 * Return a collection of Core User entities with all references set.
	 */
	@Provides
	@Named("coreUserEntitiesWithRefs")
	public Collection<CoreUserEntity> coreUserEntitiesWithRefs(
		// % protected region % [Apply any additional constructor parameters for coreUserEntitiesWithRefs here] off begin
		// % protected region % [Apply any additional constructor parameters for coreUserEntitiesWithRefs here] end
		CoreUserEntityFactory coreUserEntityFactory
	) {
		log.trace("Creating entities of type CoreUserEntity with references");

		// % protected region % [Apply any additional logic for coreUserEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for coreUserEntitiesWithNoRef before the main body here] end

		Collection<CoreUserEntity> newEntities = coreUserEntityFactory.createMultipleWithRefs(10);

		// % protected region % [Apply any additional logic for coreUserEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for coreUserEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type CoreUserEntity with references");

		return newEntities;
	}

	// % protected region % [Apply any additional class methods here] off begin
	// % protected region % [Apply any additional class methods here] end
}
