/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.inject.modules.entities;

import lmsspring.entities.*;
import lmsspring.entities.enums.*;
import lmsspring.inject.factories.*;
import com.google.inject.AbstractModule;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.andreinc.mockneat.MockNeat;
import net.andreinc.mockneat.types.enums.StringType;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.UUID;

// % protected region % [Apply any additional imports here] off begin
// % protected region % [Apply any additional imports here] end

/**
 * Guice module for Lesson Form Submission used to register providers for dependency injection.
 */
@Slf4j
@AllArgsConstructor
public class LessonFormSubmissionModule extends AbstractModule {
	// % protected region % [Apply any additional class fields here] off begin
	// % protected region % [Apply any additional class fields here] end

	@Override
	protected void configure() {
		log.trace("Configuring LessonFormSubmissionModule");

		super.configure();

		// % protected region % [Apply any additional configure steps here] off begin
		// % protected region % [Apply any additional configure steps here] end

		log.trace("Configured LessonFormSubmissionModule");
	}

	/**
	 * Return a factory for mass data generation.
	 */
	@Provides
	public LessonFormSubmissionEntityFactory lessonFormSubmissionEntityFactory(
			// % protected region % [Apply any additional injected arguments for lessonFormSubmissionEntityFactory here] off begin
			// % protected region % [Apply any additional injected arguments for lessonFormSubmissionEntityFactory here] end
			Injector injector
	) {
		log.trace("Creating LessonFormSubmissionEntityFactory");

		// % protected region % [Apply any additional logic for lessonFormSubmissionEntityFactory before the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormSubmissionEntityFactory before the main body here] end

		LessonFormSubmissionEntityFactory entityFactory = new LessonFormSubmissionEntityFactory(
				// % protected region % [Apply any additional constructor arguments for LessonFormSubmissionEntityFactory here] off begin
				// % protected region % [Apply any additional constructor arguments for LessonFormSubmissionEntityFactory here] end
				injector
		);

		// % protected region % [Apply any additional logic for lessonFormSubmissionEntityFactory after the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormSubmissionEntityFactory after the main body here] end

		log.trace("Created LessonFormSubmissionEntityFactory");

		return entityFactory;
	}

	/**
	 * Return an empty Lesson Form Submission entity with no references set.
	 */
	@Provides
	@Named("lessonFormSubmissionEntityWithNoRef")
	public LessonFormSubmissionEntity lessonFormSubmissionEntityWithNoRef(
			// % protected region % [Apply any additional constructor parameters for lessonFormSubmissionEntityWithNoRef here] off begin
			// % protected region % [Apply any additional constructor parameters for lessonFormSubmissionEntityWithNoRef here] end
			MockNeat mock
	) {
		log.trace("Creating entity of type LessonFormSubmissionEntity with no reference");

		// % protected region % [Apply any additional logic for lessonFormSubmissionWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormSubmissionWithNoRef before the main body here] end

		LessonFormSubmissionEntity newEntity = new LessonFormSubmissionEntity();
		newEntity.setId(UUID.randomUUID());
		newEntity.setCreated(OffsetDateTime.now());
		newEntity.setModified(OffsetDateTime.now());
		// % protected region % [Add customisation for Submission Data here] off begin
		String randomStringforSubmissionData = mock
				.strings()
				.get();
		newEntity.setSubmissionData(randomStringforSubmissionData);
		// % protected region % [Add customisation for Submission Data here] end

		// % protected region % [Apply any additional logic for lessonFormSubmissionWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormSubmissionWithNoRef after the main body here] end

		log.trace("Created entity of type LessonFormSubmissionEntity with no reference");

		return newEntity;
	}

	/**
	 * Return a collection of Lesson Form Submission entities with no reference at all.
	 */
	@Provides
	@Named("lessonFormSubmissionEntitiesWithNoRef")
	public Collection<LessonFormSubmissionEntity> lessonFormSubmissionEntitiesWithNoRef(
		// % protected region % [Apply any additional constructor parameters for lessonFormSubmissionEntitiesWithNoRef here] off begin
		// % protected region % [Apply any additional constructor parameters for lessonFormSubmissionEntitiesWithNoRef here] end
		LessonFormSubmissionEntityFactory lessonFormSubmissionEntityFactory
	) {
		log.trace("Creating entities of type LessonFormSubmissionEntity with no reference");

		// % protected region % [Apply any additional logic for lessonFormSubmissionEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormSubmissionEntitiesWithNoRef before the main body here] end

		Collection<LessonFormSubmissionEntity> newEntities = lessonFormSubmissionEntityFactory.createMultipleWithNoRef(10);

		// % protected region % [Apply any additional logic for lessonFormSubmissionEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormSubmissionEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type LessonFormSubmissionEntity with no reference");

		return newEntities;
	}

	/**
	 * Return a Lesson Form Submission entity with all references set.
	 */
	@Provides
	@Named("lessonFormSubmissionEntityWithRefs")
	public LessonFormSubmissionEntity lessonFormSubmissionEntityWithRefs(
			// % protected region % [Apply any additional constructor parameters for lessonFormSubmissionEntityWithRefs here] off begin
			// % protected region % [Apply any additional constructor parameters for lessonFormSubmissionEntityWithRefs here] end
			@Named("lessonFormVersionEntityWithRefs") LessonFormVersionEntity submittedForm,
			Injector injector
	) {
		log.trace("Creating entity of type LessonFormSubmissionEntity with references");

		// % protected region % [Apply any additional logic for lessonFormSubmissionEntityWithRefs before the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormSubmissionEntityWithRefs before the main body here] end

		LessonFormSubmissionEntity lessonFormSubmissionEntity = injector.getInstance(Key.get(LessonFormSubmissionEntity.class, Names.named("lessonFormSubmissionEntityWithNoRef")));
		lessonFormSubmissionEntity.setSubmittedForm(submittedForm, true);

		// % protected region % [Apply any additional logic for lessonFormSubmissionEntityWithRefs after the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormSubmissionEntityWithRefs after the main body here] end

		log.trace("Created entity of type LessonFormSubmissionEntity with references");

		return lessonFormSubmissionEntity;
	}

	/**
	 * Return a collection of Lesson Form Submission entities with all references set.
	 */
	@Provides
	@Named("lessonFormSubmissionEntitiesWithRefs")
	public Collection<LessonFormSubmissionEntity> lessonFormSubmissionEntitiesWithRefs(
		// % protected region % [Apply any additional constructor parameters for lessonFormSubmissionEntitiesWithRefs here] off begin
		// % protected region % [Apply any additional constructor parameters for lessonFormSubmissionEntitiesWithRefs here] end
		LessonFormSubmissionEntityFactory lessonFormSubmissionEntityFactory
	) {
		log.trace("Creating entities of type LessonFormSubmissionEntity with references");

		// % protected region % [Apply any additional logic for lessonFormSubmissionEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormSubmissionEntitiesWithNoRef before the main body here] end

		Collection<LessonFormSubmissionEntity> newEntities = lessonFormSubmissionEntityFactory.createMultipleWithRefs(10);

		// % protected region % [Apply any additional logic for lessonFormSubmissionEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormSubmissionEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type LessonFormSubmissionEntity with references");

		return newEntities;
	}

	// % protected region % [Apply any additional class methods here] off begin
	// % protected region % [Apply any additional class methods here] end
}
