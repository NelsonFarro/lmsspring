/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.inject.modules.entities;

import lmsspring.entities.*;
import lmsspring.entities.enums.*;
import lmsspring.inject.factories.*;
import com.google.inject.AbstractModule;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.andreinc.mockneat.MockNeat;
import net.andreinc.mockneat.types.enums.StringType;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.UUID;

// % protected region % [Apply any additional imports here] off begin
// % protected region % [Apply any additional imports here] end

/**
 * Guice module for Workflow State used to register providers for dependency injection.
 */
@Slf4j
@AllArgsConstructor
public class WorkflowStateModule extends AbstractModule {
	// % protected region % [Apply any additional class fields here] off begin
	// % protected region % [Apply any additional class fields here] end

	@Override
	protected void configure() {
		log.trace("Configuring WorkflowStateModule");

		super.configure();

		// % protected region % [Apply any additional configure steps here] off begin
		// % protected region % [Apply any additional configure steps here] end

		log.trace("Configured WorkflowStateModule");
	}

	/**
	 * Return a factory for mass data generation.
	 */
	@Provides
	public WorkflowStateEntityFactory workflowStateEntityFactory(
			// % protected region % [Apply any additional injected arguments for workflowStateEntityFactory here] off begin
			// % protected region % [Apply any additional injected arguments for workflowStateEntityFactory here] end
			Injector injector
	) {
		log.trace("Creating WorkflowStateEntityFactory");

		// % protected region % [Apply any additional logic for workflowStateEntityFactory before the main body here] off begin
		// % protected region % [Apply any additional logic for workflowStateEntityFactory before the main body here] end

		WorkflowStateEntityFactory entityFactory = new WorkflowStateEntityFactory(
				// % protected region % [Apply any additional constructor arguments for WorkflowStateEntityFactory here] off begin
				// % protected region % [Apply any additional constructor arguments for WorkflowStateEntityFactory here] end
				injector
		);

		// % protected region % [Apply any additional logic for workflowStateEntityFactory after the main body here] off begin
		// % protected region % [Apply any additional logic for workflowStateEntityFactory after the main body here] end

		log.trace("Created WorkflowStateEntityFactory");

		return entityFactory;
	}

	/**
	 * Return an empty Workflow State entity with no references set.
	 */
	@Provides
	@Named("workflowStateEntityWithNoRef")
	public WorkflowStateEntity workflowStateEntityWithNoRef(
			// % protected region % [Apply any additional constructor parameters for workflowStateEntityWithNoRef here] off begin
			// % protected region % [Apply any additional constructor parameters for workflowStateEntityWithNoRef here] end
			MockNeat mock
	) {
		log.trace("Creating entity of type WorkflowStateEntity with no reference");

		// % protected region % [Apply any additional logic for workflowStateWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for workflowStateWithNoRef before the main body here] end

		WorkflowStateEntity newEntity = new WorkflowStateEntity();
		newEntity.setId(UUID.randomUUID());
		newEntity.setCreated(OffsetDateTime.now());
		newEntity.setModified(OffsetDateTime.now());
		// % protected region % [Add customisation for Display Index here] off begin
		newEntity.setDisplayIndex(mock.ints().get());
		// % protected region % [Add customisation for Display Index here] end
		// % protected region % [Add customisation for Step Name here] off begin
		String randomStringforStepName = mock
				.strings()
				.get();
		newEntity.setStepName(randomStringforStepName);
		// % protected region % [Add customisation for Step Name here] end
		// % protected region % [Add customisation for State Description here] off begin
		String randomStringforStateDescription = mock
				.strings()
				.get();
		newEntity.setStateDescription(randomStringforStateDescription);
		// % protected region % [Add customisation for State Description here] end
		// % protected region % [Add customisation for Is Start State here] off begin
		newEntity.setIsStartState(mock.bools().get());
		// % protected region % [Add customisation for Is Start State here] end

		// % protected region % [Apply any additional logic for workflowStateWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for workflowStateWithNoRef after the main body here] end

		log.trace("Created entity of type WorkflowStateEntity with no reference");

		return newEntity;
	}

	/**
	 * Return a collection of Workflow State entities with no reference at all.
	 */
	@Provides
	@Named("workflowStateEntitiesWithNoRef")
	public Collection<WorkflowStateEntity> workflowStateEntitiesWithNoRef(
		// % protected region % [Apply any additional constructor parameters for workflowStateEntitiesWithNoRef here] off begin
		// % protected region % [Apply any additional constructor parameters for workflowStateEntitiesWithNoRef here] end
		WorkflowStateEntityFactory workflowStateEntityFactory
	) {
		log.trace("Creating entities of type WorkflowStateEntity with no reference");

		// % protected region % [Apply any additional logic for workflowStateEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for workflowStateEntitiesWithNoRef before the main body here] end

		Collection<WorkflowStateEntity> newEntities = workflowStateEntityFactory.createMultipleWithNoRef(10);

		// % protected region % [Apply any additional logic for workflowStateEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for workflowStateEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type WorkflowStateEntity with no reference");

		return newEntities;
	}

	/**
	 * Return a Workflow State entity with all references set.
	 */
	@Provides
	@Named("workflowStateEntityWithRefs")
	public WorkflowStateEntity workflowStateEntityWithRefs(
			// % protected region % [Apply any additional constructor parameters for workflowStateEntityWithRefs here] off begin
			// % protected region % [Apply any additional constructor parameters for workflowStateEntityWithRefs here] end
			@Named("workflowVersionEntityWithRefs") WorkflowVersionEntity workflowVersion,
			Injector injector
	) {
		log.trace("Creating entity of type WorkflowStateEntity with references");

		// % protected region % [Apply any additional logic for workflowStateEntityWithRefs before the main body here] off begin
		// % protected region % [Apply any additional logic for workflowStateEntityWithRefs before the main body here] end

		WorkflowStateEntity workflowStateEntity = injector.getInstance(Key.get(WorkflowStateEntity.class, Names.named("workflowStateEntityWithNoRef")));
		workflowStateEntity.setWorkflowVersion(workflowVersion, true);

		// % protected region % [Apply any additional logic for workflowStateEntityWithRefs after the main body here] off begin
		// % protected region % [Apply any additional logic for workflowStateEntityWithRefs after the main body here] end

		log.trace("Created entity of type WorkflowStateEntity with references");

		return workflowStateEntity;
	}

	/**
	 * Return a collection of Workflow State entities with all references set.
	 */
	@Provides
	@Named("workflowStateEntitiesWithRefs")
	public Collection<WorkflowStateEntity> workflowStateEntitiesWithRefs(
		// % protected region % [Apply any additional constructor parameters for workflowStateEntitiesWithRefs here] off begin
		// % protected region % [Apply any additional constructor parameters for workflowStateEntitiesWithRefs here] end
		WorkflowStateEntityFactory workflowStateEntityFactory
	) {
		log.trace("Creating entities of type WorkflowStateEntity with references");

		// % protected region % [Apply any additional logic for workflowStateEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for workflowStateEntitiesWithNoRef before the main body here] end

		Collection<WorkflowStateEntity> newEntities = workflowStateEntityFactory.createMultipleWithRefs(10);

		// % protected region % [Apply any additional logic for workflowStateEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for workflowStateEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type WorkflowStateEntity with references");

		return newEntities;
	}

	// % protected region % [Apply any additional class methods here] off begin
	// % protected region % [Apply any additional class methods here] end
}
