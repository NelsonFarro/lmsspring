/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.inject.modules.entities;

import lmsspring.entities.*;
import lmsspring.entities.enums.*;
import lmsspring.inject.factories.*;
import com.google.inject.AbstractModule;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.andreinc.mockneat.MockNeat;
import net.andreinc.mockneat.types.enums.StringType;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.UUID;

// % protected region % [Apply any additional imports here] off begin
// % protected region % [Apply any additional imports here] end

/**
 * Guice module for Article used to register providers for dependency injection.
 */
@Slf4j
@AllArgsConstructor
public class ArticleModule extends AbstractModule {
	// % protected region % [Apply any additional class fields here] off begin
	// % protected region % [Apply any additional class fields here] end

	@Override
	protected void configure() {
		log.trace("Configuring ArticleModule");

		super.configure();

		// % protected region % [Apply any additional configure steps here] off begin
		// % protected region % [Apply any additional configure steps here] end

		log.trace("Configured ArticleModule");
	}

	/**
	 * Return a factory for mass data generation.
	 */
	@Provides
	public ArticleEntityFactory articleEntityFactory(
			// % protected region % [Apply any additional injected arguments for articleEntityFactory here] off begin
			// % protected region % [Apply any additional injected arguments for articleEntityFactory here] end
			Injector injector
	) {
		log.trace("Creating ArticleEntityFactory");

		// % protected region % [Apply any additional logic for articleEntityFactory before the main body here] off begin
		// % protected region % [Apply any additional logic for articleEntityFactory before the main body here] end

		ArticleEntityFactory entityFactory = new ArticleEntityFactory(
				// % protected region % [Apply any additional constructor arguments for ArticleEntityFactory here] off begin
				// % protected region % [Apply any additional constructor arguments for ArticleEntityFactory here] end
				injector
		);

		// % protected region % [Apply any additional logic for articleEntityFactory after the main body here] off begin
		// % protected region % [Apply any additional logic for articleEntityFactory after the main body here] end

		log.trace("Created ArticleEntityFactory");

		return entityFactory;
	}

	/**
	 * Return an empty Article entity with no references set.
	 */
	@Provides
	@Named("articleEntityWithNoRef")
	public ArticleEntity articleEntityWithNoRef(
			// % protected region % [Apply any additional constructor parameters for articleEntityWithNoRef here] off begin
			// % protected region % [Apply any additional constructor parameters for articleEntityWithNoRef here] end
			MockNeat mock
	) {
		log.trace("Creating entity of type ArticleEntity with no reference");

		// % protected region % [Apply any additional logic for articleWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for articleWithNoRef before the main body here] end

		ArticleEntity newEntity = new ArticleEntity();
		newEntity.setId(UUID.randomUUID());
		newEntity.setCreated(OffsetDateTime.now());
		newEntity.setModified(OffsetDateTime.now());
		// % protected region % [Add customisation for Title here] off begin
		String randomStringforTitle = mock
				.strings()
				.get();
		newEntity.setTitle(randomStringforTitle);
		// % protected region % [Add customisation for Title here] end
		// % protected region % [Add customisation for Summary here] off begin
		String randomStringforSummary = mock
				.strings()
				.get();
		newEntity.setSummary(randomStringforSummary);
		// % protected region % [Add customisation for Summary here] end
		// % protected region % [Add customisation for Content here] off begin
		String randomStringforContent = mock
				.strings()
				.get();
		newEntity.setContent(randomStringforContent);
		// % protected region % [Add customisation for Content here] end

		// % protected region % [Apply any additional logic for articleWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for articleWithNoRef after the main body here] end

		log.trace("Created entity of type ArticleEntity with no reference");

		return newEntity;
	}

	/**
	 * Return a collection of Article entities with no reference at all.
	 */
	@Provides
	@Named("articleEntitiesWithNoRef")
	public Collection<ArticleEntity> articleEntitiesWithNoRef(
		// % protected region % [Apply any additional constructor parameters for articleEntitiesWithNoRef here] off begin
		// % protected region % [Apply any additional constructor parameters for articleEntitiesWithNoRef here] end
		ArticleEntityFactory articleEntityFactory
	) {
		log.trace("Creating entities of type ArticleEntity with no reference");

		// % protected region % [Apply any additional logic for articleEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for articleEntitiesWithNoRef before the main body here] end

		Collection<ArticleEntity> newEntities = articleEntityFactory.createMultipleWithNoRef(10);

		// % protected region % [Apply any additional logic for articleEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for articleEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type ArticleEntity with no reference");

		return newEntities;
	}

	/**
	 * Return a Article entity with all references set.
	 */
	@Provides
	@Named("articleEntityWithRefs")
	public ArticleEntity articleEntityWithRefs(
			// % protected region % [Apply any additional constructor parameters for articleEntityWithRefs here] off begin
			// % protected region % [Apply any additional constructor parameters for articleEntityWithRefs here] end
			Injector injector
	) {
		log.trace("Creating entity of type ArticleEntity with references");

		// % protected region % [Apply any additional logic for articleEntityWithRefs before the main body here] off begin
		// % protected region % [Apply any additional logic for articleEntityWithRefs before the main body here] end

		ArticleEntity articleEntity = injector.getInstance(Key.get(ArticleEntity.class, Names.named("articleEntityWithNoRef")));

		// % protected region % [Apply any additional logic for articleEntityWithRefs after the main body here] off begin
		// % protected region % [Apply any additional logic for articleEntityWithRefs after the main body here] end

		log.trace("Created entity of type ArticleEntity with references");

		return articleEntity;
	}

	/**
	 * Return a collection of Article entities with all references set.
	 */
	@Provides
	@Named("articleEntitiesWithRefs")
	public Collection<ArticleEntity> articleEntitiesWithRefs(
		// % protected region % [Apply any additional constructor parameters for articleEntitiesWithRefs here] off begin
		// % protected region % [Apply any additional constructor parameters for articleEntitiesWithRefs here] end
		ArticleEntityFactory articleEntityFactory
	) {
		log.trace("Creating entities of type ArticleEntity with references");

		// % protected region % [Apply any additional logic for articleEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for articleEntitiesWithNoRef before the main body here] end

		Collection<ArticleEntity> newEntities = articleEntityFactory.createMultipleWithRefs(10);

		// % protected region % [Apply any additional logic for articleEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for articleEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type ArticleEntity with references");

		return newEntities;
	}

	// % protected region % [Apply any additional class methods here] off begin
	// % protected region % [Apply any additional class methods here] end
}
