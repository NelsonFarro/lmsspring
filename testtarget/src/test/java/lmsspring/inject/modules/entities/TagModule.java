/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.inject.modules.entities;

import lmsspring.entities.*;
import lmsspring.entities.enums.*;
import lmsspring.inject.factories.*;
import com.google.inject.AbstractModule;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.andreinc.mockneat.MockNeat;
import net.andreinc.mockneat.types.enums.StringType;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.UUID;

// % protected region % [Apply any additional imports here] off begin
// % protected region % [Apply any additional imports here] end

/**
 * Guice module for Tag used to register providers for dependency injection.
 */
@Slf4j
@AllArgsConstructor
public class TagModule extends AbstractModule {
	// % protected region % [Apply any additional class fields here] off begin
	// % protected region % [Apply any additional class fields here] end

	@Override
	protected void configure() {
		log.trace("Configuring TagModule");

		super.configure();

		// % protected region % [Apply any additional configure steps here] off begin
		// % protected region % [Apply any additional configure steps here] end

		log.trace("Configured TagModule");
	}

	/**
	 * Return a factory for mass data generation.
	 */
	@Provides
	public TagEntityFactory tagEntityFactory(
			// % protected region % [Apply any additional injected arguments for tagEntityFactory here] off begin
			// % protected region % [Apply any additional injected arguments for tagEntityFactory here] end
			Injector injector
	) {
		log.trace("Creating TagEntityFactory");

		// % protected region % [Apply any additional logic for tagEntityFactory before the main body here] off begin
		// % protected region % [Apply any additional logic for tagEntityFactory before the main body here] end

		TagEntityFactory entityFactory = new TagEntityFactory(
				// % protected region % [Apply any additional constructor arguments for TagEntityFactory here] off begin
				// % protected region % [Apply any additional constructor arguments for TagEntityFactory here] end
				injector
		);

		// % protected region % [Apply any additional logic for tagEntityFactory after the main body here] off begin
		// % protected region % [Apply any additional logic for tagEntityFactory after the main body here] end

		log.trace("Created TagEntityFactory");

		return entityFactory;
	}

	/**
	 * Return an empty Tag entity with no references set.
	 */
	@Provides
	@Named("tagEntityWithNoRef")
	public TagEntity tagEntityWithNoRef(
			// % protected region % [Apply any additional constructor parameters for tagEntityWithNoRef here] off begin
			// % protected region % [Apply any additional constructor parameters for tagEntityWithNoRef here] end
			MockNeat mock
	) {
		log.trace("Creating entity of type TagEntity with no reference");

		// % protected region % [Apply any additional logic for tagWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for tagWithNoRef before the main body here] end

		TagEntity newEntity = new TagEntity();
		newEntity.setId(UUID.randomUUID());
		newEntity.setCreated(OffsetDateTime.now());
		newEntity.setModified(OffsetDateTime.now());
		// % protected region % [Add customisation for Name here] off begin
		String randomStringforName = mock
				.strings()
				.get();
		newEntity.setName(randomStringforName);
		// % protected region % [Add customisation for Name here] end

		// % protected region % [Apply any additional logic for tagWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for tagWithNoRef after the main body here] end

		log.trace("Created entity of type TagEntity with no reference");

		return newEntity;
	}

	/**
	 * Return a collection of Tag entities with no reference at all.
	 */
	@Provides
	@Named("tagEntitiesWithNoRef")
	public Collection<TagEntity> tagEntitiesWithNoRef(
		// % protected region % [Apply any additional constructor parameters for tagEntitiesWithNoRef here] off begin
		// % protected region % [Apply any additional constructor parameters for tagEntitiesWithNoRef here] end
		TagEntityFactory tagEntityFactory
	) {
		log.trace("Creating entities of type TagEntity with no reference");

		// % protected region % [Apply any additional logic for tagEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for tagEntitiesWithNoRef before the main body here] end

		Collection<TagEntity> newEntities = tagEntityFactory.createMultipleWithNoRef(10);

		// % protected region % [Apply any additional logic for tagEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for tagEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type TagEntity with no reference");

		return newEntities;
	}

	/**
	 * Return a Tag entity with all references set.
	 */
	@Provides
	@Named("tagEntityWithRefs")
	public TagEntity tagEntityWithRefs(
			// % protected region % [Apply any additional constructor parameters for tagEntityWithRefs here] off begin
			// % protected region % [Apply any additional constructor parameters for tagEntityWithRefs here] end
			Injector injector
	) {
		log.trace("Creating entity of type TagEntity with references");

		// % protected region % [Apply any additional logic for tagEntityWithRefs before the main body here] off begin
		// % protected region % [Apply any additional logic for tagEntityWithRefs before the main body here] end

		TagEntity tagEntity = injector.getInstance(Key.get(TagEntity.class, Names.named("tagEntityWithNoRef")));

		// % protected region % [Apply any additional logic for tagEntityWithRefs after the main body here] off begin
		// % protected region % [Apply any additional logic for tagEntityWithRefs after the main body here] end

		log.trace("Created entity of type TagEntity with references");

		return tagEntity;
	}

	/**
	 * Return a collection of Tag entities with all references set.
	 */
	@Provides
	@Named("tagEntitiesWithRefs")
	public Collection<TagEntity> tagEntitiesWithRefs(
		// % protected region % [Apply any additional constructor parameters for tagEntitiesWithRefs here] off begin
		// % protected region % [Apply any additional constructor parameters for tagEntitiesWithRefs here] end
		TagEntityFactory tagEntityFactory
	) {
		log.trace("Creating entities of type TagEntity with references");

		// % protected region % [Apply any additional logic for tagEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for tagEntitiesWithNoRef before the main body here] end

		Collection<TagEntity> newEntities = tagEntityFactory.createMultipleWithRefs(10);

		// % protected region % [Apply any additional logic for tagEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for tagEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type TagEntity with references");

		return newEntities;
	}

	// % protected region % [Apply any additional class methods here] off begin
	// % protected region % [Apply any additional class methods here] end
}
