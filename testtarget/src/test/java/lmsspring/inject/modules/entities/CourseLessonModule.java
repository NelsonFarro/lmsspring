/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.inject.modules.entities;

import lmsspring.entities.*;
import lmsspring.entities.enums.*;
import lmsspring.inject.factories.*;
import com.google.inject.AbstractModule;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.andreinc.mockneat.MockNeat;
import net.andreinc.mockneat.types.enums.StringType;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.UUID;

// % protected region % [Apply any additional imports here] off begin
// % protected region % [Apply any additional imports here] end

/**
 * Guice module for Course Lesson used to register providers for dependency injection.
 */
@Slf4j
@AllArgsConstructor
public class CourseLessonModule extends AbstractModule {
	// % protected region % [Apply any additional class fields here] off begin
	// % protected region % [Apply any additional class fields here] end

	@Override
	protected void configure() {
		log.trace("Configuring CourseLessonModule");

		super.configure();

		// % protected region % [Apply any additional configure steps here] off begin
		// % protected region % [Apply any additional configure steps here] end

		log.trace("Configured CourseLessonModule");
	}

	/**
	 * Return a factory for mass data generation.
	 */
	@Provides
	public CourseLessonEntityFactory courseLessonEntityFactory(
			// % protected region % [Apply any additional injected arguments for courseLessonEntityFactory here] off begin
			// % protected region % [Apply any additional injected arguments for courseLessonEntityFactory here] end
			Injector injector
	) {
		log.trace("Creating CourseLessonEntityFactory");

		// % protected region % [Apply any additional logic for courseLessonEntityFactory before the main body here] off begin
		// % protected region % [Apply any additional logic for courseLessonEntityFactory before the main body here] end

		CourseLessonEntityFactory entityFactory = new CourseLessonEntityFactory(
				// % protected region % [Apply any additional constructor arguments for CourseLessonEntityFactory here] off begin
				// % protected region % [Apply any additional constructor arguments for CourseLessonEntityFactory here] end
				injector
		);

		// % protected region % [Apply any additional logic for courseLessonEntityFactory after the main body here] off begin
		// % protected region % [Apply any additional logic for courseLessonEntityFactory after the main body here] end

		log.trace("Created CourseLessonEntityFactory");

		return entityFactory;
	}

	/**
	 * Return an empty Course Lesson entity with no references set.
	 */
	@Provides
	@Named("courseLessonEntityWithNoRef")
	public CourseLessonEntity courseLessonEntityWithNoRef(
			// % protected region % [Apply any additional constructor parameters for courseLessonEntityWithNoRef here] off begin
			// % protected region % [Apply any additional constructor parameters for courseLessonEntityWithNoRef here] end
			MockNeat mock
	) {
		log.trace("Creating entity of type CourseLessonEntity with no reference");

		// % protected region % [Apply any additional logic for courseLessonWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for courseLessonWithNoRef before the main body here] end

		CourseLessonEntity newEntity = new CourseLessonEntity();
		newEntity.setId(UUID.randomUUID());
		newEntity.setCreated(OffsetDateTime.now());
		newEntity.setModified(OffsetDateTime.now());
		// % protected region % [Add customisation for Order here] off begin
		newEntity.setOrder(mock.ints().get());
		// % protected region % [Add customisation for Order here] end

		// % protected region % [Apply any additional logic for courseLessonWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for courseLessonWithNoRef after the main body here] end

		log.trace("Created entity of type CourseLessonEntity with no reference");

		return newEntity;
	}

	/**
	 * Return a collection of Course Lesson entities with no reference at all.
	 */
	@Provides
	@Named("courseLessonEntitiesWithNoRef")
	public Collection<CourseLessonEntity> courseLessonEntitiesWithNoRef(
		// % protected region % [Apply any additional constructor parameters for courseLessonEntitiesWithNoRef here] off begin
		// % protected region % [Apply any additional constructor parameters for courseLessonEntitiesWithNoRef here] end
		CourseLessonEntityFactory courseLessonEntityFactory
	) {
		log.trace("Creating entities of type CourseLessonEntity with no reference");

		// % protected region % [Apply any additional logic for courseLessonEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for courseLessonEntitiesWithNoRef before the main body here] end

		Collection<CourseLessonEntity> newEntities = courseLessonEntityFactory.createMultipleWithNoRef(10);

		// % protected region % [Apply any additional logic for courseLessonEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for courseLessonEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type CourseLessonEntity with no reference");

		return newEntities;
	}

	/**
	 * Return a Course Lesson entity with all references set.
	 */
	@Provides
	@Named("courseLessonEntityWithRefs")
	public CourseLessonEntity courseLessonEntityWithRefs(
			// % protected region % [Apply any additional constructor parameters for courseLessonEntityWithRefs here] off begin
			// % protected region % [Apply any additional constructor parameters for courseLessonEntityWithRefs here] end
			Injector injector
	) {
		log.trace("Creating entity of type CourseLessonEntity with references");

		// % protected region % [Apply any additional logic for courseLessonEntityWithRefs before the main body here] off begin
		// % protected region % [Apply any additional logic for courseLessonEntityWithRefs before the main body here] end

		CourseLessonEntity courseLessonEntity = injector.getInstance(Key.get(CourseLessonEntity.class, Names.named("courseLessonEntityWithNoRef")));

		// % protected region % [Apply any additional logic for courseLessonEntityWithRefs after the main body here] off begin
		// % protected region % [Apply any additional logic for courseLessonEntityWithRefs after the main body here] end

		log.trace("Created entity of type CourseLessonEntity with references");

		return courseLessonEntity;
	}

	/**
	 * Return a collection of Course Lesson entities with all references set.
	 */
	@Provides
	@Named("courseLessonEntitiesWithRefs")
	public Collection<CourseLessonEntity> courseLessonEntitiesWithRefs(
		// % protected region % [Apply any additional constructor parameters for courseLessonEntitiesWithRefs here] off begin
		// % protected region % [Apply any additional constructor parameters for courseLessonEntitiesWithRefs here] end
		CourseLessonEntityFactory courseLessonEntityFactory
	) {
		log.trace("Creating entities of type CourseLessonEntity with references");

		// % protected region % [Apply any additional logic for courseLessonEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for courseLessonEntitiesWithNoRef before the main body here] end

		Collection<CourseLessonEntity> newEntities = courseLessonEntityFactory.createMultipleWithRefs(10);

		// % protected region % [Apply any additional logic for courseLessonEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for courseLessonEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type CourseLessonEntity with references");

		return newEntities;
	}

	// % protected region % [Apply any additional class methods here] off begin
	// % protected region % [Apply any additional class methods here] end
}
