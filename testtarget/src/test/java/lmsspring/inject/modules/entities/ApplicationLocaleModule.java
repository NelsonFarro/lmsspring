/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.inject.modules.entities;

import lmsspring.entities.*;
import lmsspring.entities.enums.*;
import lmsspring.inject.factories.*;
import com.google.inject.AbstractModule;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.andreinc.mockneat.MockNeat;
import net.andreinc.mockneat.types.enums.StringType;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.UUID;

// % protected region % [Apply any additional imports here] off begin
// % protected region % [Apply any additional imports here] end

/**
 * Guice module for Application Locale used to register providers for dependency injection.
 */
@Slf4j
@AllArgsConstructor
public class ApplicationLocaleModule extends AbstractModule {
	// % protected region % [Apply any additional class fields here] off begin
	// % protected region % [Apply any additional class fields here] end

	@Override
	protected void configure() {
		log.trace("Configuring ApplicationLocaleModule");

		super.configure();

		// % protected region % [Apply any additional configure steps here] off begin
		// % protected region % [Apply any additional configure steps here] end

		log.trace("Configured ApplicationLocaleModule");
	}

	/**
	 * Return a factory for mass data generation.
	 */
	@Provides
	public ApplicationLocaleEntityFactory applicationLocaleEntityFactory(
			// % protected region % [Apply any additional injected arguments for applicationLocaleEntityFactory here] off begin
			// % protected region % [Apply any additional injected arguments for applicationLocaleEntityFactory here] end
			Injector injector
	) {
		log.trace("Creating ApplicationLocaleEntityFactory");

		// % protected region % [Apply any additional logic for applicationLocaleEntityFactory before the main body here] off begin
		// % protected region % [Apply any additional logic for applicationLocaleEntityFactory before the main body here] end

		ApplicationLocaleEntityFactory entityFactory = new ApplicationLocaleEntityFactory(
				// % protected region % [Apply any additional constructor arguments for ApplicationLocaleEntityFactory here] off begin
				// % protected region % [Apply any additional constructor arguments for ApplicationLocaleEntityFactory here] end
				injector
		);

		// % protected region % [Apply any additional logic for applicationLocaleEntityFactory after the main body here] off begin
		// % protected region % [Apply any additional logic for applicationLocaleEntityFactory after the main body here] end

		log.trace("Created ApplicationLocaleEntityFactory");

		return entityFactory;
	}

	/**
	 * Return an empty Application Locale entity with no references set.
	 */
	@Provides
	@Named("applicationLocaleEntityWithNoRef")
	public ApplicationLocaleEntity applicationLocaleEntityWithNoRef(
			// % protected region % [Apply any additional constructor parameters for applicationLocaleEntityWithNoRef here] off begin
			// % protected region % [Apply any additional constructor parameters for applicationLocaleEntityWithNoRef here] end
			MockNeat mock
	) {
		log.trace("Creating entity of type ApplicationLocaleEntity with no reference");

		// % protected region % [Apply any additional logic for applicationLocaleWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for applicationLocaleWithNoRef before the main body here] end

		ApplicationLocaleEntity newEntity = new ApplicationLocaleEntity();
		newEntity.setId(UUID.randomUUID());
		newEntity.setCreated(OffsetDateTime.now());
		newEntity.setModified(OffsetDateTime.now());
		// % protected region % [Add customisation for Locale here] off begin
		String randomStringforLocale = mock
				.strings()
				.get();
		newEntity.setLocale(randomStringforLocale);
		// % protected region % [Add customisation for Locale here] end
		// % protected region % [Add customisation for Key here] off begin
		String randomStringforKey = mock
				.strings()
				.get();
		newEntity.setKey(randomStringforKey);
		// % protected region % [Add customisation for Key here] end
		// % protected region % [Add customisation for Value here] off begin
		String randomStringforValue = mock
				.strings()
				.get();
		newEntity.setValue(randomStringforValue);
		// % protected region % [Add customisation for Value here] end

		// % protected region % [Apply any additional logic for applicationLocaleWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for applicationLocaleWithNoRef after the main body here] end

		log.trace("Created entity of type ApplicationLocaleEntity with no reference");

		return newEntity;
	}

	/**
	 * Return a collection of Application Locale entities with no reference at all.
	 */
	@Provides
	@Named("applicationLocaleEntitiesWithNoRef")
	public Collection<ApplicationLocaleEntity> applicationLocaleEntitiesWithNoRef(
		// % protected region % [Apply any additional constructor parameters for applicationLocaleEntitiesWithNoRef here] off begin
		// % protected region % [Apply any additional constructor parameters for applicationLocaleEntitiesWithNoRef here] end
		ApplicationLocaleEntityFactory applicationLocaleEntityFactory
	) {
		log.trace("Creating entities of type ApplicationLocaleEntity with no reference");

		// % protected region % [Apply any additional logic for applicationLocaleEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for applicationLocaleEntitiesWithNoRef before the main body here] end

		Collection<ApplicationLocaleEntity> newEntities = applicationLocaleEntityFactory.createMultipleWithNoRef(10);

		// % protected region % [Apply any additional logic for applicationLocaleEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for applicationLocaleEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type ApplicationLocaleEntity with no reference");

		return newEntities;
	}

	/**
	 * Return a Application Locale entity with all references set.
	 */
	@Provides
	@Named("applicationLocaleEntityWithRefs")
	public ApplicationLocaleEntity applicationLocaleEntityWithRefs(
			// % protected region % [Apply any additional constructor parameters for applicationLocaleEntityWithRefs here] off begin
			// % protected region % [Apply any additional constructor parameters for applicationLocaleEntityWithRefs here] end
			Injector injector
	) {
		log.trace("Creating entity of type ApplicationLocaleEntity with references");

		// % protected region % [Apply any additional logic for applicationLocaleEntityWithRefs before the main body here] off begin
		// % protected region % [Apply any additional logic for applicationLocaleEntityWithRefs before the main body here] end

		ApplicationLocaleEntity applicationLocaleEntity = injector.getInstance(Key.get(ApplicationLocaleEntity.class, Names.named("applicationLocaleEntityWithNoRef")));

		// % protected region % [Apply any additional logic for applicationLocaleEntityWithRefs after the main body here] off begin
		// % protected region % [Apply any additional logic for applicationLocaleEntityWithRefs after the main body here] end

		log.trace("Created entity of type ApplicationLocaleEntity with references");

		return applicationLocaleEntity;
	}

	/**
	 * Return a collection of Application Locale entities with all references set.
	 */
	@Provides
	@Named("applicationLocaleEntitiesWithRefs")
	public Collection<ApplicationLocaleEntity> applicationLocaleEntitiesWithRefs(
		// % protected region % [Apply any additional constructor parameters for applicationLocaleEntitiesWithRefs here] off begin
		// % protected region % [Apply any additional constructor parameters for applicationLocaleEntitiesWithRefs here] end
		ApplicationLocaleEntityFactory applicationLocaleEntityFactory
	) {
		log.trace("Creating entities of type ApplicationLocaleEntity with references");

		// % protected region % [Apply any additional logic for applicationLocaleEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for applicationLocaleEntitiesWithNoRef before the main body here] end

		Collection<ApplicationLocaleEntity> newEntities = applicationLocaleEntityFactory.createMultipleWithRefs(10);

		// % protected region % [Apply any additional logic for applicationLocaleEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for applicationLocaleEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type ApplicationLocaleEntity with references");

		return newEntities;
	}

	// % protected region % [Apply any additional class methods here] off begin
	// % protected region % [Apply any additional class methods here] end
}
