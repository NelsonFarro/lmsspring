/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.inject.modules.entities;

import lmsspring.entities.*;
import lmsspring.entities.enums.*;
import lmsspring.inject.factories.*;
import com.google.inject.AbstractModule;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.andreinc.mockneat.MockNeat;
import net.andreinc.mockneat.types.enums.StringType;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.UUID;

// % protected region % [Apply any additional imports here] off begin
// % protected region % [Apply any additional imports here] end

/**
 * Guice module for Lesson Form Version used to register providers for dependency injection.
 */
@Slf4j
@AllArgsConstructor
public class LessonFormVersionModule extends AbstractModule {
	// % protected region % [Apply any additional class fields here] off begin
	// % protected region % [Apply any additional class fields here] end

	@Override
	protected void configure() {
		log.trace("Configuring LessonFormVersionModule");

		super.configure();

		// % protected region % [Apply any additional configure steps here] off begin
		// % protected region % [Apply any additional configure steps here] end

		log.trace("Configured LessonFormVersionModule");
	}

	/**
	 * Return a factory for mass data generation.
	 */
	@Provides
	public LessonFormVersionEntityFactory lessonFormVersionEntityFactory(
			// % protected region % [Apply any additional injected arguments for lessonFormVersionEntityFactory here] off begin
			// % protected region % [Apply any additional injected arguments for lessonFormVersionEntityFactory here] end
			Injector injector
	) {
		log.trace("Creating LessonFormVersionEntityFactory");

		// % protected region % [Apply any additional logic for lessonFormVersionEntityFactory before the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormVersionEntityFactory before the main body here] end

		LessonFormVersionEntityFactory entityFactory = new LessonFormVersionEntityFactory(
				// % protected region % [Apply any additional constructor arguments for LessonFormVersionEntityFactory here] off begin
				// % protected region % [Apply any additional constructor arguments for LessonFormVersionEntityFactory here] end
				injector
		);

		// % protected region % [Apply any additional logic for lessonFormVersionEntityFactory after the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormVersionEntityFactory after the main body here] end

		log.trace("Created LessonFormVersionEntityFactory");

		return entityFactory;
	}

	/**
	 * Return an empty Lesson Form Version entity with no references set.
	 */
	@Provides
	@Named("lessonFormVersionEntityWithNoRef")
	public LessonFormVersionEntity lessonFormVersionEntityWithNoRef(
			// % protected region % [Apply any additional constructor parameters for lessonFormVersionEntityWithNoRef here] off begin
			// % protected region % [Apply any additional constructor parameters for lessonFormVersionEntityWithNoRef here] end
			MockNeat mock
	) {
		log.trace("Creating entity of type LessonFormVersionEntity with no reference");

		// % protected region % [Apply any additional logic for lessonFormVersionWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormVersionWithNoRef before the main body here] end

		LessonFormVersionEntity newEntity = new LessonFormVersionEntity();
		newEntity.setId(UUID.randomUUID());
		newEntity.setCreated(OffsetDateTime.now());
		newEntity.setModified(OffsetDateTime.now());
		// % protected region % [Add customisation for Version here] off begin
		newEntity.setVersion(mock.ints().get());
		// % protected region % [Add customisation for Version here] end
		// % protected region % [Add customisation for Form Data here] off begin
		String randomStringforFormData = mock
				.strings()
				.get();
		newEntity.setFormData(randomStringforFormData);
		// % protected region % [Add customisation for Form Data here] end

		// % protected region % [Apply any additional logic for lessonFormVersionWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormVersionWithNoRef after the main body here] end

		log.trace("Created entity of type LessonFormVersionEntity with no reference");

		return newEntity;
	}

	/**
	 * Return a collection of Lesson Form Version entities with no reference at all.
	 */
	@Provides
	@Named("lessonFormVersionEntitiesWithNoRef")
	public Collection<LessonFormVersionEntity> lessonFormVersionEntitiesWithNoRef(
		// % protected region % [Apply any additional constructor parameters for lessonFormVersionEntitiesWithNoRef here] off begin
		// % protected region % [Apply any additional constructor parameters for lessonFormVersionEntitiesWithNoRef here] end
		LessonFormVersionEntityFactory lessonFormVersionEntityFactory
	) {
		log.trace("Creating entities of type LessonFormVersionEntity with no reference");

		// % protected region % [Apply any additional logic for lessonFormVersionEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormVersionEntitiesWithNoRef before the main body here] end

		Collection<LessonFormVersionEntity> newEntities = lessonFormVersionEntityFactory.createMultipleWithNoRef(10);

		// % protected region % [Apply any additional logic for lessonFormVersionEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormVersionEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type LessonFormVersionEntity with no reference");

		return newEntities;
	}

	/**
	 * Return a Lesson Form Version entity with all references set.
	 */
	@Provides
	@Named("lessonFormVersionEntityWithRefs")
	public LessonFormVersionEntity lessonFormVersionEntityWithRefs(
			// % protected region % [Apply any additional constructor parameters for lessonFormVersionEntityWithRefs here] off begin
			// % protected region % [Apply any additional constructor parameters for lessonFormVersionEntityWithRefs here] end
			Injector injector
	) {
		log.trace("Creating entity of type LessonFormVersionEntity with references");

		// % protected region % [Apply any additional logic for lessonFormVersionEntityWithRefs before the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormVersionEntityWithRefs before the main body here] end

		LessonFormVersionEntity lessonFormVersionEntity = injector.getInstance(Key.get(LessonFormVersionEntity.class, Names.named("lessonFormVersionEntityWithNoRef")));

		// % protected region % [Apply any additional logic for lessonFormVersionEntityWithRefs after the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormVersionEntityWithRefs after the main body here] end

		log.trace("Created entity of type LessonFormVersionEntity with references");

		return lessonFormVersionEntity;
	}

	/**
	 * Return a collection of Lesson Form Version entities with all references set.
	 */
	@Provides
	@Named("lessonFormVersionEntitiesWithRefs")
	public Collection<LessonFormVersionEntity> lessonFormVersionEntitiesWithRefs(
		// % protected region % [Apply any additional constructor parameters for lessonFormVersionEntitiesWithRefs here] off begin
		// % protected region % [Apply any additional constructor parameters for lessonFormVersionEntitiesWithRefs here] end
		LessonFormVersionEntityFactory lessonFormVersionEntityFactory
	) {
		log.trace("Creating entities of type LessonFormVersionEntity with references");

		// % protected region % [Apply any additional logic for lessonFormVersionEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormVersionEntitiesWithNoRef before the main body here] end

		Collection<LessonFormVersionEntity> newEntities = lessonFormVersionEntityFactory.createMultipleWithRefs(10);

		// % protected region % [Apply any additional logic for lessonFormVersionEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormVersionEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type LessonFormVersionEntity with references");

		return newEntities;
	}

	// % protected region % [Apply any additional class methods here] off begin
	// % protected region % [Apply any additional class methods here] end
}
