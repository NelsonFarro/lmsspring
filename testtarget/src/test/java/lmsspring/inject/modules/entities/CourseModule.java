/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.inject.modules.entities;

import lmsspring.entities.*;
import lmsspring.entities.enums.*;
import lmsspring.inject.factories.*;
import com.google.inject.AbstractModule;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.andreinc.mockneat.MockNeat;
import net.andreinc.mockneat.types.enums.StringType;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.UUID;

// % protected region % [Apply any additional imports here] off begin
// % protected region % [Apply any additional imports here] end

/**
 * Guice module for Course used to register providers for dependency injection.
 */
@Slf4j
@AllArgsConstructor
public class CourseModule extends AbstractModule {
	// % protected region % [Apply any additional class fields here] off begin
	// % protected region % [Apply any additional class fields here] end

	@Override
	protected void configure() {
		log.trace("Configuring CourseModule");

		super.configure();

		// % protected region % [Apply any additional configure steps here] off begin
		// % protected region % [Apply any additional configure steps here] end

		log.trace("Configured CourseModule");
	}

	/**
	 * Return a factory for mass data generation.
	 */
	@Provides
	public CourseEntityFactory courseEntityFactory(
			// % protected region % [Apply any additional injected arguments for courseEntityFactory here] off begin
			// % protected region % [Apply any additional injected arguments for courseEntityFactory here] end
			Injector injector
	) {
		log.trace("Creating CourseEntityFactory");

		// % protected region % [Apply any additional logic for courseEntityFactory before the main body here] off begin
		// % protected region % [Apply any additional logic for courseEntityFactory before the main body here] end

		CourseEntityFactory entityFactory = new CourseEntityFactory(
				// % protected region % [Apply any additional constructor arguments for CourseEntityFactory here] off begin
				// % protected region % [Apply any additional constructor arguments for CourseEntityFactory here] end
				injector
		);

		// % protected region % [Apply any additional logic for courseEntityFactory after the main body here] off begin
		// % protected region % [Apply any additional logic for courseEntityFactory after the main body here] end

		log.trace("Created CourseEntityFactory");

		return entityFactory;
	}

	/**
	 * Return an empty Course entity with no references set.
	 */
	@Provides
	@Named("courseEntityWithNoRef")
	public CourseEntity courseEntityWithNoRef(
			// % protected region % [Apply any additional constructor parameters for courseEntityWithNoRef here] off begin
			// % protected region % [Apply any additional constructor parameters for courseEntityWithNoRef here] end
			MockNeat mock
	) {
		log.trace("Creating entity of type CourseEntity with no reference");

		// % protected region % [Apply any additional logic for courseWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for courseWithNoRef before the main body here] end

		CourseEntity newEntity = new CourseEntity();
		newEntity.setId(UUID.randomUUID());
		newEntity.setCreated(OffsetDateTime.now());
		newEntity.setModified(OffsetDateTime.now());
		// % protected region % [Add customisation for Name here] off begin
		String randomStringforName = mock
				.strings()
				.get();
		newEntity.setName(randomStringforName);
		// % protected region % [Add customisation for Name here] end
		// % protected region % [Add customisation for Summary here] off begin
		String randomStringforSummary = mock
				.strings()
				.get();
		newEntity.setSummary(randomStringforSummary);
		// % protected region % [Add customisation for Summary here] end
		// % protected region % [Add customisation for Cover Image here] off begin
		// % protected region % [Add customisation for Cover Image here] end
		// % protected region % [Add customisation for Difficulty here] off begin
		newEntity.setDifficulty(DifficultyEnum.BEGINNER);
		// % protected region % [Add customisation for Difficulty here] end

		// % protected region % [Apply any additional logic for courseWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for courseWithNoRef after the main body here] end

		log.trace("Created entity of type CourseEntity with no reference");

		return newEntity;
	}

	/**
	 * Return a collection of Course entities with no reference at all.
	 */
	@Provides
	@Named("courseEntitiesWithNoRef")
	public Collection<CourseEntity> courseEntitiesWithNoRef(
		// % protected region % [Apply any additional constructor parameters for courseEntitiesWithNoRef here] off begin
		// % protected region % [Apply any additional constructor parameters for courseEntitiesWithNoRef here] end
		CourseEntityFactory courseEntityFactory
	) {
		log.trace("Creating entities of type CourseEntity with no reference");

		// % protected region % [Apply any additional logic for courseEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for courseEntitiesWithNoRef before the main body here] end

		Collection<CourseEntity> newEntities = courseEntityFactory.createMultipleWithNoRef(10);

		// % protected region % [Apply any additional logic for courseEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for courseEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type CourseEntity with no reference");

		return newEntities;
	}

	/**
	 * Return a Course entity with all references set.
	 */
	@Provides
	@Named("courseEntityWithRefs")
	public CourseEntity courseEntityWithRefs(
			// % protected region % [Apply any additional constructor parameters for courseEntityWithRefs here] off begin
			// % protected region % [Apply any additional constructor parameters for courseEntityWithRefs here] end
			Injector injector
	) {
		log.trace("Creating entity of type CourseEntity with references");

		// % protected region % [Apply any additional logic for courseEntityWithRefs before the main body here] off begin
		// % protected region % [Apply any additional logic for courseEntityWithRefs before the main body here] end

		CourseEntity courseEntity = injector.getInstance(Key.get(CourseEntity.class, Names.named("courseEntityWithNoRef")));

		// % protected region % [Apply any additional logic for courseEntityWithRefs after the main body here] off begin
		// % protected region % [Apply any additional logic for courseEntityWithRefs after the main body here] end

		log.trace("Created entity of type CourseEntity with references");

		return courseEntity;
	}

	/**
	 * Return a collection of Course entities with all references set.
	 */
	@Provides
	@Named("courseEntitiesWithRefs")
	public Collection<CourseEntity> courseEntitiesWithRefs(
		// % protected region % [Apply any additional constructor parameters for courseEntitiesWithRefs here] off begin
		// % protected region % [Apply any additional constructor parameters for courseEntitiesWithRefs here] end
		CourseEntityFactory courseEntityFactory
	) {
		log.trace("Creating entities of type CourseEntity with references");

		// % protected region % [Apply any additional logic for courseEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for courseEntitiesWithNoRef before the main body here] end

		Collection<CourseEntity> newEntities = courseEntityFactory.createMultipleWithRefs(10);

		// % protected region % [Apply any additional logic for courseEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for courseEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type CourseEntity with references");

		return newEntities;
	}

	// % protected region % [Apply any additional class methods here] off begin
	// % protected region % [Apply any additional class methods here] end
}
