/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.inject.modules.entities;

import lmsspring.entities.*;
import lmsspring.entities.enums.*;
import lmsspring.inject.factories.*;
import com.google.inject.AbstractModule;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.andreinc.mockneat.MockNeat;
import net.andreinc.mockneat.types.enums.StringType;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.UUID;

// % protected region % [Apply any additional imports here] off begin
// % protected region % [Apply any additional imports here] end

/**
 * Guice module for Workflow Transition used to register providers for dependency injection.
 */
@Slf4j
@AllArgsConstructor
public class WorkflowTransitionModule extends AbstractModule {
	// % protected region % [Apply any additional class fields here] off begin
	// % protected region % [Apply any additional class fields here] end

	@Override
	protected void configure() {
		log.trace("Configuring WorkflowTransitionModule");

		super.configure();

		// % protected region % [Apply any additional configure steps here] off begin
		// % protected region % [Apply any additional configure steps here] end

		log.trace("Configured WorkflowTransitionModule");
	}

	/**
	 * Return a factory for mass data generation.
	 */
	@Provides
	public WorkflowTransitionEntityFactory workflowTransitionEntityFactory(
			// % protected region % [Apply any additional injected arguments for workflowTransitionEntityFactory here] off begin
			// % protected region % [Apply any additional injected arguments for workflowTransitionEntityFactory here] end
			Injector injector
	) {
		log.trace("Creating WorkflowTransitionEntityFactory");

		// % protected region % [Apply any additional logic for workflowTransitionEntityFactory before the main body here] off begin
		// % protected region % [Apply any additional logic for workflowTransitionEntityFactory before the main body here] end

		WorkflowTransitionEntityFactory entityFactory = new WorkflowTransitionEntityFactory(
				// % protected region % [Apply any additional constructor arguments for WorkflowTransitionEntityFactory here] off begin
				// % protected region % [Apply any additional constructor arguments for WorkflowTransitionEntityFactory here] end
				injector
		);

		// % protected region % [Apply any additional logic for workflowTransitionEntityFactory after the main body here] off begin
		// % protected region % [Apply any additional logic for workflowTransitionEntityFactory after the main body here] end

		log.trace("Created WorkflowTransitionEntityFactory");

		return entityFactory;
	}

	/**
	 * Return an empty Workflow Transition entity with no references set.
	 */
	@Provides
	@Named("workflowTransitionEntityWithNoRef")
	public WorkflowTransitionEntity workflowTransitionEntityWithNoRef(
			// % protected region % [Apply any additional constructor parameters for workflowTransitionEntityWithNoRef here] off begin
			// % protected region % [Apply any additional constructor parameters for workflowTransitionEntityWithNoRef here] end
			MockNeat mock
	) {
		log.trace("Creating entity of type WorkflowTransitionEntity with no reference");

		// % protected region % [Apply any additional logic for workflowTransitionWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for workflowTransitionWithNoRef before the main body here] end

		WorkflowTransitionEntity newEntity = new WorkflowTransitionEntity();
		newEntity.setId(UUID.randomUUID());
		newEntity.setCreated(OffsetDateTime.now());
		newEntity.setModified(OffsetDateTime.now());
		// % protected region % [Add customisation for Transition Name here] off begin
		String randomStringforTransitionName = mock
				.strings()
				.get();
		newEntity.setTransitionName(randomStringforTransitionName);
		// % protected region % [Add customisation for Transition Name here] end

		// % protected region % [Apply any additional logic for workflowTransitionWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for workflowTransitionWithNoRef after the main body here] end

		log.trace("Created entity of type WorkflowTransitionEntity with no reference");

		return newEntity;
	}

	/**
	 * Return a collection of Workflow Transition entities with no reference at all.
	 */
	@Provides
	@Named("workflowTransitionEntitiesWithNoRef")
	public Collection<WorkflowTransitionEntity> workflowTransitionEntitiesWithNoRef(
		// % protected region % [Apply any additional constructor parameters for workflowTransitionEntitiesWithNoRef here] off begin
		// % protected region % [Apply any additional constructor parameters for workflowTransitionEntitiesWithNoRef here] end
		WorkflowTransitionEntityFactory workflowTransitionEntityFactory
	) {
		log.trace("Creating entities of type WorkflowTransitionEntity with no reference");

		// % protected region % [Apply any additional logic for workflowTransitionEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for workflowTransitionEntitiesWithNoRef before the main body here] end

		Collection<WorkflowTransitionEntity> newEntities = workflowTransitionEntityFactory.createMultipleWithNoRef(10);

		// % protected region % [Apply any additional logic for workflowTransitionEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for workflowTransitionEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type WorkflowTransitionEntity with no reference");

		return newEntities;
	}

	/**
	 * Return a Workflow Transition entity with all references set.
	 */
	@Provides
	@Named("workflowTransitionEntityWithRefs")
	public WorkflowTransitionEntity workflowTransitionEntityWithRefs(
			// % protected region % [Apply any additional constructor parameters for workflowTransitionEntityWithRefs here] off begin
			// % protected region % [Apply any additional constructor parameters for workflowTransitionEntityWithRefs here] end
			@Named("workflowStateEntityWithRefs") WorkflowStateEntity sourceState,
			@Named("workflowStateEntityWithRefs") WorkflowStateEntity targetState,
			Injector injector
	) {
		log.trace("Creating entity of type WorkflowTransitionEntity with references");

		// % protected region % [Apply any additional logic for workflowTransitionEntityWithRefs before the main body here] off begin
		// % protected region % [Apply any additional logic for workflowTransitionEntityWithRefs before the main body here] end

		WorkflowTransitionEntity workflowTransitionEntity = injector.getInstance(Key.get(WorkflowTransitionEntity.class, Names.named("workflowTransitionEntityWithNoRef")));
		workflowTransitionEntity.setSourceState(sourceState, true);
		workflowTransitionEntity.setTargetState(targetState, true);

		// % protected region % [Apply any additional logic for workflowTransitionEntityWithRefs after the main body here] off begin
		// % protected region % [Apply any additional logic for workflowTransitionEntityWithRefs after the main body here] end

		log.trace("Created entity of type WorkflowTransitionEntity with references");

		return workflowTransitionEntity;
	}

	/**
	 * Return a collection of Workflow Transition entities with all references set.
	 */
	@Provides
	@Named("workflowTransitionEntitiesWithRefs")
	public Collection<WorkflowTransitionEntity> workflowTransitionEntitiesWithRefs(
		// % protected region % [Apply any additional constructor parameters for workflowTransitionEntitiesWithRefs here] off begin
		// % protected region % [Apply any additional constructor parameters for workflowTransitionEntitiesWithRefs here] end
		WorkflowTransitionEntityFactory workflowTransitionEntityFactory
	) {
		log.trace("Creating entities of type WorkflowTransitionEntity with references");

		// % protected region % [Apply any additional logic for workflowTransitionEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for workflowTransitionEntitiesWithNoRef before the main body here] end

		Collection<WorkflowTransitionEntity> newEntities = workflowTransitionEntityFactory.createMultipleWithRefs(10);

		// % protected region % [Apply any additional logic for workflowTransitionEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for workflowTransitionEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type WorkflowTransitionEntity with references");

		return newEntities;
	}

	// % protected region % [Apply any additional class methods here] off begin
	// % protected region % [Apply any additional class methods here] end
}
