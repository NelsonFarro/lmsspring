/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.inject.modules.entities;

import lmsspring.entities.*;
import lmsspring.entities.enums.*;
import lmsspring.inject.factories.*;
import com.google.inject.AbstractModule;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.andreinc.mockneat.MockNeat;
import net.andreinc.mockneat.types.enums.StringType;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.UUID;

// % protected region % [Apply any additional imports here] off begin
// % protected region % [Apply any additional imports here] end

/**
 * Guice module for Workflow Version used to register providers for dependency injection.
 */
@Slf4j
@AllArgsConstructor
public class WorkflowVersionModule extends AbstractModule {
	// % protected region % [Apply any additional class fields here] off begin
	// % protected region % [Apply any additional class fields here] end

	@Override
	protected void configure() {
		log.trace("Configuring WorkflowVersionModule");

		super.configure();

		// % protected region % [Apply any additional configure steps here] off begin
		// % protected region % [Apply any additional configure steps here] end

		log.trace("Configured WorkflowVersionModule");
	}

	/**
	 * Return a factory for mass data generation.
	 */
	@Provides
	public WorkflowVersionEntityFactory workflowVersionEntityFactory(
			// % protected region % [Apply any additional injected arguments for workflowVersionEntityFactory here] off begin
			// % protected region % [Apply any additional injected arguments for workflowVersionEntityFactory here] end
			Injector injector
	) {
		log.trace("Creating WorkflowVersionEntityFactory");

		// % protected region % [Apply any additional logic for workflowVersionEntityFactory before the main body here] off begin
		// % protected region % [Apply any additional logic for workflowVersionEntityFactory before the main body here] end

		WorkflowVersionEntityFactory entityFactory = new WorkflowVersionEntityFactory(
				// % protected region % [Apply any additional constructor arguments for WorkflowVersionEntityFactory here] off begin
				// % protected region % [Apply any additional constructor arguments for WorkflowVersionEntityFactory here] end
				injector
		);

		// % protected region % [Apply any additional logic for workflowVersionEntityFactory after the main body here] off begin
		// % protected region % [Apply any additional logic for workflowVersionEntityFactory after the main body here] end

		log.trace("Created WorkflowVersionEntityFactory");

		return entityFactory;
	}

	/**
	 * Return an empty Workflow Version entity with no references set.
	 */
	@Provides
	@Named("workflowVersionEntityWithNoRef")
	public WorkflowVersionEntity workflowVersionEntityWithNoRef(
			// % protected region % [Apply any additional constructor parameters for workflowVersionEntityWithNoRef here] off begin
			// % protected region % [Apply any additional constructor parameters for workflowVersionEntityWithNoRef here] end
			MockNeat mock
	) {
		log.trace("Creating entity of type WorkflowVersionEntity with no reference");

		// % protected region % [Apply any additional logic for workflowVersionWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for workflowVersionWithNoRef before the main body here] end

		WorkflowVersionEntity newEntity = new WorkflowVersionEntity();
		newEntity.setId(UUID.randomUUID());
		newEntity.setCreated(OffsetDateTime.now());
		newEntity.setModified(OffsetDateTime.now());
		// % protected region % [Add customisation for Workflow Name here] off begin
		String randomStringforWorkflowName = mock
				.strings()
				.get();
		newEntity.setWorkflowName(randomStringforWorkflowName);
		// % protected region % [Add customisation for Workflow Name here] end
		// % protected region % [Add customisation for Workflow Description here] off begin
		String randomStringforWorkflowDescription = mock
				.strings()
				.get();
		newEntity.setWorkflowDescription(randomStringforWorkflowDescription);
		// % protected region % [Add customisation for Workflow Description here] end
		// % protected region % [Add customisation for Version Number here] off begin
		newEntity.setVersionNumber(mock.ints().get());
		// % protected region % [Add customisation for Version Number here] end
		// % protected region % [Add customisation for Article Association here] off begin
		newEntity.setArticleAssociation(mock.bools().get());
		// % protected region % [Add customisation for Article Association here] end

		// % protected region % [Apply any additional logic for workflowVersionWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for workflowVersionWithNoRef after the main body here] end

		log.trace("Created entity of type WorkflowVersionEntity with no reference");

		return newEntity;
	}

	/**
	 * Return a collection of Workflow Version entities with no reference at all.
	 */
	@Provides
	@Named("workflowVersionEntitiesWithNoRef")
	public Collection<WorkflowVersionEntity> workflowVersionEntitiesWithNoRef(
		// % protected region % [Apply any additional constructor parameters for workflowVersionEntitiesWithNoRef here] off begin
		// % protected region % [Apply any additional constructor parameters for workflowVersionEntitiesWithNoRef here] end
		WorkflowVersionEntityFactory workflowVersionEntityFactory
	) {
		log.trace("Creating entities of type WorkflowVersionEntity with no reference");

		// % protected region % [Apply any additional logic for workflowVersionEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for workflowVersionEntitiesWithNoRef before the main body here] end

		Collection<WorkflowVersionEntity> newEntities = workflowVersionEntityFactory.createMultipleWithNoRef(10);

		// % protected region % [Apply any additional logic for workflowVersionEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for workflowVersionEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type WorkflowVersionEntity with no reference");

		return newEntities;
	}

	/**
	 * Return a Workflow Version entity with all references set.
	 */
	@Provides
	@Named("workflowVersionEntityWithRefs")
	public WorkflowVersionEntity workflowVersionEntityWithRefs(
			// % protected region % [Apply any additional constructor parameters for workflowVersionEntityWithRefs here] off begin
			// % protected region % [Apply any additional constructor parameters for workflowVersionEntityWithRefs here] end
			@Named("workflowEntityWithRefs") WorkflowEntity workflow,
			Injector injector
	) {
		log.trace("Creating entity of type WorkflowVersionEntity with references");

		// % protected region % [Apply any additional logic for workflowVersionEntityWithRefs before the main body here] off begin
		// % protected region % [Apply any additional logic for workflowVersionEntityWithRefs before the main body here] end

		WorkflowVersionEntity workflowVersionEntity = injector.getInstance(Key.get(WorkflowVersionEntity.class, Names.named("workflowVersionEntityWithNoRef")));
		workflowVersionEntity.setWorkflow(workflow, true);

		// % protected region % [Apply any additional logic for workflowVersionEntityWithRefs after the main body here] off begin
		// % protected region % [Apply any additional logic for workflowVersionEntityWithRefs after the main body here] end

		log.trace("Created entity of type WorkflowVersionEntity with references");

		return workflowVersionEntity;
	}

	/**
	 * Return a collection of Workflow Version entities with all references set.
	 */
	@Provides
	@Named("workflowVersionEntitiesWithRefs")
	public Collection<WorkflowVersionEntity> workflowVersionEntitiesWithRefs(
		// % protected region % [Apply any additional constructor parameters for workflowVersionEntitiesWithRefs here] off begin
		// % protected region % [Apply any additional constructor parameters for workflowVersionEntitiesWithRefs here] end
		WorkflowVersionEntityFactory workflowVersionEntityFactory
	) {
		log.trace("Creating entities of type WorkflowVersionEntity with references");

		// % protected region % [Apply any additional logic for workflowVersionEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for workflowVersionEntitiesWithNoRef before the main body here] end

		Collection<WorkflowVersionEntity> newEntities = workflowVersionEntityFactory.createMultipleWithRefs(10);

		// % protected region % [Apply any additional logic for workflowVersionEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for workflowVersionEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type WorkflowVersionEntity with references");

		return newEntities;
	}

	// % protected region % [Apply any additional class methods here] off begin
	// % protected region % [Apply any additional class methods here] end
}
