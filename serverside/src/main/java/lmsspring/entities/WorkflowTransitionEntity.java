/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

package lmsspring.entities;

import lmsspring.dtos.WorkflowTransitionEntityDto;
import lmsspring.entities.listeners.WorkflowTransitionEntityListener;
import lmsspring.serializers.WorkflowTransitionSerializer;
import lmsspring.lib.validators.ValidatorPatterns;
import lmsspring.services.utils.converters.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.opencsv.bean.CsvIgnore;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.URL;
import org.springframework.lang.Nullable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Optional;
import java.util.UUID;

// % protected region % [Import any additional imports here] off begin
// % protected region % [Import any additional imports here] end

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Entity
// % protected region % [Override the auditing annotation or add additional annotations here] off begin
@Audited
// % protected region % [Override the auditing annotation or add additional annotations here] end
@ExcludeSuperclassListeners
@EntityListeners({WorkflowTransitionEntityListener.class, AuditingEntityListener.class})
@JsonSerialize(using = WorkflowTransitionSerializer.class)
@Table(
	uniqueConstraints = {
	}
)
public class WorkflowTransitionEntity extends AbstractEntity {

	/**
	 * Takes a WorkflowTransitionEntityDto and converts it into a WorkflowTransitionEntity.
	 * Primarily used when endpoints are invoked, as they take DTOs as input, which need to be converted to the entity type
	 *
	 * @param workflowTransitionEntityDto
	 */
	public WorkflowTransitionEntity(WorkflowTransitionEntityDto workflowTransitionEntityDto) {
		// Need to account for potential that any field is empty.  This will be run before any server validation and
		// endpoints will most likely not be subject to clientside validation

		if (workflowTransitionEntityDto.getId() != null) {
			this.setId(workflowTransitionEntityDto.getId());
		}

		if (workflowTransitionEntityDto.getTransitionName() != null) {
			this.setTransitionName(workflowTransitionEntityDto.getTransitionName());
		}

		if (workflowTransitionEntityDto.getSourceState() != null) {
			this.setSourceState(workflowTransitionEntityDto.getSourceState());
		}

		if (workflowTransitionEntityDto.getTargetState() != null) {
			this.setTargetState(workflowTransitionEntityDto.getTargetState());
		}

		// % protected region % [Add any additional DTO constructor logic here] off begin
		// % protected region % [Add any additional DTO constructor logic here] end
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Attributes
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Modify attribute annotation for Transition Name here] off begin
	@CsvBindByName(column = "TRANSITION_NAME", required = true)
	@NotNull(message = "Transition Name must not be empty")
	@Column(name = "transition_name")
	@ApiModelProperty(notes = "The Transition Name of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Transition Name here] end
	private String transitionName;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing references
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Update the annotation for Source State here] off begin
	@ApiModelProperty(notes = "The Source State entities that are related to this entity.")
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@NotNull(message = "Reference Source State is required")
	@CsvIgnore
	// % protected region % [Update the annotation for Source State here] end
	private WorkflowStateEntity sourceState;

	// % protected region % [Update the annotation for sourceStateId here] off begin
	@Transient
	@CsvCustomBindByName(column = "SOURCE_STATE_ID", converter = UUIDConverter.class)
	// % protected region % [Update the annotation for sourceStateId here] end
	private UUID sourceStateId;

	// % protected region % [Update the annotation for Target State here] off begin
	@ApiModelProperty(notes = "The Target State entities that are related to this entity.")
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@NotNull(message = "Reference Target State is required")
	@CsvIgnore
	// % protected region % [Update the annotation for Target State here] end
	private WorkflowStateEntity targetState;

	// % protected region % [Update the annotation for targetStateId here] off begin
	@Transient
	@CsvCustomBindByName(column = "TARGET_STATE_ID", converter = UUIDConverter.class)
	// % protected region % [Update the annotation for targetStateId here] end
	private UUID targetStateId;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing reference methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Similar to {@link this#setSourceState(WorkflowStateEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given WorkflowStateEntity to be set to this entity
	 */
	public void setSourceState(@NotNull WorkflowStateEntity entity) {
		setSourceState(entity, true);
	}

	/**
	 * Set or update the sourceState in this entity with single WorkflowStateEntity.
	 *
	 * @param entity the given WorkflowStateEntity to be set or updated to sourceState
	 * @param reverseAdd whether this entity should be set or updated to the given entity
	 */
	public void setSourceState(@NotNull WorkflowStateEntity entity, boolean reverseAdd) {
		// % protected region % [Add any additional logic here before the main logic for setSourceState here] off begin
		// % protected region % [Add any additional logic here before the main logic for setSourceState here] end

		if (sameAsFormer(this.sourceState, entity)) {
			return;
		}

		if (this.sourceState != null) {
			this.sourceState.removeOutgoingTransitions(this, false);
		}
		this.sourceState = entity;
		if (reverseAdd) {
			this.sourceState.addOutgoingTransitions(this, false);
		}

		// % protected region % [Add any additional logic here after the main logic for setWorkflowState here] off begin
		// % protected region % [Add any additional logic here after the main logic for setWorkflowState here] end
	}

	/**
	 * Similar to {@link this#unsetSourceState(boolean)} but default to true.
	 */
	public void unsetSourceState() {
		this.unsetSourceState(true);
	}

	/**
	 * Remove Source State in this entity.
	 *
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void unsetSourceState(boolean reverse) {
		if (reverse && this.sourceState != null) {
			this.sourceState.removeOutgoingTransitions(this, false);
		}
		this.sourceState = null;
	}
	/**
	 * Similar to {@link this#setTargetState(WorkflowStateEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given WorkflowStateEntity to be set to this entity
	 */
	public void setTargetState(@NotNull WorkflowStateEntity entity) {
		setTargetState(entity, true);
	}

	/**
	 * Set or update the targetState in this entity with single WorkflowStateEntity.
	 *
	 * @param entity the given WorkflowStateEntity to be set or updated to targetState
	 * @param reverseAdd whether this entity should be set or updated to the given entity
	 */
	public void setTargetState(@NotNull WorkflowStateEntity entity, boolean reverseAdd) {
		// % protected region % [Add any additional logic here before the main logic for setTargetState here] off begin
		// % protected region % [Add any additional logic here before the main logic for setTargetState here] end

		if (sameAsFormer(this.targetState, entity)) {
			return;
		}

		if (this.targetState != null) {
			this.targetState.removeIncomingTransitions(this, false);
		}
		this.targetState = entity;
		if (reverseAdd) {
			this.targetState.addIncomingTransitions(this, false);
		}

		// % protected region % [Add any additional logic here after the main logic for setWorkflowState here] off begin
		// % protected region % [Add any additional logic here after the main logic for setWorkflowState here] end
	}

	/**
	 * Similar to {@link this#unsetTargetState(boolean)} but default to true.
	 */
	public void unsetTargetState() {
		this.unsetTargetState(true);
	}

	/**
	 * Remove Target State in this entity.
	 *
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void unsetTargetState(boolean reverse) {
		if (reverse && this.targetState != null) {
			this.targetState.removeIncomingTransitions(this, false);
		}
		this.targetState = null;
	}

	/**
	 * The CSV annotations used to generate and import CSV files require headers on the abstract entity attributes
	 * (id, created, modified, created by, modified by).  These should not be present in imports, as this information is
	 * added by the server when saving entities.  Therefore a string containing the example csv headers is required
	 *
	 * @return the headers recommended in the CSV import file in CSV format
	 */
	public static String getExampleCsvHeader() {

		// % protected region % [Modify the headers in the CSV file here] off begin
		return "TRANSITION_NAME,SOURCE_STATE_ID,TARGET_STATE_ID,ID";
		// % protected region % [Modify the headers in the CSV file here] end
	}

	public void addRelationEntitiesToIdSet() {
		// % protected region % [Add any additional logic for entity relations here] off begin
		Optional<WorkflowStateEntity> sourceStateRelation = Optional.ofNullable(this.sourceState);
		sourceStateRelation.ifPresent(entity -> this.sourceStateId = entity.getId());

		Optional<WorkflowStateEntity> targetStateRelation = Optional.ofNullable(this.targetState);
		targetStateRelation.ifPresent(entity -> this.targetStateId = entity.getId());

		// % protected region % [Add any additional logic for entity relations here] end
	}
	// % protected region % [Add any additional class methods  here] off begin
	// % protected region % [Add any additional class methods  here] end
}
