/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

package lmsspring.entities;

import lmsspring.dtos.ArticleEntityDto;
import lmsspring.entities.listeners.ArticleEntityListener;
import lmsspring.serializers.ArticleSerializer;
import lmsspring.lib.validators.ValidatorPatterns;
import lmsspring.services.utils.converters.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.opencsv.bean.CsvIgnore;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.URL;
import org.springframework.lang.Nullable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.hibernate.envers.Audited;

import java.util.*;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Optional;
import java.util.UUID;

// % protected region % [Import any additional imports here] off begin
// % protected region % [Import any additional imports here] end

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Entity
// % protected region % [Override the auditing annotation or add additional annotations here] off begin
@Audited
// % protected region % [Override the auditing annotation or add additional annotations here] end
@ExcludeSuperclassListeners
@EntityListeners({ArticleEntityListener.class, AuditingEntityListener.class})
@JsonSerialize(using = ArticleSerializer.class)
@Table(
	uniqueConstraints = {
		@UniqueConstraint(columnNames = {"title"}, name = "title"),
	}
)
public class ArticleEntity extends AbstractEntity {

	/**
	 * Takes a ArticleEntityDto and converts it into a ArticleEntity.
	 * Primarily used when endpoints are invoked, as they take DTOs as input, which need to be converted to the entity type
	 *
	 * @param articleEntityDto
	 */
	public ArticleEntity(ArticleEntityDto articleEntityDto) {
		// Need to account for potential that any field is empty.  This will be run before any server validation and
		// endpoints will most likely not be subject to clientside validation

		if (articleEntityDto.getId() != null) {
			this.setId(articleEntityDto.getId());
		}

		if (articleEntityDto.getTitle() != null) {
			this.setTitle(articleEntityDto.getTitle());
		}

		if (articleEntityDto.getSummary() != null) {
			this.setSummary(articleEntityDto.getSummary());
		}

		if (articleEntityDto.getContent() != null) {
			this.setContent(articleEntityDto.getContent());
		}

		if (articleEntityDto.getWorkflowStates() != null) {
			this.setWorkflowStates(articleEntityDto.getWorkflowStates());
		}

		if (articleEntityDto.getBook() != null) {
			this.setBook(articleEntityDto.getBook());
		}

		if (articleEntityDto.getTags() != null) {
			this.setTags(articleEntityDto.getTags());
		}

		// % protected region % [Add any additional DTO constructor logic here] off begin
		// % protected region % [Add any additional DTO constructor logic here] end
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Attributes
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Modify attribute annotation for Title here] off begin
	@CsvBindByName(column = "TITLE", required = true)
	@NotNull(message = "Title must not be empty")
	@Column(name = "title")
	@ApiModelProperty(notes = "The Title of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Title here] end
	private String title;

	// % protected region % [Modify attribute annotation for Summary here] off begin
	@CsvBindByName(column = "SUMMARY", required = true)
	@NotNull(message = "Summary must not be empty")
	@Column(name = "summary")
	@ApiModelProperty(notes = "The Summary of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Summary here] end
	private String summary;

	// % protected region % [Modify attribute annotation for Content here] off begin
	@CsvBindByName(column = "CONTENT", required = false)
	@Nullable
	@Lob
	@Column(name = "content")
	@ApiModelProperty(notes = "The Content of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Content here] end
	private String content;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing references
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Update the annotation for Workflow States here] off begin
	@ApiModelProperty(notes = "The Workflow State entities that are related to this entity.")
	@ManyToMany(cascade = {CascadeType.MERGE}, fetch = FetchType.LAZY)
	@CsvIgnore
	// % protected region % [Update the annotation for Workflow States here] end
	private Set<WorkflowStateEntity> workflowStates = new HashSet<>();

	// % protected region % [Update the annotation for workflowStatesIds here] off begin
	@Transient
	@CsvCustomBindByName(column = "WORKFLOW_STATES_IDS", converter = UUIDHashSetConverter.class)
	// % protected region % [Update the annotation for workflowStatesIds here] end
	private Set<UUID> workflowStatesIds = new HashSet<>();

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Update the annotation for Book here] off begin
	@ApiModelProperty(notes = "The Book entities that are related to this entity.")
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@CsvIgnore
	// % protected region % [Update the annotation for Book here] end
	private BookEntity book;

	// % protected region % [Update the annotation for bookId here] off begin
	@Transient
	@CsvCustomBindByName(column = "BOOK_ID", converter = UUIDConverter.class)
	// % protected region % [Update the annotation for bookId here] end
	private UUID bookId;

	// % protected region % [Update the annotation for Tags here] off begin
	@ApiModelProperty(notes = "The Tags entities that are related to this entity.")
	@ManyToMany(mappedBy = "articles", fetch = FetchType.LAZY)
	@CsvIgnore
	// % protected region % [Update the annotation for Tags here] end
	private Set<TagEntity> tags = new HashSet<>();

	// % protected region % [Update the annotation for tagsIds here] off begin
	@Transient
	@CsvCustomBindByName(column = "TAGS_IDS", converter = UUIDHashSetConverter.class)
	// % protected region % [Update the annotation for tagsIds here] end
	private Set<UUID> tagsIds = new HashSet<>();

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing reference methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Similar to {@link this#addWorkflowStates(WorkflowStateEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given WorkflowStateEntity to be added to this entity
	 */
	public void addWorkflowStates(@NotNull WorkflowStateEntity entity) {
		addWorkflowStates(entity, true);
	}

	/**
	 * Add a new WorkflowStateEntity to Workflow States in this entity.
	 *
	 * @param entity the given WorkflowStateEntity to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entity
	 */
	public void addWorkflowStates(@NonNull WorkflowStateEntity entity, boolean reverseAdd) {
		if (!this.workflowStates.contains(entity)) {
			this.workflowStates.add(entity);
			if (reverseAdd) {
				entity.addArticle(this, false);
			}
		}
	}

	/**
	 * Similar to {@link this#addWorkflowStates(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given entities to be added to this entity
	 */
	public void addWorkflowStates(@NotNull Collection<WorkflowStateEntity> entities) {
		addWorkflowStates(entities, true);
	}

	/**
	 * Add new collection of WorkflowStateEntity to workflowStates in this entity.
	 *
	 * @param entities the given entities to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void addWorkflowStates(@NonNull Collection<WorkflowStateEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> addWorkflowStates(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#removeWorkflowStates(WorkflowStateEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given WorkflowStateEntity to be set to this entity
	 */
	public void removeWorkflowStates(@NotNull WorkflowStateEntity entity) {
		this.removeWorkflowStates(entity, true);
	}

	/**
	 * Remove the given WorkflowStateEntity from this entity.
	 *
	 * @param entity the give WorkflowStateEntity to be removed from this entity
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void removeWorkflowStates(@NotNull WorkflowStateEntity entity, boolean reverse) {
		if (reverse) {
			entity.removeArticle(this, false);
		}
		this.workflowStates.remove(entity);
	}

	/**
	 * Similar to {@link this#removeWorkflowStates(Collection, boolean)} but
	 * default to true for reverse remove.
	 *
	 * @param entities the given entities to be removed to this entity
	 */
	public void removeWorkflowStates(@NotNull Collection<WorkflowStateEntity> entities) {
		this.removeWorkflowStates(entities, true);
	}

	/**
	 * Remove the given collection of WorkflowStateEntity in workflowStates from  to this entity.
	 *
	 * @param entities the given entities to be removed to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void removeWorkflowStates(@NonNull Collection<WorkflowStateEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> this.removeWorkflowStates(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#setWorkflowStates(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given entities to be set to this entity
	 */
	public void setWorkflowStates(@NotNull Collection<WorkflowStateEntity> entities) {
		this.setWorkflowStates(entities, true);
	}

	/**
	 * Replace the current entities in workflowStates with the given ones.
	 *
	 * @param entities the given entities to replace the old ones
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void setWorkflowStates(@NotNull Collection<WorkflowStateEntity> entities, boolean reverseAdd) {
		this.unsetWorkflowStates();
		this.workflowStates = new HashSet<>(entities);
		if (reverseAdd) {
			this.workflowStates.forEach(entity -> entity.addArticle(this, false));
		}
	}

	/**
	 * Similar to {@link this#unsetWorkflowStates(boolean)} but
	 * default to true for reverse unset
	 */
	public void unsetWorkflowStates() {
		this.unsetWorkflowStates(true);
	}

	/**
	 * Remove all entities in workflowStates from this entity.
	 * @param doReverse whether this entity should be removed from the given entities
	 */
	public void unsetWorkflowStates(boolean doReverse) {
		if (doReverse) {
			this.workflowStates.forEach(entity -> entity.removeArticle(this, false));
		}
		this.workflowStates.clear();
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Similar to {@link this#setBook(BookEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given BookEntity to be set to this entity
	 */
	public void setBook(@NotNull BookEntity entity) {
		setBook(entity, true);
	}

	/**
	 * Set or update the book in this entity with single BookEntity.
	 *
	 * @param entity the given BookEntity to be set or updated to book
	 * @param reverseAdd whether this entity should be set or updated to the given entity
	 */
	public void setBook(@NotNull BookEntity entity, boolean reverseAdd) {
		// % protected region % [Add any additional logic here before the main logic for setBook here] off begin
		// % protected region % [Add any additional logic here before the main logic for setBook here] end

		if (sameAsFormer(this.book, entity)) {
			return;
		}

		if (this.book != null) {
			this.book.removeArticles(this, false);
		}
		this.book = entity;
		if (reverseAdd) {
			this.book.addArticles(this, false);
		}

		// % protected region % [Add any additional logic here after the main logic for setBook here] off begin
		// % protected region % [Add any additional logic here after the main logic for setBook here] end
	}

	/**
	 * Similar to {@link this#unsetBook(boolean)} but default to true.
	 */
	public void unsetBook() {
		this.unsetBook(true);
	}

	/**
	 * Remove Book in this entity.
	 *
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void unsetBook(boolean reverse) {
		if (reverse && this.book != null) {
			this.book.removeArticles(this, false);
		}
		this.book = null;
	}
	/**
	 * Similar to {@link this#addTags(TagEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given TagEntity to be added to tags
	 */
	public void addTags(@NotNull TagEntity entity) {
		this.addTags(entity, true);
	}

	/**
	 * Add a new TagEntity to tags in this entity.
	 *
	 * @param entity the given TagEntity to be added to tags
	 * @param reverseAdd whether this entity should be added to the given entity
	 */
	public void addTags(@NotNull TagEntity entity, boolean reverseAdd) {
		if (!this.tags.contains(entity)) {
			this.tags.add(entity);
			if (reverseAdd) {
				entity.addArticles(this, false);
			}
		}
	}

	/**
	 * Similar to {@link this#addTags(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of TagEntity to be added into tags
	 */
	public void addTags(@NotNull Collection<TagEntity> entities) {
		this.addTags(entities, true);
	}

	/**
	 * Add new collection of TagEntity to tags in this entity.
	 *
	 * @param entities the given collection of TagEntity to be added into tags in this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void addTags(@NonNull Collection<TagEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> this.addTags(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#removeTags(TagEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given TagEntity to be set to tags in this entity
	 */
	public void removeTags(@NotNull TagEntity entity) {
		this.removeTags(entity, true);
	}

	/**
	 * Remove the given TagEntity from tags in this entity.
	 *
	 * @param entity the given TagEntity to be removed from tags
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void removeTags(@NotNull TagEntity entity, boolean reverse) {
		if (reverse) {
			entity.removeArticles(this, false);
		}
		this.tags.remove(entity);
	}

	/**
	 * Similar to {@link this#removeTags(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of TagEntity to be removed from tags in this entity
	 */
	public void removeTags(@NotNull Collection<TagEntity> entities) {
		this.removeTags(entities, true);
	}

	/**
	 * Remove the given collection of TagEntity from tags in this entity.
	 *
	 * @param entities the given collection of TagEntity to be removed from tags
	 * @param reverseRemove whether this entity should be removed to the given entities
	 */
	public void removeTags(@NonNull Collection<TagEntity> entities, boolean reverseRemove) {
		entities.forEach(entity -> this.removeTags(entity, reverseRemove));
	}

	/**
	 * Similar to {@link this#setTags(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of TagEntity to replace the old ones in tags
	 */
	public void setTags(@NotNull Collection<TagEntity> entities) {
		this.setTags(entities, true);
	}

	/**
	 * Replace the current collection of TagEntity in tags with the given ones.
	 *
	 * @param entities the given collection of TagEntity to replace the old ones in tags
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void setTags(@NotNull Collection<TagEntity> entities, boolean reverseAdd) {
		unsetTags();
		this.tags = new HashSet<>(entities);
		if (reverseAdd) {
			this.tags.forEach(tagsEntity -> tagsEntity.addArticles(this, false));
		}
	}

	/**
	 * Remove all entities in Tags from this entity.
	 */
	public void unsetTags() {
		this.tags.forEach(entity -> entity.removeArticles(this, false));
		this.tags.clear();
	}

	/**
	 * The CSV annotations used to generate and import CSV files require headers on the abstract entity attributes
	 * (id, created, modified, created by, modified by).  These should not be present in imports, as this information is
	 * added by the server when saving entities.  Therefore a string containing the example csv headers is required
	 *
	 * @return the headers recommended in the CSV import file in CSV format
	 */
	public static String getExampleCsvHeader() {

		// % protected region % [Modify the headers in the CSV file here] off begin
		return "TITLE,SUMMARY,CONTENT,BOOK_ID,TAGS_IDS,WORKFLOW_STATES_IDS,ID";
		// % protected region % [Modify the headers in the CSV file here] end
	}

	public void addRelationEntitiesToIdSet() {
		// % protected region % [Add any additional logic for entity relations here] off begin
		Optional<BookEntity> bookRelation = Optional.ofNullable(this.book);
		bookRelation.ifPresent(entity -> this.bookId = entity.getId());

		for (TagEntity tags: this.tags) {
			this.tagsIds.add(tags.getId());
		}

		for (WorkflowStateEntity workflowStates: this.workflowStates) {
			this.workflowStatesIds.add(workflowStates.getId());
		}

		// % protected region % [Add any additional logic for entity relations here] end
	}
	// % protected region % [Add any additional class methods  here] off begin
	// % protected region % [Add any additional class methods  here] end
}
