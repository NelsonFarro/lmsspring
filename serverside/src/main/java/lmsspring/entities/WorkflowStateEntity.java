/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

package lmsspring.entities;

import lmsspring.dtos.WorkflowStateEntityDto;
import lmsspring.entities.listeners.WorkflowStateEntityListener;
import lmsspring.serializers.WorkflowStateSerializer;
import lmsspring.lib.validators.ValidatorPatterns;
import lmsspring.services.utils.converters.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.opencsv.bean.CsvIgnore;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.URL;
import org.springframework.lang.Nullable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.hibernate.envers.Audited;

import java.util.*;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Optional;
import java.util.UUID;

// % protected region % [Import any additional imports here] off begin
// % protected region % [Import any additional imports here] end

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Entity
// % protected region % [Override the auditing annotation or add additional annotations here] off begin
@Audited
// % protected region % [Override the auditing annotation or add additional annotations here] end
@ExcludeSuperclassListeners
@EntityListeners({WorkflowStateEntityListener.class, AuditingEntityListener.class})
@JsonSerialize(using = WorkflowStateSerializer.class)
@Table(
	uniqueConstraints = {
	}
)
public class WorkflowStateEntity extends AbstractEntity {

	/**
	 * Takes a WorkflowStateEntityDto and converts it into a WorkflowStateEntity.
	 * Primarily used when endpoints are invoked, as they take DTOs as input, which need to be converted to the entity type
	 *
	 * @param workflowStateEntityDto
	 */
	public WorkflowStateEntity(WorkflowStateEntityDto workflowStateEntityDto) {
		// Need to account for potential that any field is empty.  This will be run before any server validation and
		// endpoints will most likely not be subject to clientside validation

		if (workflowStateEntityDto.getId() != null) {
			this.setId(workflowStateEntityDto.getId());
		}

		if (workflowStateEntityDto.getDisplayIndex() != null) {
			this.setDisplayIndex(workflowStateEntityDto.getDisplayIndex());
		}

		if (workflowStateEntityDto.getStepName() != null) {
			this.setStepName(workflowStateEntityDto.getStepName());
		}

		if (workflowStateEntityDto.getStateDescription() != null) {
			this.setStateDescription(workflowStateEntityDto.getStateDescription());
		}

		if (workflowStateEntityDto.getIsStartState() != null) {
			this.setIsStartState(workflowStateEntityDto.getIsStartState());
		}

		if (workflowStateEntityDto.getOutgoingTransitions() != null) {
			this.setOutgoingTransitions(workflowStateEntityDto.getOutgoingTransitions());
		}

		if (workflowStateEntityDto.getIncomingTransitions() != null) {
			this.setIncomingTransitions(workflowStateEntityDto.getIncomingTransitions());
		}

		if (workflowStateEntityDto.getWorkflowVersion() != null) {
			this.setWorkflowVersion(workflowStateEntityDto.getWorkflowVersion());
		}

		if (workflowStateEntityDto.getArticle() != null) {
			this.setArticle(workflowStateEntityDto.getArticle());
		}

		// % protected region % [Add any additional DTO constructor logic here] off begin
		// % protected region % [Add any additional DTO constructor logic here] end
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Attributes
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Modify attribute annotation for Display Index here] off begin
	@CsvBindByName(column = "DISPLAY_INDEX", required = false)
	@Nullable
	@Column(name = "display_index")
	@ApiModelProperty(notes = "The Display Index of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Display Index here] end
	private Integer displayIndex;

	// % protected region % [Modify attribute annotation for Step Name here] off begin
	@CsvBindByName(column = "STEP_NAME", required = true)
	@NotNull(message = "Step Name must not be empty")
	@Column(name = "step_name")
	@ApiModelProperty(notes = "The Step Name of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Step Name here] end
	private String stepName;

	// % protected region % [Modify attribute annotation for State Description here] off begin
	@CsvBindByName(column = "STATE_DESCRIPTION", required = false)
	@Nullable
	@Column(name = "state_description")
	@ApiModelProperty(notes = "The State Description of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for State Description here] end
	private String stateDescription;

	// % protected region % [Modify attribute annotation for Is Start State here] off begin
	@CsvBindByName(column = "IS_START_STATE", required = false)
	@Nullable
	@Column(name = "is_start_state")
	@ApiModelProperty(notes = "The Is Start State of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Is Start State here] end
	private Boolean isStartState;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing references
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Update the annotation for Outgoing Transitions here] off begin
	@ApiModelProperty(notes = "The Workflow Transition entities that are related to this entity.")
	@OneToMany(mappedBy = "sourceState", cascade = {CascadeType.MERGE, CascadeType.REMOVE}, fetch = FetchType.LAZY)
	@CsvIgnore
	// % protected region % [Update the annotation for Outgoing Transitions here] end
	private Set<WorkflowTransitionEntity> outgoingTransitions = new HashSet<>();

	// % protected region % [Update the annotation for outgoingTransitionsIds here] off begin
	@Transient
	@CsvCustomBindByName(column = "OUTGOING_TRANSITIONS_IDS", converter = UUIDHashSetConverter.class)
	// % protected region % [Update the annotation for outgoingTransitionsIds here] end
	private Set<UUID> outgoingTransitionsIds = new HashSet<>();

	// % protected region % [Update the annotation for Incoming Transitions here] off begin
	@ApiModelProperty(notes = "The Workflow Transition entities that are related to this entity.")
	@OneToMany(mappedBy = "targetState", cascade = {CascadeType.MERGE, CascadeType.REMOVE}, fetch = FetchType.LAZY)
	@CsvIgnore
	// % protected region % [Update the annotation for Incoming Transitions here] end
	private Set<WorkflowTransitionEntity> incomingTransitions = new HashSet<>();

	// % protected region % [Update the annotation for incomingTransitionsIds here] off begin
	@Transient
	@CsvCustomBindByName(column = "INCOMING_TRANSITIONS_IDS", converter = UUIDHashSetConverter.class)
	// % protected region % [Update the annotation for incomingTransitionsIds here] end
	private Set<UUID> incomingTransitionsIds = new HashSet<>();

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Update the annotation for Workflow Version here] off begin
	@ApiModelProperty(notes = "The Workflow Version entities that are related to this entity.")
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@NotNull(message = "Reference Workflow Version is required")
	@CsvIgnore
	// % protected region % [Update the annotation for Workflow Version here] end
	private WorkflowVersionEntity workflowVersion;

	// % protected region % [Update the annotation for workflowVersionId here] off begin
	@Transient
	@CsvCustomBindByName(column = "WORKFLOW_VERSION_ID", converter = UUIDConverter.class)
	// % protected region % [Update the annotation for workflowVersionId here] end
	private UUID workflowVersionId;

	// % protected region % [Update the annotation for Article here] off begin
	@ApiModelProperty(notes = "The Article entities that are related to this entity.")
	@ManyToMany(mappedBy = "workflowStates", fetch = FetchType.LAZY)
	@CsvIgnore
	// % protected region % [Update the annotation for Article here] end
	private Set<ArticleEntity> article = new HashSet<>();

	// % protected region % [Update the annotation for articleIds here] off begin
	@Transient
	@CsvCustomBindByName(column = "ARTICLE_IDS", converter = UUIDHashSetConverter.class)
	// % protected region % [Update the annotation for articleIds here] end
	private Set<UUID> articleIds = new HashSet<>();

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing reference methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
	 * Similar to {@link this#addOutgoingTransitions(WorkflowTransitionEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given WorkflowTransitionEntity to be added to this entity
	 */
	public void addOutgoingTransitions(@NotNull WorkflowTransitionEntity entity) {
		addOutgoingTransitions(entity, true);
	}

	/**
	 * Add a new WorkflowTransitionEntity to outgoingTransitions in this entity.
	 *
	 * @param entity the given WorkflowTransitionEntity  to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entity
	 */
	public void addOutgoingTransitions(@NonNull WorkflowTransitionEntity entity, boolean reverseAdd) {
		if (!this.outgoingTransitions.contains(entity)) {
			outgoingTransitions.add(entity);
			if (reverseAdd) {
				entity.setSourceState(this, false);
			}
		}
	}

	/**
	 * Similar to {@link this#addOutgoingTransitions(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of WorkflowTransitionEntity to be added to this entity
	 */
	public void addOutgoingTransitions(@NotNull Collection<WorkflowTransitionEntity> entities) {
		addOutgoingTransitions(entities, true);
	}

	/**
	 * Add a new collection of WorkflowTransitionEntity to Outgoing Transitions in this entity.
	 *
	 * @param entities the given collection of WorkflowTransitionEntity to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void addOutgoingTransitions(@NonNull Collection<WorkflowTransitionEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> addOutgoingTransitions(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#removeOutgoingTransitions(WorkflowTransitionEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given WorkflowTransitionEntity to be set to this entity
	 */
	public void removeOutgoingTransitions(@NotNull WorkflowTransitionEntity entity) {
		this.removeOutgoingTransitions(entity, true);
	}

	/**
	 * Remove the given WorkflowTransitionEntity from this entity.
	 *
	 * @param entity the given WorkflowTransitionEntity to be removed from this entity
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void removeOutgoingTransitions(@NotNull WorkflowTransitionEntity entity, boolean reverse) {
		if (reverse) {
			entity.unsetSourceState(false);
		}
		this.outgoingTransitions.remove(entity);
	}

	/**
	 * Similar to {@link this#removeOutgoingTransitions(Collection, boolean)} but
	 * default to true for reverse remove.
	 *
	 * @param entities the given collection of WorkflowTransitionEntity to be removed to this entity
	 */
	public void removeOutgoingTransitions(@NotNull Collection<WorkflowTransitionEntity> entities) {
		this.removeOutgoingTransitions(entities, true);
	}

	/**
	 * Remove the given collection of WorkflowTransitionEntity from  to this entity.
	 *
	 * @param entities the given collection of WorkflowTransitionEntity to be removed to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void removeOutgoingTransitions(@NonNull Collection<WorkflowTransitionEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> this.removeOutgoingTransitions(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#setOutgoingTransitions(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of WorkflowTransitionEntity to be set to this entity
	 */
	public void setOutgoingTransitions(@NotNull Collection<WorkflowTransitionEntity> entities) {
		setOutgoingTransitions(entities, true);
	}

	/**
	 * Replace the current entities in Outgoing Transitions with the given ones.
	 *
	 * @param entities the given collection of WorkflowTransitionEntity to replace the old ones
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void setOutgoingTransitions(@NotNull Collection<WorkflowTransitionEntity> entities, boolean reverseAdd) {

		this.unsetOutgoingTransitions();
		this.outgoingTransitions = new HashSet<>(entities);
		if (reverseAdd) {
			this.outgoingTransitions.forEach(outgoingTransitionsEntity -> outgoingTransitionsEntity.setSourceState(this, false));
		}
	}

	/**
	 * Similar to {@link this#unsetOutgoingTransitions(boolean)} but
	 * default to true for reverse unset
	 */
	public void unsetOutgoingTransitions() {
		this.unsetOutgoingTransitions(true);
	}

	/**
	 * Remove all the entities in Outgoing Transitions from this entity.
	 * @param doReverse whether this entity should be removed from the given entities
	 */
	public void unsetOutgoingTransitions(boolean doReverse) {
		if (doReverse) {
			this.outgoingTransitions.forEach(outgoingTransitionsEntity -> outgoingTransitionsEntity.unsetSourceState(false));
		}
		this.outgoingTransitions.clear();
	}

/**
	 * Similar to {@link this#addIncomingTransitions(WorkflowTransitionEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given WorkflowTransitionEntity to be added to this entity
	 */
	public void addIncomingTransitions(@NotNull WorkflowTransitionEntity entity) {
		addIncomingTransitions(entity, true);
	}

	/**
	 * Add a new WorkflowTransitionEntity to incomingTransitions in this entity.
	 *
	 * @param entity the given WorkflowTransitionEntity  to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entity
	 */
	public void addIncomingTransitions(@NonNull WorkflowTransitionEntity entity, boolean reverseAdd) {
		if (!this.incomingTransitions.contains(entity)) {
			incomingTransitions.add(entity);
			if (reverseAdd) {
				entity.setTargetState(this, false);
			}
		}
	}

	/**
	 * Similar to {@link this#addIncomingTransitions(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of WorkflowTransitionEntity to be added to this entity
	 */
	public void addIncomingTransitions(@NotNull Collection<WorkflowTransitionEntity> entities) {
		addIncomingTransitions(entities, true);
	}

	/**
	 * Add a new collection of WorkflowTransitionEntity to Incoming Transitions in this entity.
	 *
	 * @param entities the given collection of WorkflowTransitionEntity to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void addIncomingTransitions(@NonNull Collection<WorkflowTransitionEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> addIncomingTransitions(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#removeIncomingTransitions(WorkflowTransitionEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given WorkflowTransitionEntity to be set to this entity
	 */
	public void removeIncomingTransitions(@NotNull WorkflowTransitionEntity entity) {
		this.removeIncomingTransitions(entity, true);
	}

	/**
	 * Remove the given WorkflowTransitionEntity from this entity.
	 *
	 * @param entity the given WorkflowTransitionEntity to be removed from this entity
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void removeIncomingTransitions(@NotNull WorkflowTransitionEntity entity, boolean reverse) {
		if (reverse) {
			entity.unsetTargetState(false);
		}
		this.incomingTransitions.remove(entity);
	}

	/**
	 * Similar to {@link this#removeIncomingTransitions(Collection, boolean)} but
	 * default to true for reverse remove.
	 *
	 * @param entities the given collection of WorkflowTransitionEntity to be removed to this entity
	 */
	public void removeIncomingTransitions(@NotNull Collection<WorkflowTransitionEntity> entities) {
		this.removeIncomingTransitions(entities, true);
	}

	/**
	 * Remove the given collection of WorkflowTransitionEntity from  to this entity.
	 *
	 * @param entities the given collection of WorkflowTransitionEntity to be removed to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void removeIncomingTransitions(@NonNull Collection<WorkflowTransitionEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> this.removeIncomingTransitions(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#setIncomingTransitions(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of WorkflowTransitionEntity to be set to this entity
	 */
	public void setIncomingTransitions(@NotNull Collection<WorkflowTransitionEntity> entities) {
		setIncomingTransitions(entities, true);
	}

	/**
	 * Replace the current entities in Incoming Transitions with the given ones.
	 *
	 * @param entities the given collection of WorkflowTransitionEntity to replace the old ones
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void setIncomingTransitions(@NotNull Collection<WorkflowTransitionEntity> entities, boolean reverseAdd) {

		this.unsetIncomingTransitions();
		this.incomingTransitions = new HashSet<>(entities);
		if (reverseAdd) {
			this.incomingTransitions.forEach(incomingTransitionsEntity -> incomingTransitionsEntity.setTargetState(this, false));
		}
	}

	/**
	 * Similar to {@link this#unsetIncomingTransitions(boolean)} but
	 * default to true for reverse unset
	 */
	public void unsetIncomingTransitions() {
		this.unsetIncomingTransitions(true);
	}

	/**
	 * Remove all the entities in Incoming Transitions from this entity.
	 * @param doReverse whether this entity should be removed from the given entities
	 */
	public void unsetIncomingTransitions(boolean doReverse) {
		if (doReverse) {
			this.incomingTransitions.forEach(incomingTransitionsEntity -> incomingTransitionsEntity.unsetTargetState(false));
		}
		this.incomingTransitions.clear();
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Similar to {@link this#setWorkflowVersion(WorkflowVersionEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given WorkflowVersionEntity to be set to this entity
	 */
	public void setWorkflowVersion(@NotNull WorkflowVersionEntity entity) {
		setWorkflowVersion(entity, true);
	}

	/**
	 * Set or update the workflowVersion in this entity with single WorkflowVersionEntity.
	 *
	 * @param entity the given WorkflowVersionEntity to be set or updated to workflowVersion
	 * @param reverseAdd whether this entity should be set or updated to the given entity
	 */
	public void setWorkflowVersion(@NotNull WorkflowVersionEntity entity, boolean reverseAdd) {
		// % protected region % [Add any additional logic here before the main logic for setWorkflowVersion here] off begin
		// % protected region % [Add any additional logic here before the main logic for setWorkflowVersion here] end

		if (sameAsFormer(this.workflowVersion, entity)) {
			return;
		}

		if (this.workflowVersion != null) {
			this.workflowVersion.removeStates(this, false);
		}
		this.workflowVersion = entity;
		if (reverseAdd) {
			this.workflowVersion.addStates(this, false);
		}

		// % protected region % [Add any additional logic here after the main logic for setWorkflowVersion here] off begin
		// % protected region % [Add any additional logic here after the main logic for setWorkflowVersion here] end
	}

	/**
	 * Similar to {@link this#unsetWorkflowVersion(boolean)} but default to true.
	 */
	public void unsetWorkflowVersion() {
		this.unsetWorkflowVersion(true);
	}

	/**
	 * Remove Workflow Version in this entity.
	 *
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void unsetWorkflowVersion(boolean reverse) {
		if (reverse && this.workflowVersion != null) {
			this.workflowVersion.removeStates(this, false);
		}
		this.workflowVersion = null;
	}
	/**
	 * Similar to {@link this#addArticle(ArticleEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given ArticleEntity to be added to article
	 */
	public void addArticle(@NotNull ArticleEntity entity) {
		this.addArticle(entity, true);
	}

	/**
	 * Add a new ArticleEntity to article in this entity.
	 *
	 * @param entity the given ArticleEntity to be added to article
	 * @param reverseAdd whether this entity should be added to the given entity
	 */
	public void addArticle(@NotNull ArticleEntity entity, boolean reverseAdd) {
		if (!this.article.contains(entity)) {
			this.article.add(entity);
			if (reverseAdd) {
				entity.addWorkflowStates(this, false);
			}
		}
	}

	/**
	 * Similar to {@link this#addArticle(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of ArticleEntity to be added into article
	 */
	public void addArticle(@NotNull Collection<ArticleEntity> entities) {
		this.addArticle(entities, true);
	}

	/**
	 * Add new collection of ArticleEntity to article in this entity.
	 *
	 * @param entities the given collection of ArticleEntity to be added into article in this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void addArticle(@NonNull Collection<ArticleEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> this.addArticle(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#removeArticle(ArticleEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given ArticleEntity to be set to article in this entity
	 */
	public void removeArticle(@NotNull ArticleEntity entity) {
		this.removeArticle(entity, true);
	}

	/**
	 * Remove the given ArticleEntity from article in this entity.
	 *
	 * @param entity the given ArticleEntity to be removed from article
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void removeArticle(@NotNull ArticleEntity entity, boolean reverse) {
		if (reverse) {
			entity.removeWorkflowStates(this, false);
		}
		this.article.remove(entity);
	}

	/**
	 * Similar to {@link this#removeArticle(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of ArticleEntity to be removed from article in this entity
	 */
	public void removeArticle(@NotNull Collection<ArticleEntity> entities) {
		this.removeArticle(entities, true);
	}

	/**
	 * Remove the given collection of ArticleEntity from article in this entity.
	 *
	 * @param entities the given collection of ArticleEntity to be removed from article
	 * @param reverseRemove whether this entity should be removed to the given entities
	 */
	public void removeArticle(@NonNull Collection<ArticleEntity> entities, boolean reverseRemove) {
		entities.forEach(entity -> this.removeArticle(entity, reverseRemove));
	}

	/**
	 * Similar to {@link this#setArticle(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of ArticleEntity to replace the old ones in article
	 */
	public void setArticle(@NotNull Collection<ArticleEntity> entities) {
		this.setArticle(entities, true);
	}

	/**
	 * Replace the current collection of ArticleEntity in article with the given ones.
	 *
	 * @param entities the given collection of ArticleEntity to replace the old ones in article
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void setArticle(@NotNull Collection<ArticleEntity> entities, boolean reverseAdd) {
		unsetArticle();
		this.article = new HashSet<>(entities);
		if (reverseAdd) {
			this.article.forEach(articleEntity -> articleEntity.addWorkflowStates(this, false));
		}
	}

	/**
	 * Remove all entities in Article from this entity.
	 */
	public void unsetArticle() {
		this.article.forEach(entity -> entity.removeWorkflowStates(this, false));
		this.article.clear();
	}

	/**
	 * The CSV annotations used to generate and import CSV files require headers on the abstract entity attributes
	 * (id, created, modified, created by, modified by).  These should not be present in imports, as this information is
	 * added by the server when saving entities.  Therefore a string containing the example csv headers is required
	 *
	 * @return the headers recommended in the CSV import file in CSV format
	 */
	public static String getExampleCsvHeader() {

		// % protected region % [Modify the headers in the CSV file here] off begin
		return "DISPLAY_INDEX,STEP_NAME,STATE_DESCRIPTION,IS_START_STATE,WORKFLOW_VERSION_ID,ARTICLE_IDS,OUTGOING_TRANSITIONS_IDS,INCOMING_TRANSITIONS_IDS,ID";
		// % protected region % [Modify the headers in the CSV file here] end
	}

	public void addRelationEntitiesToIdSet() {
		// % protected region % [Add any additional logic for entity relations here] off begin
		Optional<WorkflowVersionEntity> workflowVersionRelation = Optional.ofNullable(this.workflowVersion);
		workflowVersionRelation.ifPresent(entity -> this.workflowVersionId = entity.getId());

		for (ArticleEntity article: this.article) {
			this.articleIds.add(article.getId());
		}

		for (WorkflowTransitionEntity outgoingTransitions: this.outgoingTransitions) {
			this.outgoingTransitionsIds.add(outgoingTransitions.getId());
		}

		for (WorkflowTransitionEntity incomingTransitions: this.incomingTransitions) {
			this.incomingTransitionsIds.add(incomingTransitions.getId());
		}

		// % protected region % [Add any additional logic for entity relations here] end
	}
	// % protected region % [Add any additional class methods  here] off begin
	// % protected region % [Add any additional class methods  here] end
}
