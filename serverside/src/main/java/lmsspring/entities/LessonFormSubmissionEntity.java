/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

package lmsspring.entities;

import lmsspring.dtos.LessonFormSubmissionEntityDto;
import lmsspring.entities.listeners.LessonFormSubmissionEntityListener;
import lmsspring.serializers.LessonFormSubmissionSerializer;
import lmsspring.lib.validators.ValidatorPatterns;
import lmsspring.services.utils.converters.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.opencsv.bean.CsvIgnore;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.URL;
import org.springframework.lang.Nullable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Optional;
import java.util.UUID;

// % protected region % [Import any additional imports here] off begin
// % protected region % [Import any additional imports here] end

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Entity
// % protected region % [Override the auditing annotation or add additional annotations here] off begin
@Audited
// % protected region % [Override the auditing annotation or add additional annotations here] end
@ExcludeSuperclassListeners
@EntityListeners({LessonFormSubmissionEntityListener.class, AuditingEntityListener.class})
@JsonSerialize(using = LessonFormSubmissionSerializer.class)
@Table(
	uniqueConstraints = {
	}
)
public class LessonFormSubmissionEntity extends AbstractEntity {

	/**
	 * Takes a LessonFormSubmissionEntityDto and converts it into a LessonFormSubmissionEntity.
	 * Primarily used when endpoints are invoked, as they take DTOs as input, which need to be converted to the entity type
	 *
	 * @param lessonFormSubmissionEntityDto
	 */
	public LessonFormSubmissionEntity(LessonFormSubmissionEntityDto lessonFormSubmissionEntityDto) {
		// Need to account for potential that any field is empty.  This will be run before any server validation and
		// endpoints will most likely not be subject to clientside validation

		if (lessonFormSubmissionEntityDto.getId() != null) {
			this.setId(lessonFormSubmissionEntityDto.getId());
		}

		if (lessonFormSubmissionEntityDto.getSubmissionData() != null) {
			this.setSubmissionData(lessonFormSubmissionEntityDto.getSubmissionData());
		}

		if (lessonFormSubmissionEntityDto.getSubmittedForm() != null) {
			this.setSubmittedForm(lessonFormSubmissionEntityDto.getSubmittedForm());
		}

		// % protected region % [Add any additional DTO constructor logic here] off begin
		// % protected region % [Add any additional DTO constructor logic here] end
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Attributes
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Modify attribute annotation for Submission Data here] off begin
	@CsvBindByName(column = "SUBMISSION_DATA", required = false)
	@Nullable
	@Lob
	@Column(name = "submission_data")
	@ApiModelProperty(notes = "The Submission Data of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Submission Data here] end
	private String submissionData;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing references
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Update the annotation for Submitted form here] off begin
	@ApiModelProperty(notes = "The Submitted form entities that are related to this entity.")
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@NotNull(message = "Reference Submitted form is required")
	@CsvIgnore
	// % protected region % [Update the annotation for Submitted form here] end
	private LessonFormVersionEntity submittedForm;

	// % protected region % [Update the annotation for submittedFormId here] off begin
	@Transient
	@CsvCustomBindByName(column = "SUBMITTED_FORM_ID", converter = UUIDConverter.class)
	// % protected region % [Update the annotation for submittedFormId here] end
	private UUID submittedFormId;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing reference methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Similar to {@link this#setSubmittedForm(LessonFormVersionEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given LessonFormVersionEntity to be set to this entity
	 */
	public void setSubmittedForm(@NotNull LessonFormVersionEntity entity) {
		setSubmittedForm(entity, true);
	}

	/**
	 * Set or update the submittedForm in this entity with single LessonFormVersionEntity.
	 *
	 * @param entity the given LessonFormVersionEntity to be set or updated to submittedForm
	 * @param reverseAdd whether this entity should be set or updated to the given entity
	 */
	public void setSubmittedForm(@NotNull LessonFormVersionEntity entity, boolean reverseAdd) {
		// % protected region % [Add any additional logic here before the main logic for setSubmittedForm here] off begin
		// % protected region % [Add any additional logic here before the main logic for setSubmittedForm here] end

		if (sameAsFormer(this.submittedForm, entity)) {
			return;
		}

		if (this.submittedForm != null) {
			this.submittedForm.removeSubmission(this, false);
		}
		this.submittedForm = entity;
		if (reverseAdd) {
			this.submittedForm.addSubmission(this, false);
		}

		// % protected region % [Add any additional logic here after the main logic for setLessonFormVersion here] off begin
		// % protected region % [Add any additional logic here after the main logic for setLessonFormVersion here] end
	}

	/**
	 * Similar to {@link this#unsetSubmittedForm(boolean)} but default to true.
	 */
	public void unsetSubmittedForm() {
		this.unsetSubmittedForm(true);
	}

	/**
	 * Remove Submitted form in this entity.
	 *
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void unsetSubmittedForm(boolean reverse) {
		if (reverse && this.submittedForm != null) {
			this.submittedForm.removeSubmission(this, false);
		}
		this.submittedForm = null;
	}

	/**
	 * The CSV annotations used to generate and import CSV files require headers on the abstract entity attributes
	 * (id, created, modified, created by, modified by).  These should not be present in imports, as this information is
	 * added by the server when saving entities.  Therefore a string containing the example csv headers is required
	 *
	 * @return the headers recommended in the CSV import file in CSV format
	 */
	public static String getExampleCsvHeader() {

		// % protected region % [Modify the headers in the CSV file here] off begin
		return "SUBMISSION_DATA,SUBMITTED_FORM_ID,ID";
		// % protected region % [Modify the headers in the CSV file here] end
	}

	public void addRelationEntitiesToIdSet() {
		// % protected region % [Add any additional logic for entity relations here] off begin
		Optional<LessonFormVersionEntity> submittedFormRelation = Optional.ofNullable(this.submittedForm);
		submittedFormRelation.ifPresent(entity -> this.submittedFormId = entity.getId());

		// % protected region % [Add any additional logic for entity relations here] end
	}
	// % protected region % [Add any additional class methods  here] off begin
	// % protected region % [Add any additional class methods  here] end
}
