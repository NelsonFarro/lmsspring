/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

package lmsspring.entities;

import lmsspring.dtos.CourseEntityDto;
import lmsspring.entities.enums.*;
import lmsspring.services.utils.converters.enums.*;
import lmsspring.entities.listeners.CourseEntityListener;
import lmsspring.serializers.CourseSerializer;
import lmsspring.lib.validators.ValidatorPatterns;
import lmsspring.services.utils.converters.*;
import lmsspring.lib.file.models.FileEntity;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.opencsv.bean.CsvIgnore;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.URL;
import org.springframework.lang.Nullable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.hibernate.envers.Audited;

import java.util.*;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Optional;
import java.util.UUID;

// % protected region % [Import any additional imports here] off begin
// % protected region % [Import any additional imports here] end

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Entity
// % protected region % [Override the auditing annotation or add additional annotations here] off begin
@Audited
// % protected region % [Override the auditing annotation or add additional annotations here] end
@ExcludeSuperclassListeners
@EntityListeners({CourseEntityListener.class, AuditingEntityListener.class})
@JsonSerialize(using = CourseSerializer.class)
@Table(
	uniqueConstraints = {
		@UniqueConstraint(columnNames = {"name"}, name = "name"),
	}
)
public class CourseEntity extends AbstractEntity {

	/**
	 * Takes a CourseEntityDto and converts it into a CourseEntity.
	 * Primarily used when endpoints are invoked, as they take DTOs as input, which need to be converted to the entity type
	 *
	 * @param courseEntityDto
	 */
	public CourseEntity(CourseEntityDto courseEntityDto) {
		// Need to account for potential that any field is empty.  This will be run before any server validation and
		// endpoints will most likely not be subject to clientside validation

		if (courseEntityDto.getId() != null) {
			this.setId(courseEntityDto.getId());
		}

		if (courseEntityDto.getName() != null) {
			this.setName(courseEntityDto.getName());
		}

		if (courseEntityDto.getSummary() != null) {
			this.setSummary(courseEntityDto.getSummary());
		}

		if (courseEntityDto.getDifficulty() != null) {
			this.setDifficulty(courseEntityDto.getDifficulty());
		}

		if (courseEntityDto.getCourseLessons() != null) {
			this.setCourseLessons(courseEntityDto.getCourseLessons());
		}

		if (courseEntityDto.getCourseCategory() != null) {
			this.setCourseCategory(courseEntityDto.getCourseCategory());
		}

		// % protected region % [Add any additional DTO constructor logic here] off begin
		// % protected region % [Add any additional DTO constructor logic here] end
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Attributes
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Modify attribute annotation for Name here] off begin
	@CsvBindByName(column = "NAME", required = true)
	@NotNull(message = "Name must not be empty")
	@Column(name = "name")
	@ApiModelProperty(notes = "The Name of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Name here] end
	private String name;

	// % protected region % [Modify attribute annotation for Summary here] off begin
	@CsvBindByName(column = "SUMMARY", required = false)
	@Nullable
	@Column(name = "summary")
	@ApiModelProperty(notes = "The Summary of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Summary here] end
	private String summary;

	// % protected region % [Modify attribute annotation for Difficulty here] off begin
	@CsvCustomBindByName(column = "DIFFICULTY", required = false, converter = DifficultyEnumConverter.class)
	@Nullable
	@Column(name = "difficulty")
	@ApiModelProperty(notes = "The Difficulty of this entity.")
	@ToString.Include
	@Enumerated
	// % protected region % [Modify attribute annotation for Difficulty here] end
	private DifficultyEnum difficulty;

	@CsvIgnore
	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.LAZY)
	private Set<FileEntity> coverImage = new HashSet<>();

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing references
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Update the annotation for Course Lessons here] off begin
	@ApiModelProperty(notes = "The Course Lesson entities that are related to this entity.")
	@OneToMany(mappedBy = "course", cascade = {CascadeType.MERGE}, fetch = FetchType.LAZY)
	@CsvIgnore
	// % protected region % [Update the annotation for Course Lessons here] end
	private Set<CourseLessonEntity> courseLessons = new HashSet<>();

	// % protected region % [Update the annotation for courseLessonsIds here] off begin
	@Transient
	@CsvCustomBindByName(column = "COURSE_LESSONS_IDS", converter = UUIDHashSetConverter.class)
	// % protected region % [Update the annotation for courseLessonsIds here] end
	private Set<UUID> courseLessonsIds = new HashSet<>();

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Update the annotation for Course Category here] off begin
	@ApiModelProperty(notes = "The Course Category entities that are related to this entity.")
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@CsvIgnore
	// % protected region % [Update the annotation for Course Category here] end
	private CourseCategoryEntity courseCategory;

	// % protected region % [Update the annotation for courseCategoryId here] off begin
	@Transient
	@CsvCustomBindByName(column = "COURSE_CATEGORY_ID", converter = UUIDConverter.class)
	// % protected region % [Update the annotation for courseCategoryId here] end
	private UUID courseCategoryId;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing reference methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
	 * Similar to {@link this#addCourseLessons(CourseLessonEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given CourseLessonEntity to be added to this entity
	 */
	public void addCourseLessons(@NotNull CourseLessonEntity entity) {
		addCourseLessons(entity, true);
	}

	/**
	 * Add a new CourseLessonEntity to courseLessons in this entity.
	 *
	 * @param entity the given CourseLessonEntity  to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entity
	 */
	public void addCourseLessons(@NonNull CourseLessonEntity entity, boolean reverseAdd) {
		if (!this.courseLessons.contains(entity)) {
			courseLessons.add(entity);
			if (reverseAdd) {
				entity.setCourse(this, false);
			}
		}
	}

	/**
	 * Similar to {@link this#addCourseLessons(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of CourseLessonEntity to be added to this entity
	 */
	public void addCourseLessons(@NotNull Collection<CourseLessonEntity> entities) {
		addCourseLessons(entities, true);
	}

	/**
	 * Add a new collection of CourseLessonEntity to Course Lessons in this entity.
	 *
	 * @param entities the given collection of CourseLessonEntity to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void addCourseLessons(@NonNull Collection<CourseLessonEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> addCourseLessons(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#removeCourseLessons(CourseLessonEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given CourseLessonEntity to be set to this entity
	 */
	public void removeCourseLessons(@NotNull CourseLessonEntity entity) {
		this.removeCourseLessons(entity, true);
	}

	/**
	 * Remove the given CourseLessonEntity from this entity.
	 *
	 * @param entity the given CourseLessonEntity to be removed from this entity
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void removeCourseLessons(@NotNull CourseLessonEntity entity, boolean reverse) {
		if (reverse) {
			entity.unsetCourse(false);
		}
		this.courseLessons.remove(entity);
	}

	/**
	 * Similar to {@link this#removeCourseLessons(Collection, boolean)} but
	 * default to true for reverse remove.
	 *
	 * @param entities the given collection of CourseLessonEntity to be removed to this entity
	 */
	public void removeCourseLessons(@NotNull Collection<CourseLessonEntity> entities) {
		this.removeCourseLessons(entities, true);
	}

	/**
	 * Remove the given collection of CourseLessonEntity from  to this entity.
	 *
	 * @param entities the given collection of CourseLessonEntity to be removed to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void removeCourseLessons(@NonNull Collection<CourseLessonEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> this.removeCourseLessons(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#setCourseLessons(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of CourseLessonEntity to be set to this entity
	 */
	public void setCourseLessons(@NotNull Collection<CourseLessonEntity> entities) {
		setCourseLessons(entities, true);
	}

	/**
	 * Replace the current entities in Course Lessons with the given ones.
	 *
	 * @param entities the given collection of CourseLessonEntity to replace the old ones
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void setCourseLessons(@NotNull Collection<CourseLessonEntity> entities, boolean reverseAdd) {

		this.unsetCourseLessons();
		this.courseLessons = new HashSet<>(entities);
		if (reverseAdd) {
			this.courseLessons.forEach(courseLessonsEntity -> courseLessonsEntity.setCourse(this, false));
		}
	}

	/**
	 * Similar to {@link this#unsetCourseLessons(boolean)} but
	 * default to true for reverse unset
	 */
	public void unsetCourseLessons() {
		this.unsetCourseLessons(true);
	}

	/**
	 * Remove all the entities in Course Lessons from this entity.
	 * @param doReverse whether this entity should be removed from the given entities
	 */
	public void unsetCourseLessons(boolean doReverse) {
		if (doReverse) {
			this.courseLessons.forEach(courseLessonsEntity -> courseLessonsEntity.unsetCourse(false));
		}
		this.courseLessons.clear();
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Similar to {@link this#setCourseCategory(CourseCategoryEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given CourseCategoryEntity to be set to this entity
	 */
	public void setCourseCategory(@NotNull CourseCategoryEntity entity) {
		setCourseCategory(entity, true);
	}

	/**
	 * Set or update the courseCategory in this entity with single CourseCategoryEntity.
	 *
	 * @param entity the given CourseCategoryEntity to be set or updated to courseCategory
	 * @param reverseAdd whether this entity should be set or updated to the given entity
	 */
	public void setCourseCategory(@NotNull CourseCategoryEntity entity, boolean reverseAdd) {
		// % protected region % [Add any additional logic here before the main logic for setCourseCategory here] off begin
		// % protected region % [Add any additional logic here before the main logic for setCourseCategory here] end

		if (sameAsFormer(this.courseCategory, entity)) {
			return;
		}

		if (this.courseCategory != null) {
			this.courseCategory.removeCourses(this, false);
		}
		this.courseCategory = entity;
		if (reverseAdd) {
			this.courseCategory.addCourses(this, false);
		}

		// % protected region % [Add any additional logic here after the main logic for setCourseCategory here] off begin
		// % protected region % [Add any additional logic here after the main logic for setCourseCategory here] end
	}

	/**
	 * Similar to {@link this#unsetCourseCategory(boolean)} but default to true.
	 */
	public void unsetCourseCategory() {
		this.unsetCourseCategory(true);
	}

	/**
	 * Remove Course Category in this entity.
	 *
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void unsetCourseCategory(boolean reverse) {
		if (reverse && this.courseCategory != null) {
			this.courseCategory.removeCourses(this, false);
		}
		this.courseCategory = null;
	}

	public void addCoverImage(FileEntity newFile) {
		coverImage.add(newFile);
	}

	public void addAllCoverImage(Collection<FileEntity> newFiles) {
		coverImage.addAll(newFiles);
	}

	public void removeCoverImage(FileEntity newFile) {
		coverImage.remove(newFile);
	}

	public boolean containsCoverImage(FileEntity newFile) {
		return coverImage.contains(newFile);
	}

	public void clearAllCoverImage() {
		coverImage.clear();
	}

	/**
	 * The CSV annotations used to generate and import CSV files require headers on the abstract entity attributes
	 * (id, created, modified, created by, modified by).  These should not be present in imports, as this information is
	 * added by the server when saving entities.  Therefore a string containing the example csv headers is required
	 *
	 * @return the headers recommended in the CSV import file in CSV format
	 */
	public static String getExampleCsvHeader() {

		// % protected region % [Modify the headers in the CSV file here] off begin
		return "NAME,SUMMARY,DIFFICULTY,COURSE_CATEGORY_ID,COURSE_LESSONS_IDS,ID";
		// % protected region % [Modify the headers in the CSV file here] end
	}

	public void addRelationEntitiesToIdSet() {
		// % protected region % [Add any additional logic for entity relations here] off begin
		Optional<CourseCategoryEntity> courseCategoryRelation = Optional.ofNullable(this.courseCategory);
		courseCategoryRelation.ifPresent(entity -> this.courseCategoryId = entity.getId());

		for (CourseLessonEntity courseLessons: this.courseLessons) {
			this.courseLessonsIds.add(courseLessons.getId());
		}

		// % protected region % [Add any additional logic for entity relations here] end
	}
	// % protected region % [Add any additional class methods  here] off begin
	// % protected region % [Add any additional class methods  here] end
}
