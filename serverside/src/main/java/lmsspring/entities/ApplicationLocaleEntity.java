/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

package lmsspring.entities;

import lmsspring.dtos.ApplicationLocaleEntityDto;
import lmsspring.entities.listeners.ApplicationLocaleEntityListener;
import lmsspring.serializers.ApplicationLocaleSerializer;
import lmsspring.lib.validators.ValidatorPatterns;
import lmsspring.services.utils.converters.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.opencsv.bean.CsvIgnore;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.URL;
import org.springframework.lang.Nullable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Optional;
import java.util.UUID;

// % protected region % [Import any additional imports here] off begin
// % protected region % [Import any additional imports here] end

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Entity
// % protected region % [Override the auditing annotation or add additional annotations here] off begin
@Audited
// % protected region % [Override the auditing annotation or add additional annotations here] end
@ExcludeSuperclassListeners
@EntityListeners({ApplicationLocaleEntityListener.class, AuditingEntityListener.class})
@JsonSerialize(using = ApplicationLocaleSerializer.class)
@Table(
	uniqueConstraints = {
		@UniqueConstraint(columnNames = {"locale"}, name = "locale"),
	}
)
public class ApplicationLocaleEntity extends AbstractEntity {

	/**
	 * Takes a ApplicationLocaleEntityDto and converts it into a ApplicationLocaleEntity.
	 * Primarily used when endpoints are invoked, as they take DTOs as input, which need to be converted to the entity type
	 *
	 * @param applicationLocaleEntityDto
	 */
	public ApplicationLocaleEntity(ApplicationLocaleEntityDto applicationLocaleEntityDto) {
		// Need to account for potential that any field is empty.  This will be run before any server validation and
		// endpoints will most likely not be subject to clientside validation

		if (applicationLocaleEntityDto.getId() != null) {
			this.setId(applicationLocaleEntityDto.getId());
		}

		if (applicationLocaleEntityDto.getLocale() != null) {
			this.setLocale(applicationLocaleEntityDto.getLocale());
		}

		if (applicationLocaleEntityDto.getKey() != null) {
			this.setKey(applicationLocaleEntityDto.getKey());
		}

		if (applicationLocaleEntityDto.getValue() != null) {
			this.setValue(applicationLocaleEntityDto.getValue());
		}

		// % protected region % [Add any additional DTO constructor logic here] off begin
		// % protected region % [Add any additional DTO constructor logic here] end
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Attributes
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Modify attribute annotation for Locale here] off begin
	@CsvBindByName(column = "LOCALE", required = true)
	@NotNull(message = "Locale must not be empty")
	@Column(name = "locale")
	@ApiModelProperty(notes = "The Locale of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Locale here] end
	private String locale;

	// % protected region % [Modify attribute annotation for Key here] off begin
	@CsvBindByName(column = "KEY", required = true)
	@NotNull(message = "Key must not be empty")
	@Column(name = "key")
	@ApiModelProperty(notes = "The Key of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Key here] end
	private String key;

	// % protected region % [Modify attribute annotation for Value here] off begin
	@CsvBindByName(column = "VALUE", required = true)
	@NotNull(message = "Value must not be empty")
	@Column(name = "value")
	@ApiModelProperty(notes = "The Value of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Value here] end
	private String value;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing references
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing reference methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



	/**
	 * The CSV annotations used to generate and import CSV files require headers on the abstract entity attributes
	 * (id, created, modified, created by, modified by).  These should not be present in imports, as this information is
	 * added by the server when saving entities.  Therefore a string containing the example csv headers is required
	 *
	 * @return the headers recommended in the CSV import file in CSV format
	 */
	public static String getExampleCsvHeader() {

		// % protected region % [Modify the headers in the CSV file here] off begin
		return "LOCALE,KEY,VALUE,ID";
		// % protected region % [Modify the headers in the CSV file here] end
	}

	public void addRelationEntitiesToIdSet() {
		// % protected region % [Add any additional logic for entity relations here] off begin
		// % protected region % [Add any additional logic for entity relations here] end
	}
	// % protected region % [Add any additional class methods  here] off begin
	// % protected region % [Add any additional class methods  here] end
}
