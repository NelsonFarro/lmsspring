/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.configs.security.helpers;

import lmsspring.configs.security.authorities.CustomGrantedAuthority;
import lombok.NonNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * Helper class exists to run piece of code with elevated permission. This class is required for some of the
 * functionality in the application, such as login or insert test data. Otherwise the application will reject any
 * attempts to interact with the database since authentication is needed (and thus the "you can't login because you're
 * not logged in" problem).
 * <p>
 * <strong>Important note</strong>: This class is not meant to be used unless absolutely necessary. Developers should
 * adopt best security practices instead of using this as a work around security.
 */
public final class AnonymousHelper {
	/**
	 * Anonymous user with elevated permissions in order to satisfy the security checking.
	 */
	private static final Authentication anonymousUser = setupAnonymousUser();

	/**
	 * Create and return an anonymous user with elevated permissions. Note that this will only get runs once.
	 *
	 * @return an anonymous user with elevated permissions.
	 */
	private static Authentication setupAnonymousUser() {
		List<CustomGrantedAuthority> authorities = new ArrayList<>();

		CustomGrantedAuthority administratorAuthority = new CustomGrantedAuthority(
				"ROLE_ADMINISTRATOR",
				"AdministratorEntity",
				true,
				true,
				true,
				true
		);
		authorities.add(administratorAuthority);

		CustomGrantedAuthority applicationLocaleAuthority = new CustomGrantedAuthority(
				"ROLE_APPLICATION_LOCALE",
				"ApplicationLocaleEntity",
				true,
				true,
				true,
				true
		);
		authorities.add(applicationLocaleAuthority);

		CustomGrantedAuthority articleAuthority = new CustomGrantedAuthority(
				"ROLE_ARTICLE",
				"ArticleEntity",
				true,
				true,
				true,
				true
		);
		authorities.add(articleAuthority);

		CustomGrantedAuthority bookAuthority = new CustomGrantedAuthority(
				"ROLE_BOOK",
				"BookEntity",
				true,
				true,
				true,
				true
		);
		authorities.add(bookAuthority);

		CustomGrantedAuthority coreUserAuthority = new CustomGrantedAuthority(
				"ROLE_CORE_USER",
				"CoreUserEntity",
				true,
				true,
				true,
				true
		);
		authorities.add(coreUserAuthority);

		CustomGrantedAuthority courseAuthority = new CustomGrantedAuthority(
				"ROLE_COURSE",
				"CourseEntity",
				true,
				true,
				true,
				true
		);
		authorities.add(courseAuthority);

		CustomGrantedAuthority courseCategoryAuthority = new CustomGrantedAuthority(
				"ROLE_COURSE_CATEGORY",
				"CourseCategoryEntity",
				true,
				true,
				true,
				true
		);
		authorities.add(courseCategoryAuthority);

		CustomGrantedAuthority courseLessonAuthority = new CustomGrantedAuthority(
				"ROLE_COURSE_LESSON",
				"CourseLessonEntity",
				true,
				true,
				true,
				true
		);
		authorities.add(courseLessonAuthority);

		CustomGrantedAuthority lessonAuthority = new CustomGrantedAuthority(
				"ROLE_LESSON",
				"LessonEntity",
				true,
				true,
				true,
				true
		);
		authorities.add(lessonAuthority);

		CustomGrantedAuthority tagAuthority = new CustomGrantedAuthority(
				"ROLE_TAG",
				"TagEntity",
				true,
				true,
				true,
				true
		);
		authorities.add(tagAuthority);

		CustomGrantedAuthority roleAuthority = new CustomGrantedAuthority(
				"ROLE_ROLE",
				"RoleEntity",
				true,
				true,
				true,
				true
		);
		authorities.add(roleAuthority);

		CustomGrantedAuthority privilegeAuthority = new CustomGrantedAuthority(
				"ROLE_PRIVILEGE",
				"PrivilegeEntity",
				true,
				true,
				true,
				true
		);
		authorities.add(privilegeAuthority);

		CustomGrantedAuthority userAuthority = new CustomGrantedAuthority(
				"ROLE_USER",
				"UserEntity",
				true,
				true,
				true,
				true
		);
		authorities.add(userAuthority);

		CustomGrantedAuthority workflowAuthority = new CustomGrantedAuthority(
				"ROLE_WORKFLOW",
				"WorkflowEntity",
				true,
				true,
				true,
				true
		);
		authorities.add(workflowAuthority);

		CustomGrantedAuthority workflowStateAuthority = new CustomGrantedAuthority(
				"ROLE_WORKFLOW_STATE",
				"WorkflowStateEntity",
				true,
				true,
				true,
				true
		);
		authorities.add(workflowStateAuthority);

		CustomGrantedAuthority workflowTransitionAuthority = new CustomGrantedAuthority(
				"ROLE_WORKFLOW_TRANSITION",
				"WorkflowTransitionEntity",
				true,
				true,
				true,
				true
		);
		authorities.add(workflowTransitionAuthority);

		CustomGrantedAuthority workflowVersionAuthority = new CustomGrantedAuthority(
				"ROLE_WORKFLOW_VERSION",
				"WorkflowVersionEntity",
				true,
				true,
				true,
				true
		);
		authorities.add(workflowVersionAuthority);

		CustomGrantedAuthority lessonFormSubmissionAuthority = new CustomGrantedAuthority(
				"ROLE_LESSON_FORM_SUBMISSION",
				"LessonFormSubmissionEntity",
				true,
				true,
				true,
				true
		);
		authorities.add(lessonFormSubmissionAuthority);

		CustomGrantedAuthority lessonFormVersionAuthority = new CustomGrantedAuthority(
				"ROLE_LESSON_FORM_VERSION",
				"LessonFormVersionEntity",
				true,
				true,
				true,
				true
		);
		authorities.add(lessonFormVersionAuthority);

		CustomGrantedAuthority lessonFormTileAuthority = new CustomGrantedAuthority(
				"ROLE_LESSON_FORM_TILE",
				"LessonFormTileEntity",
				true,
				true,
				true,
				true
		);
		authorities.add(lessonFormTileAuthority);

		// % protected region % [Add any additional authorities here] off begin
		// % protected region % [Add any additional authorities here] end

		return new UsernamePasswordAuthenticationToken(null, null, authorities);
	}

	/**
	 * Given a runnable, run it with an anonymous user with elevated permissions. It is guaranteed that once the task
	 * is done or failed, the permissions will be cleared. If this method is called when there's already a user logged
	 * in, then their permissions will be restored when the method finishes.
	 *
	 * @param taskToPerform a task to be run with elevated permissions
	 */
	public static void runAnonymously(@NonNull Runnable taskToPerform) {
		Authentication oldAuthentication = SecurityContextHolder.getContext().getAuthentication();
		try {
			SecurityContextHolder.getContext().setAuthentication(anonymousUser);
			taskToPerform.run();
		} finally {
			SecurityContextHolder.clearContext();
			SecurityContextHolder.getContext().setAuthentication(oldAuthentication);
		}
	}

	/**
	 * Given a supplier, run it with an anonymous user with elevated permissions and then return the value. It is
	 * guaranteed that once the task is done or failed, the permissions will be cleared. If this method is called when
	 * there's already a user logged in, then their permissions will be restored when the method finishes.
	 *
	 * @param taskToPerform a task to be run with elevated permissions
	 * @return the result supplied by the given task
	 */
	public static <T> T runAnonymouslyAndReturnValue(@NonNull Supplier<T> taskToPerform) {
		Authentication oldAuthentication = SecurityContextHolder.getContext().getAuthentication();
		try {
			SecurityContextHolder.getContext().setAuthentication(anonymousUser);
			return taskToPerform.get();
		} finally {
			SecurityContextHolder.clearContext();
			SecurityContextHolder.getContext().setAuthentication(oldAuthentication);
		}
	}
}