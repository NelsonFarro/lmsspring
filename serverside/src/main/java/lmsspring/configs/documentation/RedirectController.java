/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.configs.documentation;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

@Controller
public class RedirectController {

	// % protected region % [Add additional class attributes here] off begin
	// % protected region % [Add additional class attributes here] end

	/**
	 * Added to provide backwards compatibility with {@see <a href="https://mvnrepository.com/artifact/io.springfox/springfox-swagger2/2.9.2">SpringFox 2.9.2</a> }
	 */
	@RequestMapping(value = "/swagger-ui.html", method = RequestMethod.GET)
	public String swaggerUi() {
		return "redirect:/swagger-ui/";
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
